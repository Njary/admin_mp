$.fn.modal.Constructor.prototype._enforceFocus = function(){}


$(window).on('load',function(){
	
	$('input').iCheck({
	  checkboxClass: 'icheckbox_square-blue',
	  radioClass   : 'iradio_square-blue',
	  increaseArea : '20%' // optional
	});

	if(parseInt($("#paramProfil").val())==3){
		setTimeout(()=>{$('.select2').select2();}, 500);
		$("#modal_edit").modal({
			backdrop:'static'
		});
	}

	$('#select-compte-container').hide();

	$(".loader").show();

	//$('.select2').select2();

	
	/*mapboxgl.accessToken = 'pk.eyJ1IjoibmphcnkiLCJhIjoiY2p1ZHU0NWYzMDJ1MTRkbW16N2pmczh6YiJ9.i3r9euc0dZsYchVvQfdG5A';
	var map = new mapboxgl.Map({
		container: 'map',
		center: [47.5217, -18.9156],
  		zoom: 14,
  		style: 'mapbox://styles/mapbox/streets-v9'
	});*/

});

var site_url = $("#site_url").val();

var app = angular.module('profilApp', []);



app.controller('profilCtrl', function($scope,$http,$compile,$timeout) {
	$scope.formIsValid = true;
	$scope.twoResponsable = false;
	$scope.loadType = false;
	$scope.notValidTypeEntreprise = false;
	$scope.numAdresse = 1;
	$scope.pseudoValid = false;
	$scope.checkingPseudo = false;
	$scope.pseudo = $("#pseudo_hidden").val();
	$scope.postes = [];
	$scope.posteInEditing = "";
	$scope.mailResp1Invalide = false;
	$scope.mailResp2Invalide = false;
	$scope.respMagInvalid = false;
	$scope.respMagEmail = '';
	$scope.numCompte = 1;
	$scope.allCompteValid = true;
	$scope.selectCompte = [];
	$scope.selectPays = [];
	$scope.selectBenPays = [];
	$scope.listPays = [];
	$scope.listCompte = [];
	$scope.telephoneIsValid = false;
	$scope.typeCompteInPays = 0;
	$scope.info_pour_logo = false;
	$scope.uploadCrop = null;
	$scope.mainLong = 47.5217;
	$scope.mainLat = -18.9156; 


	$scope.onFileImageChange = function(element){
        var files = $('#input-file-image').prop('files');
        var divFilePreview = $('#div-file-preview');
        divFilePreview.html('');
        $scope.images = [];
        $scope.showImage(divFilePreview, files, 0);
    };

    $scope.showImage = function(divFilePreview, files, index, array){
        if(index < files.length){
            var reader = new FileReader();
            reader.onloadend = function () {
                if(array != undefined){
                    array.push({
                        extension: $scope.getImgExt(files[index].name),
                        dataURL: reader.result
                    });
                }
                else{
                    $scope.images.push({
                        extension: $scope.getImgExt(files[index].name),
                        dataURL: reader.result
                    });
                }
                divFilePreview.append('<div class="col-6 div-img-preview" ><img src="'+reader.result+'" /></div>');
                $scope.showImage(divFilePreview, files, index+1, array);
            }
            if (files[index]) {
                reader.readAsDataURL(files[index]);
            }
        }
    };

    $scope.getImgExt = function(imgName){
        var strs = imgName.split('.');
        if(strs.length > 1){
            return strs[strs.length-1];
        }
        return 'png';
    };

    //////////////////CROP Croppie.JS////////////////////////////////

    $scope.onLogoEntrepriseChange = function(input){
    	$scope.uploadCrop = null;

    	$('#upload-demo').html('');
    	$("#logo_to_edit").hide();

    	if (input.files && input.files[0]) {
            var reader = new FileReader();          
            reader.onload = function (e) {

            	$scope.uploadCrop = $('#upload-demo').croppie({
			        viewport: {
			            width: 256,
			            height: 256,
			            type: 'square'
			        },
			        boundary: {
			            width: 300,
			            height: 300
			        }
			    });

                $scope.uploadCrop.croppie('bind', {
                    url: e.target.result
                });
                $('#upload-demo').addClass('ready');
            }           
            reader.readAsDataURL(input.files[0]);

        }
    };

    //////////////////////////////////////////////////////////////////
    //////////////////CROP IMAGE jcrop////////////////////////////////


	// convert bytes into friendly format

	$scope.bytesToSize = function(bytes) {

	    var sizes = ['Bytes', 'KB', 'MB'];

	    if (bytes == 0) return 'n/a';

	    var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));

	    return (bytes / Math.pow(1024, i)).toFixed(1) + ' ' + sizes[i];

	};

	// check for selected crop region

	$scope.checkCropRegion = function() {

	    if (parseInt($('#w').val())) return true;

	    //$('.error').html('Please select a crop region and then press Upload').show();

	    return false;

	};

	// update info by cropping (onChange and onSelect events handler)

	$scope.updateInfo = function(e) {

	    $('#x1').val(e.x);

	    $('#y1').val(e.y);

	    $('#x2').val(e.x2);

	    $('#y2').val(e.y2);

	    $('#w').val(e.w);

	    $('#h').val(e.h);

	};

	// clear info by cropping (onRelease event handler)

	$scope.clearInfo = function() {

	    $('.info #w').val('');

	    $('.info #h').val('');

	};

	// Create variables (in this scope) to hold the Jcrop API and image size

	var jcrop_api, boundx, boundy;

	$scope.fileSelectHandler = function(element) {

	    // get selected file

	    var oFile = $('#input-file-image')[0].files[0];

	    //show info

	    $('.info_pour_logo').show();

	    // hide all errors

	    $('.error').hide();

	    // check for image type (jpg and png are allowed)

	    var rFilter = /^(image\/jpeg|image\/png)$/i;

	    if (! rFilter.test(oFile.type)) {

	        $('.error').html('Veuillez choisir une image valide (les fichiers jpg and png sont acceptés)').show();

	        return;

	    }

	    // check for file size

	    /*if (oFile.size > 250 * 1024) {

	        $('.error').html('Le fichier que vous avez sélectionner est trop volumineux, veuillez choisir un plus léger').show();

	        return;

	    }*/

	    // preview element

	    var oImage = document.getElementById('preview');

	    // prepare HTML5 FileReader

	    var oReader = new FileReader();

	        oReader.onload = function(e) {

	        // e.target.result contains the DataURL which we can use as a source of the image

	        oImage.src = e.target.result;

	        oImage.onload = function () { // onload event handler

	            // display step 2

	            $('.step2').fadeIn(500);

	            // display some basic image info

	            var sResultFileSize = $scope.bytesToSize(oFile.size);

	            $('#filesize').val(sResultFileSize);

	            $('#filetype').val(oFile.type);

	            $('#filedim').val(oImage.naturalWidth + ' x ' + oImage.naturalHeight);

	            // destroy Jcrop if it is existed

	            if (typeof jcrop_api != 'undefined') {

	                jcrop_api.destroy();

	                jcrop_api = null;

	            }

	            $('#preview').width(oImage.naturalWidth);

	            $('#preview').height(oImage.naturalHeight);

	            setTimeout(function(){

	                // initialize Jcrop

	                $('#preview').Jcrop({

	                    minSize: [32, 32], // min crop size

	                    aspectRatio : 1, // keep aspect ratio 1:1

	                    bgFade: true, // use fade effect

	                    bgOpacity: .3, // fade opacity

	                    onChange: $scope.updateInfo,

	                    onSelect: $scope.updateInfo,

	                    onRelease: $scope.clearInfo

	                }, function(){

	                    // use the Jcrop API to get the real image size

	                    var bounds = this.getBounds();

	                    boundx = bounds[0];

	                    boundy = bounds[1];

	                    // Store the Jcrop API in the jcrop_api variable

	                    jcrop_api = this;

	                });

	            },1000);

	        };

	    };

	    // read selected file as DataURL

	    oReader.readAsDataURL(oFile);

	}




	////////////////////////////////////////////////////////////


	$scope.edit = function(){
		setTimeout(()=>{$scope.initSelect2();}, 500);
		$("#modal_edit").modal('show');
	}

	$scope.update_logo = function(){
		$("#modal_edit_logo").modal('show');
		$("#contratModal").modal('hide');
	}
	$scope.getValueSearchField = function(idDivParent){
        for(var i = 0 ; i < $('.select2-search__field').length ; i++){
            if(idDivParent == $( $('.select2-search__field')[i] ).parent().parent().parent().parent().parent().parent().attr('id')){
                return $('.select2-search__field')[i].value;
            }

            
        }
        return "";    
    }
	$scope.addTypeEntreprise = function(){
		$scope.new_type = $scope.getValueSearchField("div_select_types_entreprise");
		$scope.createTypeEntreprise();
		//$("#modal_type_entreprise").modal('show');
	}

	$scope.addPoste = function(posteSelelector){
		$scope.posteInEditing = posteSelelector;
		var nouveau_poste =  $(posteSelelector).data("select2").dropdown.$search.val();
		$scope.new_type = nouveau_poste;
		$scope.createPoste();
		//$('#new-poste-name').val(nouveau_poste);
		//console.log($('#input-nom-post-new').val());
		//$('#bouton_ajouter_poste').click();
		//$("#modal_new_poste").modal();
	}

	$scope.createPoste = function(){
		$scope.loadType = true;
		var url = site_url+"a-n-p"; 
		var str_type = $scope.new_type;

		if(str_type && str_type!=="" && str_type!=undefined){
			str_type = str_type[0].toUpperCase()+str_type.substr(1,str_type.length-1);

			$scope.notValidTypeEntreprise = false;

			var data = $.param({
				'nomPoste': str_type,
				'typePoste':1
			});

			$http({
				url:url,
				method:"POST",
				data:data,
				headers:{'Content-type':'application/x-www-form-urlencoded'}
			})
				.then(function(response){
					$scope.loadType = false;
					var data = response.data;
					
					if(data.idPoste>0){
						var select = $($scope.posteInEditing);
						if(!select.html().includes('<option value="'+data.idPoste+'">')){
							select.append("<option value='"+data.idPoste+"'>"+str_type+"</option>");
						}
						$($scope.posteInEditing+" option[value="+data.idPoste+"]").attr("selected","");
						$($scope.posteInEditing).trigger('change.select2');
						$($scope.posteInEditing).select2('close');
						/*$($scope.posteInEditing).blur();
						$("#modal_edit").focus();*/
					}else{
						toastr.error("Erreur","Impossible d'enregistrer l'information dans la base de donnée. Veuillez réessayer.");
					}
					/*$("#modal_new_poste").on('shown.bs.modal' , function(){
						$("#modal_new_poste").modal('hide');

					});*/
					 
					
				},function(error){
					console.log(error);
					toastr.error("Erreur","Un problème est survenue. Veuillez réessayer.");
				});
		}else{
			$scope.loadType = false;
			$scope.notValidTypeEntreprise = true;
		}	
	}

	$scope.createTypeEntreprise = function(){
		$scope.loadType = true;
		var url = site_url+"entreprise/entreprise/create_type"; 
		var str_type = $scope.new_type;

		if(str_type && str_type!=="" && str_type!=undefined){
			str_type = str_type[0].toUpperCase()+str_type.substr(1,str_type.length-1);

			$scope.notValidTypeEntreprise = false;

			var data = $.param({
				'type_label': str_type
			});
			//$("#modal_edit").modal('hide');
			$http({
				url:url,
				method:"POST",
				data:data,
				headers:{'Content-type':'application/x-www-form-urlencoded'}
			})
				.then(function(response){
					$scope.loadType = false;
					var data = response.data;
					
					if(data.type_id>0){
						if(!data.exist){
							$("#select_types_entreprise").append("<option value='"+data.type_id+"'>"+str_type+"</option>");
						}
						$("option[value="+data.type_id+"]").attr("selected","");
						//$scope.initSelect2()
						$("#select_types_entreprise").trigger('change.select2');
						$("#select_types_entreprise").select2('close');
						$scope.edit();
					}else{
						toastr.error("Erreur","Impossible d'enregistrer l'information dans la base de donnée. Veuillez réessayer.");
					}
					//$("#modal_type_entreprise").modal('hide');
				},function(error){
					console.log(error);
					toastr.error("Erreur","Un problème est survenue. Veuillez réessayer.");
				});
		}else{
			$scope.loadType = false;
			$scope.notValidTypeEntreprise = true;
		}	
	}

	$scope.addAdresse = function(){
		var html = '<div class="form-group input-group adr'+$scope.numAdresse+'" id="id-addresse-'+$scope.numAdresse+'">'+
		'<input name="adresse_entreprise'+$scope.numAdresse+'" type="text" class="form-control form-to-test" placeholder="Autre adresse de votre entreprise" title="Adresse de l\'entreprise">'+
		'<div class="input-group-append" style="cursor: pointer;" title="Supprimer" ng-click="removeHtml(\'.adr'+$scope.numAdresse+'\')">'+
		'<span class="input-group-text"><i class="fa fa-minus"></i></span>'+
		'</div>'+
		'</div>'+
		'<div class="form-group adr'+$scope.numAdresse+'"">'+
			'<input name="code_postal_entreprise'+$scope.numAdresse+'" type="text" class="form-control form-to-test" placeholder="Code postal*" title="Code postal" require/>'+
		'</div>'+
		'<div class="form-group adr'+$scope.numAdresse+'"">'+
			'<input name="ville_entreprise'+$scope.numAdresse+'" type="text" class="form-control form-to-test" placeholder="Ville*" title="Ville" require/>'+
		'</div>'+
		'<div class="form-group adr'+$scope.numAdresse+'"">'+
			'<select name="pays_entreprise'+$scope.numAdresse+'" class="form-control select2 form-to-test" data-placeholder="Pays*" style="width: 100%;" require>'+
				$('#pays_entreprise').html()+
			'</select>'+
		'</div>'+
		'<h6 class="adr'+$scope.numAdresse+'">Coordonnée géographique (Latitude, Longitude)</h6>'+
		'<div class="form-group row adr'+$scope.numAdresse+' ">'+
            '<div class="form-group col-md-5 adr'+$scope.numAdresse+'"">'+
				'<input id="infoLatitude999'+$scope.numAdresse+'" name="latitude_entreprise'+$scope.numAdresse+'" type="text" class="form-control form-to-test lat-to-test" placeholder="Latitude*" require="" title="Latitude"/>'+
			'</div>'+
			'<div class="form-group col-md-5 adr'+$scope.numAdresse+'"">'+
				'<input id="infoLongitude999'+$scope.numAdresse+'" name="longitude_entreprise'+$scope.numAdresse+'" type="text" class="form-control form-to-test long-to-test" placeholder="Longitude*" require="" title="Longitude"/>'+ 
			'</div>'+
			'<div class="col-md-2 adr'+$scope.numAdresse+'"">'+
				'<a href="#"  class="btn btn-primary btn-block" ng-click="initShowMap(999'+$scope.numAdresse+')" data-toggle="modal" data-target="#modal-show-map" title="Sélectionner les coordonnées sur une carte" ><i class="fa fa-map"></i> </a>'+
			'</div>'+
		'</div>';

		$("#otherAdress").append($compile(html)($scope));
		$scope.initSelect2();
		var elmnt = document.getElementById('id-addresse-'+$scope.numAdresse);
		elmnt.scrollIntoView();
		if($scope.numAdresse==1)
			toastr.info('Ajouter une adresse se réfère à une deuxième adresse de l’entreprise et son siège. Pour ajouter une filiale il faut utiliser la section « Gérer les filiales »','notification')
		$scope.numAdresse+=1;

	}

	$scope.addCompte = function(){
		var loader = $('#loader_checking').val();
		var option = "";
		for(var i=0; i<$scope.listCompte.length; i++){
			if( $scope.listCompte[i].pays_id == $scope.typeCompteInPays ){
				option += '<option></option> <option value="'+$scope.listCompte[i].id_compte+'" >'+$scope.listCompte[i].label+'</option>';
			}
		}
		var html = '<div class="form-group row compteADD compte'+$scope.numCompte+'"">'+
	        					'<div class="col-10">'+
		        					'<select id="select-compte-paiement1'+$scope.numCompte+'" class="form-control select2 select_compte_entreprise addCompte select_additional" name="information_compte1'+$scope.numCompte+'" data-placeholder="Autre compte de paiement" ng-model="selectCompte1'+$scope.numCompte+'" ng-change="changeTypeCompte(1'+$scope.numCompte+')" style="width: 100%;">'+
										 //$('#select_compte').html()+
										 option +
						            '</select>'+
					        	'</div>'+
					        	'<div class="col-2">'+
		        					'<a class="btn btn-default btn-block" style="cursor: pointer;" ng-click="removeHtml(\'.compte'+$scope.numCompte+'\')" title="Supprimer"><i class="fa fa-minus"></i></a>'+
	        					'</div>'+
	        				'</div>'+
							'<div class="form-group has-feedback compteADD compte'+$scope.numCompte+'">'+
								'<div class="row">'+
									'<div class="col-md-3">'+
										'<span class="input-group-text labelCompte" id="labelCompte1'+$scope.numCompte+'"> Compte n° </span>'+
									'</div>'+
									'<div class="col-md-9">'+
										'<input id="numero-compte-paiement1'+$scope.numCompte+'" name="numero_compte_entreprise1'+$scope.numCompte+'" type="text" class="form-control numero_compte_entreprise" placeholder="Numéro de compte" />'+
										'<span class="loader-default col-1" id="loader-numCompte-resp1'+$scope.numCompte+'" style="display: none; position: absolute;right: 0px;top: 5px;"><img src="'+loader+'"></span>'+
									'</div>'+
								'</div'+
							'</div>';
		//console.log(option);
		
		$("#otherCompte").append($compile(html)($scope));
		$scope.initSelect2()
		$scope.numCompte+=1;
	}

	$scope.removeHtml = function(selector){
		$(selector).remove();
	}

	$scope.addResponsable = function(){
		$scope.twoResponsable = true;
		$scope.mailResp2Invalide = true;
		setTimeout(()=>{
			$("input[name='responsable2_nom']").prop('required',true);
			$("input[name='responsable2_prenom']").prop('required',true);
			$("select[name='responsable2_poste']").prop('required',true);
			$("input[name='responsable2_email']").prop('required',true);
			$("input[name='responsable2_phone']").prop('required',true);
			$("#select_poste2B").addClass('form-control select2');
			$("#select-sexe2B").addClass('form-control select2');
			$scope.initSelect2();
			$scope.changePays(0);
		}, 200);

	}

	$scope.addTypeAliment = function(){
		$("#modal_type_aliment").modal({
			backdrop:'static'
		});
	}

	$scope.createTypeAliment = function(){
		$scope.loadType = true;
		var url = site_url+"entreprise/entreprise/create_type_aliment"; 
		var str_type = $scope.new_type_aliment;

		if(str_type && str_type!=="" && str_type!=undefined){
			str_type = str_type[0].toUpperCase()+str_type.substr(1,str_type.length-1);

			$scope.notValidTypeEntreprise = false;

			var data = $.param({
				'type_label': str_type
			});

			$http({
				url:url,
				method:"POST",
				data:data,
				headers:{'Content-type':'application/x-www-form-urlencoded'}
			})
				.then(function(response){
					$scope.loadType = false;
					var data = response.data;
					
					if(data.type_id>0){
						if(!data.exist){
							$("#typesAliment").append('<label class="check"><input name="type_aliment_'+data.type_id+'" type="checkbox" class="flat-red" checked>'+str_type+'</label>');

							$('input').iCheck({
						      checkboxClass: 'icheckbox_square-blue',
						      radioClass   : 'iradio_square-blue',
						      increaseArea : '20%' // optional
						    });
							toastr.success("Mise à jour type d'aliments","Le nouveau type d'aliment a été ajouté avec succès");
						}
						else{
							toastr.success("","Le type d'aliment existe déjà");
						}
					}else{
						toastr.error("Erreur","Impossible d'enregistrer l'information dans la base de donnée. Veuillez réessayer.");
					}
					$("#modal_type_aliment").modal('hide');
				},function(error){
					console.log(error);
					toastr.error("Erreur","Un problème est survenue. Veuillez réessayer.");
				});
		}else{
			$scope.loadType = false;
			$scope.notValidTypeEntreprise = true;
		}	
	}

	$scope.checkPseudo = function(pseudo){
		if(pseudo==undefined || pseudo=="")
			return;

		var url = site_url+"utilisateur/acces/available_pseudo/"+pseudo;
		$scope.checkingPseudo = true;
		$http({
				url:url,
				method:"GET"
			})
			.then(function(response){
				var data = response.data;

				if(data.available==1){
					pseudoIsValid = 1;
				}else{
					pseudoIsValid = -1;
				}

				$scope.pseudoValid = (data.available==1)?false:true;
				$scope.checkingPseudo = false;

			},function(error){

			});
	}

	$scope.validForm = function(){
		if($scope.password==$scope.cpassword){
			$scope.formIsValid = true;
			$("button[name='creer_compte']").removeAttr("disabled");
		}else{
			$scope.formIsValid = false;
			$("button[name='creer_compte']").attr("disabled","");
		}
	}

	$scope.requiredRetype = function(){
		$scope.cpassword = "";
		$scope.formIsValid = true;
	}

	/* -------------- RAVO --------------- */

	$scope.showAlert = function(title, message){
		$('#modal-alert-title').html(title);
		$('#modal-alert-message').html(message);
		$('#modal-alert').modal('show');
	};

	$scope.addNewPoste = function(lien){
		var nomPoste = $('#input-nom-post-new').val().trim();
		if(nomPoste != ''){
			$('#modal-new-poste').modal('hide');
			var data = new FormData();
			data.append('typePoste', 1);
			data.append('nomPoste', nomPoste);
			$http({
				url:lien,
				method:"POST",
				data:data,
				headers:{'Content-type':undefined},
				transformRequest: angular.identity
			})
			.then(
				response=>{
					if(response.data.success){
						$('#input-nom-post-new').val('');
						var select = $('#select-post-resp');
						if(!select.html().includes('<option value="'+response.data.idPoste)){
							select.append('<option value="'+response.data.idPoste+'" >'+nomPoste+'</option>');
							$scope.postes.push({id_poste: response.data.idPoste, label: nomPoste});
						}
						select.select2('val', response.data.idPoste);
					}
				},
				error=>{
					$scope.showAlert('', 'Le nouveau poste n\'a pas été ajouté');
				}
			);
		}
	};

	$scope.numeroIsValide = function(numero){
		if(!$scope.isEmpty(numero)){
			var chars = ['+','0','1','2','3','4','5','6','7','8','9',' '];
			var ok;
			for(var i=0; i<numero.length; i++){
				ok = false;
				for(var c in chars){
					if(chars[c] == numero.charAt(i)){
						ok = true;
						break;
					}
				}
				if(!ok){
					return false;
				}
			}
		}
		else{
			return false;
		}
		return true;
	};

	$scope.mailIsValide = function(email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
	};

	$scope.isEmpty = function(string){
        if(string == "" || string == " "){
            return true;
        }
        else{
            var nbSpace = 0;
            for(var i=0; i<string.length; i++){
                if(string.charAt(i)==' '){
                    nbSpace++;
                }
            }
            if(nbSpace == string.length){
                return true;
            }
            else{
                return false;
            }
        }
	};

	$scope.initCompteValue = function(id){
		$scope.selectCompte[id] = $("#compteValue"+id).val();
		$scope.changeTypeCompte(id);
	}

	$scope.verifMail = function(respClass){
		var email = $('#responsable'+respClass+'_email').val();
		var classOther = respClass == '1' ? '2' : '1';
		$scope['mailResp'+respClass+'Invalide'] = !$scope.mailIsValide(email);
		if($scope.mailIsValide(email)){
			var loader = $('#loader-email-resp'+respClass);
			loader.show();
			var data = $.param({
				'email': email
			});
			$http({
				url: site_url+'utilisateur/acces/mailExist',
				method: "POST",
				data:data,
				headers: {'Content-type':'application/x-www-form-urlencoded'}
			})
			.then(
				response=>{
					if(response.data.exist){
						if(email != responsables['responsable'+respClass+'_email']){
							$('#msg-email-resp'+respClass).show();
							$scope['mailResp'+respClass+'Invalide'] = true;
						}
						else{
							$scope['mailResp'+respClass+'Invalide'] = false;
							$('#msg-email-resp'+respClass).hide();
						}
					}
					else{
						if(email != $('#responsable'+classOther+'_email').val()){
							$scope['mailResp'+respClass+'Invalide'] = false;
							$('#msg-email-resp'+respClass).hide();
						}
						else{
							$('#msg-email-resp'+respClass).show();
							$scope['mailResp'+respClass+'Invalide'] = true;
						}
					}
					loader.hide();
				},
				error=>{
					loader.hide();
				}
			);
		}
	};

	$scope.verifMailRespMag = function(){
		var email = $('#input-email-resp').val();
		if($scope.mailIsValide(email)){
			var loader = $('#loader-email-resp');
			loader.show();
			var data = $.param({
				'email': email
			});
			$http({
				url: site_url+'utilisateur/acces/mailExist',
				method: "POST",
				data:data,
				headers: {'Content-type':'application/x-www-form-urlencoded'}
			})
			.then(
				response=>{
					if(response.data.exist){
						if(email != $scope.respMagEmail){
							$('#msg-email-resp').show();
							$scope.respMagInvalid = true;
						}
						else{
							$('#msg-email-resp').hide();
							$scope.respMagInvalid = false;
						}
					}
					else{
						$scope.respMagInvalid = false;
						$('#msg-email-resp').hide();
					}
					loader.hide();
				},
				error=>{
					loader.hide();
				}
			);
		}
	};

	$scope.verifyCompte = function(id_compte_entreprise){
		var loader = $('#loader-numCompte-resp'+id_compte_entreprise);
		var typeCompte = $("#select-compte-paiement"+id_compte_entreprise).val();
		var numeroCompte = $("#numero-compte-paiement"+id_compte_entreprise).val();
		
		if($scope.validatePhoneNumberForCompte(typeCompte, numeroCompte)){	
			$scope.allCompteValid = true;
			loader.show();
			setTimeout(function(){
				loader.hide();
			},2000);


		}else{
			$scope.allCompteValid = false;
			toastr.error("Numéro de compte invalide","Veuillez renseigner un numéro associée au type de compte spécifié");
			$("#numero-compte-paiement"+id_compte_entreprise).get(0).scrollIntoView();
			$("#numero-compte-paiement"+id_compte_entreprise).get(0).focus();
		}
	};

	$scope.validatePhoneNumberForCompte = function(id_type_compte, number){
		if(number!=''){
			 number = number.replace(/\s/g, '').replace(/\-/g,'');

			var regexNumber = "";
	   		switch(id_type_compte){
	  			case "1":
			  		regexNumber = /^(\+26132\d{7}|032\d{7})$/;
			  		break;
			  	case "2":
			  		regexNumber = /^(\+26134\d{7}|034\d{7})$/;
			  		break;
			  	case "3":
			  		regexNumber = /^(\+26133\d{7}|033\d{7})$/;
			  		break;
			  	default:
			  		//regexNumber = /^(\+261\d{9}|033\d{7}|032\d{7}|034\d{7}|039\d{7}|020\d{7})$/;
			  		regexNumber = "";
			  		break;
	  		}
	  		return regexNumber.test(number);	
		}
			
		return false;
	 
	};

	$scope.verifyFormatCompte = function(id_compte_entreprise){
		var loader = $('#loader-numCompte-resp'+id_compte_entreprise);
		var typeCompte = $("#select-compte-paiement"+id_compte_entreprise).val();
		var numeroCompte = $("#numero-compte-paiement"+id_compte_entreprise).val();
		
		if($scope.validatePhoneNumberForCompte2(typeCompte, numeroCompte)){	
			$scope.allCompteValid = true;
			loader.show();
			setTimeout(function(){
				loader.hide();
			},2000);


		}else{
			$scope.allCompteValid = false;
			toastr.error("Numéro de compte invalide","Veuillez renseigner un numéro associée au type de compte spécifié");
			$("#numero-compte-paiement"+id_compte_entreprise).get(0).scrollIntoView();
			$("#numero-compte-paiement"+id_compte_entreprise).get(0).focus();
		}
	};

	$scope.validatePhoneNumberForCompte2 = function(id_type_compte, number){
		if(number != ''){
			 number = number.replace(/\s/g, '').replace(/\-/g,'');

			var regexNumber = "";
			if(id_type_compte == "1" || id_type_compte == "2" || id_type_compte == "3"){
				regexNumber = /^(\d{7})$/;
			}else{
				regexNumber = /^(\d{15})$/;
			}
	  		return regexNumber.test(number);	
		}
			
		return false;
	 
	};

	$scope.changeTypeCompte = function(id_compte_entreprise){
		
		var typeCompte = $("#select-compte-paiement"+id_compte_entreprise).val();
		if(typeCompte == ""){
			typeCompte = $("#compteValue"+id_compte_entreprise).val();
		}
		

		var labelnumeroCompte = $("#labelCompte"+id_compte_entreprise);
		switch(typeCompte){
			case "1":
				labelnumeroCompte.html('+26132');
				break;
			case "2":
				labelnumeroCompte.html('+26134');
				break;
			case "3":
				labelnumeroCompte.html('+26133');
				break;
			default:
				labelnumeroCompte.html('Compte n°');
				break;
		}	
	};

	$scope.initValPays = function(id){
		$scope.selectPays[id] = $("#paysValue"+id).val();
		var isInit = 1;
		$scope.changePays(id, isInit);
	}

	$scope.changePays = function(id, isInit=0){
		var id_pays = parseInt($("#pays_entreprise").val());
		$scope.typeCompteInPays = id_pays;

		//console.log("type compte pays : " + $scope.typeCompteInPays);
		
		var labelPhone = "N°";
		for(var i=0; i < $scope.listPays.length; i++){
			if($scope.listPays[i].id_pays == $scope.typeCompteInPays){
				labelPhone = $scope.listPays[i].prefixe_tel;
			}
		}

		$(".labelTelephone"+id).html(labelPhone);

		if( isInit != 1){
			
			//$("#select-compte-paiement0").select2('val','');

			//console.log("ato tsik e");

			$("#numero-compte-paiement0").val('');

			$(".compteADD").remove();
		
			var optionCompte = "";

			for(var i=0; i<$scope.listCompte.length; i++){
				if( $scope.listCompte[i].pays_id == $scope.typeCompteInPays ){
					optionCompte += '<option></option> <option value="'+$scope.listCompte[i].id_compte+'" >'+$scope.listCompte[i].label+'</option>';
				}
			}

			//$('.addCompte').html(optionCompte);
			//$('.addCompte').select2();

			//$("#select-compte-paiement0").select2();
			
			var otherCompte = $(".addCompte");

			for(var i=0; i<otherCompte.length; i++){
				var elementSelect = otherCompte.get(i);
				$(elementSelect).html(optionCompte);
				//$(elementSelect).select2();
			}
			$scope.initSelect2();
		}
		

		
	};

	$scope.verifyFormatTelephone = function(id){
		var idPays = $("#pays_entreprise").val();
		var numeroTelephone = $("#responsable1_phone").val();
		
		if($scope.validatePhoneNumberForCompte3(idPays, numeroTelephone)){	
			$scope.telephoneIsValid = true;
		}else{
			$scope.telephoneIsValid = false;
		}
	};

	$scope.validatePhoneNumberForCompte3 = function(id_pays, number){
		if(number != ''){
			 number = number.replace(/\s/g, '').replace(/\-/g,'');

			var regexNumber = "";
			switch(id_pays){
				case "1": 
					regexNumber = /^(\d{9})$/;
					break;
				case "2":
					regexNumber = /^(\d{6})$/;
					break;
				case "3": 
					regexNumber = /^(\d{7})$/;
					break;
				case "4": 
					regexNumber = /^(\d{7})$/;
					break;
				default:
					regexNumber = /^(\d{7})$/;
					break;
			}
			
	  		return regexNumber.test(number);	
		}
			
		return false;
	 
	};

	$scope.getLabelPays = function(id_pays){
		var labelTelPays = "";
		for(var i=0; i<$scope.listPays.length; i++){
			if($scope.listPays[i].id_pays == id_pays){
				labelTelPays = $scope.listPays[i].prefixe_tel;
			}
		}
		return labelTelPays;
	}

	$scope.editField = function(typeHTML, fieldname, scrollTo){
		$scope.edit();
		setTimeout(function() {
			$(scrollTo).get(0).scrollIntoView();
			$(typeHTML+fieldname)[0].focus();;
		}, 700);
	};

	$scope.validLogoEdit = function(){
		//Logo entreprise
		if($("#input-file-image").val() != ""){
			//console.log($scope.checkCropRegion());
			if( !$scope.checkCropRegion() ){
				toastr.error("Information sur la logo","Veuillez choisir une region pour le photo de profil");
				return;
			}else{

				$('#btn-modifier_logo')[0].click();
			}
		}else{
			toastr.error("Information sur la logo","Image non reconnu, veuillez insérer une image");
			return;
		}	

	};

	$scope.checkCoordValue = function(value){
        var result = parseFloat(value.toString().replace(',', '.'));
        if( !isNaN(result) ){
            return result;
        }
        else{
            return null;
        }
    };

	$scope.validFormEdit = function(){

		/*
		//Logo entreprise
		if($("#input-file-image").val() != ""){
			//console.log($scope.checkCropRegion());
			if( !$scope.checkCropRegion() ){
				toastr.error("Information sur la logo","Veuillez choisir une region pour le photo de profil");
				return;
			}
		}*/
		
		// teste de valeur ny type d'entreprise
		if($('#select_types_entreprise').select2('val').length <= 0){
			$('#scroll-type-entreprise').get(0).scrollIntoView();
			$('.select2-search__field').get(0).focus();
			toastr.error("Information incomplète","Veuillez renseigner le champ type d'entreprise");
			return;
		}
		// teste de valeur pour les inputs simples
		var allInput = $('.form-to-test');
		for(var i=0; i<allInput.length; i++){
			var element = allInput.get(i);
			if(element.value.trim() == ''){
				var title=$(element).attr("title");
				element.scrollIntoView();
				element.focus();
				toastr.error("Information incomplète","Veuillez renseigner le champ "+title);
				return;
			}
		}

		var allLatitude = $('.lat-to-test');
		var allLongitude = $('.long-to-test');
		for(var i=0; i<allLatitude.length; i++){
			var elementLatitude = allLatitude.get(i);
			var elementLongitude = allLongitude.get(i);

			var valLatitude = allLatitude.get(i).value.trim();
			var valLongitude = allLongitude.get(i).value.trim();

			if($scope.checkCoordValue(valLatitude) == null ){
				elementLatitude.scrollIntoView();
				elementLatitude.focus();
				toastr.error("Erreur","La latitude doit être une valeur numérique");
				return;
			}else if($scope.checkCoordValue(valLongitude) == null){
				elementLongitude.scrollIntoView();
				elementLongitude.focus();
				toastr.error("Erreur","La longitude doit être une valeur numérique");
				return;
			}else if(valLatitude == 0 && valLongitude == 0){
				elementLatitude.scrollIntoView();
				elementLatitude.focus();
				toastr.error("Erreur","Les coordonnées géographiques renseignées sont incorrectes");
				return;
			}

			$(elementLatitude).val(valLatitude.toString().replace(",","."));
			$(elementLongitude).val(valLongitude.toString().replace(",","."));

		}

		var telephoneResponsable1 = $("#responsable1_phone");
		var id_pays = $("#pays_entreprise").val();
		if(! $scope.validatePhoneNumberForCompte3(id_pays, telephoneResponsable1.val())){
			toastr.error("Erreur","Le numéro de télephone du premier responsable est invalide");
			telephoneResponsable1.get(0).scrollIntoView();
			telephoneResponsable1.get(0).focus();
			return;
		}
		// verification des email
		if($scope.twoResponsable || twoResponsable){
			if ($scope.mailResp1Invalide){
				toastr.error("Erreur","L'email du premier responsable est invalide");
				var element = $('#responsable1_email').get(0);
				element.scrollIntoView();
				element.focus();
				return;
			}
			if($scope.mailResp2Invalide){
				toastr.error("Erreur","L'email du second responsable est invalide");
				var element = $('#responsable2_email').get(0);
				element.scrollIntoView();
				element.focus();
				return;
			}
			var telephoneResponsable2 = $("#responsable2_phone");
			if(! $scope.validatePhoneNumberForCompte3(id_pays, telephoneResponsable2.val())){
				toastr.error("Erreur","Le numéro de télephone du second responsable est invalide");
				telephoneResponsable2.get(0).scrollIntoView();
				telephoneResponsable2.get(0).focus();
				return;
			}
		}
		else{
			if ($scope.mailResp1Invalide){
				toastr.error("Erreur","L'email du premier responsable est invalide");
				var element = $('#responsable1_email').get(0);
				element.scrollIntoView();
				element.focus();
				return;
			}
		}
		//Verification compte mobile banking
		// teste de valeur ny type d'entreprise
		var allSelectCompte = $('.select_compte_entreprise');
		var num_compte = $('.numero_compte_entreprise');

		for(var i=0; i<allSelectCompte.length; i++){
			var elementSelect = allSelectCompte.get(i);
			var elementInput = num_compte.get(i);

			//console.log($(elementSelect).select2('val'));
			
			if($(elementSelect).select2('val').length <= 0){
				elementSelect.scrollIntoView();
				elementSelect.focus();
				toastr.error("Information incomplète","Veuillez renseigner le type de compte");
				return;
			}
			if(elementInput.value.trim() == ''){
				elementInput.scrollIntoView();
				elementInput.focus();
				toastr.error("Information incomplète","Veuillez renseigner le numéro compte de l'entreprise");
				return;
			}

			var typeCompteSelected = $(elementSelect).select2('val');
			var etat = $scope.validatePhoneNumberForCompte2(typeCompteSelected, elementInput.value);
			if(!etat){
				elementInput.scrollIntoView();
				elementInput.focus();
				toastr.error("Numéro de compte invalide","Veuillez renseigner un numéro associée au type de compte spécifié");
				return;
			}


		}


		for(var i=0; i<num_compte.length; i++){
			var element = num_compte.get(i);
			if(element.value.trim() == ''){
				element.scrollIntoView();
				element.focus();
				toastr.error("Information incomplète","Veuillez renseigner le numéro compte de l'entreprise");
				return;
			}
		}

		// verification type aliment
		/*var test=false;
		var checkbox = $("input[name*='type_aliment']");
		for (var i=0; i<checkbox.length; i++){
			var choix=$(checkbox[i]).is(":checked");
			if (choix==true){
				test=true;
				break;
			}
		}
		//console.log('ato e');
		if(test==false){
			toastr.error("Information incomplète","Veuillez choisir le ou les types d'aliments que fourni votre entreprise");
			return;
		}*/
		// si tout est ok
		//console.log('ok e');
		/*if(!$scope.allCompteValid){
			toastr.error("Numéro de compte invalide","Veuillez renseigner un numéro associée au type de compte spécifié");
		}else{
			
		}*/
		for(var i=0; i<allSelectCompte.length; i++){
			var elementSelect = allSelectCompte.get(i);
			var typeNumCompte = $(elementSelect).select2('val');
			var elementInput = num_compte.get(i);

			switch(typeNumCompte){
				case "1":
					$(elementInput).val("+26132"+$(elementInput).val());
					break;
				case "2":
					$(elementInput).val("+26134"+$(elementInput).val());
					break;
				case "3":
					$(elementInput).val("+26133"+$(elementInput).val());
					break;		
			}

		}
		var labelTelephoneResponsable = "";
		labelTelephoneResponsable = $scope.getLabelPays(id_pays);
		telephoneResponsable1.val( labelTelephoneResponsable+telephoneResponsable1.val());

		if($scope.twoResponsable || twoResponsable){
			telephoneResponsable2.val( labelTelephoneResponsable+telephoneResponsable2.val());
		}

	
		//Crop logo 
		if($scope.uploadCrop != null){
			$scope.uploadCrop.croppie('result', {
	            type: 'canvas',
	            size: 'original'
	        }).then(function (resp) { 
	            //console.log("resp : "+ resp);
	            $('#imagebase64').val(resp);
	            $('#btn-modifier_compte')[0].click();
	            //console.log("val : "+ $('#imagebase64').val());
	        });
		}else{

			$('#btn-modifier_compte')[0].click();

		}	
		
		
	};

	$scope.initShowMap = function(identifiant){

		$(".load_map").show();
		$(".contenu_carte").hide();

		$("#map").html("");

		$("#identifiantMere").val(identifiant);

		mapboxgl.accessToken = 'pk.eyJ1IjoibmphcnkiLCJhIjoiY2p1ZHU0NWYzMDJ1MTRkbW16N2pmczh6YiJ9.i3r9euc0dZsYchVvQfdG5A';
		var map = new mapboxgl.Map({
			container: 'map',
			center: [ $scope.mainLong, $scope.mainLat ],
	  		zoom: 14,
	  		style: 'mapbox://styles/mapbox/streets-v9'
		});

		$(".lat_value_in_map").html( "(Non défini)" );
		$(".long_value_in_map").html( "(Non défini)" );


		var currentLong = $scope.checkCoordValue( $("#infoLongitude"+String(identifiant)).val() );
		var currentLat = $scope.checkCoordValue( $("#infoLatitude"+String(identifiant)).val() );

		var el = document.createElement('div');
      	el.className = 'marker';
      	el.style.backgroundImage = 'url('+site_url+'assets/img/marker_fw.png)';
      	el.style.width = '35px';
      	el.style.height = '35px';
      	el.style.backgroundRepeat = 'no-repeat';
      	el.style.backgroundSize = 'contain';

		var marker = new mapboxgl.Marker(el);
		
		if(currentLong != null && currentLat != null){

			if(currentLong != 0 && currentLat != 0){
				$scope.mainLong = currentLong;
				$scope.mainLat = currentLat;
			}

			$(".lat_value_in_map").html( String($scope.mainLat) );
			$(".long_value_in_map").html( String($scope.mainLong) );

			map.flyTo({ center: [$scope.mainLong, $scope.mainLat] });

			marker.remove();
			marker.setLngLat([$scope.mainLong, $scope.mainLat]).addTo(map);

		}

		//console.log("identifiant : "+identifiant);

		map.on('load', function () {
			
			// *** Add zoom and rotation controls to the map ...
			map.addControl(new mapboxgl.NavigationControl());

			$(".load_map").hide();
			$(".contenu_carte").show();

		});

		map.on('click', function(e){
			
			$scope.mainLong = e.lngLat.lng;
			$scope.mainLat = e.lngLat.lat;
			
			$(".lat_value_in_map").html( String($scope.mainLat) );
			$(".long_value_in_map").html( String($scope.mainLong) );

			map.flyTo({ center: [$scope.mainLong, $scope.mainLat] });

			marker.remove();
			marker.setLngLat([$scope.mainLong,$scope.mainLat]).addTo(map);						  			  

		});


		$("#search_in_carte").autocomplete({

			appendTo: "#autocomplete_list",
	        source: function (request, response) {
	            $.ajax({
	                url: 'https://api.mapbox.com/geocoding/v5/mapbox.places/'+ $("#search_in_carte").val() +'.json',
					type: "GET",
					data:{ 
						types : "locality,address",
						limit : 10,
						country : "mg",
						access_token : "pk.eyJ1IjoibmphcnkiLCJhIjoiY2p1ZHU0NWYzMDJ1MTRkbW16N2pmczh6YiJ9.i3r9euc0dZsYchVvQfdG5A"
					},
	                success: function (data) {
	                    //console.log(data);
	                    response($.map(data.features, function (item) {
	                    	//console.log(item);
	                      	return {
		                        label:  item.text + ", " + item.context[1].text,
		                        value:  item.text + ", " + item.context[1].text,
		                        coord: 	item.center 
	                      	}
	                   }))
	                },
	                error: function (xhr, status, err) {
	                    alert("Error on autocomplete");
	                }
	            });
	        },
	        select: function (even, ui) {
	            //console.log(ui.item.coord);
	            map.flyTo({ center: ui.item.coord });

				marker.remove();
				marker.setLngLat( ui.item.coord ).addTo(map);

				$scope.mainLong = ui.item.coord[0];
                $scope.mainLat = ui.item.coord[1];
            
                $(".lat_value_in_map").html( String($scope.mainLat) );
                $(".long_value_in_map").html( String($scope.mainLong) );
                
	        }
	    });

	}

	$scope.putCoordonneChoisi = function(){

		var identifiant = $("#identifiantMere").val();

		var currentLong = $scope.checkCoordValue( $scope.mainLong );
		var currentLat = $scope.checkCoordValue( $scope.mainLat );

		if( currentLong != null && currentLat != null ){

			$("#infoLongitude"+String(identifiant)).val( currentLong );
			$("#infoLatitude"+String(identifiant)).val( currentLat );

		}else{
			console.log("Coordinates undefined");
		}

	}


	// ------------------------------ POUR LE CONTRAT ------------------------------
	$scope.CONTRAT = {};
	$scope.CONTRAT.contrat = {};
    $scope.CONTRAT.contrat.contrat = {};
    $scope.CONTRAT.loading = false;
    $scope.CONTRAT.askDownload = true;
	$scope.CONTRAT.contrat_to_valid = contrat_to_valid;
    $scope.CONTRAT.contrat.entreprise = entreprise;
    $scope.CONTRAT.contrat.employe1 = employe1;
    $scope.CONTRAT.contrat.logoDonateur = {
    	dataURL: '',
    	ext: ''
    };

    $scope.CONTRAT.mailResp1Invalide = false;
    ////////////////////////$scope.CONTRAT.mailResp2Invalide = true;
    $scope.CONTRAT.modalSize = "";
    $scope.CONTRAT.contrat.contrat.validity = "";

    $scope.CONTRAT.modalOnClose = 'poste';
    $scope.CONTRAT.selectChoosing = 'select-contrat-poste-resp1';

    $scope.completeProtocole = function(){
    	if(paysEntreprise == 1){
	    	$('#contratModal').modal('show');
	    }
	    else{
	    	toastr.error("","Vous devez d'abord enregistrer le pays où se trouve votre entreprise");
	    	setTimeout(function() {
		    	$('#modal_edit').modal('show');
		    	setTimeout(function() {
		    		$('#scroll-adresse-entreprise').get(0).scrollIntoView();
		    	}, 700);
		    }, 1000);
	    }
    };

    $scope.CONTRAT.ngInit = function () {


    	//initialisation select2 pour contrat
    	$scope.initSelect2();

        /*if (employe2) {
            $scope.CONTRAT.contrat.employe2 = employe2;
            if (employe2['sexe'] == undefined || employe2['sexe'] == null || employe2['sexe'] == '') {
                $scope.CONTRAT.contrat.employe2['sexe'] = 'M';
            }
            $scope.CONTRAT.mailResp2Invalide = false;

            var employe2phone = $scope.CONTRAT.contrat.employe2.telephone;
			var res2 = employe2phone.slice(4);
	        $scope.CONTRAT.contrat.employe2.telephone = res2;

        } else {
            $scope.CONTRAT.contrat.employe2 = {
                nom: '',
                prenom: '',
                sexe: 'M',
                label: '',
                email: '',
                telephone: ''
            }
        }*/

        if ($scope.CONTRAT.contrat_to_valid == 1) {
            $scope.CONTRAT.modalSize = "modal-lg";
        }

	    /*if(contrat){
	    	$scope.CONTRAT.contrat.contrat.validity = contrat.validity;
	    	$('#contrat-select-validite').select2('val', contrat.validity);
	    }*/

        if(erreurUploadImage == 4){
        	$('#contratModal').modal('show');
        }

        $('#select-contrat-poste-resp1').select2('val', $scope.CONTRAT.contrat.employe1.id_poste);
        //$('#select-contrat-poste-resp2').select2('val', $scope.CONTRAT.contrat.employe2.id_poste);

        var employe1phone = $scope.CONTRAT.contrat.employe1.telephone;
		var res = employe1phone.slice(4);
        $scope.CONTRAT.contrat.employe1.telephone = res;
       
    };

    $scope.CONTRAT.onLogoDonateurChange = function(arg){
        var file = $('#input-logo-donateur').prop('files')[0];
        var reader = new FileReader();
        reader.onloadend = function () {
            $scope.CONTRAT.contrat.logoDonateur.dataURL = reader.result;
            $scope.CONTRAT.contrat.logoDonateur.ext = $scope.getImgExt(file.name);
        	$('#img-logo-donateur').attr('src', reader.result);
        };
        reader.readAsDataURL(file);
    };

    $scope.CONTRAT.posteEmployeChange = function(select, number){
    	$scope.CONTRAT.contrat['employe'+number].id_poste = select.value;
    	var options = select.children;
    	for(var i=0; i<options.length; i++){
    		if(options[i].value == select.value){
    			$scope.CONTRAT.contrat['employe'+number].label = options[i].innerHTML.trim();
    			break;
    		}
    	}
    };

    $scope.CONTRAT.submit = function () {

    	if(!$scope.CONTRAT.formIsBad()){
    		$scope.CONTRAT.loading = true;
	        $http({
	            method: "POST",
	            url: BASE_URL + "contrat/entente/submit",
	            data: $scope.CONTRAT.contrat,
	            headers: {
	                'Content-Type': 'application/x-www-form-urlencoded'
	            }
	        }).then(function successCallback(response) {
	            $scope.CONTRAT.loading = false;
	            window.location = BASE_URL + "entreprise/profil/4/1";

	        }, function errorCallback(response) {
	            console.log(response);
	        });
    	}else{
    		//toastr.error("Information incorrecte","Veuillez vérifiez ce que vous avez rempli");
    	}
	    	
    };

    $scope.CONTRAT.saveOnly = function($event, lienFichier){
    	$event.preventDefault();
        $scope.CONTRAT.loading = true;
        //window.location = lienFichier;

        $http({
            method: "POST",
            url: BASE_URL + "contrat/entente/saveOnly",
            data: $scope.CONTRAT.contrat,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(response=>{
            
            //lienFichier = 'http://itwedo-appdev.betaksys.mg/uploads/contrats/1/20181206072628_protocole_entente_MG_Softimad_MP.docx';
        	$scope.CONTRAT.downloadProtocolePDF(lienFichier);

        }, error=>{
            console.log(error);
        });

    };

    $scope.CONTRAT.validate = function($event, lienFichier){

        $event.preventDefault();
        $scope.CONTRAT.loading = true;
        //window.location = lienFichier;

        $http({

            method: "POST",
            url: BASE_URL + "contrat/entente/validate",
            data: $scope.CONTRAT.contrat,
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded'
            }
        }).then(function successCallback(response) {

        	//lienFichier = 'http://itwedo-appdev.betaksys.mg/uploads/contrats/1/20181206072628_protocole_entente_MG_Softimad_MP.docx';
        	$scope.CONTRAT.downloadProtocolePDF(lienFichier);  
            
        }, function errorCallback(response) {
            console.log(response);
        });
        
    };

    $scope.CONTRAT.downloadProtocolePDF = function(file_url){
		//Secret : MIHyh0gFxiJjghKU
		
		var formData = new FormData();
		formData.append('File', file_url);
		formData.append('StoreFile', true);

		$.ajax({
		    url: 'https://v2.convertapi.com/convert/docx/to/pdf?Secret=j7KeZRUbO0Bfodus',
		    data: formData,
		    processData: false,
		    contentType: false,  
		    method: 'POST',
		    success: function(data) {
		        /*console.log(data);
		        console.log("mandeh");*/
		        location.href = data.Files[0].Url;
		        $scope.CONTRAT.askDownload = false;
		        $scope.CONTRAT.loading = false;
		        $timeout(function () {
	                window.location.href = BASE_URL + "entreprise/profil";
	            }, 2000);    
		    }
		});
		
	};

	$scope.CONTRAT.downloadProtocole = function(){
		$scope.CONTRAT.askDownload = true;
		$scope.CONTRAT.loading = false;
		var lien = $("#contrat_file_url").val();
		//console.log(lien);
		$('#modal-download-protocole-pdf').modal('show');
		$("#downPdf").val(lien);
		//console.log($("#downPdf").val());
	};

    $scope.CONTRAT.launchDownloadPdf = function(){
    	
    	var url = $("#downPdf").val();  //à décommenter
    	//url = 'http://itwedo-appdev.betaksys.mg/uploads/contrats/1/20181206072628_protocole_entente_MG_Softimad_MP.docx';
    	$scope.CONTRAT.askDownload = false;
    	$scope.CONTRAT.loading = true;
        $scope.CONTRAT.downloadProtocolePDF(url);

    };

    $scope.CONTRAT.addNewPoste = function (lien) {
        var nomPoste = $('#input-contrat-nom-post-new').val().trim();
        if (nomPoste != '') {
            $('#modal-contrat-new-poste').modal('hide');
            var data = new FormData();
            data.append('nomPoste', nomPoste);
            data.append('typePoste', 1);
            $http({
                url: lien,
                method: "POST",
                data: data,
                headers: {'Content-type': undefined},
                transformRequest: angular.identity
            })
            .then(
                response => {
                    if (response.data.success) {
                        $('#input-contrat-nom-post-new').val('');
                        var select1 = $('#select-contrat-poste-resp1');
                        if(!select1.html().includes('<option value="'+response.data.idPoste)){
                            select1.append('<option value="' + response.data.idPoste + '" >' + nomPoste + '</option>');
                            $('#select-contrat-poste-resp2').append('<option value="' + response.data.idPoste + '" >' + nomPoste + '</option>');
                        }
                        $('#'+$scope.CONTRAT.selectChoosing).select2('val', response.data.idPoste);
                    }
                },
                error => {
                    console.log(error);
                }
            );
        }
    };

    $scope.CONTRAT.mailIsValide = function (email) {
        var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    };

    $scope.CONTRAT.verifMail = function (respClass) {
        var email = $('#contrat-responsable' + respClass + '_email').val();
        var classOther = respClass == '1' ? '2' : '1';
        if ($scope.CONTRAT.mailIsValide(email)) {
            var loader = $('#loader-email-resp' + respClass);
            loader.show();
            var data = $.param({
                'email': email
            });
            $http({
                url: BASE_URL + 'utilisateur/acces/mailExist',
                method: "POST",
                data: data,
                headers: {'Content-type': 'application/x-www-form-urlencoded'}
            })
            .then(
                response => {
                    if (response.data.exist) {
                        if (email != responsables['responsable' + respClass + '_email']) {
                            $('#contrat-msg-email-resp' + respClass).show();
                            $scope.CONTRAT['mailResp' + respClass + 'Invalide'] = true;
                        } else {
                            $scope.CONTRAT['mailResp' + respClass + 'Invalide'] = false;
                            $('#contrat-msg-email-resp' + respClass).hide();
                        }
                    } else {
                        if (email != $('#responsable' + classOther + '_email').val()) {
                            $scope.CONTRAT['mailResp' + respClass + 'Invalide'] = false;
                            $('#contrat-msg-email-resp' + respClass).hide();
                        } else {
                            $('#contrat-msg-email-resp' + respClass).show();
                            $scope.CONTRAT['mailResp' + respClass + 'Invalide'] = true;
                        }
                    }
                    loader.hide();
                },
                error => {
                    loader.hide();
                }
            );
        }
        else{
        	$scope.CONTRAT['mailResp' + respClass + 'Invalide'] = true;
        }
    };

	$scope.CONTRAT.formIsBad = function(){


        /*if($scope.CONTRAT.contrat.contrat.validity == '' || $scope.CONTRAT.contrat.contrat.validity == undefined){	
           $("#contrat-select-validite").get(0).scrollIntoView();
           $("#contrat-select-validite").get(0).focus();
           toastr.error("Information incomplète","Veuillez renseigner le champ validité");
           return true;

        }*/

        var allInput = $('.form-contrat-to-test');
		for(var i=0; i<allInput.length; i++){
			var element = allInput.get(i);
			if(element.value.trim() == ''){
				var title=$(element).attr("title");
				element.scrollIntoView();
				element.focus();
				toastr.error("Information incomplète","Veuillez renseigner le champ "+title);
				return true;
			}
		}

		if($scope.CONTRAT.mailResp1Invalide){
			$("#contrat-responsable1_email").get(0).scrollIntoView();
           	$("#contrat-responsable1_email").get(0).focus();
           	toastr.error("Erreur","L'email du responsable de la communication est invalide");
           	return true;
        }

        var telephoneResponsable1 = $("#resp_com_direct_phone");
		var id_pays = $("#pays_entreprise").val();
		if(! $scope.validatePhoneNumberForCompte3(id_pays, telephoneResponsable1.val())){
			toastr.error("Erreur","Le numéro de télephone du responsable de la communication est invalide");
			telephoneResponsable1.get(0).scrollIntoView();
			telephoneResponsable1.get(0).focus();
			return true;
		}

        /*if($scope.CONTRAT.contrat.employe2.id_poste == '' || $scope.CONTRAT.contrat.employe2.id_poste == undefined){   	
           $("#select-contrat-poste-resp2").get(0).scrollIntoView();
           $("#select-contrat-poste-resp2").get(0).focus();
           toastr.error("Information incomplète","Veuillez bien renseigner le champ poste du responsable de la collecte");
            return true;
        }*/

        /*if($scope.CONTRAT.mailResp2Invalide){
        	$("#contrat-responsable2_email").get(0).scrollIntoView();
           	$("#contrat-responsable2_email").get(0).focus();
           	toastr.error("Erreur","L'email du responsable de la collecte est invalide");
           	return true;
        }*/

        /*var telephoneResponsable2 = $("#resp_collecte_phone");
		var id_pays = $("#pays_entreprise").val();
		if(! $scope.validatePhoneNumberForCompte3(id_pays, telephoneResponsable2.val())){
			toastr.error("Erreur","Le numéro de télephone du responsable de la collecte est invalide");
			telephoneResponsable2.get(0).scrollIntoView();
			telephoneResponsable2.get(0).focus();
			return true;
		}*/

		var labelTelephoneResponsable = "";
		labelTelephoneResponsable = $scope.getLabelPays(id_pays);
		$scope.CONTRAT.contrat.employe1.telephone = labelTelephoneResponsable+telephoneResponsable1.val();
		//$scope.CONTRAT.contrat.employe2.telephone = labelTelephoneResponsable+telephoneResponsable2.val();
		return false;
        //return ($scope.CONTRAT.mailResp1Invalide || $scope.CONTRAT.mailResp2Invalide);
	};

	/* -------------- GESTION DE MAGASINS -------------- */

	$scope.MAGASIN = {};
	$scope.MAGASIN.magasins = magasins;
	$scope.MAGASIN.modalTitle = '';
	$scope.MAGASIN.deleteMessage = '';
	$scope.MAGASIN.isAdd = true;
	$scope.MAGASIN.indexChoosen = -1;

	$scope.MAGASIN.openModalMagasins = function(){
		if($scope.MAGASIN.magasins.length <= 0){
			$scope.MAGASIN.modalTitle = "Ajout d'un nouveau magasin";
			$('#modal-add-magasin').modal('show');
		}
		else{
			$('#modal-manage-magasin').modal('show');
		}
	};

	$scope.MAGASIN.openModalAddMagasin = function(){
		$scope.MAGASIN.isAdd = true;
		$scope.MAGASIN.modalTitle = "Ajout d'un nouveau magasin";
		$('#modal-manage-magasin').modal('hide');
		$('#modal-add-magasin').modal('show');
		$('.mag-input-to-clear').val('');
	};

	$scope.MAGASIN.openModalEdit = function(id_magasin){
		$scope.MAGASIN.isAdd = false;
		$scope.MAGASIN.modalTitle = "Modification du magasin";
		$('#modal-manage-magasin').modal('hide');
		$('#modal-add-magasin').modal('show');
		var index = 0;
		for(var i=0; i<$scope.MAGASIN.magasins.length; i++){
			if($scope.MAGASIN.magasins[i].id_magasin == id_magasin){
				index = i;
				$scope.MAGASIN.indexChoosen = i;
				break;
			}
		}
		$('#input-nom-magasin').val($scope.MAGASIN.magasins[index].nom);
		$('#input-nom-resp').val($scope.MAGASIN.magasins[index].responsable.nom);
		$('#input-prenom-resp').val($scope.MAGASIN.magasins[index].responsable.prenom);
		$('#input-sexe-resp').val($scope.MAGASIN.magasins[index].responsable.sexe);
		$('#input-email-resp').val($scope.MAGASIN.magasins[index].responsable.email);

		var respPhone = $scope.MAGASIN.magasins[index].responsable.telephone;
		var res = respPhone.slice(4);
        $scope.MAGASIN.magasins[index].responsable.telephone = res;
		$('#input-tel-resp').val($scope.MAGASIN.magasins[index].responsable.telephone);

		$('#select-post-resp').select2('val', $scope.MAGASIN.magasins[index].responsable.poste_id);
		$scope.respMagEmail = $scope.MAGASIN.magasins[index].responsable.email;
	};

	$scope.MAGASIN.updateResponsable = function(lien){
		var magasin = $('#input-nom-magasin').val().trim();
		var nom = $('#input-nom-resp').val().trim();
		var prenom = $('#input-prenom-resp').val().trim();
		var sexe = $('#select-sexe-resp').val();
		var email = $('#input-email-resp').val().trim();
		var telephone = $('#input-tel-resp').val().trim();
		var poste = $('#select-post-resp').val();
		if(magasin == ""){
			toastr.error("","Le nom du magasin est vide");
			$('#input-nom-magasin').get(0).scrollIntoView();
			$('#input-nom-magasin').get(0).focus();
			return;
		}
		if(nom == "" || (sexe!='F' && sexe!='M')){
			toastr.error("","Le nom du responsable est vide");
			$('#input-nom-resp').get(0).scrollIntoView();
			$('#input-nom-resp').get(0).focus();
			return;
		}
		if(prenom == ""){
			toastr.error("","Le prénom du responsable est vide");
			$('#input-prenom-resp').get(0).scrollIntoView();
			$('#input-prenom-resp').get(0).focus();
			return;
		}
		if(!$scope.mailIsValide(email)){
			toastr.error("","Veuillez entrer un e-mail valide pour ce responsable");
			$('#input-email-resp').get(0).scrollIntoView();
			$('#input-email-resp').get(0).focus();
			return;
		}
		/*if(!$scope.numeroIsValide(telephone)){
			toastr.error("","Le numéro de téléphone est invalide");
			return;
		}*/
		var id_pays = $("#pays_entreprise").val();
		if(! $scope.validatePhoneNumberForCompte3(id_pays, telephone)){
			toastr.error("","Le numéro de télephone du responsable est invalide");
			$('#input-tel-resp').get(0).scrollIntoView();
			$('#input-tel-resp').get(0).focus();
			return;
		}

		if(poste == "" || poste == null){
			toastr.error("","Veuillez choisir un poste pour ce responsable");
			$('#select-post-resp').get(0).scrollIntoView();
			$('#select-post-resp').get(0).focus();
			return;
		}
		
		$('#lds-roller-responsable').show();
		$('#footer-resp-modal').hide();
		var index = $scope.MAGASIN.indexChoosen;
		var data = new FormData();
		data.append('magasin', magasin);
		data.append('nom', nom);
		data.append('prenom', prenom);
		data.append('sexe', sexe);
		data.append('email', email);

		var labelTelephoneResponsable = "";
		labelTelephoneResponsable = $scope.getLabelPays(id_pays);
		telephone = labelTelephoneResponsable + telephone;

		data.append('telephone', telephone);
		data.append('poste', poste);
		if(!$scope.MAGASIN.isAdd){
			data.append('id_magasin', $scope.MAGASIN.magasins[index].id_magasin);
			data.append('employe_responsable_id', $scope.MAGASIN.magasins[index].employe_responsable_id);
		}
		$http({
			url:lien,
			method:"POST",
			data:data,
			headers:{'Content-type':undefined},
			transformRequest: angular.identity
		})
		.then(
			response=>{
				if(response.data.success){
					$('#modal-add-magasin').modal('hide');
					if($scope.MAGASIN.isAdd){
						$scope.MAGASIN.magasins.push(response.data.result);
					}
					else{
						$scope.MAGASIN.magasins[index] = response.data.result;
					}
					$('#modal-manage-magasin').modal('show');
					$('.mag-input-to-clear').val('');
				}
				toastr.success('', response.data.message);
				$('#lds-roller-responsable').hide();
				$('#footer-resp-modal').show();
			},
			error=>{
				toastr.error('La connexion au serveur est impossible', 'Oops');
				$('#footer-resp-modal').show();
				$('#lds-roller-responsable').hide();
			}
		);
	};

	$scope.MAGASIN.openConfirmDelete = function(id_magasin){
		var index = 0;
		for(var i=0; i<$scope.MAGASIN.magasins.length; i++){
			if($scope.MAGASIN.magasins[i].id_magasin == id_magasin){
				index = i;
				$scope.MAGASIN.indexChoosen = i;
				break;
			}
		}
		$scope.MAGASIN.deleteMessage = 'Voulez-vous supprimer le magasin "'+$scope.MAGASIN.magasins[index].nom+'" dont le responsable est "'+$scope.MAGASIN.magasins[index].responsable.nom+' '+$scope.MAGASIN.magasins[index].responsable.prenom+'"';
		$('#modal-manage-magasin').modal('hide');
		$('#modal-confirm-delete').modal('show');
	};

	$scope.MAGASIN.delete = function(lien){
		$('.modal-footer-delete').hide();
		$('#lds-roller-delete').show();
		var index = $scope.MAGASIN.indexChoosen;
		var data = new FormData();
		data.append('id_magasin', $scope.MAGASIN.magasins[index].id_magasin);
		data.append('employe_responsable_id', $scope.MAGASIN.magasins[index].employe_responsable_id);
		$http({
			url:lien,
			method:"POST",
			data:data,
			headers:{'Content-type':undefined},
			transformRequest: angular.identity
		})
		.then(
			response=>{
				$('#modal-confirm-delete').modal('hide');
				$('.modal-footer-delete').show();
				$('#lds-roller-delete').hide();
				$('#modal-manage-magasin').modal('show');
				$scope.MAGASIN.magasins.splice(index, 1);
				toastr.success('', 'Le magasin est supprimé');
			},
			error=>{
				toastr.error('La connexion au serveur est impossible', 'Oops');
				$('.modal-footer-delete').show();
				$('#lds-roller-delete').hide();
			}
		);
	};

	$scope.initSelect2 = function(){
		$('#select_types_entreprise').select2({
		    width: '100%',
		    placeholder: 'Choisisser ou ajouter',
		    language: {
		      noResults: function() {
		      	var bouton = '<button id="no-results-btn" class="btn btn-primary btn-block" ng-click="addTypeEntreprise()">Ajouter Type</a>';
		        return $compile(bouton)($scope);
		      },
		    },
		    escapeMarkup: function(markup) {
		      return markup;
		    },
		   
	    });
		$('.select_compte_entreprise').select2({
		    width: '100%',
		    placeholder: 'Choisisser ou ajouter',
		    language: {
		      noResults: function() {
		        return 'Aucun resultat';
		      },
		    },
		    escapeMarkup: function(markup) {
		      return markup;
		    },
	    });
		$('#select_poste1').select2({
		    width: '100%',
		    placeholder: 'Choisisser ou ajouter',
		    language: {
		      noResults: function() {
		      	var bouton = '<button id="no-results-btn" class="btn btn-primary btn-block" ng-click="addPoste(\'#select_poste1\')">Ajouter poste</a>';
		        return $compile(bouton)($scope);
		      },
		    },
		    escapeMarkup: function(markup) {
		      return markup;
		    },
	    });
		$('#select_poste2A').select2({
		    width: '100%',
		    placeholder: 'Choisisser ou ajouter',
		    language: {
		      noResults: function() {
		      	var bouton = '<button id="no-results-btn" class="btn btn-primary btn-block" ng-click="addPoste(\'#select_poste2A\')">Ajouter poste</a>';
		        return $compile(bouton)($scope);
		      },
		    },
		    escapeMarkup: function(markup) {
		      return markup;
		    },
		   
	    });
		$('#contrat-select-validite').select2({
		    width: '100%',
		    placeholder: 'Choisisser ou ajouter',
		    language: {
		      noResults: function() {
		        return 'Aucun resultat';
		      },
		    },
		    escapeMarkup: function(markup) {
		      return markup;
		    },
	    });
		$('#select-contrat-poste-resp1').select2({
		    width: '100%',
		    placeholder: 'Choisisser ou ajouter',
		    language: {
		      noResults: function() {
		        var bouton = '<button id="no-results-btn" class="btn btn-primary btn-block" ng-click="addPoste(\'#select-contrat-poste-resp1\')">Ajouter poste</a>';
		        return $compile(bouton)($scope);
		      },
		    },
		    escapeMarkup: function(markup) {
		      return markup;
		    },
	    });
		$('#select-contrat-poste-resp2').select2({
		    width: '100%',
		    placeholder: 'Choisisser ou ajouter',
		    language: {
		      noResults: function() {
		        var bouton = '<button id="no-results-btn" class="btn btn-primary btn-block" ng-click="addPoste(\'#select-contrat-poste-resp2\')">Ajouter poste</a>';
		        return $compile(bouton)($scope);
		      },
		    },
		    escapeMarkup: function(markup) {
		      return markup;
		    },
	    });
		$('#select-post-resp').select2({
		    width: '100%',
		    placeholder: 'Choisisser ou ajouter',
		    language: {
		      noResults: function() {
		        var bouton = '<button id="no-results-btn" class="btn btn-primary btn-block" ng-click="addPoste(\'#select-post-resp\')">Ajouter poste</a>';
		        return $compile(bouton)($scope);
		      },
		    },
		    escapeMarkup: function(markup) {
		      return markup;
		    },
	    });
	}

});