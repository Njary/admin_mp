var site_url = $("#site_url").val();

//Cloud Firestore configuration
/*var config = {
    apiKey: "AIzaSyAL25o7WT-fEy4oJ9P10hFvvG9ssxZr6BI",
    authDomain: "manzerpartazer-55257.firebaseapp.com",
    databaseURL: "https://manzerpartazer-55257.firebaseio.com",
    projectId: "manzerpartazer-55257",
    storageBucket: "manzerpartazer-55257.appspot.com",
    messagingSenderId: "249167550160"
};*/
//firebase.initializeApp(config);

//Initialize cloud firestore through Firebase
//var db = firebase.firestore();


var app = angular.module('VenteApp', []);

app.controller('VenteCtrl', ["$scope", "$http", "$compile", function($scope, $http, $compile) {

    $scope.pushDate = null;
    $scope.images = [];
    $scope.excelImages = []; // contient les images pour les produit depuis import excel
    $scope.productFromExcel = []; // contient les produits depuis import excel
    $scope.photoExcelIndex = 0; // savoir a quel index du produit (depuis import excel) sont les photos a importer
    $scope.numStrategie = (parseInt($('#statut_vente_modif').val()) == 1) ? parseInt($("#nb_strategie_marketing").val())+1 : 2 ;
    $scope.limite_strategie = parseInt($("#nb_periode_reduction").val());
    $scope.numPeriodeRecuperation = (parseInt($('#statut_vente_modif').val()) == 1) ? parseInt($("#nb_periode_recuperation").val())+1 : 2 ;
    //$scope.numPeriodeRecuperation = 2;
    $scope.mainLong = 47.5217;
    $scope.mainLat = -18.9156; 

    $scope.formList = [
        {
            id: '1',
            images: []
        }
    ];
    $scope.formClicked = ''; // alalana ny formulaire miasa, mcontenir ny id ny element ny formulaire

    $scope.showAlert = function(title, message){
        $('#modal-alert-title').html(title);
        $('#modal-alert-message').html(message);
        $('#modal-alert').modal('show');
    };

     $scope.showAlertVente = function(title, message){
        $('#modal-alert-title-vente').html(title);
        $('#modal-alert-message-vente').html(message);
        $('#modal-alert-vente').modal('show');
    };

    $scope.venteFinish = function(etat){
        if(etat == 1){
            window.location.href = BASE_URL + "produit/2";    
        }else{
            $('#modal-alert-vente').modal('hide');
        }    
        
    }

    $scope.showLoading = function(title){
        $('#modal-loading-title').html(title);
        $('#modal-loading').css('display','block');
    };

    $scope.hideLoading = function(){
        $('#modal-loading').css('display','none');
    };

    $scope.formClick = function(id){
        $scope.formClicked = id;
    };


    $scope.getNumber = function(value){
        var result = parseFloat(value.toString().replace(',', '.'));
        if( !isNaN(result) && result >= 0){
            return result;
        }
        else{
            return -1;
        }
    };

    $scope.test = function(){
        console.log("azea");
        alert("mamdfs");
    }

    $scope.mileSeparator = function(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ' ' + '$2');
        }
        return x1 + x2;
    }

    $scope.formatNumberToMileSep = function(selector){
        var value = $(selector).val().trim().replace(/\s/g,"");
        var newVal = $scope.mileSeparator(value);
        $(selector).val(newVal);
    }

    $scope.addNewType = function(lien){
        $http({
            url: lien, 
            method: 'POST',
            data: 'type='+$('#type-name-modal').val().trim(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(response=>{
            var data = response.data;
            if(data.success){
                $('#type-name-modal').val("");
                var select = $('#select-type'+$scope.formClicked);
                select.append('<option value="'+data.id_type_produit+'" >'+data.label+'</option>');
                select.select2('val', data.id_type_produit);
                for(var i=0; i<$scope.formList.length; i++){
                    if($scope.formList[i].id != $scope.formClicked){
                        $('#select-type'+$scope.formList[i].id).append('<option value="'+data.id_type_produit+'" >'+data.label+'</option>');
                    }
                }
                $scope.initDonSelect2($scope.formClicked);
            }
            else{
                $scope.showAlert('', data.message);
            }
        }, error=>{
            $scope.showAlert('', 'Une erreur est survenue pendant l\'ajout du nouveau type');
        });
    };

    $scope.addNewInfoNutris = function(lien){
        $http({
            url: lien, 
            method: 'POST',
            data: 'infoNutris='+$('#info-nutris-name-modal').val(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(response=>{
            var data = response.data;
            if(data.success){
                $('#info-nutris-name-modal').val("");
                var select = $('#select-info-nutris'+$scope.formClicked);
                select.append('<option value="'+data.id_information_nutritionnelle+'" >'+data.label+'</option>');
                select.select2("val", select.select2("val").concat(data.id_information_nutritionnelle));
                for(var i=0; i<$scope.formList.length; i++){
                    if($scope.formList[i].id != $scope.formClicked){
                        $('#select-info-nutris'+$scope.formList[i].id).append('<option value="'+data.id_information_nutritionnelle+'" >'+data.label+'</option>');
                    }
                }

                $('#select-type-info-nutri-modal').append('<option value="'+data.id_information_nutritionnelle+'" >'+data.label+'</option>');
                $scope.initDonSelect2($scope.formClicked);
            }
            else{
                $scope.showAlert('', data.message);
            }
        }, error=>{
            $scope.showAlert('', 'Une erreur est survenue pendant l\'ajout de la nouvelle information nutritionnelle');
        });
    };

    $scope.addNewRaisonSurplus = function(lien){
        $http({
            url: lien, 
            method: 'POST',
            data: 'raisonSurplus='+$('#raison-surplus-name-modal').val(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(response=>{
            var data = response.data;
            if(data.success){
                $('#raison-surplus-name-modal').val("");
                var select = $('#select-raison-surplus'+$scope.formClicked);
                select.append('<option value="'+data.id_raison_surplus+'" >'+data.label+'</option>');
                select.select2("val", select.select2("val").concat(data.id_raison_surplus));
                for(var i=0; i<$scope.formList.length; i++){
                    if($scope.formList[i].id != $scope.formClicked){
                        $('#select-raison-surplus'+$scope.formList[i].id).append('<option value="'+data.id_raison_surplus+'" >'+data.label+'</option>');
                    }
                }
                $scope.initDonSelect2($scope.formClicked);
            }
            else{
                $scope.showAlert('', data.message);
            }
        }, error=>{
            $scope.showAlert('', 'Une erreur est survenue pendant l\'ajout du raison de surplus');
        });
    };

    $scope.clickGroupement = function(arg){
        if(arg==1){
            $('#input-groupement-total').prop('checked', false);
        }
        else{
            $('#input-groupement-unite').prop('checked', false);
        }
        //console.log('click');
    };

    $scope.onFileImageChange = function(element){

        var files = $('#input-file-image'+$scope.formClicked).prop('files');

        for(var i = 0 ; i < files.length ; i++){
            if( $scope.testeImage(files[i].type)!=1 ){
                toastr.error("Erreur","Veuillez choisir des fichiers de type image");
                return;
            }

        }
        var divFilePreview = $('#div-file-preview'+$scope.formClicked);
        $scope.showImage(divFilePreview, files, 0);
    };

    $scope.getImgExt = function(imgName){
        var strs = imgName.split('.');
        if(strs.length > 1){
            return strs[strs.length-1];
        }
        return 'png';
    };
    $scope.testeImage = function(typeFile){
        var strs = typeFile.split('/');
        if(strs[0]=='image'){
            return 1;
        }
        return 0;
    };

    $scope.imageExist = function(dataURL, array){
        var existe = false;
        for(var i in array){
            if(!array[i].fromServer){
                if(array[i].dataURL == dataURL){
                    existe = true;
                    break;
                }
            }
        }
        return existe;
    };

    $scope.showImage = function(divFilePreview, files, index, array){

        if(index < files.length){

            var reader = new FileReader();
            var arrayName = 1;
            var _index = Date.now() + index;
            var idForm;
            reader.onloadend = function () {
                var newPhoto = {
                    extension: $scope.getImgExt(files[index].name),
                    dataURL: reader.result,
                    index: _index,
                    fromServer: false
                };
                var existe = false;
                if(array != undefined){
                    arrayName = 2;
                    existe = $scope.imageExist(newPhoto.dataURL, array);
                    if(!existe){
                        array.push(newPhoto);
                    }
                }
                else{
                    var arrayForm;
                    for(var i=0; i<$scope.formList.length; i++){
                        if($scope.formList[i].id == $scope.formClicked){
                            arrayForm = $scope.formList[i].images;
                            idForm = $scope.formClicked;
                            break;
                        }
                    }
                    existe = $scope.imageExist(newPhoto.dataURL, arrayForm);
                    if(!existe){
                        arrayForm.push(newPhoto);
                    }
                }
                if(!existe){
                    
                    divFilePreview.append($compile('<div class="col-6 div-img-preview" ><img src="'+reader.result+'" /><button ng-click="removeImage('+arrayName+', '+_index+', '+idForm+')" class="btn btn-secondary"><i class="fa fa-trash" ></i></button></div>')($scope));
                }
                $scope.showImage(divFilePreview, files, index+1, array);
            }
            if (files[index]) {
                reader.readAsDataURL(files[index]);
            }
        }
    };

    $scope.removeImage = function(arrayName, index, idForm){
        var divFilePreview;
        var array;
        if(arrayName == 1){
            divFilePreview = $('#div-file-preview'+idForm);
            for(var i=0; i<$scope.formList.length; i++){
                if($scope.formList[i].id == idForm){
                    array = $scope.formList[i].images;
                    break;
                }
            }
        }
        else{
            divFilePreview = $('#div-image-preview-excel');
            array = $scope.excelImages;
        }

        for(var i=0; i<array.length; i++){
            if(array[i].index == index){
                array.splice(i, 1);
                break;
            }
        }

        divFilePreview.html('');
        for(var i=0; i<array.length; i++){
            if(array[i].fromServer){
                divFilePreview.append($compile('<div class="col-6 div-img-preview" ><img src="'+array[i].url+'" /><button ng-click="removeImage('+arrayName+', '+array[i].index+', '+idForm+')" class="btn btn-secondary"><i class="fa fa-trash" ></i></button></div>')($scope));
            }
            else{
                divFilePreview.append($compile('<div class="col-6 div-img-preview" ><img src="'+array[i].dataURL+'" /><button ng-click="removeImage('+arrayName+', '+array[i].index+', '+idForm+')" class="btn btn-secondary"><i class="fa fa-trash" ></i></button></div>')($scope));
            }
        }
    };

    $scope.removeImageProduct = function(id_photo_produit){
        $("#div-input-img-product"+id_photo_produit).remove();
    };

    $scope.showPhotoPicker = function(idForm, baseUrl, lien, forExcel){
        $('#modal-photo-picker').modal('show');
        var divPreview = $('#div-file-preview-modal');
        var loading = $('#lds-roller-photo-modal');
        divPreview.html('');
        loading.show();
        $scope.formClicked = idForm;
        $http({
            url: lien, 
            method: 'POST',
            data: 'nom='+$('#input-photo-modal').val().trim(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(
            response=>{
                loading.hide();
                var liste = response.data;
                for(var i=0; i<liste.length; i++){
                    if(forExcel == undefined){
                        divPreview.append($compile('<div class="col-3 div-img-preview" ><img id="img-photo-'+liste[i].id_photo_produit+'" class="photo-in-picker" ng-click="clickPhotoFromServer('+idForm+','+liste[i].id_photo_produit+')" src="'+baseUrl+liste[i].url+'" /><i ng-show="photoIsChoosing('+idForm+','+liste[i].id_photo_produit+')" class="fa fa-check"></i></div>')($scope));
                    }
                    else{
                        divPreview.append($compile('<div class="col-3 div-img-preview" ><img id="img-photo-'+liste[i].id_photo_produit+'" class="photo-in-picker" ng-click="clickPhotoFromServer('+idForm+','+liste[i].id_photo_produit+', '+forExcel+')" src="'+baseUrl+liste[i].url+'" /><i ng-show="photoIsChoosing('+idForm+','+liste[i].id_photo_produit+', '+forExcel+')" class="fa fa-check"></i></div>')($scope));
                    }
                }
            },
            error=>{
                loading.hide();
                divPreview.html("Pas encore d'image du côté du serveur");
            }
        );
    };

    $scope.searchPhoto = function(baseUrl, lien, forExcel){
        var divPreview = $('#div-file-preview-modal');
        var loading = $('#lds-roller-photo-modal');
        loading.show();
        $http({
            url: lien, 
            method: 'POST',
            data: 'nom='+$('#input-photo-modal').val().trim(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(
            response=>{
                loading.hide();
                divPreview.html('');
                var liste = response.data;
                for(var i=0; i<liste.length; i++){
                    if(forExcel == undefined){
                        divPreview.append($compile('<div class="col-3 div-img-preview" ><img id="img-photo-'+liste[i].id_photo_produit+'" class="photo-in-picker" ng-click="clickPhotoFromServer('+$scope.formClicked+','+liste[i].id_photo_produit+')" src="'+baseUrl+liste[i].url+'" /><i ng-show="photoIsChoosing('+$scope.formClicked+','+liste[i].id_photo_produit+')" class="fa fa-check"></i></div>')($scope));
                    }
                    else{
                        divPreview.append($compile('<div class="col-3 div-img-preview" ><img id="img-photo-'+liste[i].id_photo_produit+'" class="photo-in-picker" ng-click="clickPhotoFromServer('+$scope.formClicked+','+liste[i].id_photo_produit+','+forExcel+')" src="'+baseUrl+liste[i].url+'" /><i ng-show="photoIsChoosing('+$scope.formClicked+','+liste[i].id_photo_produit+','+forExcel+')" class="fa fa-check"></i></div>')($scope));
                    }
                }
            },
            error=>{
                loading.hide();
                divPreview.html("Pas encore d'image du côté du serveur");
            }
        );
    };

    $scope.photoIsChoosing = function(idForm, id_photo_produit, forExcel){
        var array;
        if(forExcel == undefined){
            for(var i=0; i<$scope.formList.length; i++){
                if($scope.formList[i].id == idForm){
                    array = $scope.formList[i].images;
                    break;
                }
            }
        }
        else{
            array = $scope.excelImages;
        }
        var existe = false;
        for(var i=0; i<array.length; i++){
            if(array[i].fromServer){
                if(array[i].idPhoto == id_photo_produit){
                    return true;
                }
            }
        }
        return false;
    };

    $scope.clickPhotoFromServer = function(idForm, id_photo_produit, forExcel){
        var divFilePreview;
        var arrayName = 1;
        if(forExcel == undefined){
            divFilePreview = $('#div-file-preview'+idForm);
        }
        else{
            divFilePreview = $('#div-image-preview-excel');
            arrayName = 2;
        }
        var url = $('#img-photo-'+id_photo_produit).prop('src');
        var _index = Date.now();
        var newPhoto = {
            url: url,
            idPhoto: id_photo_produit,
            index: _index,
            fromServer: true
        };
        var array;
        if(forExcel == undefined){
            for(var i=0; i<$scope.formList.length; i++){
                if($scope.formList[i].id == idForm){
                    array = $scope.formList[i].images;
                    break;
                }
            }
        }
        else{
            array = $scope.excelImages;
        }
        var existe = false;
        for(var i=0; i<array.length; i++){
            if(array[i].fromServer){
                if(array[i].idPhoto == id_photo_produit){
                    existe = true;
                    _index = array[i].index;
                    break;
                }
            }
        }
        if (existe) {
            $scope.removeImage(arrayName, _index, idForm);
        }
        else{
            array.push(newPhoto);
            divFilePreview.append($compile('<div class="col-6 div-img-preview" ><img src="'+url+'" /><button ng-click="removeImage('+arrayName+', '+_index+', '+idForm+')" class="btn btn-secondary"><i class="fa fa-trash" ></i></button></div>')($scope));
        }
    };

    $scope.dateIsGood = function(date){
        if(date == ""){
            return false;
        }
        else{
            var strs = date.split('/');
            if(strs.length != 3){
                return false;
            }
            else{
                if(parseInt(strs[2]) < 2018){
                    return false;
                }
                else{
                    return true;
                }
            }
        }
    };

    // maka ny datetime au format mysql yyyy-mm-jj hh:mm:ss
    $scope.getDatetimeString = function(){
        var datetime = new Date(Date.now());
        var date = datetime.getFullYear()+'-'+(datetime.getMonth()+1)+'-'+datetime.getDate();
        var time = datetime.getHours()+':'+datetime.getMinutes()+':'+datetime.getSeconds();
        return date+' '+time;
    }

    $scope.formIsGood = function(formIndex, idForm){
        var nom = $("#input-nom"+idForm).val().trim();
        var poids = $scope.getNumber($("#input-poids"+idForm).val().trim().replace(/\s/g,""));
        var prix = $scope.getNumber($("#input-prix"+idForm).val().trim().replace(/\s/g,""));
        var groupement = $("#input-groupement-unite"+idForm).prop('checked') ? 1 : ($("#input-groupement-portion"+idForm).prop('checked') ? 3 : 2);
        var quantite = 0;
        if(groupement == 2){
            $("#input-quantite"+idForm).val( 1 );
            quantite = 1;
        }else{
            quantite = parseInt($scope.getNumber($("#input-quantite"+idForm).val().trim().replace(/\s/g,"")));
        }
        var pourcentage = $scope.getNumber($("#input-pourcentage-reduction"+idForm).val().trim());
        var datePerempOrProd = $('#input-date-peremption-production'+idForm).val();
        var categorie = $("#select-categorie"+idForm).val();
        var type = $("#select-type"+idForm).val();
        //var infoNutris = $("#select-info-nutris"+idForm).select2('val').toString().split(",");
        //var raisonSurplus = $("#select-raison-surplus"+idForm).select2('val').toString().split(",");
        //var haveRaisonSurplus = $("#input-surplus-alim-oui"+idForm).prop('checked') ? 1 : 0;

        /*console.log(quantite);
        console.log(groupement);*/

        //facultatif
        var volume = $("#input-volume"+idForm).val().trim();


        //fin facultatif

        var isValide = true;
        var errorMsg = "";

         if(volume != '' && $scope.getNumber( volume.replace(/\s/g,"") )==-1){
            $("#input-volume"+idForm).addClass("erreur_input");
            isValide = false;
            var html = '<div class="message-erreur"><span class="message_erreur">Le volume doit être un nombre positif<span></div>'
            $("#div-input-volume"+idForm).append(html);
        }

        if(nom == ""){
            errorMsg = "Veuillez definir le nom du produit";
            isValide = false;
            
        }
        if(nom == ""){
            $("#input-nom"+idForm).addClass("erreur_input");
            var html = '<div class="message-erreur"><span class="message_erreur">Le nom doit être défini<span></div>'
            $("#div-input-nom"+idForm).append(html);
        }

        if(isValide && poids <= 0){
            errorMsg = "Le poids du produit est invalide";
            isValide = false;
        }
        if( poids <= 0){
            $("#input-poids"+idForm).addClass("erreur_input");
            var html = '<div class="message-erreur"><span class="message_erreur">Le poids doit être un nombre positif<span></div>'
            $("#div-input-poids"+idForm).append(html);
        }

        if(isValide && prix <= 0){
            errorMsg = "Le prix du produit est invalide";
            isValide = false;
        }
        if( prix <= 0){
            $("#input-prix"+idForm).addClass("erreur_input");
             var html = '<div class="message-erreur"><span class="message_erreur">Le prix doit être un nombre positif<span></div>'
            $("#div-input-prix"+idForm).append(html);
        }

        if(isValide && quantite <= 0){
            /*console.log(quantite);
            console.log(groupement);*/
            errorMsg = "La quantité du produit est invalide";
            isValide = false;
        }
        if( quantite <= 0) {
            $("#input-quantite"+idForm).addClass("erreur_input");
             var html = '<div class="message-erreur"><span class="message_erreur">La quantité doit être un nombre positif<span></div>'
            $("#div-input-quantite"+idForm).append(html);
        }

        if(isValide && (pourcentage<=0 || pourcentage > 100)){
            errorMsg = "Le pourcentage de réduction de prix du produit est invalide";
            isValide = false;
        }
        if( pourcentage<=0 || pourcentage > 100 ){
            $("#input-pourcentage-reduction"+idForm).addClass("erreur_input");
             var html = '<div class="message-erreur"><span class="message_erreur">La réduction doit être un nombre compris entre 0 et 99<span></div>'
            $("#div-input-pourcentage-reduction"+idForm).append(html);
        }

        if(isValide && !$scope.dateIsGood(datePerempOrProd)){
            errorMsg = "La date de péremption ou production est invalide";
            isValide = false;
        }
        if( !$scope.dateIsGood(datePerempOrProd)){
            $("#input-date-peremption-production"+idForm).addClass("erreur_input");
             var html = '<div class="message-erreur"><span class="message_erreur">La date de péremption ou production est invalide<span></div>'
            $("#div-input-date-peremption-production"+idForm).append(html);
        }

        if(isValide && categorie == ""){
            errorMsg = "La catégorie du produit est invalide";
            isValide = false;
        } 
        if(categorie == ""){
            $("#select-categorie"+idForm).select2({ containerCssClass : "erreur_input" });
            var html = '<div class="message-erreur"><span class="message_erreur">Champs obligatoire<span></div>'
            $("#div-select-categorie"+idForm).append(html);
        }

        if(isValide && type == ""){
            errorMsg = "Le type du produit est invalide";
            isValide = false;
        }
         if( type == ""){
            $("#select-type"+idForm).select2({ containerCssClass : "erreur_input" });
            var html = '<div class="message-erreur"><span class="message_erreur">Champs obligatoire<span></div>'
            $("#div-select-type"+idForm).append(html);
        }

        /*if(isValide && infoNutris.length <= 0){
            errorMsg = "Il faut choisir au moins une information nutritionnelle du produit";
            isValide = false;
        }*/
        /*if(haveRaisonSurplus == 1){
            if(isValide && raisonSurplus.length <= 0){
                errorMsg = "Il faut choisir au moins une raison de surplus au produit";
                isValide = false;
            }  
        }*/
            
        /*if(isValide && (pourcentageStrategie<=0 || pourcentageStrategie > 100)){
            errorMsg = "Le pourcentage de réduction d'une stratégie marketing est invalide";
            isValide = false;
        }
        if(isValide && periodeStrategie == ""){
            errorMsg = "Le période du stratégie est invalide";
            isValide = false;
        }*/

        /*
        STRATEFIE MARKETING
        var allPeriodeStrategie = $('.periode-to-test');
        for(var i=0; i<allPeriodeStrategie.length; i++){
            var element = allPeriodeStrategie.get(i);
            var periode = element.value.trim();
            if(isValide && periode == ""){
                errorMsg = "Le période de réduction d'une stratégie est invalide";
                isValide = false;
            }
        }*/

        var allPourcentageStrategiePerWeek = $('.percent-par-sem-to-test');
        var allDivPourcentageStrategiePerWeek = $('.div-percent-par-sem-to-test');
        
        for(var i=0; i<allPourcentageStrategiePerWeek.length; i++){
            var element = allPourcentageStrategiePerWeek.get(i);
            var div_element = allDivPourcentageStrategiePerWeek.get(i);
            
            var percent = $scope.getNumber(element.value.trim());

            if(isValide && element.value.trim() != '' && (percent<=0 || percent >= 100) ){ 
                errorMsg = "Le pourcentage de réduction d'une stratégie du prix du produit est invalide. Cette pourcentage doit être un nombre compris entre 1 à 99";
                isValide = false;
                
            }
            if(element.value.trim() != '' && (percent<=0 || percent >= 100)){
               $(element).addClass("erreur_input"); 
               var html = '<div class="message-erreur"><span class="message_erreur">Cette pourcentage doit être un nombre compris entre 1 à 99<span></div>'
               $(div_element).append(html);
            }

        }

        var allPourcentageStrategie = $('.percent-to-test');
        var allDivPourcentageStrategie = $('.div-percent-to-test');
        for(var i=0; i<allPourcentageStrategie.length; i++){
            var element = allPourcentageStrategie.get(i);
            var div_element = allDivPourcentageStrategie.get(i);
            var percent = $scope.getNumber(element.value.trim());
            if(isValide  && element.value.trim() != '' && (percent<=0 || percent >= 100)){ 
                errorMsg = "Le pourcentage de réduction d'une stratégie du prix du produit est invalide. Cette pourcentage doit être un nombre compris entre 1 à 99";
                isValide = false;
            }
            if(element.value.trim() != '' && (percent<=0 || percent >= 100)){
               $(element).addClass("erreur_input"); 
                var html = '<div class="message-erreur"><span class="message_erreur">Cette pourcentage doit être un nombre compris entre 1 à 99<span></div>'
               $(div_element).append(html);
            }
        }

        if( !$('#input-lieu-ramassage-adresse_entreprise1').prop('checked')){
            var allAdresseInput = $('.adresse-to-test');
            var div_erreur = $('#div-erreur-adresse');
            var teste = true ;
            for(var j=0; j<allAdresseInput.length; j++){
                var element = allAdresseInput.get(j);
                if(element.value.trim() == ''){ 
                    errorMsg = "Une information sur l'adresse de ramassage est invalide";
                    isValide = false;
                    teste = false ;
                    $(element).addClass("erreur_input"); 
                    
                }
            }
            if(!teste){
                var html = '<div class="message-erreur"><span class="message_erreur">Toutes les informations sont obligatoires.<span></div>'
                $(div_erreur).append(html);
            }

            var allLatitude = $('.lat-to-test');
            var allLongitude = $('.long-to-test');

            var allDivLatLon = $('.div-lat-long-to-test');
            for(var i=0; i<allLatitude.length; i++){
                var elementLatitude = allLatitude.get(i);
                var elementLongitude = allLongitude.get(i);

                var divLatLon = allDivLatLon.get(i);

                var valLatitude = allLatitude.get(i).value.trim();
                var valLongitude = allLongitude.get(i).value.trim();

                if($scope.checkCoordValue(valLatitude) == null ){
                    elementLatitude.scrollIntoView();
                    elementLatitude.focus();
                    $(elementLatitude).addClass("erreur_input"); 
                    //toastr.error("Erreur","La latitude doit être une valeur numérique");
                    
                    var html = '<div class="message-erreur"><span class="message_erreur">Les coordonnées devront être des valeur numériques.<span></div>'
                    $(divLatLon).append(html);

                    isValide = false;
                    
                    //return;
                }else if($scope.checkCoordValue(valLongitude) == null){
                    elementLongitude.scrollIntoView();
                    elementLongitude.focus();
                    $(elementLongitude).addClass("erreur_input"); 
                    //toastr.error("Erreur","La longitude doit être une valeur numérique");
                    
                    var html = '<div class="message-erreur"><span class="message_erreur">Les coordonnées devront être des valeur numériques.<span></div>'
                    $(divLatLon).append(html);

                    isValide = false;

                    //return;
                }else if(valLatitude == 0 && valLongitude == 0){
                    elementLatitude.scrollIntoView();
                    elementLatitude.focus();
                    $(elementLatitude).addClass("erreur_input"); 
                    //toastr.error("Erreur","Les coordonnées géographiques renseignées sont incorrectes");

                    var html = '<div class="message-erreur"><span class="message_erreur">Les coordonnées entrées ne sont pas valides.<span></div>'
                    $(divLatLon).append(html);

                    isValide = false;

                    //return;
                }

                $(elementLatitude).val(valLatitude.toString().replace(",","."));
                $(elementLongitude).val(valLongitude.toString().replace(",","."));

            }

        }

         /* Info Periode Recuperation commande */


            var allJourRecuperation = $('.periode-recup-to-test');

            var div_erreur = $('.div-erreur-horaire');

            for(var i=0; i<allJourRecuperation.length; i++){
                var element = allJourRecuperation.get(i);

                var div_element = div_erreur.get(i);

                var jourRecup = element.value.trim();
                if(isValide && jourRecup == ""){
                    element.focus();
                    errorMsg = "Un jour de récupération est invalide";
                    isValide = false;
                }

                if(jourRecup == ""){
                    $(element).select2({ containerCssClass : "erreur_input" });
                    var html = '<div class="message-erreur"><span class="message_erreur">Jour de récupération invalide<span></div>'
                    $(div_element).append(html);
                }

            }
        

            var allHoraireDebut = $('.horaire-debut-to-test');
            var allHoraireFin = $('.horaire-fin-to-test');

            

            for(var i=0; i<allHoraireDebut.length; i++){
                var elementDebut = allHoraireDebut.get(i);
                var elementFin = allHoraireFin.get(i);
                var element_div_erreur = div_erreur.get(i);
                
                var div_element = div_erreur.get(i);
                
                var horaireDeb = elementDebut.value.trim();
                var horaireFin = elementFin.value.trim();

                var heure_deb = elementDebut.value.trim();
                var heure_fin = elementFin.value.trim();

                if(isValide && (horaireDeb == "" || !$scope.checkHoraire(horaireDeb)) || (horaireFin == "" || !$scope.checkHoraire(horaireFin)) ) {
                    isValide = false;
                }
                if(horaireDeb == "" || !$scope.checkHoraire(horaireDeb) ){
                    $(elementDebut).addClass("erreur_input");
                }
                if(horaireFin == "" || !$scope.checkHoraire(horaireFin))
                    $(elementFin).addClass("erreur_input"); 

                if( !$scope.checkHoraire(horaireFin)  || !$scope.checkHoraire(horaireDeb)  ){
                     var html = '<div class="message-erreur"><span class="message_erreur">Horaire de récupération invalide<span></div>'
                    $(element_div_erreur).append(html);
                }
                var etat = $scope.dateCompare(heure_deb, heure_fin);
                if(etat == 1){
                    var html = '<div class="message-erreur"><span class="message_erreur">L\'horaire de debut de récupération doit être inférieur à l\'horaire de fin de récupération<span></div>'
                    $(element_div_erreur).append(html);
                    isValide = false;

                }else if(etat==0 && horaireDeb!=""){
                    var html = '<div class="message-erreur"><span class="message_erreur">L\'horaire de debut de récupération doit être différent de l\'horaire de fin de récupération<span></div>'
                    $(element_div_erreur).append(html);
                    isValide = false;

                }
            }
        

          /* var allHoraireFin = $('.horaire-fin-to-test');
           var allDivHoraireFin = $('.div-horaire-fin-to-test');
            for(var i=0; i<allHoraireFin.length; i++){ 
                var element = allHoraireFin.get(i);
                var horaireFin = element.value.trim();
                if(isValide && (horaireFin == "" || !$scope.checkHoraire(horaireFin))) {
                    //element.focus();
                    errorMsg = "Un horaire de fin de récupération est invalide";
                    isValide = false;
                }
                if(horaireFin == "" || !$scope.checkHoraire(horaireFin))
                    $(element).addClass("erreur_input"); 
            } */
        

        

        if(isValide){
            for(var i=0; i < allHoraireDebut.length; i++){
                var debut = allHoraireDebut.get(i);
                var heure_deb = debut.value.trim();
                var fin = allHoraireFin.get(i);
                var heure_fin = fin.value.trim();

                var etat = $scope.dateCompare(heure_deb, heure_fin);
                if(etat == 1){
                    //debut.focus();
                    $(debut).addClass("erreur_input");
                    errorMsg = "L'horaire de debut de récupération doit être inférieur à l'horaire de fin de récupération";
                    isValide = false;
                }else if(etat == 0){
                    //debut.focus();
                    $(debut).addClass("erreur_input");
                    errorMsg = "L'horaire de debut de récupération doit être différent de l'horaire de fin de récupération";
                    isValide = false;
                }
            }
        }
        
        /* ---- Fin periode recuperation ---- */
       
        /*if(isValide && $scope.formList[formIndex].images.length <= 0){
            errorMsg = "Il faut choisir au moins une photo pour le produit";
            isValide = false;
        }*/

        return {isValide: isValide, errorMsg: errorMsg};
    };

    $scope.checkHoraire = function (horaire) {
        var regexHoraire = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
        //console.log( regexHoraire.test(horaire) );
        return regexHoraire.test(horaire);
    }

    $scope.dateCompare = function(time1,time2) {
        var t1 = new Date();
        var parts = time1.split(":");
        t1.setHours(parts[0],parts[1],"00",0);
        var t2 = new Date();
        parts = time2.split(":");
        t2.setHours(parts[0],parts[1],"00",0);

        // returns 1 if greater, -1 if less and 0 if the same
        if (t1.getTime()>t2.getTime()) return 1;
        if (t1.getTime()<t2.getTime()) return -1;
        return 0;
    }


    $scope.checkFormulaire = function(lien){
        $scope.showLoading('Verification des données');
        $scope.removeMessageErreur();
        var dataIsGood = true;
        for(var i=0; i<$scope.formList.length; i++){
            var verification = $scope.formIsGood(i, $scope.formList[i].id);
            dataIsGood = verification.isValide;
            if(!dataIsGood){
                $scope.hideLoading();
                //$scope.showAlert('Erreur', verification.errorMsg+' dans le formulaire ');
                toastr.error("Une ou plusieurs données ne sont pas valides", "Erreur");
                break;
            }
        }
        if(dataIsGood){
            $('#modal-loading-title').html('Enregistrement');
            if($scope.pushDate == null){
                $scope.pushDate = $scope.getDatetimeString();
            }
            $scope.pushForm(lien);
        }
    };

    $scope.pushForm = function(lien){
        var formIndex = $scope.formList.length - 1;
        var idForm = $scope.formList[formIndex].id;

        var statutVenteModif = $('#statut_vente_modif').val().trim();
        var id_produit = $("#id_produit").val().trim();
        var id_donation = $("#id_donation").val().trim();
        var id_etat_donation = $("#id_etat_donation").val().trim();

        var nom = $("#input-nom"+idForm).val().trim();
        var groupement = $("#input-groupement-unite"+idForm).prop('checked') ? 1 : ($("#input-groupement-portion"+idForm).prop('checked') ? 3 : 2);
        var poids = $scope.getNumber($("#input-poids"+idForm).val().trim().replace(/\s/g,""));
        var volume = $scope.getNumber($("#input-volume"+idForm).val().trim().replace(/\s/g,""));
        var prix = $scope.getNumber($("#input-prix"+idForm).val().trim().replace(/\s/g,""));
        var quantite = 0;
        if(groupement == 2){
            quantite = 1;
            $("#input-quantite"+idForm).val(1);
            //var quantiteMinimum = 1;
        }else{
            quantite = parseInt($scope.getNumber($("#input-quantite"+idForm).val().trim().replace(/\s/g,"")));
            //var quantiteMinimum = parseInt($scope.getNumber($("#input-quantite-minimum"+idForm).val().trim()));
        }
        var quantiteMinimum = 1;
        if( $("#input-quantite-minimum"+idForm).val() != ''){
            quantiteMinimum = parseInt($scope.getNumber($("#input-quantite-minimum"+idForm).val().trim().replace(/\s/g,"")));
        }       

        var pourcentage = $scope.getNumber($("#input-pourcentage-reduction"+idForm).val().trim());
        var typeDate = $("#input-choix-date-peremption"+idForm).prop('checked') ? 1 : 2;
        var datePerempOrProd = $('#input-date-peremption-production'+idForm).val();

         // STRATEGIE PAR SEMAINE
        var reduc_4_semaine = $scope.getNumber($("#input-pourcentage_reduction_strategie-4-sem"+idForm).val().trim().replace(/\s/g,""));
        var reduc_3_semaine = $scope.getNumber($("#input-pourcentage_reduction_strategie-3-sem"+idForm).val().trim().replace(/\s/g,""));
        var reduc_2_semaine = $scope.getNumber($("#input-pourcentage_reduction_strategie-2-sem"+idForm).val().trim().replace(/\s/g,""));
        var reduc_1_semaine = $scope.getNumber($("#input-pourcentage_reduction_strategie-1-sem"+idForm).val().trim().replace(/\s/g,""));
        // FIN STRATEGIE PAR SEMAINE

        var categorie = $("#select-categorie"+idForm).val();
        var type = $("#select-type"+idForm).val();
        var infoNutris = $("#select-info-nutris"+idForm).select2('val').toString().split(",");
        //var haveRaisonSurplus = $("#input-surplus-alim-oui"+idForm).prop('checked') ? 1 : 0;
        var raisonSurplus = $("#select-raison-surplus"+idForm).select2('val').toString().split(",");

        //console.log(raisonSurplus);

        var refrigere = $("#input-refrigere"+idForm).is(":checked") ? 1 : 0; // true na false
        var congele = $("#input-congele"+idForm).is(":checked") ? 1 : 0;
        var consoDirect = $("#input-consoDirect"+idForm).is(":checked") ? 1 : 0;

        var description = $("#textarea-description"+idForm).val().trim();
        var adresseEntreprise = $('#input-lieu-ramassage-adresse_entreprise'+idForm).prop('checked') ? 1 : 2;
        var adresse = $("#autreAdresse"+idForm).val().trim();
        var codePostal = $("#codePostalAutreAdresse"+idForm).val().trim();
        var ville = $("#villeAutreAdresse"+idForm).val().trim();
        var pays = $("#pays_autre_adresse"+idForm).val();
        var latitude = $("#infoLatitude"+idForm).val();
        var longitude = $("#infoLongitude"+idForm).val();

        var formData = new FormData();

        formData.append('statutVenteModif', statutVenteModif);
        formData.append('idProduit', id_produit); 
        formData.append('idDonation', id_donation);  
        formData.append('idEtatDonation', id_etat_donation);

        formData.append('pushDate', $scope.pushDate);
        formData.append('nom', nom);
        formData.append('groupement', groupement);
        formData.append('poids', poids);
        formData.append('volume', volume);
        formData.append('prix', prix);
        formData.append('quantite', quantite);
        formData.append('quantiteMinimum', quantiteMinimum);
        formData.append('pourcentage',pourcentage);
        formData.append('typeDate', typeDate);
        formData.append('date', datePerempOrProd);
         // STRATEGIE PAR SEMAINE
        formData.append('reduction4Semaine', reduc_4_semaine);
        formData.append('reduction3Semaine', reduc_3_semaine);
        formData.append('reduction2Semaine', reduc_2_semaine);
        formData.append('reduction1Semaine', reduc_1_semaine);
        // FIN STRATEGIE PAR SEMAINE
        formData.append('refrigere', refrigere);
        formData.append('congele', congele);
        formData.append('consoDirect', consoDirect);
        formData.append('description', description);   
        formData.append('categorie', categorie);
        formData.append('type', type);
        formData.append('NbInfoNutris', infoNutris.length);
        for(var i=0; i<infoNutris.length; i++){
            formData.append('infoNutris'+i, infoNutris[i]);
        }

        //formData.append('haveRaisonSurplus', haveRaisonSurplus);
        
        if( raisonSurplus[0] == ""){
            formData.append('NbRaisonSurplus', 0);
            formData.append('raisonSurplus0', -1);
        }else{
            formData.append('NbRaisonSurplus', raisonSurplus.length);
            for(var i=0; i<raisonSurplus.length; i++){
                formData.append('raisonSurplus'+i, raisonSurplus[i]);
            } 
        }
        
       /*   if( haveRaisonSurplus && raisonSurplus.length != 0){
                for(var i=0; i<raisonSurplus.length; i++){
                formData.append('raisonSurplus'+i, raisonSurplus[i]);
            } 
        }*/

        if($("#input-lieu-ramassage-adresse_entreprise"+idForm).is(":checked")){
            var id_adresseEntreprise = $("#id_adresseCourantHidden"+idForm).val().trim();
            adresse = $("#adresseCourantHidden"+idForm).val().trim();
            codePostal = $("#codePostalCourantHidden"+idForm).val().trim();
            ville = $("#villeCourantHidden"+idForm).val().trim();
            pays = $("#paysCourantHidden"+idForm).val().trim();
            latitude = $("#latitudeCourantHidden"+idForm).val().trim();
            longitude = $("#longitudeCourantHidden"+idForm).val().trim();
        }else{
            var id_adresseEntreprise = $("#id_lieu_rmsg"+idForm).val().trim();
            adresse = $("#autreAdresse"+idForm).val().trim();
            codePostal = $("#codePostalAutreAdresse"+idForm).val().trim();
            ville = $("#villeAutreAdresse"+idForm).val().trim();
            pays = $("#pays_autre_adresse"+idForm).val().trim();
            latitude = $("#infoLatitude"+idForm).val().trim();
            longitude = $("#infoLongitude"+idForm).val().trim();
        }
        formData.append('id_adresseEntreprise', id_adresseEntreprise);
        formData.append('isAdresseEntreprise', adresseEntreprise);
        formData.append('adresseRmsg', adresse);
        formData.append('codePostalRmsg', codePostal);
        formData.append('villeRmsg', ville);
        formData.append('paysRmsg', pays);
        formData.append('latitudeRmsg', latitude);
        formData.append('longitudeRmsg', longitude);

        var allPeriodeStrategie = $('.periode-to-test');
        formData.append('NbPeriodeReduction', allPeriodeStrategie.length);
        for(var i=0; i<allPeriodeStrategie.length; i++){
            var element = allPeriodeStrategie.get(i);
            var periode = element.value.trim();
            formData.append('periodeReduction'+i, periode);
        }

        var allPourcentageStrategie = $('.percent-to-test');
        formData.append('NbPourcentageReduction', allPourcentageStrategie.length);
        for(var i=0; i<allPourcentageStrategie.length; i++){
            var element = allPourcentageStrategie.get(i);
            var percent = element.value.trim();
            formData.append('pourcentageReduction'+i, percent);
        }

         /* Info Periode Recuperation commande */
        var allJourRecuperation = $('.periode-recup-to-test');
        formData.append('NbPeriodeRecuperation', allJourRecuperation.length);
        for(var i=0; i<allJourRecuperation.length; i++){
            var element = allJourRecuperation.get(i);
            var jourRecup = element.value.trim();
            formData.append('periodeRecuperation'+i, jourRecup);
        }

        var allHoraireDebut = $('.horaire-debut-to-test');
        formData.append('NbHoraireDebut', allHoraireDebut.length);
        for(var i=0; i<allHoraireDebut.length; i++){
            var element = allHoraireDebut.get(i);
            var horaireDeb = element.value.trim();
            formData.append('horaireDebut'+i, horaireDeb);
        }

        var allHoraireFin = $('.horaire-fin-to-test');
        formData.append('NbHoraireFin', allHoraireFin.length);
        for(var i=0; i<allHoraireFin.length; i++){
            var element = allHoraireFin.get(i);
            var horaireFin = element.value.trim();
            formData.append('horaireFin'+i, horaireFin);
        }
        /* ---- Fin periode recuperation ---- */

        var allPhotoInBase = $('.photoInBase');
        formData.append('NbPhotoAlreadyInBase', allPhotoInBase.length);
        for(var i=0; i<allPhotoInBase.length; i++){
            var element = allPhotoInBase.get(i);
            var idPhoto = element.value.trim();
            formData.append('pictures'+i, idPhoto);
        }

        formData.append('NbImages', $scope.formList[formIndex].images.length);
        for(var i=0; i<$scope.formList[formIndex].images.length; i++){
            if($scope.formList[formIndex].images[i].fromServer){
                formData.append('fromServer'+i, 1);
                formData.append('idPhoto'+i, $scope.formList[formIndex].images[i].idPhoto);
            }
            else{
                formData.append('fromServer'+i, 0);
                formData.append('image'+i+'ext', $scope.formList[formIndex].images[i].extension);
                formData.append('image'+i+'data', $scope.formList[formIndex].images[i].dataURL);
            }
        }

        formData.append('isDraft', 0);


        $http({
            method: 'POST',
            url: lien,
            data: formData,
            headers: {'Content-Type': undefined},
            transformRequest: angular.identity
        })
        .then(
            response=>{
                if(response.data.success){
                    if(formIndex == 0){

                        if(statutVenteModif == 0){
                            // Add a new document in collection "products"
                            /*db.collection("products").doc().set({
                                owner: response.data.saveProduct.owner,
                                topic: response.data.saveProduct.topic,
                                topic_id: response.data.saveProduct.topic_id
                            })
                            .then(function() {
                                console.log("Document successfully written!");
                            })
                            .catch(function(error) {
                                console.error("Error writing document: ", error);
                            });*/
                        }

                        $scope.hideLoading();
                        $scope.showAlert('',response.data.message);

                        $("#input-nom1").val('');
                        $("#input-poids1").val('');
                        $("#input-volume1").val('');
                        $("#input-prix1").val('');
                        $("#input-quantite1").val('');
                        $("#input-quantite-minimum1").val('');
                        $("#input-pourcentage-reduction1").val('');
                        $("#input-date-peremption-production1").val('');
                        $("#select-strategie-prix1").select2('val', '');
                        $("#input-pourcentage_reduction_strategie1").val('');
                        $("#otherStrategie").html('');
                        $("#select-categorie1").select2('val', '');
                        $("#select-type1").select2('val', '');
                        $("#select-info-nutris1").select2('val', '');
                        $("#select-raison-surplus1").select2('val', '');
                        $("#input-congele1, #input-refrigere1, #input-consoDirect1").iCheck('uncheck');
                        $("#textarea-description1").val('');
                        $("#div-file-preview1").html('');
                        $("#autre_adresse1").val('');
                        $("#code_postal_autre_adresse1").val('');
                        $("#ville_autre_adresse1").val('');
                        $("#pays_autre_adresse1").select2('val', '');
                        $("#infoLatitude1").val('');
                        $("#infoLongitude1").val('');
                        $("#select-periode-recuperation1").select2('val', '');
                        $("#input-horaire-debut1").val('');
                        $("#input-horaire-fin1").val('');
                        $("#otherPeriodeRecuperation").html('');
                        $("#div-file-product1").html('');
                        $scope.formList[formIndex].images = [];
                        $scope.pushDate = null;

                        var etat_donation = $scope.getNumber($("#id_etat_donation").val().trim().replace(/\s/g,""));
                        
                        if(statutVenteModif == 0){
                            $scope.showAlertVente('Enregistrement','Félicitations! Vos produits sont en vente! Vous pouvez voir ce produit sur la liste');
                        }else if(statutVenteModif == 1){

                            if(etat_donation == 2){
                                $scope.showAlertVente('Modification','Félicitations! la modification de cette vente est bien enregistrée');
                            }else{
                                $scope.showAlertVente('Enregistrement','Félicitations! Vos produits sont en vente! Vous pouvez voir ce produit sur la liste');
                            }
                        }

                        
                        
                    }
                    else{
                        $scope.formList.splice(formIndex, 1);
                        $('#card'+idForm).remove();
                        $scope.pushForm(lien);
                    }
                }
                else{
                    $scope.hideLoading();
                    $scope.showAlert('',response.data.message + ' dans le formulaire ');
                    $scope.pushDate = null;
                }
            },
            error=>{
                $scope.hideLoading();
                $scope.showAlert('Erreur','Une erreur est survenue');
                $scope.pushDate = null;
            }
        );
    };

    $scope.addStrategieMarketing = function(){
        var statutVenteModif = $('#statut_vente_modif').val();
        /*if(parseInt(statutVenteModif) == 1){
            var nb_strategie = $("#nb_strategie_marketing").val();
            $scope.numStrategie = parseInt(nb_strategie)+1;
            //VenteModif = 0;
            console.log($scope.numStrategie);
        }*/
        //var limite_strategie = $("#nb_periode_reduction").val();
         //$('.add-periode-strategie').html()+
        var stop_create_strategie = parseInt($scope.limite_strategie) + 1;

        var html = '<div class="form-group row strategie'+ $scope.numStrategie +'">'+
                    '<div class="col-5 ">'+
                        '<select  id="select-strategie-prix10'+ $scope.numStrategie +'" name="periodeStrategie'+ $scope.numStrategie +'" data-placeholder="Période du stratégie" class="form-control select2 periode-to-test" style="width: 100%;">'+
                            $('#selectStrategieMarketing').html()+
                        '</select>'+
                        '<a href="#" ng-click="createPeriodeStrategie(10'+ $scope.numStrategie +')" data-toggle="modal" data-target="#modal-new-periode-strategie" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>'+     
                    '</div>'+
                    '<div class="col-5">'+
                        '<div class="input-group mb-3">'+
                            '<input  id="pourcentageStrategie" name="pourcentage_reduction_strategie'+ $scope.numStrategie +'" type="text" class="form-control percent-to-test" placeholder="Réduction" list="propositionReduction" value="" required="">'+
                            '<div class="input-group-append">'+
                                '<span class="input-group-text">%</span>'+
                            '</div>'+
                            '<datalist id="propositionReduction">'+
                                '<option>20</option>'+
                                '<option>25</option>'+
                                '<option>30</option>'+
                                '<option>50</option>'+
                                '<option>60</option>'+
                                '<option>70</option>'+
                                '<option>80</option>'+
                                '<option>90</option>'+
                            '</datalist>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group col-2">'+
                        '<a class="btn btn-default btn-block" ng-click="removeStrategie( \'.strategie'+ $scope.numStrategie +'\')" title="Supprimer cette stratégie marketing"><i class="fa fa-minus"></i></a>'+
                    '</div>'+
                '</div>';
        if( $scope.numStrategie <= $scope.limite_strategie ){
            $("#otherStrategie").append($compile(html)($scope)); 
            $('.select2').select2();
            $scope.numStrategie+=1;
        }
        
       if( $scope.numStrategie == stop_create_strategie ){
            $('.addStrategie').css("pointer-events", "none");
            $('.addStrategie').css("opacity", "0.5");
       } 
    };

    $scope.removeStrategie = function(selector){
        $(selector).remove();
        $('.addStrategie').css("pointer-events", "" );
         $('.addStrategie').css("opacity", "1");
        $scope.numStrategie-=1;
    
    };

    $scope.createPeriodeStrategie = function(identifiant){
    
        $("#identifiantMere").val(identifiant);
        //console.log($("#identifiantMere").val());
    };

    $scope.saveDraft = function(lien){

        var formIndex = $scope.formList.length - 1;
        var idForm = $scope.formList[formIndex].id;

        if($scope.pushDate == null){
            $scope.pushDate = $scope.getDatetimeString();
        }

        var statutVenteModif = $('#statut_vente_modif').val().trim();
        var id_produit = $("#id_produit").val().trim();
        var id_donation = $("#id_donation").val().trim();

        var nom = $("#input-nom"+idForm).val().trim();
        var groupement = $("#input-groupement-unite"+idForm).prop('checked') ? 1 : ($("#input-groupement-portion"+idForm).prop('checked') ? 3 : 2);
        var poids = $scope.getNumber($("#input-poids"+idForm).val().trim().replace(/\s/g,""));
        var volume = $scope.getNumber($("#input-volume"+idForm).val().trim().replace(/\s/g,""));
        var prix = $scope.getNumber($("#input-prix"+idForm).val().trim().replace(/\s/g,""));
        var quantite = 0;
        if(groupement == 2){
            quantite = 1;
            //var quantiteMinimum = 1;
        }else{
            quantite = parseInt($scope.getNumber($("#input-quantite"+idForm).val().trim().replace(/\s/g,"")));
            //var quantiteMinimum = parseInt($scope.getNumber($("#input-quantite-minimum"+idForm).val().trim()));
        }

        var quantiteMinimum = 1;
        if( $("#input-quantite-minimum"+idForm).val() != ''){
            quantiteMinimum = parseInt($scope.getNumber($("#input-quantite-minimum"+idForm).val().trim().replace(/\s/g,"")));
        }   

        var pourcentage = $scope.getNumber($("#input-pourcentage-reduction"+idForm).val().trim().replace(/\s/g,""));
        var typeDate = $("#input-choix-date-peremption"+idForm).prop('checked') ? 1 : 2;
        var datePerempOrProd = $('#input-date-peremption-production'+idForm).val();

        // STRATEGIE PAR SEMAINE
        var reduc_4_semaine = $scope.getNumber($("#input-pourcentage_reduction_strategie-4-sem"+idForm).val().trim().replace(/\s/g,""));
        var reduc_3_semaine = $scope.getNumber($("#input-pourcentage_reduction_strategie-3-sem"+idForm).val().trim().replace(/\s/g,""));
        var reduc_2_semaine = $scope.getNumber($("#input-pourcentage_reduction_strategie-2-sem"+idForm).val().trim().replace(/\s/g,""));
        var reduc_1_semaine = $scope.getNumber($("#input-pourcentage_reduction_strategie-1-sem"+idForm).val().trim().replace(/\s/g,""));
        // FIN STRATEGIE PAR SEMAINE

        var categorie = $("#select-categorie"+idForm).val();
        var type = $("#select-type"+idForm).val();
        var infoNutris = $("#select-info-nutris"+idForm).select2('val').toString().split(",");
        //var haveRaisonSurplus = $("#input-surplus-alim-oui"+idForm).prop('checked') ? 1 : 0;
        var raisonSurplus = $("#select-raison-surplus"+idForm).select2('val').toString().split(",");
        var refrigere = $("#input-refrigere"+idForm).is(":checked") ? 1 : 0; // true na false
        var congele = $("#input-congele"+idForm).is(":checked") ? 1 : 0;
        var consoDirect = $("#input-consoDirect"+idForm).is(":checked") ? 1 : 0;

        var description = $("#textarea-description"+idForm).val().trim();
        var adresseEntreprise = $('#input-lieu-ramassage-adresse_entreprise'+idForm).prop('checked') ? 1 : 2;
        var adresse = $("#autreAdresse"+idForm).val().trim();
        var codePostal = $("#codePostalAutreAdresse"+idForm).val().trim();
        var ville = $("#villeAutreAdresse"+idForm).val().trim();
        var pays = $("#pays_autre_adresse"+idForm).val().trim();
        var latitude = $("#infoLatitude"+idForm).val();
        var longitude = $("#infoLongitude"+idForm).val();

        var formData = new FormData();

        formData.append('statutVenteModif', statutVenteModif);
        formData.append('idProduit', id_produit); 
        formData.append('idDonation', id_donation); 
        formData.append('pushDate', $scope.pushDate);
        formData.append('nom', nom);
        formData.append('groupement', groupement);
        formData.append('poids', poids);
        formData.append('volume', volume);
        formData.append('prix', prix);
        formData.append('quantite', quantite);
        formData.append('quantiteMinimum', quantiteMinimum);
        formData.append('pourcentage',pourcentage);
        formData.append('typeDate', typeDate);
        formData.append('date', datePerempOrProd);
        // STRATEGIE PAR SEMAINE
        formData.append('reduction4Semaine', reduc_4_semaine);
        formData.append('reduction3Semaine', reduc_3_semaine);
        formData.append('reduction2Semaine', reduc_2_semaine);
        formData.append('reduction1Semaine', reduc_1_semaine);
        // FIN STRATEGIE PAR SEMAINE
        formData.append('refrigere', refrigere);
        formData.append('congele', congele);
        formData.append('consoDirect', consoDirect);
        formData.append('description', description);   
        formData.append('categorie', categorie);
        formData.append('type', type);
        formData.append('NbInfoNutris', infoNutris.length);
        for(var i=0; i<infoNutris.length; i++){
            formData.append('infoNutris'+i, infoNutris[i]);
        }

        //formData.append('haveRaisonSurplus', haveRaisonSurplus);
        
        if( raisonSurplus[0] == ""){
            formData.append('NbRaisonSurplus', 0);
            formData.append('raisonSurplus0', -1);
        }else{
            formData.append('NbRaisonSurplus', raisonSurplus.length);
            for(var i=0; i<raisonSurplus.length; i++){
                formData.append('raisonSurplus'+i, raisonSurplus[i]);
            } 
        }
        
       /* if( haveRaisonSurplus && raisonSurplus.length != 0){
            for(var i=0; i<raisonSurplus.length; i++){
                formData.append('raisonSurplus'+i, raisonSurplus[i]);
            } 
        }*/

        if($("#input-lieu-ramassage-adresse_entreprise"+idForm).is(":checked")){
            var id_adresseEntreprise = $("#id_adresseCourantHidden"+idForm).val().trim();
            adresse = $("#adresseCourantHidden"+idForm).val().trim();
            codePostal = $("#codePostalCourantHidden"+idForm).val().trim();
            ville = $("#villeCourantHidden"+idForm).val().trim();
            pays = $("#paysCourantHidden"+idForm).val().trim();
            latitude = $("#latitudeCourantHidden"+idForm).val().trim();
            longitude = $("#longitudeCourantHidden"+idForm).val().trim();
        }else{
            var id_adresseEntreprise = $("#id_lieu_rmsg"+idForm).val().trim();
            adresse = $("#autreAdresse"+idForm).val().trim();
            codePostal = $("#codePostalAutreAdresse"+idForm).val().trim();
            ville = $("#villeAutreAdresse"+idForm).val().trim();
            pays = $("#pays_autre_adresse"+idForm).val().trim();
            latitude = $("#infoLatitude"+idForm).val().trim();
            longitude = $("#infoLongitude"+idForm).val().trim();
        }
        formData.append('id_adresseEntreprise', id_adresseEntreprise);
        formData.append('isAdresseEntreprise', adresseEntreprise);
        formData.append('adresseRmsg', adresse);
        formData.append('codePostalRmsg', codePostal);
        formData.append('villeRmsg', ville);
        formData.append('paysRmsg', pays);
        formData.append('latitudeRmsg', latitude);
        formData.append('longitudeRmsg', longitude);

        var allPeriodeStrategie = $('.periode-to-test');
        formData.append('NbPeriodeReduction', allPeriodeStrategie.length);
        for(var i=0; i<allPeriodeStrategie.length; i++){
            var element = allPeriodeStrategie.get(i);
            var periode = element.value.trim();
            formData.append('periodeReduction'+i, periode);
        }

        var allPourcentageStrategie = $('.percent-to-test');
        formData.append('NbPourcentageReduction', allPourcentageStrategie.length);
        for(var i=0; i<allPourcentageStrategie.length; i++){
            var element = allPourcentageStrategie.get(i);
            var percent = element.value.trim();
            formData.append('pourcentageReduction'+i, percent);
        }

        /* Info Periode Recuperation commande */
        var allJourRecuperation = $('.periode-recup-to-test');
        formData.append('NbPeriodeRecuperation', allJourRecuperation.length);
        for(var i=0; i<allJourRecuperation.length; i++){
            var element = allJourRecuperation.get(i);
            var jourRecup = element.value.trim();
            formData.append('periodeRecuperation'+i, jourRecup);
        }

        var allHoraireDebut = $('.horaire-debut-to-test');
        formData.append('NbHoraireDebut', allHoraireDebut.length);
        for(var i=0; i<allHoraireDebut.length; i++){
            var element = allHoraireDebut.get(i);
            var horaireDeb = element.value.trim();
            formData.append('horaireDebut'+i, horaireDeb);
        }

        var allHoraireFin = $('.horaire-fin-to-test');
        formData.append('NbHoraireFin', allHoraireFin.length);
        for(var i=0; i<allHoraireFin.length; i++){
            var element = allHoraireFin.get(i);
            var horaireFin = element.value.trim();
            formData.append('horaireFin'+i, horaireFin);
        }
        /* ---- Fin periode recuperation ---- */


        var allPhotoInBase = $('.photoInBase');
        formData.append('NbPhotoAlreadyInBase', allPhotoInBase.length);
        for(var i=0; i<allPhotoInBase.length; i++){
            var element = allPhotoInBase.get(i);
            var idPhoto = element.value.trim();
            formData.append('pictures'+i, idPhoto);
        }
       
        formData.append('NbImages', $scope.formList[formIndex].images.length);

        for(var i=0; i<$scope.formList[formIndex].images.length; i++){
            if($scope.formList[formIndex].images[i].fromServer){
                formData.append('fromServer'+i, 1);
                formData.append('idPhoto'+i, $scope.formList[formIndex].images[i].idPhoto);
            }
            else{
                formData.append('fromServer'+i, 0);
                formData.append('image'+i+'ext', $scope.formList[formIndex].images[i].extension);
                formData.append('image'+i+'data', $scope.formList[formIndex].images[i].dataURL);
            }
        }

        formData.append('isDraft', 1);


        $http({
            method: 'POST',
            url: lien,
            data: formData,
            headers: {'Content-Type': undefined},
            transformRequest: angular.identity
        })
        .then(
            response=>{
                if(response.data.success){
                    if(formIndex == 0){
                        $scope.hideLoading();
                        $scope.showAlert('',response.data.message);
                        $scope.pushDate = null;
                        
                        
                        $scope.showAlert('Notification','Informations bien enregistrées comme brouillon !');
                        
                    }
                    else{
                        $scope.formList.splice(formIndex, 1);
                        $('#card'+idForm).remove();
                        $scope.saveDraft(lien);
                    }
                }
                else{
                    $scope.hideLoading();
                    $scope.showAlert('',response.data.message + ' dans le formulaire ');
                    $scope.pushDate = null;
                }
            },
            error=>{
                $scope.hideLoading();
                $scope.showAlert('Erreur','Une erreur est survenue');
                $scope.pushDate = null;
            }
        );
    };

    

    $scope.resetAllField = function(){
        $("#input-nom1").val('');
        $("#input-poids1").val('');
        $("#input-volume1").val('');
        $("#input-prix1").val('');
        $("#input-quantite1").val('');
        $("#input-quantite-minimum1").val('');
        $("#input-pourcentage-reduction1").val('');
        $("#input-date-peremption-production1").val('');
        $("#input-pourcentage_reduction_strategie-4-sem1"+idForm).val('');
        $("#input-pourcentage_reduction_strategie-3-sem1"+idForm).val('');
        $("#input-pourcentage_reduction_strategie-2-sem1"+idForm).val('');
        $("#input-pourcentage_reduction_strategie-1-sem1"+idForm).val('');
        $("#select-strategie-prix1").select2('val', '');
        $("#input-pourcentage_reduction_strategie1").val('');
        $("#otherStrategie").html('');
        $("#select-categorie1").select2('val', '');
        $("#select-type1").select2('val', '');
        $("#select-info-nutris1").select2('val', '');
        $("#select-raison-surplus1").select2('val', '');
        $("#input-congele1, #input-refrigere1, #input-consoDirect1").iCheck('uncheck');
        $("#textarea-description1").val('');
        $("#div-file-preview1").html('');
        $("#autre_adresse1").val('');
        $("#code_postal_autre_adresse1").val('');
        $("#ville_autre_adresse1").val('');
        $("#pays_autre_adresse1").select2('val', '');
        $("#infoLatitude1").val('');
        $("#infoLongitude1").val('');
        $("#select-periode-recuperation1").select2('val','');
        $("#input-horaire-debut1").val('');
        $("#input-horaire-fin1").val('');
        $("#otherPeriodeRecuperation").html('');

    };
    
    $scope.removeMessageErreur = function(){
        var div_erreur = $('.message-erreur');
        for(var i = 0 ; i < div_erreur.length ; i++){
            $( div_erreur.get(i) ).remove();
        }
    }

    $scope.changeAdresse = function(){
        $scope.adresseDefault = !$scope.adresseDefault;
    };

    $scope.makePrixInAppli = function(selector){
       
        var prixOriginal = $("#input-prix1").val().trim().replace(/\s/g,"");
        var reduction = 0;
        if( $("#input-pourcentage-reduction1").val().trim().replace(/\s/g,"") != ""){
            reduction = $scope.getNumber( $("#input-pourcentage-reduction1").val().trim().replace(/\s/g,"") );
        }else{
            reduction = 0;
        }

        if(prixOriginal != ""){
            if(reduction == ""){
                $('#input-prix-sur-appli1').val(prixOriginal);
            }else{
                var prixAppli = prixOriginal - ((prixOriginal*reduction)/100);
                if(prixAppli>0){

                    $('#input-prix-sur-appli1').val( prixAppli );
                    $scope.formatNumberToMileSep('#input-prix-sur-appli1');

                 }else{
                    $('#input-prix-sur-appli1').val("0");
                 }
               
            }
        }else{
            $('#input-prix-sur-appli1').val("");
        }

        $scope.formatNumberToMileSep(selector);
        
    };

    $scope.addPeriodeRecuperation = function(){
        //var statutVenteModif = $('#statut_vente_modif').val();
        //console.log("statut vente : " + statutVenteModif);
        //console.log("Num periode recuperation : " + $scope.numPeriodeRecuperation);   
        //console.log("nb_periode_recuperation : "+ $("#nb_periode_recuperation").val());
      
        var html = '<div class="form-group row recuperation0'+ $scope.numPeriodeRecuperation +'">'+
                        '<div class="col-4">'+
                            
                            '<select name="selectPeriodeRecuperation0'+ $scope.numPeriodeRecuperation +'" data-placeholder="Période de récupération" class="form-control select2 select-periode-recuperation1 periode-recup-to-test" style="width: 100%;">'+      
                                $("#selectPeriodeRecuperation").html()+
                            '</select>'+

                        '</div>'+
                        '<div class="col-3">'+
                            '<div class="input-group mb-3">'+
                                '<input name="horaire-debut0'+ $scope.numPeriodeRecuperation +'" type="text" class="form-control horaire-debut-to-test" placeholder="Début" required="" >'+
                                '<div class="input-group-append">'+
                                    '<span class="input-group-text"><i class="fa fa-clock-o"></i></span>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="col-3">'+
                            '<div class="input-group mb-3">'+
                                '<input name="horaire-fin0'+ $scope.numPeriodeRecuperation +'" type="text" class="form-control horaire-fin-to-test" placeholder="Fin" required="" >'+
                                '<div class="input-group-append">'+
                                    '<span class="input-group-text"><i class="fa fa-clock-o"></i></span>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                        '<div class="form-group col-2">'+
                            '<a class="btn btn-default btn-block" ng-click="removePeriodeRecuperation( \'.recuperation0'+ $scope.numPeriodeRecuperation +'\')" title="Supprimer période de récupération"><i class="fa fa-minus"></i></a>'+
                        '</div>'+
                    '</div>'+
                    '<div class="div-erreur-horaire"></div> ';
                

        $("#otherPeriodeRecuperation").append($compile(html)($scope)); 
         $('.select2').select2();

        $('.horaire-debut-to-test').timepicker({ 
            'timeFormat': 'H:i',
            'step': 15,
            'scrollDefault': 'now'
           
        });
        $('.horaire-fin-to-test').timepicker({ 
            'timeFormat': 'H:i',
            'step': 15,
            'scrollDefault': 'now'
           
        });

       
        $scope.numPeriodeRecuperation +=1;
       
    };

    $scope.removePeriodeRecuperation = function(selector){
        $(selector).remove();
        $scope.numPeriodeRecuperation-=1;
    };  


    $scope.validateForm = function(lien){
        $('#btn-validate').hide();
        $('#lds-roller').show();

        var nom = $("#input-nom").val().trim();
        var poids = $scope.getNumber($("#input-poids").val().trim().replace(/\s/g,""));
        var volume = $scope.getNumber($("#input-volume").val().trim().replace(/\s/g,""));
        var prix = $scope.getNumber($("#input-prix").val().trim().replace(/\s/g,""));
        var quantite = parseInt($scope.getNumber($("#input-quantite").val().trim().replace(/\s/g,"")));
        var categorie = $("#select-categorie").val();
        var type = $("#select-type").val();
        var infoNutris = $("#select-info-nutris").select2('val').toString().split(",");
        var refrigere = $("#input-refrigere").is(":checked") ? 1 : 0; // true na false
        var congele = $("#input-congele").is(":checked") ? 1 : 0;
        var consoDirect =$("#input-consoDirect").is(":checked") ? 1 : 0;
        var description = $("#textarea-description").val().trim();

        var isValide = true;
        var errorMsg = "";
        if(nom == ""){
            errorMsg = "Veuillez definir le nom du produit";
            isValide = false;
        }
        if(isValide && poids <= 0){
            errorMsg = "Le poids du produit est invalide";
            isValide = false;
        }
        if(isValide && prix <= 0){
            errorMsg = "Le prix du produit est invalide";
            isValide = false;
        }
        if(isValide && quantite <= 0){
            errorMsg = "La quantité du produit est invalide";
            isValide = false;
        }
        /*if(isValide && volume <= 0){
            errorMsg = "Le volume du produit est invalide";
            isValide = false;
        }*/
        if(isValide && categorie == ""){
            errorMsg = "La catégorie du produit est invalide";
            isValide = false;
        }
        if(isValide && type == ""){
            errorMsg = "Le type du produit est invalide";
            isValide = false;
        }
        if(isValide && !$scope.dateIsGood(datePeremption)){
            errorMsg = "La date de péremption est invalide";
            isValide = false;
        }
        if(isValide && infoNutris.length <= 0){
            errorMsg = "Il faut choisir au moins une information nutritionnelle du produit";
            isValide = false;
        }
        /*if(isValide && description == ""){
            errorMsg = "La description du produit est vide";
            isValide = false;
        }*/
        if(isValide && $scope.images.length <= 0){
            errorMsg = "Il faut choisir au moins une photo pour le produit";
            isValide = false;
        }

        if(isValide) {
            var formData = new FormData();
            formData.append('nom', nom);
            formData.append('poids', poids);
            formData.append('volume', volume);
            formData.append('quantite', quantite);
            formData.append('prix', prix);
            formData.append('refrigere', refrigere);
            formData.append('congele', congele);
            formData.append('description', description);
            formData.append('date', datePeremption);
            formData.append('categorie', categorie);
            formData.append('type', type);
            formData.append('NbInfoNutris', infoNutris.length);
            for(var i=0; i<infoNutris.length; i++){
                formData.append('infoNutris'+i, infoNutris[i]);
            }
            formData.append('NbImages', $scope.images.length);
            for(var i=0; i<$scope.images.length; i++){
                if($scope.images[i].fromServer){
                    formData.append('fromServer'+i, 1);
                    formData.append('idPhoto'+i, $scope.images[i].idPhoto);
                }
                else{
                    formData.append('fromServer'+i, 0);
                    formData.append('image'+i+'ext', $scope.images[i].extension);
                    formData.append('image'+i+'data', $scope.images[i].dataURL);
                }
            }
            $http({
                method: 'POST',
                url: lien,
                data: formData,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            })
            .then(
                response=>{
                    $scope.showAlert('',response.data.message);
                    $('#btn-validate').show();
                    $('#lds-roller').hide();
                    if(response.data.success){
                        $("#input-nom").val('');
                        $("#input-poids").val('');
                        $("#input-volume").val('');
                        $("#input-prix").val('');
                        $("#input-quantite").val('');
                        $("#input-date-peremption").val('');
                        $("#select-categorie").select2('val', '');
                        $("#select-type").select2('val', '');
                        $("#select-info-nutris").select2('val', '');
                        $("#input-congele, #input-refrigere, #input-consoDirect").iCheck('uncheck');
                        $("#textarea-description").val('');
                        $('#div-file-preview').html('');
                        $scope.images = [];
                    }
                },
                error=>{
                    $scope.showAlert('Erreur','Une erreur est survenue');
                    $('#btn-validate').show();
                    $('#lds-roller').hide();
                }
            );
        }
        else{
            $scope.showAlert('Erreur',errorMsg);
            $('#btn-validate').show();
            $('#lds-roller').hide();
        }
    };

    $scope.import = function(lien){
        var files = $('#input-file-excel').prop('files');
        if(files.length < 1){
            $scope.showAlert("", "Veuillez choisir un fichier excel");
        }
        else{
            $('#btn-import').hide();
            $('#lds-roller-import').show();

            var reader = new FileReader();
            var list = [];
            reader.onload = function(e) {
                var data = reader.result;
                var workbook = XLSX.read(data, {type: 'binary'});
                var pageIndex = 0;
                workbook.SheetNames.forEach(function(sheetName) {
                    if(pageIndex > 0){
                        var XL_row_object = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName], {raw: true});
                        for(var i=0; i<XL_row_object.length; i++){
                            var obj = XL_row_object[i];
                            var date = XLSX.SSF.parse_date_code(obj['Date de péremption'], {date1904:false});
                            obj['Date de péremption'] = date.y+'-'+date.m+'-'+date.d;
                            list.push(obj);
                        }
                    }
                    pageIndex++;
                });
                $scope.beginImport(list, lien);
            };
            reader.onerror = function(e){
                $('#btn-import').show();
                $('#lds-roller-import').hide();
                $scope.showAlert("","Une erreur est survenue pendant la lecture du fichier");
            }
            reader.readAsBinaryString(files[0]);
        }
    };

    $scope.beginImport = function(list, lien){
        var columns = ["Nom","Catégorie","Type","Groupement","Poids","Volume","Quantité(s)","Prix unitaire","Date de péremption","Description","Informations nutritionnelles","Le produit nécessite d'être réfrigéré","Le produit nécessite d'être congelé","Raison de surplus"];
        var colsObj = ['nom','categorie','type','groupement','poids','volume','quantite','prix','datePeremption','description','infoNutris','refrigere','congele','raisonSurplus'];
        var array = [];
        if(list.length > 0){
            var valuesOk = true;
            for(var i=0; i<list.length; i++){
                var row = {};
                for(var ci=0; ci<colsObj.length; ci++){
                    if(list[i][columns[ci]] != undefined){
                        if(ci == 3){
                            if(!(list[i][columns[ci]].toString().trim() == 'Par unité' || list[i][columns[ci]].toString().trim() == 'Totalité')){
                                valuesOk = false;
                                break;
                            }
                        }
                        row[colsObj[ci]] = list[i][columns[ci]];
                    }
                    else{
                        if(ci == 5 || ci == 9){ // ra volume sy description (facultatif)
                            row[colsObj[ci]] = "";
                        }
                        else{
                            valuesOk = false;
                            break;
                        }
                    }
                }
                if(!valuesOk){
                    break;
                }
                array.push(row);
            }
            
            if(valuesOk){
                $scope.startImport(array, lien);
            }
            else{
                $('#btn-import').show();
                $('#lds-roller-import').hide();
                $scope.showAlert('Erreur','Le format de données du fichier ne correspond pas au modèle ou certaines valeur sont invalides');
            }
        }
        else{
            $('#btn-import').show();
            $('#lds-roller-import').hide();
            $scope.showAlert('Erreur','Il n\'y a aucune ligne dans le fichier');
        }
    };

    $scope.startImport = function(array, lien){
        var error = null;
        var formData = new FormData();
        formData.append('moment', $scope.getDatetimeString());
        formData.append('nbRow', array.length);
        for(var i=0; i<array.length; i++){
            formData.append('nom'+i, array[i]['nom']);
            formData.append('categorie'+i, array[i]['categorie']);
            formData.append('type'+i, array[i]['type']);
            formData.append('groupement'+i, array[i]['groupement']);
            formData.append('volume'+i, array[i]['volume']);
            formData.append('datePeremption'+i, array[i]['datePeremption']);
            formData.append('description'+i, array[i]['description']);
            formData.append('infoNutris'+i, array[i]['infoNutris']);
            formData.append('raisonSurplus'+i, array[i]['raisonSurplus']);
            formData.append('refrigere'+i, array[i]['refrigere']);
            formData.append('congele'+i, array[i]['congele']);
            if($scope.getNumber(array[i]['poids']) > 0){
                formData.append('poids'+i, array[i]['poids']);
            }
            else{
                error = "Le poids de <strong>'"+array[i]['nom']+"'</strong> est invalide";
                break;
            }
            if($scope.getNumber(array[i]['prix']) > 0){
                formData.append('prix'+i, array[i]['prix']);
            }
            else{
                error = "Le prix de <strong>'"+array[i]['nom']+"'</strong> est invalide";
                break;
            }
            if($scope.getNumber(array[i]['quantite']) > 0){
                formData.append('quantite'+i, array[i]['quantite']);
            }
            else{
                error = "La quantité de <strong>'"+array[i]['nom']+"'</strong> est invalide";
                break;
            }
        }
        if(error == null){
            $http({
                method: 'POST',
                url: lien,
                data: formData,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            })
            .then(
                response=>{
                    $scope.showAlert('',response.data.message);
                    if(response.data.success){
                        $scope.productFromExcel = response.data.list;
                        $("#label-file-excel").hide();
                        $("#input-file-excel").val('');
                        $('#label-file-excel').html("Veuiller choisir le fichier Excel contenant les données");
                        $('#lds-roller-import').hide();
                        $('#btn-import').hide();
                        $('#btn-download-model').hide();

                        $('#div-photo-excel').show();
                        $('#td-nom-excel').html($scope.productFromExcel[0]['nom']);
                        $('#td-type-excel').html($scope.productFromExcel[0]['type']);
                        $('#div-image-preview-excel').html('');
                    }
                    else{
                        $('#btn-import').show();
                        $('#lds-roller-import').hide();
                    }
                },
                error=>{
                    $scope.showAlert('Erreur','Une erreur est survenue');
                    $('#btn-import').show();
                    $('#lds-roller-import').hide();
                }
            );  
        }
        else{
            $scope.showAlert('Oops', error);
            $('#btn-import').show();
            $('#lds-roller-import').hide();
        }
    };

    $scope.onFileImageExcelChange = function(input){
        var files = $('#input-image-excel').prop('files');
        var divFilePreview = $('#div-image-preview-excel');
        $scope.showImage(divFilePreview, files, 0, $scope.excelImages);
    };

    $scope.importExcelPhoto = function(lien){
        if($scope.excelImages.length > 0){
            if($scope.photoExcelIndex < $scope.productFromExcel.length){
                $('#div-photo-excel').hide();
                $('#lds-roller-import').show();
                var formData = new FormData();
                formData.append('id_produit', $scope.productFromExcel[$scope.photoExcelIndex]['id_produit']);
                formData.append('nbImage', $scope.excelImages.length);
                for(var i=0; i<$scope.excelImages.length; i++){
                    if($scope.excelImages[i].fromServer){
                        formData.append('fromServer'+i, 1);
                        formData.append('idPhoto'+i, $scope.excelImages[i].idPhoto);
                    }
                    else{
                        formData.append('fromServer'+i, 0);
                        formData.append('image'+i+'ext', $scope.excelImages[i].extension);
                        formData.append('image'+i+'data', $scope.excelImages[i].dataURL);
                    }
                }
                $http({
                    method: 'POST',
                    url: lien,
                    data: formData,
                    headers: {'Content-Type': undefined},
                    transformRequest: angular.identity
                })
                .then(
                    response=>{
                        $scope.photoExcelIndex++;
                        if($scope.photoExcelIndex < $scope.productFromExcel.length){
                            $('#td-nom-excel').html($scope.productFromExcel[$scope.photoExcelIndex]['nom']);
                            $('#td-type-excel').html($scope.productFromExcel[$scope.photoExcelIndex]['type']);
                            $('#div-photo-excel').show();
                        }
                        else{
                            $scope.photoExcelIndex = 0;
                            $scope.productFromExcel = [];
                            $scope.showAlert('Terminé', "C'est la fin de la liste");
                            $('#div-photo-excel').hide();
                            $('#btn-import').show();
                            $("#label-file-excel").show();
                        }
                        $scope.excelImages = [];
                        $('#lds-roller-import').hide();
                        $('#div-image-preview-excel').html('');
                    },
                    error=>{
                        $('#lds-roller-import').hide();
                        $scope.showAlert('Erreur','Une erreur est survenue');
                    }
                );
            }
            else{
                $scope.showAlert('Terminé', "C'est la fin de la liste");
            }
        }
        else{
            $scope.showAlert('', 'Vous devez choisir au moins une photo');
        }
    };

    $scope.addForm = function(baseUrl, lien){
        var idForm = Date.now();
        $scope.formList.push(
            {
                id: idForm,
                images: []
            }
        );

        $('#div-form-list').append($compile($scope.getForm(idForm, baseUrl, lien))($scope));

        $('.datepicker').datepicker({
            weekStart: 1,
            language: 'fr',
            format: 'dd/mm/yyyy'
        });

        $(function () {
            $('.select2').select2();
        });
        
        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass   : 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass   : 'iradio_flat-green'
        });
        // radio button
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass   : 'iradio_square-blue',
            increaseArea : '20%' // optional
        });

        // radio type de groupement
        $(".radio-groupement-unite").on('ifChanged', function(event){
            var input = $(this);
            var idForm = input.attr('id').toString().replace('input-groupement-unite', '');
            if(input.prop('checked') == true){
                //<span class="info_vente_facultatif">(facultatif)</span>
                $('#input-poids'+idForm).attr('placeholder', 'Poids de l\'unité');
                $('#input-volume'+idForm).attr('placeholder', 'Volume de l\'unité');
                $('.info_vente_facultatif'+idForm).show();
                $('#input-prix'+idForm).attr('placeholder', 'Prix de l\'unité');
                var quantite = $('#input-quantite'+idForm);
                var quantite_minimum = $('#input-quantite-minimum'+idForm);
                /*quantite.val("");
                quantite_minimum.val("");*/
                quantite.parent().parent().show();
                quantite_minimum.parent().parent().show();

            }
        });


        $(".radio-groupement-portion").on('ifChanged', function(event){
            var input = $(this);
            var idForm = input.attr('id').toString().replace('input-groupement-portion', '');
            if(input.prop('checked') == true){
                //<span class="info_vente_facultatif">(facultatif)</span>
                $('#input-poids'+idForm).attr('placeholder', 'Poids du portion');
                $('#input-volume'+idForm).attr('placeholder', 'Volume du portion');
                $('.info_vente_facultatif'+idForm).show();
                $('#input-prix'+idForm).attr('placeholder', 'Prix du portion');
                var quantite = $('#input-quantite'+idForm);
                var quantite_minimum = $('#input-quantite-minimum'+idForm);
                /*quantite.val("");
                quantite_minimum.val("");*/
                quantite.parent().parent().show();
                quantite_minimum.parent().parent().show();

            }
        });

        $(".radio-groupement-totalite").on('ifChanged', function(event){
            var input = $(this);
            var idForm = input.attr('id').toString().replace('input-groupement-total', '');
            if(input.prop('checked') == true){
                //<span class="info_vente_facultatif">(facultatif)</span>
                $('#input-poids'+idForm).attr('placeholder', 'Poids');
                $('#input-volume'+idForm).attr('placeholder', 'Volume');
                $('.info_vente_facultatif'+idForm).hide();
                $('#input-prix'+idForm).attr('placeholder', 'Prix');
                var quantite = $('#input-quantite'+idForm);
                var quantite_minimum = $('#input-quantite-minimum'+idForm);
                /*quantite.val(1);
                quantite_minimum.val(1);*/
                quantite.parent().parent().hide();
                quantite_minimum.parent().parent().hide();

            }
        });


    }; 

    $scope.removeForm = function(idForm){
        for(var i=1; i<$scope.formList.length; i++){
            if($scope.formList[i].id == idForm){
                $scope.formList.splice(i, 1);
                break;
            }
        }
        for(var i=1; i<$scope.formList.length; i++){
            $('#card-title-'+$scope.formList[i].id).html('Formulaire '+(i+1));
        }
    };

    $scope.getForm = function(idForm, baseUrl, lien){
        return '<div class="card card-success col-md-12" id="card'+idForm+'">'+
        '<div class="card-header">'+
            '<h3 class="card-title" id="card-title-'+idForm+'">Formulaire '+$scope.formList.length+'</h3>'+
            '<div class="card-tools">'+
                '<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>'+
                '<button ng-click="removeForm('+idForm+')" type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>'+
            '</div>'+
        '</div>'+
        '<div class="card-body">'+
            '<div class="row">'+

                '<div class="col-md-6">'+
                    '<div class="form-group">'+
                        '<label>Nom</label>'+
                        '<input id="input-nom'+idForm+'" class="form-control" type="text" placeholder="Nom du produit"/>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label>Groupement</label><br>'+
                        '<label style="margin-right: 10px;">'+
                            '<input class="type-groupement radio-groupement-unite" id="input-groupement-unite'+idForm+'" type="radio" checked="true" name="unite'+idForm+'"/>'+
                        'Par unité'+
                        '</label>'+
                        '<label style="margin-right: 10px;">'+
                            '<input class="type-groupement radio-groupement-portion" id="input-groupement-portion'+idForm+'" type="radio" name="unite'+idForm+'"/>'+
                        'Par portion'+
                        '</label>'+
                        '<label>'+
                            '<input class="type-groupement radio-groupement-totalite" id="input-groupement-total'+idForm+'" type="radio" name="unite'+idForm+'"/>'+
                        'Totalité'+
                        '</label>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label>Poids</label>'+
                        '<div class="input-group mb-3">'+
                            '<input id="input-poids'+idForm+'" class="form-control" type="text" placeholder="Poids de l\'unité">'+
                            '<div class="input-group-append">'+
                                '<span class="input-group-text">Kg</span>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label>Volume <span class="info_vente_facultatif1">(facultatif)</span> </label>'+
                        '<div class="input-group mb-3">'+
                            '<input id="input-volume'+idForm+'" class="form-control" type="text" placeholder="Volume de l\'unité ">'+
                            '<div class="input-group-append">'+
                                '<span class="input-group-text"><var>m<sup>3</sup></var></span>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label>Prix</label>'+
                        '<div class="input-group mb-3">'+
                            '<input id="input-prix'+idForm+'" class="form-control" type="text" placeholder="Prix de l\'unité">'+
                            '<div class="input-group-append">'+
                                '<span class="input-group-text">MGA</span>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label>Quantité(s)</label>'+
                        '<div class="input-group mb-3">'+
                            '<input id="input-quantite'+idForm+'" class="form-control" type="number" min="1" placeholder="Quantité">'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label>Date de péremption</label>'+
                        '<div class="input-group mb-3">'+
                            '<input id="input-date-peremption'+idForm+'" class="form-control datepicker" type="text" placeholder="Date de péremption">'+
                            '<div class="input-group-append">'+
                                '<span class="input-group-text">'+
                                    '<i class="fa fa-calendar"></i>'+
                                '</span>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label class="check" style="font-weight: bolder !important;">'+
                            '<input id="input-refrigere'+idForm+'" type="checkbox" class="flat-red"> Le produit nécessite d\'être réfrigéré'+
                        '</label>'+
                        '<label class="check" style="font-weight: bolder !important;">'+
                            '<input id="input-congele'+idForm+'" type="checkbox" class="flat-red"> Le produit nécessite d\'être congelé'+
                        '</label>'+
                        '<label class="check" style="font-weight: bolder !important;">'+
                            '<input id="input-consoDirect'+idForm+'" type="checkbox" class="flat-red" /> Le produit est à consommer directement'+
                        '</label>'+
                    '</div>'+
                '</div>'+

                '<div class="col-md-6">'+
                    '<div class="form-group">'+
                        '<label>Catégorie</label>'+
                        '<div class="row">'+
                            '<div class="col-11">'+
                                '<select onchange="angular.element(this).scope().TYPE.onValueChanged('+idForm+')" id="select-categorie'+idForm+'" data-placeholder="Catégorie du produit" class="form-control select2" style="width: 100%;">'+
                                    $('#select-categorie1').html()+
                                '</select>'+
                            '</div>'+
                            '<div class="col-1" style="margin-left: -7.5px;">'+
                                '<button ng-click="formClick(\''+idForm+'\')" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal-new-categorie"><i class="fa fa-plus"></i></button>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label>Type</label>'+
                        '<div class="row">'+
                            '<div class="col-11">'+
                                '<select id="select-type'+idForm+'" data-placeholder="Type du produit" class="form-control select2" style="width: 100%;">'+
                                $('#select-type1').html()+
                                '</select>'+
                            '</div>'+
                            '<div class="col-1" style="margin-left: -7.5px;">'+
                                '<button ng-click="formClick(\''+idForm+'\')" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal-new-type"><i class="fa fa-plus"></i></button>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label>Informations nutritionnelles</label>'+
                        '<div class="row">'+
                            '<div class="col-11">'+
                                '<select id="select-info-nutris'+idForm+'" class="form-control select2 select2-hidden-accessible" multiple="" data-placeholder="Veuillez choisir au moins une" style="width: 100%;" tabindex="-1" aria-hidden="true">'+
                                    $('#select-info-nutris1').html()+
                                '</select>'+
                            '</div>'+
                            '<div class="col-1" style="margin-left: -7.5px;">'+
                                '<button ng-click="formClick(\''+idForm+'\')" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal-new-info-nutri"><i class="fa fa-plus"></i></button>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label>Raisons des surplus</label>'+
                        '<div class="row">'+
                            '<div class="col-11">'+
                                '<select id="select-raison-surplus'+idForm+'" class="form-control select2 select2-hidden-accessible" multiple="" data-placeholder="Veuillez choisir au moins une" style="width: 100%;" tabindex="-1" aria-hidden="true">'+
                                    $('#select-raison-surplus1').html()+
                                '</select>'+
                            '</div>'+
                            '<div class="col-1" style="margin-left: -7.5px;">'+
                                '<button ng-click="formClick(\''+idForm+'\')" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal-new-raison-surplus"><i class="fa fa-plus"></i></button>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label>Description</label>'+
                        '<textarea id="textarea-description'+idForm+'" class="form-control" rows="3" placeholder="Description du produit"></textarea>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label>Photos</label><br>'+
                        '<div class="row">'+
                            '<div class="col-6">'+
                                '<label ng-click="formClick(\''+idForm+'\')" for="input-file-image'+idForm+'" class="form-control btn btn-secondary">Ajouter depuis mon ordinateur<i class="fa fa-picture-o" style="margin-left:8px;"></i></label>'+
                                '<input id="input-file-image'+idForm+'" onchange="angular.element(this).scope().onFileImageChange(this)" type="file" accept="image/*" style="display: none;" multiple />'+
                            '</div>'+
                            '<div class="col-6">'+
                                '<button ng-click="showPhotoPicker(\''+idForm+'\', \''+baseUrl+'\',\''+lien+'\')" class="form-control btn btn-secondary"><strong style="color: white;">Ajouter depuis le serveur</strong><i class="fa fa-picture-o" style="margin-left:8px;"></i></button>'+
                            '</div>'+
                        '</div>'+
                        '<div class="row" id="div-file-preview'+idForm+'">'+
                            
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+
    '</div>';
    };
    /* ---------- AJOUT DE NOUVELLE PERIODE STRATEGIE ---------- */

    $scope.addPeriodeStrategie = function(lien){
 
        var nbJour = $scope.getNumber($("#input-nombre-jour").val().trim().replace(/\s/g,"")); 
        var nbSemaine = $scope.getNumber($("#input-nombre-semaine").val().trim().replace(/\s/g,"")); 
        var nbMois = $scope.getNumber($("#input-nombre-mois").val().trim().replace(/\s/g,"")); 
        var idToAdd = $("#identifiantMere").val();
        //$scope.infoErrorPeriode = false;

        var isValide = true;
        var errorMsg = "";
        if(isValide && (nbJour == 0 && nbSemaine == 0 && nbMois == 0)){
            errorMsg = "La période de stratégie saisie est invalide, veuillez vérifier ce que vous avez saisie!";
            isValide = false;
            //$scope.infoErrorPeriode = true;
        }
    
        if(!isValide){
            toastr.error("Une erreur est survenue",errorMsg);
            //$scope.showAlert('Une erreur est survenue', errorMsg);
        }else{
            var formData = new FormData();
            formData.append('nbJour', nbJour);
            formData.append('nbSemaine', nbSemaine);
            formData.append('nbMois', nbMois);
            $http({
                url: lien, 
                method: 'POST',
                data: formData,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            })
            .then(response=>{
                var data = response.data;
                if(data.success){
                    $("#input-nombre-jour").val("");
                    $("#input-nombre-semaine").val("");
                    $("#input-nombre-mois").val("");
                    $("#identifiantMere").val("");

                    var select_periode = $('.periode-to-test');
                
                    for(var i=0; i<select_periode.length; i++){
                        var element = select_periode.get(i);
                        $(element).append('<option value="'+data.id_periode+'" >'+data.label+'</option>');
                    }

                    var template_select_periode = $('.periode-to-test-other');
                    template_select_periode.append('<option value="'+data.id_periode+'" >'+data.label+'</option>');
                    //console.log("lim : "+$scope.limite_strategie);
                    $scope.limite_strategie = $scope.limite_strategie + 1;
                    //console.log("lim vao : "+$scope.limite_strategie);
                    //var select = $('.periodeStrategie'+numStrategie);
                    //select.select2('val', data.id_periode);
                    $("#select-strategie-prix"+idToAdd).select2('val', data.id_periode);

                    toastr.success("La période de stratégie est bien enregistrée","Notification");
                }
                else{
                    $scope.showAlert('Notification', data.message);
                }
            }, error=>{
                $scope.showAlert('Notification', 'Une erreur est survenue pendant l\'ajout du nouveau période');
            });
        }
        
    };

    /* ---------- GESTION DE CATEGORIE ---------- */

    $scope.TYPE = {};

    $scope.TYPE.addNewCategorie = function(lien){
        var infoNutris = $("#select-type-info-nutri-modal").select2('val').toString();
        var formData = new FormData();
        formData.append('categorie', $('#categorie-name-modal').val().trim());
        formData.append('infoNutris', infoNutris);
        $http({
            url: lien, 
            method: 'POST',
            data: formData,
            headers: {'Content-Type': undefined},
            transformRequest: angular.identity
        })
        .then(response=>{
            var data = response.data;
            if(data.success){
                $('#categorie-name-modal').val("");
                $("#select-type-info-nutri-modal").select2('val', '');
                var select = $('#select-categorie'+$scope.formClicked);
                select.append('<option value="'+data.id_type_aliment+'" >'+data.label+'</option>');
                select.select2('val', data.id_type_aliment);
                for(var i=0; i<$scope.formList.length; i++){
                    if($scope.formList[i].id != $scope.formClicked){
                        $('#select-categorie'+$scope.formList[i].id).append('<option value="'+data.id_type_aliment+'" >'+data.label+'</option>');
                    }
                }
                // auto select
                infoNutris = infoNutris.split(',');
                var select = $('#select-info-nutris'+$scope.formClicked);
                for(var i=0; i<infoNutris.length; i++)
                    select.select2("val", select.select2("val").concat(infoNutris[i]));
            }
            else{
                $scope.showAlert('', data.message);
            }
        }, error=>{
            $scope.showAlert('', 'Une erreur est survenue pendant l\'ajout du nouveau catégorie');
        });
    };

    $scope.TYPE.onValueChanged = function(formIndex){
        var formData = new FormData();
        formData.append('idCategorie', $('#select-categorie'+formIndex).val());
        $http({
            method: 'POST',
            url: base_url+'produit/vente/getInfoNutrisLinked',
            data: formData,
            headers: {'Content-Type': undefined},
            transformRequest: angular.identity
        })
        .then(
            response=>{
                var data = response.data;
                if(data.length > 0){
                    var select = $('#select-info-nutris'+formIndex);
                    select.select2("val", "");
                    for(var i=0; i<data.length; i++){
                        select.select2("val", select.select2("val").concat(data[i]));
                    }
                }
            },
            error=>{
                console.log(error);
            });
    };

    $scope.checkCoordValue = function(value){
        var result = parseFloat(value.toString().replace(',', '.'));
        if( !isNaN(result) ){
            return result;
        }
        else{
            return null;
        }
    };

    $scope.initShowMap = function(identifiant){

        $(".load_map").show();
        $(".contenu_carte").hide();

        $("#map").html("");

        $("#identifiantMere").val(identifiant);

        mapboxgl.accessToken = 'pk.eyJ1IjoibmphcnkiLCJhIjoiY2p1ZHU0NWYzMDJ1MTRkbW16N2pmczh6YiJ9.i3r9euc0dZsYchVvQfdG5A';
        var map = new mapboxgl.Map({
            container: 'map',
            center: [ $scope.mainLong, $scope.mainLat ],
            zoom: 14,
            style: 'mapbox://styles/mapbox/streets-v9'
        });

        $(".lat_value_in_map").html( "(Non défini)" );
        $(".long_value_in_map").html( "(Non défini)" );


        var currentLong = $scope.checkCoordValue( $("#infoLongitude"+String(identifiant)).val() );
        var currentLat = $scope.checkCoordValue( $("#infoLatitude"+String(identifiant)).val() );

        var el = document.createElement('div');
        el.className = 'marker';
        el.style.backgroundImage = 'url('+site_url+'assets/img/marker_fw.png)';
        el.style.width = '35px';
        el.style.height = '35px';
        el.style.backgroundRepeat = 'no-repeat';
        el.style.backgroundSize = 'contain';

        //console.log( el.style.backgroundImage );

        var marker = new mapboxgl.Marker(el);
        
        if(currentLong != null && currentLat != null){

            if( currentLong != 0 && currentLat != 0){
                $scope.mainLong = currentLong;
                $scope.mainLat = currentLat;
            }   

            $(".lat_value_in_map").html( String($scope.mainLat) );
            $(".long_value_in_map").html( String($scope.mainLong) );

            map.flyTo({ center: [$scope.mainLong, $scope.mainLat] });

            marker.remove();
            marker.setLngLat([$scope.mainLong, $scope.mainLat]).addTo(map);

        }

        //console.log("identifiant : "+identifiant);

        map.on('load', function () {
            
            // *** Add zoom and rotation controls to the map ...
            map.addControl(new mapboxgl.NavigationControl());

            $(".load_map").hide();
            $(".contenu_carte").show();

        });

        map.on('click', function(e){
            
            $scope.mainLong = e.lngLat.lng;
            $scope.mainLat = e.lngLat.lat;
            
            $(".lat_value_in_map").html( String($scope.mainLat) );
            $(".long_value_in_map").html( String($scope.mainLong) );

            map.flyTo({ center: [$scope.mainLong, $scope.mainLat] });

            marker.remove();
            marker.setLngLat([$scope.mainLong,$scope.mainLat]).addTo(map);                                    

        });


        $("#search_in_carte").autocomplete({

            appendTo: "#autocomplete_list",
            source: function (request, response) {
                $.ajax({
                    url: 'https://api.mapbox.com/geocoding/v5/mapbox.places/'+ $("#search_in_carte").val() +'.json',
                    type: "GET",
                    data:{ 
                        types : "locality,address",
                        limit : 10,
                        country : "mg",
                        access_token : "pk.eyJ1IjoibmphcnkiLCJhIjoiY2p1ZHU0NWYzMDJ1MTRkbW16N2pmczh6YiJ9.i3r9euc0dZsYchVvQfdG5A"
                    },
                    success: function (data) {
                        //console.log(data);
                        response($.map(data.features, function (item) {
                            //console.log(item);
                            return {
                                label:  item.text + ", " + item.context[1].text,
                                value:  item.text + ", " + item.context[1].text,
                                coord:  item.center 
                            }
                       }))
                    },
                    error: function (xhr, status, err) {
                        alert("Error on autocomplete");
                    }
                });
            },
            select: function (even, ui) {
                //console.log(ui.item.coord);
                map.flyTo({ center: ui.item.coord });

                marker.remove();
                marker.setLngLat( ui.item.coord ).addTo(map);

                $scope.mainLong = ui.item.coord[0];
                $scope.mainLat = ui.item.coord[1];
            
                $(".lat_value_in_map").html( String($scope.mainLat) );
                $(".long_value_in_map").html( String($scope.mainLong) );
            }
        });

    }

    $scope.putCoordonneChoisi = function(){

        var identifiant = $("#identifiantMere").val();

        var currentLong = $scope.checkCoordValue( $scope.mainLong );
        var currentLat = $scope.checkCoordValue( $scope.mainLat );

        if( currentLong != null && currentLat != null ){

            $("#infoLongitude"+String(identifiant)).val( currentLong );
            $("#infoLatitude"+String(identifiant)).val( currentLat );

        }else{
            console.log("Coordinates undefined");
        }

    }

     $scope.initDonSelect2 = function(idForm){
         $('.select-categorie').select2({
                width: '100%',
                placeholder: 'Choisisser ou ajouter',
                language: {
                  noResults: function() {
                    return '<button id="no-results-btn" class="btn btn-primary btn-block" onclick="ajouterCategorie('+idForm+')">Ajouter catégorie</a>';
                  },
                },
                escapeMarkup: function(markup) {
                  return markup;
                },
            });
            $('.select-type').select2({
                width: '100%',
                placeholder: 'Choisisser ou ajouter',
                language: {
                  noResults: function() {
                    return '<button id="no-results-btn" class="btn btn-primary btn-block" onclick="ajouterType('+idForm+')">Ajouter type</a>';
                  },
                },
                escapeMarkup: function(markup) {
                  return markup;
                },
            });
            $('.select-info-nutris').select2({
                width: '100%',
                placeholder: 'Choisisser ou ajouter',
                language: {
                  noResults: function() {
                    return '<button id="no-results-btn" class="btn btn-primary btn-block" onclick="ajouterInfoNutris('+idForm+')">Ajouter information</a>';
                  },
                },
                escapeMarkup: function(markup) {
                  return markup;
                },
            });
            $('.select-raison-surplus').select2({
                width: '100%',
                placeholder: 'Choisisser ou ajouter',
                language: {
                  noResults: function() {
                    return '<button id="no-results-btn" class="btn btn-primary btn-block" onclick="ajouterRaisonSurplus('+idForm+')">Ajouter raison</a>';
                  },
                },
                escapeMarkup: function(markup) {
                  return markup;
                },
            });
    }

}]);

$(window).on('load',function(){
    /*if(profilOk==0 || contratAccepter != 1)*/
    $("#containerSelectStrategieMarketing").hide();
    $("#containerSelectPeriodeRecuperation").hide();

    $('.horaire-debut-to-test').timepicker({ 
        'timeFormat': 'H:i',
        'step': 15,
        'scrollDefault': 'now'
       
    });
    $('.horaire-fin-to-test').timepicker({ 
        'timeFormat': 'H:i',
        'step': 15,
        'scrollDefault': 'now'
       
    });

    if( profilOk==0 ){
        $('#modal-block-vente').modal('show');

        $('#modal-block-vente').on('hide.bs.modal', function () {
            // do something…
            window.location.href = BASE_URL + "admin/dashboard";
        });
    }
    else{
        $('.datepicker').datepicker({
            todayHighlight: true,
            weekStart: 1,
            language: 'fr',
            format: 'dd/mm/yyyy'
        });
        /*$("input[type=number]").on('mousewheel.disableScroll', function (e) {
            e.preventDefault();
        });*/
        $(':input[type=number]').on('mousewheel',function(e){ $(this).blur(); });

        //Information bulle
        new Tippy('.info_choix_totalite',{
            position:'top',
            arrow:'true'
        });
        new Tippy('.info_qte_min',{
            position:'right',
            arrow:'true'
        });
        new Tippy('.info_pourcentage_reduc',{
            position:'right',
            arrow:'true'
        });
        new Tippy('.info_strategie_reduc',{
            position:'right',
            arrow:'true'
        });

        $('.info_to_hide').hide();

        // radio type de groupement
        $(".radio-groupement-unite").on('ifChanged', function(event){
            var input = $(this);
            var idForm = input.attr('id').toString().replace('input-groupement-unite', '');
            if(input.prop('checked') == true){
                //<span class="info_vente_facultatif">(facultatif)</span>
                $('#input-poids'+idForm).attr('placeholder', 'Poids de l\'unité');
                $('#input-volume'+idForm).attr('placeholder', 'Volume de l\'unité');
                $('.info_vente_facultatif'+idForm).show();
                $('.info_vente_totalite'+idForm).hide();
                $('#input-prix'+idForm).attr('placeholder', 'Prix de l\'unité');
                var quantite = $('#input-quantite'+idForm);
                var quantite_minimum = $('#input-quantite-minimum'+idForm);
                /*quantite.val("");
                quantite_minimum.val("");*/
                quantite.parent().parent().show();
                quantite_minimum.parent().parent().show();

            }
        });


        $(".radio-groupement-portion").on('ifChanged', function(event){
            var input = $(this);
            var idForm = input.attr('id').toString().replace('input-groupement-portion', '');
            if(input.prop('checked') == true){
                //<span class="info_vente_facultatif">(facultatif)</span>
                $('#input-poids'+idForm).attr('placeholder', 'Poids du portion');
                $('#input-volume'+idForm).attr('placeholder', 'Volume du portion');
                $('.info_vente_facultatif'+idForm).show();
                $('.info_vente_totalite'+idForm).hide();
                $('#input-prix'+idForm).attr('placeholder', 'Prix du portion');
                var quantite = $('#input-quantite'+idForm);
                var quantite_minimum = $('#input-quantite-minimum'+idForm);
                /*quantite.val("");
                quantite_minimum.val("");*/
                quantite.parent().parent().show();
                quantite_minimum.parent().parent().show();

            }
        });

        $(".radio-groupement-totalite").on('ifChanged', function(event){
            var input = $(this);
            var idForm = input.attr('id').toString().replace('input-groupement-total', '');
            if(input.prop('checked') == true){
                //<span class="info_vente_facultatif">(facultatif)</span>
                $('#input-poids'+idForm).attr('placeholder', 'Poids');
                $('#input-volume'+idForm).attr('placeholder', 'Volume');
                $('.info_vente_facultatif'+idForm).hide();
                $('.info_vente_totalite'+idForm).show();
                $('#input-prix'+idForm).attr('placeholder', 'Prix');
                var quantite = $('#input-quantite'+idForm);
                var quantite_minimum = $('#input-quantite-minimum'+idForm);
                /*quantite.val(1);
                quantite_minimum.val(1);*/
                quantite.parent().parent().hide();
                quantite_minimum.parent().parent().show();

            }
        });


         //Radio type de date
        $('.radio-choix-type-date').on('ifChanged',function(event){
            var input = $(this);
            var idForm = input.attr('id').toString().replace('input-choix-date-peremption','');
            if(input.prop('checked') == true){
                $('#input-date-peremption-production'+idForm).attr('placeholder', 'Date de péremption');
            }
            else{
                $('#input-date-peremption-production'+idForm).attr('placeholder', 'Date de production');
            }
        });

        //Choix de surplus

        /*$(".radio-surplus-alimentaire").on('ifChanged', function(event){
            var input = $(this);
            var idForm = input.attr('id').toString().replace('input-surplus-alim-non', '');
            if(input.prop('checked') == true ){
                $('#select-raison-surplus'+idForm).attr('disabled','disabled');
                $('#ajout_raison_surplus'+idForm).attr('disabled','disabled');
                
            }
            else{
                $('#select-raison-surplus'+idForm).removeAttr('disabled','');
                $('#ajout_raison_surplus'+idForm).removeAttr('disabled','');
               
            }
        });*/

        

        //adresseCourant, adresseAutre
        var statutVenteModif = $('#statut_vente_modif').val();
        if(statutVenteModif == 0 ){
            $('#adresseCourant').show();
            $('#adresseAutre').hide();
        }else if(statutVenteModif == 1){
            /*$('#adresseCourant').show();
            $('#adresseAutre').hide();*/

            if($("#input-lieu-ramassage-adresse_entreprise1").prop('checked') == true){
                $('#adresseCourant').show();
                $('#adresseAutre').hide();
            }else{
                $('#adresseCourant').hide();
                $('#adresseAutre').show();
            }
            /*if($("#input-surplus-alim-oui1").prop('checked') == true){
                $('#select-raison-surplus1').removeAttr('disabled','');
                $('#ajout_raison_surplus1').removeAttr('disabled','');
            }*/
            if($("#input-groupement-total1").prop('checked') == true){
                var quantite = $('#input-quantite1');
                var quantite_minimum = $('#input-quantite-minimum1');
                quantite.parent().parent().hide();
                quantite_minimum.parent().parent().show();
            }
        }
        


        $(".radio-lieu-ramassage").on('ifChanged', function(event){
            var input = $(this);
            var idForm = input.attr('id').toString().replace('input-lieu-ramassage-adresse_entreprise','');
            if(input.prop('checked') == true ){
                $('#adresseCourant').show();
                $('#adresseAutre').hide();

            }else{
                $('#adresseCourant').hide();
                $('#adresseAutre').show();
            }     
            
        });
    }
});

$(window).on('load',function(){
    $(".loader").show();
});

$(document).ready(function(){
    /*$('.horaire-debut-to-test').timepicker({
        timeFormat: 'HH:mm:ss',
        minTime : '00:00:00',
        maxHour : 20,
        zindex : '10000'

    });
    $('.horaire-fin-to-test').timepicker({
       
    });*/

});