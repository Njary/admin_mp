var appAdmin = angular.module('AdminApp', []);

appAdmin.controller('AdminCtrl', ["$scope", "$http","$timeout" , function($scope, $http , $timeout) {
    
    $scope.offset = 0;
    $scope.limit = 5;
     $scope.isCheckedNePlusAfficher = false;
    $scope.loadType = false;

    this.$onInit = function(){
        $("#id_checkbox_bienvenu").on("ifChanged", function(){

            var base_url = $("#base_url").val();
            var etat = $(this).is(":checked");
            url = base_url+'n-p-a-b';
            $scope.setCloseIntoBienvenu(url , etat);
             
            
           
        });
        
        var teste = $("#test-first-time").val();
        if(teste=="true"){
           $timeout( function(){
                 $scope.closeInfoBienvenu();
            }, 10 );
            
        }
        
    }

    $scope.showQtePerYear = function(poids, volumes){
        var ticksStyle = {
            fontColor: '#495057',
            fontStyle: 'normal'
        };
        var mode = 'index';
        var intersect = true;
        var $salesChart = $('#qte-per-year-chart');
        var salesChart = new Chart($salesChart, {
            data: {
                labels: ['JAN', 'FEV', 'MAR', 'AVR', 'MAI', 'JUI', 'JUI', 'AOU', 'SEP', 'OCT', 'NOV', 'DEC'],
                datasets: [
                    {
                        type: 'line',
                        fill: false,
                        backgroundColor: '#F05423',
                        borderColor: '#F05423',
                        data: poids
                    },
                    {
                        type: 'line',
                        fill: false,
                        backgroundColor: '#FEC745',
                        borderColor: '#FEC745',
                        data: volumes
                    }
                ]
            },
            options: {
                maintainAspectRatio: false,
                tooltips: {
                    mode: mode,
                    intersect: intersect
                },
                hover: {
                    mode: mode,
                    intersect: intersect
                },
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        gridLines: {
                            display: true
                        },
                        ticks: $.extend({
                            beginAtZero: true,
                            callback: function (value, index, values) {
                                return $scope.numberFormat(value.toFixed(1), 2);
                            }
                        }, ticksStyle)
                    }],
                    xAxes: [{
                        display  : true,
                        gridLines: {
                            display: true
                        },
                        ticks    : ticksStyle
                    }]
                }
            }
        });
    };

    $scope.getQtePerYear = function(lien, firstCall){
        $http({
            url: lien, 
            method: 'POST',
            data: 'year='+$('#select-year-qte-per-year').val(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(
            response=>{
                var data = response.data;
                var poids = [];
                var volumes = [];
                for(var i=0; i<data.length; i++){
                    poids.push(data[i].poids);
                    volumes.push(data[i].volumes);
                }
                if(firstCall == true){
                    $('#graph-qte-per-year').html('<canvas id="qte-per-year-chart" height="200"></canvas>');
                }
                $scope.showQtePerYear(poids, volumes);
            },
            error=>{
                console.log(error);
            }
        );
    };

    $scope.yearQteChange = function(lien){
        $('#graph-qte-per-year').html('<div class="lds-roller" style="display: block; margin: 0 auto;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>');
        $scope.getQtePerYear(lien, true);
    };

    $scope.showMontantPerYear = function(montants){
        var ticksStyle = {
            fontColor: '#495057',
            fontStyle: 'normal'
        };
        var mode = 'index';
        var intersect = true;
        var $salesChart = $('#montant-chart');
        var salesChart = new Chart($salesChart, {
            data: {
                labels: ['JAN', 'FEV', 'MAR', 'AVR', 'MAI', 'JUI', 'JUI', 'AOU', 'SEP', 'OCT', 'NOV', 'DEC'],
                datasets: [
                    {
                        type: 'line',
                        fill: false,
                        backgroundColor: '#00AFAA',
                        borderColor: '#00AFAA',
                        data: montants
                    }
                ]
            },
            options: {
                maintainAspectRatio: false,
                tooltips: {
                    mode: mode,
                    intersect: intersect
                },
                hover: {
                    mode: mode,
                    intersect: intersect
                },
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        gridLines: {
                            display: true
                        },
                        ticks: $.extend({
                            beginAtZero: true,
                            callback: function (value, index, values) {
                                return $scope.numberFormat(value.toFixed(1), 0);
                            }
                        }, ticksStyle)
                    }],
                    xAxes: [{
                        display  : true,
                        gridLines: {
                            display: true
                        },
                        ticks    : ticksStyle
                    }]
                }
            }
        });
    };

    $scope.getMontantPerYear = function(lien, firstCall){
        $http({
            url: lien, 
            method: 'POST',
            data: 'year='+$('#select-year-montant').val(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(
            response=>{
                var data = response.data;
                if(firstCall == true){
                    $('#graph-montant-per-year').html('<canvas id="montant-chart" height="200"></canvas>');
                }
                $scope.showMontantPerYear(data.list);
                $('#montant-total').html($scope.numberFormat(data.total, 0)+' MGA');
            },
            error=>{
                console.log(error);
            }
        );
    };

    $scope.yearMontantChange = function(lien){
        $('#graph-montant-per-year').html('<div class="lds-roller" style="display: block; margin: 0 auto;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>');
        $scope.getMontantPerYear(lien, true);
    };

    $scope.getNombrePerGenre = function(lien){
        $http({
            url: lien, 
            method: 'POST',
            data: 'genre='+$('#select-genre-nombre').val(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(
            response=>{
                var list = response.data.list;
                list.sort((a, b)=>{
                    if(a.label > b.label){
                        return 1;
                    }
                    else if(a.label < b.label){
                        return -1;
                    }
                    return 0;
                });
                var total = parseInt(response.data.total);
                var div = $('#list-genre');
                div.html('');
                $('#nombre-total').html($scope.numberFormat(total, 0));
                for(var i=0; i<list.length; i++){
                    var split = list[i].label.split("(");
                    div.append('<div class="col-md-4 border-bottom">'+
                            '<span>'+split[0]+'</span>'+
                        '</div>'+
                        '<div class="col-md-8">'+
                            '<div id="nombre-node-d-'+i+'" class="div-stat-horizontal" style="width: 0px;" data-toggle="tooltip" title="'+list[i].nombre+'"></div>'+
                        '</div>');
                }
                
                if(total <= 0){
                    total = 1;
                }
                for(var i=0; i<list.length; i++){
                    $('#nombre-node-d-'+i).css('width', (100 * list[i].nombre / total)+'%'); 
                    $('#nombre-node-d-'+i).css('background-color', '#00AFAA');
                }
            },
            error=>{
                console.log(error);
            }
        );
    };

    $scope.getQtePerGenre = function(lien){
        $http({
            url: lien, 
            method: 'POST',
            data: 'genre='+$('#select-genre-qte').val(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(
            response=>{
                var list = response.data.list;
                list.sort((a, b)=>{
                    if(a.label > b.label){
                        return 1;
                    }
                    else if(a.label < b.label){
                        return -1;
                    }
                    return 0;
                });
                var totalPoids = parseFloat(response.data.totalPoids);
                var totalVolumes = parseFloat(response.data.totalVolumes);
                var total = totalPoids > totalVolumes ? totalPoids : totalVolumes;
                var div = $('#list-qte');
                div.html('');
                $('#qte-total').html($scope.numberFormat(total, 2));
                for(var i=0; i<list.length; i++){
                    var split = list[i].label.split("(");
                    div.append('<div class="col-md-4 border-bottom">'+
                            '<span>'+split[0]+'</span>'+
                        '</div>'+
                        '<div class="col-md-8" style="margin-bottom: 5px;">'+
                            '<div id="nombre-node-p-d-'+i+'" class="div-stat-horizontal-p" style="width: 0px;" data-toggle="tooltip" title="'+list[i].poids+'"></div>'+
                            '<div id="nombre-node-v-d-'+i+'" class="div-stat-horizontal-v" style="width: 0px;" data-toggle="tooltip" title="'+list[i].volumes+'"></div>'+
                        '</div>');
                }
                
                if(total <= 0){
                    total = 1;
                }
                for(var i=0; i<list.length; i++){
                    $('#nombre-node-p-d-'+i).css('width', (100 * list[i].poids / total)+'%');
                    $('#nombre-node-v-d-'+i).css('width', (100 * list[i].volumes / total)+'%');
                    $('#nombre-node-p-d-'+i).css('background-color', '#F05423');
                    $('#nombre-node-v-d-'+i).css('background-color', '#FEC745');
                }
            },
            error=>{
                console.log(error);
            }
        );
    };

    $scope.init = function( lienNombreDonVente){
        var firstCall = true;
        $scope.getNbDonVentePerYear(lienNombreDonVente, firstCall);
    };

    $scope.loadVenteInfo = function(lienQteVente, lienMontantVente, lienNombreGenreVente, lienQteGenreVente){
        var firstCall = true;
        $scope.getQteSalesPerYear(lienQteVente,firstCall);
        $scope.getMontantSalesPerYear(lienMontantVente,firstCall);
        $scope.getNombreSalesPerGenre(lienNombreGenreVente);
        $scope.getQteSalesPerGenre(lienQteGenreVente);
    };

    $scope. loadDonInfo = function(lienQte, lienMontant, lienNombre, lienQteGenre){
        var firstCall = true;
        $scope.getQtePerYear(lienQte, firstCall);
        $scope.getMontantPerYear(lienMontant, firstCall);
        $scope.getNombrePerGenre(lienNombre);
        $scope.getQtePerGenre(lienQteGenre);
    }
    
    $scope.showMoreDon = function(lien){
        $('#btn-show-more-don').hide();
        $('#lds-roller-list-recent-don').show();

        $scope.offset += $scope.limit;
        $http({
            url: lien, 
            method: 'POST',
            data: 'offset='+$scope.offset,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(response=>{
            var list = response.data;
            var tbody = $('#body-list-recent-don');
            if(list.length > 0){
                for(var i=0; i<list.length; i++){
                    tbody.append("<tr>"+
                        "<td>"+list[i].datetime.date+"<span style=\"font-size: 13px; opacity: 0.9;\"> à "+list[i].datetime.time+"</span></td>"+
                        "<td>"+list[i].beneficiaire+"</td>"+
                        "<td>"+list[i].produit+"</td>"+
                        "<td>"+list[i].type+"</td>"+
                        "<td>"+list[i].quantites+"</td>"+
                        "<td>"+list[i].etat+"</td>"+
                    +"</tr>");
                }
                if(list.length >= $scope.limit){
                    $('#btn-show-more-don').show();
                }
            }
            $('#lds-roller-list-recent-don').hide();
        }, error=>{
            $('#lds-roller-list-recent-don').hide();
        });
    };
    $scope.showMoreVente = function(lien){
        $('#btn-show-more-vente').hide();
        $('#lds-roller-list-recent-vente').show();

        $scope.offset += $scope.limit;
        $http({
            url: lien, 
            method: 'POST',
            data: 'offset='+$scope.offset,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(response=>{
            var list = response.data;
            var tbody = $('#body-list-recent-vente');
            if(list.length > 0){
                for(var i=0; i<list.length; i++){
                    var splitCat = list[i].categorie.split("(");
                    tbody.append("<tr class=\"etat"+list[i].id_etat+"\">"+
                        "<td>"+list[i].datetime.date+"<span style=\"font-size: 13px; opacity: 0.9;\"> à "+list[i].datetime.time+"</span></td>"+
                        "<td>"+list[i].produit+"</td>"+
                        "<td>"+splitCat[0]+"</td>"+
                        "<td>"+list[i].quantites+"</td>"+
                        "<td>"+list[i].prix+" MGA </td>"+
                        "<td> - "+list[i].pourcentage_reduction+"% | "+(list[i].prix - (list[i].prix * (list[i].pourcentage_reduction / 100)))+" MGA </td>"+
                        "<td>"+list[i].etat+"</td>"+
                    +"</tr>");
                }
                if(list.length >= $scope.limit){
                    $('#btn-show-more-vente').show();
                }
            }
            $('#lds-roller-list-recent-vente').hide();
        }, error=>{
            $('#lds-roller-list-recent-vente').hide();
        });
    };

    $scope.numberFormat = function(number, dec){
        var result = number.toString().split('.');
        var entier = parseInt(result[0]);
        var decimal = 0;
        if(result.length > 1){
            decimal = parseInt(result[1]);
        }
        result = '';
        var counter = 0;
        var  str = entier.toString();
        for(var i=str.length-1; i>-1; i--){
            counter++;
            result = str.charAt(i) + result;
            if(counter%3==0 && i>0){
                result = ' '+result;
            }
        }
        if(dec>0){
            if(decimal > 0){
                return result+','+decimal;
            }
            else{
                return result+',00';
            }
        }
        else{
            return result;
        }
    };


    // Vente
    $scope.showQteSalesPerYear = function(poids, volumes){
        var ticksStyle = {
            fontColor: '#495057',
            fontStyle: 'normal'
        };
        var mode = 'index';
        var intersect = true;
        var $salesChart = $('#qte-sales-per-year-chart');
        var salesChart = new Chart($salesChart, {
            data: {
                labels: ['JAN', 'FEV', 'MAR', 'AVR', 'MAI', 'JUI', 'JUI', 'AOU', 'SEP', 'OCT', 'NOV', 'DEC'],
                datasets: [
                    {
                        type: 'line',
                        fill: false,
                        backgroundColor: '#F05423',
                        borderColor: '#F05423',
                        data: poids
                    },
                    {
                        type: 'line',
                        fill: false,
                        backgroundColor: '#FEC745',
                        borderColor: '#FEC745',
                        data: volumes
                    }
                ]
            },
            options: {
                maintainAspectRatio: false,
                tooltips: {
                    mode: mode,
                    intersect: intersect
                },
                hover: {
                    mode: mode,
                    intersect: intersect
                },
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        gridLines: {
                            display: true
                        },
                        ticks: $.extend({
                            beginAtZero: true,
                            callback: function (value, index, values) {
                                return $scope.numberFormat(value.toFixed(1), 2);
                            }
                        }, ticksStyle)
                    }],
                    xAxes: [{
                        display  : true,
                        gridLines: {
                            display: true
                        },
                        ticks    : ticksStyle
                    }]
                }
            }
        });
    };

    $scope.getQteSalesPerYear = function(lien, firstCall){
        $http({
            url: lien, 
            method: 'POST',
            data: 'year='+$('#select-year-qte-sales-per-year').val(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(
            response=>{
                var data = response.data;
                var poids = [];
                var volumes = [];
                for(var i=0; i<data.length; i++){
                    poids.push(data[i].poids);
                    volumes.push(data[i].volumes);
                }
                if(firstCall == true){
                    $('#graph-qte-sales-per-year').html('<canvas id="qte-sales-per-year-chart" height="200"></canvas>');
                }
                $scope.showQteSalesPerYear(poids, volumes);
            },
            error=>{
                console.log(error);
            }
        );
    };

    $scope.yearQteSalesChange = function(lien){
        $('#graph-qte-sales-per-year').html('<div class="lds-roller" style="display: block; margin: 0 auto;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>');
        $scope.getQteSalesPerYear(lien, true);
    };

    $scope.showMontantSalesPerYear = function(montants){
        var ticksStyle = {
            fontColor: '#495057',
            fontStyle: 'normal'
        };
        var mode = 'index';
        var intersect = true;
        var $salesChart = $('#montant-vente-chart');
        var salesChart = new Chart($salesChart, {
            data: {
                labels: ['JAN', 'FEV', 'MAR', 'AVR', 'MAI', 'JUI', 'JUI', 'AOU', 'SEP', 'OCT', 'NOV', 'DEC'],
                datasets: [
                    {
                        type: 'line',
                        fill: false,
                        backgroundColor: '#00AFAA',
                        borderColor: '#00AFAA',
                        data: montants
                    }
                ]
            },
            options: {
                maintainAspectRatio: false,
                tooltips: {
                    mode: mode,
                    intersect: intersect
                },
                hover: {
                    mode: mode,
                    intersect: intersect
                },
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        gridLines: {
                            display: true
                        },
                        ticks: $.extend({
                            beginAtZero: true,
                            callback: function (value, index, values) {
                                return $scope.numberFormat(value.toFixed(1), 0);
                            }
                        }, ticksStyle)
                    }],
                    xAxes: [{
                        display  : true,
                        gridLines: {
                            display: true
                        },
                        ticks    : ticksStyle
                    }]
                }
            }
        });
    };

    $scope.getMontantSalesPerYear = function(lien, firstCall){
        $http({
            url: lien, 
            method: 'POST',
            data: 'year='+$('#select-montant-sales-year').val(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(
            response=>{
                var data = response.data;
                if(firstCall == true){
                    $('#graph-montant-sales-per-year').html('<canvas id="montant-vente-chart" height="200"></canvas>');
                }
                $scope.showMontantSalesPerYear(data.list);
                $('#montant-vente-total').html($scope.numberFormat(data.total, 0)+' MGA');
            },
            error=>{
                console.log(error);
            }
        );
    };

    $scope.yearMontantSalesChange = function(lien){
        $('#graph-montant-sales-per-year').html('<div class="lds-roller" style="display: block; margin: 0 auto;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>');
        $scope.getMontantSalesPerYear(lien, true);
    };

    $scope.getNombreSalesPerGenre = function(lien){
        $http({
            url: lien, 
            method: 'POST',
            data: 'genre='+$('#select-nombre-sales-genre').val(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(
            response=>{
                var list = response.data.list;
                list.sort((a, b)=>{
                    if(a.label > b.label){
                        return 1;
                    }
                    else if(a.label < b.label){
                        return -1;
                    }
                    return 0;
                });
                var total = parseInt(response.data.total);
                var div = $('#list-genre-vente');
                div.html('');
                $('#nombre-total-vente').html($scope.numberFormat(total, 0));
                for(var i=0; i<list.length; i++){
                    var split = list[i].label.split("("); 
                    div.append('<div class="col-md-4 border-bottom">'+
                            '<span>'+split[0]+'</span>'+
                        '</div>'+
                        '<div class="col-md-8">'+
                            '<div id="nombre-node-'+i+'" class="div-stat-horizontal" style="width: 0px;" data-toggle="tooltip" title="'+list[i].nombre+'"></div>'+
                        '</div>');
                }
                
                if(total <= 0){
                    total = 1;
                }
                for(var i=0; i<list.length; i++){
                    $('#nombre-node-'+i).css('width', (100 * list[i].nombre / total)+'%');
                    $('#nombre-node-'+i).css('background-color', '#00AFAA');
                }
            },
            error=>{
                console.log(error);
            }
        );
    };

    $scope.getQteSalesPerGenre = function(lien){
        $http({
            url: lien, 
            method: 'POST',
            data: 'genre='+$('#select-qte-sales-genre').val(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(
            response=>{
                var list = response.data.list;
                list.sort((a, b)=>{
                    if(a.label > b.label){
                        return 1;
                    }
                    else if(a.label < b.label){
                        return -1;
                    }
                    return 0;
                });
                var totalPoids = parseFloat(response.data.totalPoids);
                var totalVolumes = parseFloat(response.data.totalVolumes);
                var total = totalPoids > totalVolumes ? totalPoids : totalVolumes;
                var div = $('#list-qte-vente');
                div.html('');
                $('#qte-total-vente').html($scope.numberFormat(total, 2));
                for(var i=0; i<list.length; i++){
                    var split = list[i].label.split("("); 
                    div.append('<div class="col-md-4 border-bottom">'+
                            '<span>'+split[0]+'</span>'+
                        '</div>'+
                        '<div class="col-md-8" style="margin-bottom: 5px;">'+
                            '<div id="nombre-node-p-'+i+'" class="div-stat-horizontal-p" style="width: 0px;" data-toggle="tooltip" title="'+list[i].poids+'"></div>'+
                            '<div id="nombre-node-v-'+i+'" class="div-stat-horizontal-v" style="width: 0px;" data-toggle="tooltip" title="'+list[i].volumes+'"></div>'+
                        '</div>');
                }
                
                if(total <= 0){
                    total = 1;
                }
                for(var i=0; i<list.length; i++){
                    $('#nombre-node-p-'+i).css('width', (100 * list[i].poids / total)+'%');
                    $('#nombre-node-v-'+i).css('width', (100 * list[i].volumes / total)+'%');
                    $('#nombre-node-p-'+i).css('background-color', '#F05423');
                    $('#nombre-node-v-'+i).css('background-color', '#FEC745');
                }
            },
            error=>{
                console.log(error);
            }
        );
    };

     $scope.showNbDonVentePerYear = function(don, vente){
        var ticksStyle = {
            fontColor: '#495057',
            fontStyle: 'normal'
        };
        var mode = 'index';
        var intersect = true;
        var $salesChart = $('#nb_don_vente-per-year-chart');
        var salesChart = new Chart($salesChart, {
            data: {
                labels: ['JAN', 'FEV', 'MAR', 'AVR', 'MAI', 'JUI', 'JUI', 'AOU', 'SEP', 'OCT', 'NOV', 'DEC'],
                datasets: [
                    {
                        type: 'line',
                        fill: false,
                        backgroundColor: '#F05423',
                        borderColor: '#F05423',
                        data: don
                    },
                    {
                        type: 'line',
                        fill: false,
                        backgroundColor: '#FEC745',
                        borderColor: '#FEC745',
                        data: vente
                    }
                ]
            },
            options: {
                maintainAspectRatio: false,
                tooltips: {
                    mode: mode,
                    intersect: intersect
                },
                hover: {
                    mode: mode,
                    intersect: intersect
                },
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                        gridLines: {
                            display: true
                        },
                        ticks: $.extend({
                            beginAtZero: true,
                            callback: function (value, index, values) {
                                return $scope.numberFormat(value.toFixed(1), 2);
                            }
                        }, ticksStyle)
                    }],
                    xAxes: [{
                        display  : true,
                        gridLines: {
                            display: true
                        },
                        ticks    : ticksStyle
                    }]
                }
            }
        });
    };

    $scope.getNbDonVentePerYear = function(lien, firstCall){
        $http({
            url: lien, 
            method: 'POST',
            data: 'year='+$('#select-year-don-vente-per-year').val(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(
            response=>{
                var data = response.data;
                var don = [];
                var vente = [];
                for(var i=0; i<data.length; i++){
                    don.push(data[i].don);
                    vente.push(data[i].vente);
                }
                if(firstCall == true){
                    $('#graph-nb-don-vente-per-year').html('<canvas id="nb_don_vente-per-year-chart" height="200"></canvas>');
                }
                $scope.showNbDonVentePerYear(don, vente);
            },
            error=>{
                console.log(error);
            }
        );
    };

    $scope.yearNbDonVenteChange = function(lien){
        $('#graph-nb-don-vente-per-year').html('<div class="lds-roller" style="display: block; margin: 0 auto;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>');
        $scope.getNbDonVentePerYear(lien, true);
    };

    $scope.showVenteByEtat = function(){
        var option = 0;
        option = $('#select-vente_etat').val();
        
        if(option == 1){
            $('.etat0').hide();
            $('.etat1').show();
            $('.etat2').show();         
        }else if(option == 2){
            $('.etat0').show();
            $('.etat1').hide();
            $('.etat2').hide();
        }else{
            console.log(option);
            $('.etat0').show();
            $('.etat1').show();
            $('.etat2').show();
        }
    };

    $scope.closeInfoBienvenu = function(){
        $('.info-bienvenu').remove();
    };

    $scope.setCloseIntoBienvenu = function(lien , etat){
        $scope.loadType = true;
       var data = $.param({
                'etat': etat,
            });

        $http({
                url:lien,
                method:"POST",
                data:data,
                headers:{'Content-type':'application/x-www-form-urlencoded'}
            })
                .then(function(response){
                    $scope.loadType = false;
                    var data = response.data;
                    $("#test-first-time").val(data);
                    
                },function(error){
                    console.log(error);
                    toastr.error("Erreur","Un problème est survenue. Veuillez réessayer.");
                });

        
    };
     $scope.ouvrirModalChoix = function(){
        $('#select-rapport').select2();
        $('#modal-choix').modal();
    };


}]);


$(window).on('load',function(){

    $('#select-rapport').select2();
    $('#select-mois').select2({
            width: '100%',
            placeholder: 'Choisir le mois',
        });
    $('#select-annee').select2({
            width: '100%',
            placeholder: 'Choisir l\'année',
        });


    $(".radio-select").on("ifChanged", function(){
        var etat = $(this).is(":checked");
        getchoixRapport($(this));
    });



  /*$('#table_vente_recente').dataTable({

    "bStateSave": true, 

    "lengthMenu": [
        [5, 15, 20, -1],
        [5, 15, 20, "Tout"] 
    ],
    
    // set the initial value
    "pageLength": 5,
    "language": {
        "lengthMenu": "Afficher _MENU_ éléments par page",
        "zeroRecords": "Aucun élément à afficher",
        "info":"",  // "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
        "search": "Rechercher",
        "paginate":{
          "previous":"Précédent",
          "next": "Suivant"
        }
    },
//            "columnDefs": [{  // set default column settings
//                'orderable': false,
//                'targets': [5]
//            }, {
//                "searchable": false,
//                "targets": [5]
//            }],
    "order": [
        [1, "asc"]
    ] // set first column as a default sort by asc
    });*/
});
