var app = angular.module('registrationApp', []);

var site_url = $("#site_url").val();

var pseudoIsValid = 0;
var telephoneIsValid = false;
var telephoneBenef_1_IsValid = false;
var telephoneBenef_2_IsValid = false;
var twoResponsable = false;

$("#formRegister").validator().on('submit', function (e) {
  if (e.isDefaultPrevented()) {
    // handle the invalid form...
    toastr.error("Erreur","Les champs avec des \"*\" sont obligatoires");
  } else {
  	
  	if(pseudoIsValid==-1){
		toastr.error("Information incomplète","Veuillez vérifier le nom de votre entreprise");
		$("#nom_entreprise").get(0).scrollIntoView();
		$("#nom_entreprise").get(0).focus();
		e.preventDefault();
		return false;
	}
	if(!telephoneIsValid){
		toastr.error("Numéro de téléphone invalide","Veuillez renseigner un numéro associée au pays spécifié");
		e.preventDefault();
		$("#numero_telephone").get(0).scrollIntoView();
		$("#numero_telephone").get(0).focus();
		return false;
	}

	return true;
  }
});

$("#formRegisterBen").validator().on('submit', function (e) {
  if (e.isDefaultPrevented() ) {
    // handle the invalid form...
    toastr.error("Erreur","Les champs avec des \"*\" sont obligatoires");
  } else {
    // everything looks good!
    var test=false;
	var checkbox = $("input[name*='info_nutri']");
	for (var i=0; i<checkbox.length; i++){
		var choix=$(checkbox[i]).is(":checked");
		if (choix==true){
			test=true;
		}
	}
	if(test==false){
		toastr.error("Information incomplète","Veuillez choisir le ou les informations nutritionnelles adaptées aux membres de votre organisation");
		location.hash = "#infoNutri";
		e.preventDefault();
		return false;
	}

	/*if($("input[name='responsable2_nom']").val()=="" && ($("input[name='responsable2_phone']").val()!="" || mailIsValid($("input[name='responsable2_email']").val()))){
		toastr.error("Information incomplète","Veuillez remplir correctement les informations concernant le responsable numéro 2");
		location.hash = "#res2";
		e.preventDefault();
		return false;
	}*/

	var telephoneResponsable1 = $("#responsable1_BenPhone");
	var id_pays = $("#select_ben_pays").val();
	if(! validatePhoneNumberForCompte(id_pays, telephoneResponsable1.val())){
			toastr.error("Erreur","Le numéro de télephone du premier responsable est invalide");
			telephoneResponsable1.get(0).scrollIntoView();
			telephoneResponsable1.get(0).focus();
			return false;
	}
		// verification des email
	if( twoResponsable ){
		
		var telephoneResponsable2 = $("#responsable2_BenPhone");
		var id_pays = $("#select_ben_pays").val();
		if(! validatePhoneNumberForCompte(id_pays, telephoneResponsable2.val())){
				toastr.error("Erreur","Le numéro de télephone du second responsable est invalide");
				telephoneResponsable2.get(0).scrollIntoView();
				telephoneResponsable2.get(0).focus();
				return false;
		}
	}

	var labelTelephoneResponsable = "";
	labelTelephoneResponsable = $scope.getLabelPays(id_pays);
	telephoneResponsable1.val( labelTelephoneResponsable+telephoneResponsable1.val());

		if($scope.twoResponsable || twoResponsable){
			telephoneResponsable2.val( labelTelephoneResponsable+telephoneResponsable2.val());
		}	

	return true;

  }
});
/*$("#formRegisterBen").submit(function(e){
	var test=false;
	var checkbox = $("input[name*='info_nutri']");
	for (var i=0; i<checkbox.length; i++){
		var choix=$(checkbox[i]).is(":checked");
		if (choix==true){
			test=true;
		}
	}
	if(test==false){
		toastr.error("Information incomplète","Veuillez choisir le ou les informations nutritionnelles adaptées aux membres de votre organisation");
		location.hash = "#infoNutri";
		e.preventDefault();
		return false;
	}

	if($("input[name='responsable2_nom']").val()=="" && (("input[name='responsable2_phone']").val()!="" || mailIsValid(("input[name='responsable2_email']").val()))){
		toastr.error("Information incomplète","Veuillez remplir correctement les informations concernant le responsable numéro 2");
		location.hash = "#res2";
		e.preventDefault();
		return false;
	}

	return true;
});*/

app.controller('formRegistrationCtrl', function($scope,$http,$compile,$timeout) {

	$scope.formIsValid = true;
	$scope.twoResponsable = false;
	$scope.loadType = false;
	$scope.notValidTypeEntreprise = false;
	$scope.numAdresse = 1;
	$scope.pseudoValid = false;
	$scope.checkingPseudo = false;
	$scope.posteInEditing = "";
	$scope.mailInvalid = true;
	$scope.mailResp1Invalide = false;
	$scope.mailResp2Invalide = false;
	$scope.typePoste = 1;
	$scope.mdpValid = true;
	$scope.selectPays = [];
	$scope.listPays = "";
	$scope.selectBenPays = [];
	$scope.listBenPays = "";
	$scope.new_type = "" ;

	$scope.validForm = function(){
		if($scope.password==$scope.cpassword){
			$scope.formIsValid = true;
			$("button[name='creer_compte']").removeAttr("disabled");
		}else{
			$scope.formIsValid = false;
			$("button[name='creer_compte']").attr("disabled","");
		}
	}

	$scope.addResponsable = function(){
		$scope.twoResponsable = true;
		twoResponsable = true;

		$timeout(function(){
			$("input[name='responsable2_nom']").attr('required','');
			$("input[name='responsable2_prenom']").attr('required','');
			$("#select_poste2A").attr('required','');
			$("input[name='responsable2_email']").attr('required','');
			$("input[name='responsable2_phone']").attr('required','');

			$('.select2').select2();
			$scope.changeBenPays(0);

			$("#formRegisterBen").validator("update");
		},50);
	}

	$scope.requiredRetype = function(){
		$scope.cpassword = "";
		if($scope.password == ""){
			$scope.mdpValid = true;
		}else{
			if($scope.password.length < 6){
				$scope.mdpValid = false;
			}else{
				$scope.mdpValid = true;
			}
		}
	}

	$scope.addPoste = function(posteSelelector,type){
		$scope.posteInEditing = posteSelelector;
		$scope.typePoste = type;
		$("#modal_new_poste").modal({
			backdrop:'static'
		});
	}

	$scope.createPoste = function(posteSelelector,type){
		$scope.posteInEditing = posteSelelector;
		$scope.typePoste = type;
		$scope.loadType = true;
		var url = site_url+"a-n-p"; 
		var str_type = $("#nouveau_poste").val();
		//var str_type = $scope.new_type;
		//console.log("str : "+str_type);

		if(str_type && str_type!=="" && str_type!=undefined){
			str_type = str_type[0].toUpperCase()+str_type.substr(1,str_type.length-1);

			$scope.notValidTypeEntreprise = false;

			var data = $.param({
				'nomPoste': str_type,
				'typePoste' : $scope.typePoste
			});

			$http({
				url:url,
				method:"POST",
				data:data,
				headers:{'Content-type':'application/x-www-form-urlencoded'}
			})
				.then(function(response){
					$scope.loadType = false;
					var data = response.data;
					
					if(data.idPoste>0){
						var select = $("select.select-poste");
						if(!select.html().includes('<option value="'+data.idPoste+'">')){
							select.append("<option value='"+data.idPoste+"'>"+str_type+"</option>");
						}

						$($scope.posteInEditing+" option[value="+data.idPoste+"]").attr("selected","");
						$($scope.posteInEditing).select2({
												    width: '100%',
												    placeholder: 'Choiser ou ajouter',
												    language: {
												      noResults: function() {
												        return '<button id="no-results-btn" class="btn btn-primary btn-block" onclick="ajouterPoste()">Ajouter poste</a>';
												      },
												    },
												    escapeMarkup: function(markup) {
												      return markup;
												    },
												    });
						$("#modal_new_poste").modal('hide');
					}else{
						toastr.error("Erreur","Impossible d'enregistrer l'information dans la base de donnée. Veuillez réessayer.");
					}
				},function(error){
					console.log(error);
					toastr.error("Erreur","Un problème est survenue. Veuillez réessayer.");
				});
		}else{
			$scope.loadType = false;
			$scope.notValidTypeEntreprise = true;
		}	
	}

	$scope.addBeneficiaire = function(){
		$("#modal_new_beneficiaire").modal({
			backdrop:'static'
		});
	}

	$scope.createBeneficiaire = function(){
		$scope.loadType = true;
		var url = site_url+"utilisateur/acces/addNewBeneficiaireOrganisation"; 
		var str_type = $scope.new_beneficiaire;

		if(str_type && str_type!=="" && str_type!=undefined){
			str_type = str_type[0].toUpperCase()+str_type.substr(1,str_type.length-1);

			$scope.notValidBeneficiaire = false;

			var data = $.param({
				'benOrg': str_type
			});

			$http({
				url:url,
				method:"POST",
				data:data,
				headers:{'Content-type':'application/x-www-form-urlencoded'}
			})
				.then(function(response){
					$scope.loadType = false;
					var data = response.data;
					
					if(data.beneficiaire_organisation_id>0){
						if(!data.exist){
							$("#select_beneficiaire").append("<option value='"+data.beneficiaire_organisation_id+"'>"+str_type+"</option>");

							$("#select_beneficiaire option[value="+data.beneficiaire_organisation_id+"]").attr("selected","");
							$('.select2').select2();
						}
						else{
							$('#select_beneficiaire').select2('val', data.beneficiaire_organisation_id);
						}
					}else{
						toastr.error("Erreur","Impossible d'enregistrer l'information dans la base de donnée. Veuillez réessayer.");
					}
					$("#modal_new_beneficiaire").modal('hide');
				},function(error){
					console.log(error);
					toastr.error("Erreur","Un problème est survenue. Veuillez réessayer.");
				});
		}else{
			$scope.loadType = false;
			$scope.notValidTypeEntreprise = true;
		}	
	}

	$scope.addTypeOrganisation = function(){
		$("#modal_new_type_organisation").modal({
			backdrop:'static'
		});
	}

	$scope.createTypeOrganisation = function(){
		$scope.loadType = true;
		var url = site_url+"utilisateur/acces/addNewTypeOrganisation"; 
		var str_type = $scope.new_type_organisation;

		if(str_type && str_type!=="" && str_type!=undefined){
			str_type = str_type[0].toUpperCase()+str_type.substr(1,str_type.length-1);

			$scope.notValidTypeOrganisation = false;

			var data = $.param({
				'typeOrganisation': str_type
			});

			$http({
				url:url,
				method:"POST",
				data:data,
				headers:{'Content-type':'application/x-www-form-urlencoded'}
			})
				.then(function(response){
					$scope.loadType = false;
					var data = response.data;
					
					if(data.type_organisation_id>0){
						if(!data.exist){
							$("select#select_type_organisation").append("<option value='"+data.type_organisation_id+"'>"+str_type+"</option>");

							$("#select_type_organisation"+" option[value="+data.type_organisation_id+"]").attr("selected","");
							$("#select_type_organisation").select2();
						}
						else{
							$("#select_type_organisation").select2('val', data.type_organisation_id);
						}
					}else{
						toastr.error("Erreur","Impossible d'enregistrer l'information dans la base de donnée. Veuillez réessayer.");
					}
					$("#modal_new_type_organisation").modal('hide');
				},function(error){
					console.log(error);
					toastr.error("Erreur","Un problème est survenue. Veuillez réessayer.");
				});
		}else{
			$scope.loadType = false;
			$scope.notValidTypeOrganisation = true;
		}	
	}

	$scope.addAdresse = function(){
		$compile($("#otherAdress").append('<div class="form-group input-group adr'+$scope.numAdresse+'"><input name="adresse_entreprise'+$scope.numAdresse+'" type="text" class="form-control" placeholder="Autre adresse de votre entreprise"><div class="input-group-append" style="cursor: pointer;" title="Supprimer" ng-click="removeHtml(\'.adr'+$scope.numAdresse+'\')"><span class="input-group-text"><i class="fa fa-minus"></i></span></div></div>'))($scope);
		$scope.numAdresse+=1;
	}

	$scope.removeHtml = function(selector){
		console.log(selector);
		$(selector).remove();
	}

	$scope.addTypeAliment = function(){
		$("#modal_type_aliment").modal({
			backdrop:'static'
		});
	}

	$scope.addInfoNutri = function(){
		$("#modal_info_nutri").modal({
			backdrop:'static'
		});
	}

	$scope.createInfoNutri = function(){
		$scope.loadType = true;
		var url = site_url+"utilisateur/acces/create_info_nutri";
		var str_info = $scope.new_info_nutri; 

		if(str_info && str_info!=="" && str_info!=undefined){
			str_info = str_info[0].toUpperCase()+str_info.substr(1,str_info.length-1);

			$scope.notValidInfoNutri = false;

			var data = $.param({
				'info_nutri': str_info
			});

			$http({
				url:url,
				method:"POST",
				data:data,
				headers:{'Content-type':'application/x-www-form-urlencoded'}
			})
				.then(function(response){
					$scope.loadType = false;
					var data = response.data;
					
					if(data.info_id>0){
						var element = $("#infoNutri");
						if(!element.html().includes('<input name="info_nutri_'+data.info_id+'"')){
							element.append('<label class="check"><input name="info_nutri_'+data.info_id+'" type="checkbox" class="flat-red" checked>'+str_info+'</label>');

							$('input').iCheck({
						      checkboxClass: 'icheckbox_square-blue',
						      radioClass   : 'iradio_square-blue',
						      increaseArea : '20%' // optional
						    });

							toastr.success("Mise à jour information nutritionnelles","La nouvelle information nutritionnelle a été ajoutée avec succès");
						}
						else{
							toastr.success("","L'information nutritionnelle existe déjà");
						}
					}else{
						toastr.error("Erreur","Impossible d'enregistrer l'information dans la base de donnée. Veuillez réessayer.");
					}

					$("#modal_info_nutri").modal('hide');
				},function(error){
					console.log(error);
					toastr.error("Erreur","Un problème est survenue. Veuillez réessayer.");
				});

		}else{
			$scope.loadType = false;
			$scope.notValidInfoNutri = true;
		}	
	}

	$scope.createTypeAliment = function(){
		$scope.loadType = true;
		var url = site_url+"entreprise/entreprise/create_type_aliment"; 
		var str_type = $scope.new_type_aliment;

		if(str_type && str_type!=="" && str_type!=undefined){
			str_type = str_type[0].toUpperCase()+str_type.substr(1,str_type.length-1);

			$scope.notValidTypeEntreprise = false;

			var data = $.param({
				'type_label': str_type
			});

			$http({
				url:url,
				method:"POST",
				data:data,
				headers:{'Content-type':'application/x-www-form-urlencoded'}
			})
				.then(function(response){
					$scope.loadType = false;
					var data = response.data;
					
					if(data.type_id>0){
						$("#typesAliment").append('<label class="check"><input name="type_aliment_'+data.type_id+'" type="checkbox" class="flat-red" checked>'+str_type+'</label>');

						$('input').iCheck({
					      checkboxClass: 'icheckbox_square-blue',
					      radioClass   : 'iradio_square-blue',
					      increaseArea : '20%' // optional
					    });

						$("#modal_type_aliment").modal('hide');

						toastr.success("Mise à jour type d'aliments","Le nouveau type d'aliment a été ajouté avec succès");
					}else{
						toastr.error("Erreur","Impossible d'enregistrer l'information dans la base de donnée. Veuillez réessayer.");
					}
				},function(error){
					console.log(error);
					toastr.error("Erreur","Un problème est survenue. Veuillez réessayer.");
				});
		}else{
			$scope.loadType = false;
			$scope.notValidTypeEntreprise = true;
		}	
	}

	$scope.checkPseudo = function(pseudo){
		if(pseudo==undefined || pseudo=="")
			return;

		var formData = new FormData();
        formData.append('pseudo', pseudo);

		var url = site_url+"utilisateur/acces/available_pseudo";
		$scope.checkingPseudo = true;
		$http({
				url:url,
				method: "POST",
				data: formData,
				headers: {'Content-Type': undefined}
			})
			.then(function(response){
				var data = response.data;

				if(data.available==1){
					pseudoIsValid = 1;
				}else{
					pseudoIsValid = -1;
				}

				$scope.pseudoValid = (data.available==1)?false:true;
				$scope.checkingPseudo = false;

			},function(error){

			});
	};

	$scope.mailIsValid = function(email) {
		var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(String(email).toLowerCase());
	};

	$scope.verifMail = function(){
		var email = $('#responsable1_email').val();
		if($scope.mailIsValid(email)){
			var loader = $('#loader-email');
			loader.show();
			var data = $.param({
				'email': email
			});
			$http({
				url: site_url+'utilisateur/acces/mailExist',
				method: "POST",
				data:data,
				headers: {'Content-type':'application/x-www-form-urlencoded'}
			})
			.then(
				response=>{
					if(response.data.exist){
						$('#msg-email').show();
						$scope.mailInvalid = true;
					}
					else{
						$scope.mailInvalid = false;
						$('#msg-email').hide();
					}
					loader.hide();
				},
				error=>{
					loader.hide();
				}
			);
		}
	};

	$scope.verifMail2 = function(respClass){
		var email = $('#responsable'+respClass+'_email').val();
		var classOther = respClass == '1' ? '2' : '1';
		if($scope.mailIsValid(email)){
			var loader = $('#loader-email-resp'+respClass);
			loader.show();
			var data = $.param({
				'email': email
			});
			$http({
				url: site_url+'utilisateur/acces/mailExist',
				method: "POST",
				data:data,
				headers: {'Content-type':'application/x-www-form-urlencoded'}
			})
			.then(
				response=>{
					if(response.data.exist){
						$('#msg-email-resp'+respClass).show();
						$scope['mailResp'+respClass+'Invalide'] = true;
					}
					else{
						if(email != $('#responsable'+classOther+'_email').val()){
							$scope['mailResp'+respClass+'Invalide'] = false;
							$('#msg-email-resp'+respClass).hide();
						}
						else{
							$('#msg-email-resp'+respClass).show();
							$scope['mailResp'+respClass+'Invalide'] = true;
						}
					}
					loader.hide();
				},
				error=>{
					loader.hide();
				}
			);
		}
	};

	$scope.mailStateIsGood = function(){
		if($scope.twoResponsable){
			if($scope.mailResp1Invalide || $scope.mailResp2Invalide){
				return true;
			}
			else{
				return false;
			}
		}
		else{
			return $scope.mailResp1Invalide;
		}
	};


	$scope.initValPays = function(id){
		$scope.selectPays[id] = $scope.listPays[0].id_pays;
		$scope.changePays(id);
	}

	$scope.teste = function(){
		console.log("eto");
	}
	$scope.changePays = function(id){
		var id_pays = parseInt($("#select_pays").val());
		var labelPhone = "N°";
		for(var i=0; i < $scope.listPays.length; i++){
			if($scope.listPays[i].id_pays == id_pays){
				labelPhone = $scope.listPays[i].prefixe_tel;
			}
		}
		$("#labelTelephone"+id).html(labelPhone);
	};

	$scope.verifyFormatTelephone = function(id){
		var loader = $('#loader-responsable1_phone-resp'+id);
		var idPays = $("#select_pays").val();
		var numeroTelephone = $("#numero_telephone").val();
		
		if($scope.validatePhoneNumberForCompte2(idPays, numeroTelephone)){	
			telephoneIsValid = true;
			loader.show();
			setTimeout(function(){
				loader.hide();
			},2000);

		}else{
			telephoneIsValid = false;
			//toastr.error("Numéro de téléphone invalide","Veuillez renseigner un numéro associée au pays spécifié");
			//$("#numero_telephone").get(0).scrollIntoView();
			//$("#numero_telephone").get(0).focus();
		}
	};

	$scope.validatePhoneNumberForCompte2 = function(id_pays, number){
		if(number != ''){
			 number = number.replace(/\s/g, '').replace(/\-/g,'');

			var regexNumber = "";
			switch(id_pays){
				case "1": 
					regexNumber = /^(\d{9})$/;
					break;
				case "2":
					regexNumber = /^(\d{6})$/;
					break;
				case "3": 
					regexNumber = /^(\d{7})$/;
					break;
				case "4": 
					regexNumber = /^(\d{7})$/;
					break;
				default:
					regexNumber = /^(\d{7})$/;
					break;
			}
			
	  		return regexNumber.test(number);	
		}
			
		return false;
	 
	};

	////////::::::::::  BENEFICIAIRE :::::::::::::: //////////////////
	$scope.initValBenPays = function(id){
		$scope.selectBenPays[id] = $scope.listPays[0].id_pays;
		var isInit = 1;
		$scope.changeBenPays(id, isInit);
	}

	$scope.changeBenPays = function(id, isInit=0){
		if(isInit == 1){
			var id_pays = $scope.listPays[0].id_pays;
		}else{
			var id_pays = parseInt($("#select_ben_pays").val());
		}
		
		var labelPhone = "N°";
		for(var i=0; i < $scope.listPays.length; i++){
			if($scope.listPays[i].id_pays == id_pays){
				labelPhone = $scope.listPays[i].prefixe_tel;
			}
		}
		$(".labelBenTelephone"+id).html(labelPhone);

	};

	$scope.verifyFormatBenefTelephone = function(){
		//var loader = $('#loader-responsable1_phone-resp'+id);
		var idPays = $("#select_ben_pays").val();
		var numeroTelephoneResp1 = $("#numero_telephone").val();
		
		if($scope.validatePhoneNumberForCompte2(idPays, numeroTelephone)){	
			telephoneIsValid = true;
		}else{
			telephoneIsValid = false;
			//toastr.error("Numéro de téléphone invalide","Veuillez renseigner un numéro associée au pays spécifié");
			//$("#numero_telephone").get(0).scrollIntoView();
			//$("#numero_telephone").get(0).focus();
		}
	};

	////////////////////////////////////////////////////////////////// 



});

function mailIsValid(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validatePhoneNumberForCompte(id_pays, number){
		if(number != ''){
			 number = number.replace(/\s/g, '').replace(/\-/g,'');

			var regexNumber = "";
			switch(id_pays){
				case "1": 
					regexNumber = /^(\d{9})$/;
					break;
				case "2":
					regexNumber = /^(\d{6})$/;
					break;
				case "3": 
					regexNumber = /^(\d{7})$/;
					break;
				case "4": 
					regexNumber = /^(\d{7})$/;
					break;
				default:
					regexNumber = /^(\d{7})$/;
					break;
			}
			
	  		return regexNumber.test(number);	
		}
			
		return false;
	 
}


