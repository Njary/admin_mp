$(function () {
            $('.select-categorie').select2({
                width: '100%',
                placeholder: 'Choisisser ou ajouter',
                language: {
                  noResults: function() {
                    return '<button id="no-results-btn" class="btn btn-primary btn-block" onclick="ajouterCategorie(1)">Ajouter catégorie</a>';
                  },
                },
                escapeMarkup: function(markup) {
                  return markup;
                },
            });
            $('.select-type').select2({
                width: '100%',
                placeholder: 'Choisisser ou ajouter',
                language: {
                  noResults: function() {
                    return '<button id="no-results-btn" class="btn btn-primary btn-block" onclick="ajouterType(1)">Ajouter type</a>';
                  },
                },
                escapeMarkup: function(markup) {
                  return markup;
                },
            });
            $('.select-info-nutris').select2({
                width: '100%',
                placeholder: 'Choisisser ou ajouter',
                language: {
                  noResults: function() {
                    return '<button id="no-results-btn" class="btn btn-primary btn-block" onclick="ajouterInfoNutris(1)">Ajouter information</a>';
                  },
                },
                escapeMarkup: function(markup) {
                  return markup;
                },
            });
            $('.select-raison-surplus').select2({
                width: '100%',
                placeholder: 'Choisisser ou ajouter',
                language: {
                  noResults: function() {
                    return '<button id="no-results-btn" class="btn btn-primary btn-block" onclick="ajouterRaisonSurplus(1)">Ajouter raison</a>';
                  },
                },
                escapeMarkup: function(markup) {
                  return markup;
                },
            });

            $('#select-type-info-nutri-modal').select2();
        });

//iCheck for checkbox and radio inputs
$('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
    checkboxClass: 'icheckbox_minimal-blue',
    radioClass   : 'iradio_minimal-blue'
});
//Red color scheme for iCheck
$('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
    checkboxClass: 'icheckbox_minimal-red',
    radioClass   : 'iradio_minimal-red'
});
//Flat red color scheme for iCheck
$('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
    checkboxClass: 'icheckbox_flat-green',
    radioClass   : 'iradio_flat-green'
});