var app = angular.module('resetApp', []);

var site_url = $("#site_url").val();

app.controller('formReset', function($scope,$http,$compile,$timeout) {

	$scope.loading = false;
	$scope.formIsValid = 0;

	$scope.checkMail = function(mail){
		$scope.loading = true;
		$scope.mailStatus = -2;

		var url = site_url+"utilisateur/acces/checkMailResponsable";
		
		$http({
				url:url,
				method:"POST",
				data:$.param({
					'mail': mail
				}),
				headers:{'Content-type':'application/x-www-form-urlencoded'}
			})
			.then(function(response){
				console.log(response);
				var data = response.data;

				if(data.status==1){
					$scope.mailStatus = 1;
				}else if(data.status==-1){
					$scope.mailStatus = -1;
					toastr.error("Erreur","Un problème est survenue lors de l'envoie du mail. Veuillez réessayer");
				}else{
					$scope.mailStatus = 0;
				}
				$scope.loading = false;
			},function(error){
				console.log(error);
				$scope.loading = false;
			});
	}

	$scope.requiredRetype = function(){
		$scope.cpassword = "";
		$scope.formIsValid = -1;
	}

	$scope.validForm = function(){
		if($scope.password==$scope.cpassword){
			$scope.formIsValid = 1;
			$("button[name='creer_compte']").removeAttr("disabled");
		}else{
			$scope.formIsValid = -1;
			$("button[name='creer_compte']").attr("disabled","");
		}
	}

	$scope.reinitPassword = function(utilisateur_id,password){
		if($scope.password!=undefined && $scope.password!="" && $scope.cpassword==$scope.password){
			var url = site_url+"utilisateur/acces/update_password";

			$scope.loading = true;
		
			$http({
					url:url,
					method:"POST",
					data:$.param({
						'utilisateur_id': utilisateur_id,
						'password':password
					}),
					headers:{'Content-type':'application/x-www-form-urlencoded'}
				})
				.then(function(response){
					var data = response.data;

					if(data.status==1){
						toastr.success("Information mis à jour","Votre mot de passe a été modifier avec succès");
						$timeout(function(){
							location.href = site_url+"login";
						},1000);
					}else{
						toastr.error("Erreur","Un problème est survenue lors de la modification du mot de passe");
					}
					$scope.loading = false;
				},function(error){
					console.log(error);
					$scope.loading = false;
				});
		}else{
			toastr.error("Erreur","Remplir correctement les deux champs pour valider votre nouveau mot de passe.");
		}
	}

});