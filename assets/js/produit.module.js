var app = angular.module('ProduitApp', []);

app.controller('ProduitCtrl', ["$scope", "$http", "$compile", function($scope, $http, $compile) {

  $scope.PRODUIT = {};
  $scope.PRODUIT.changeToDonationMessage = '';
  $scope.PRODUIT.Donation_id = '';

   $scope.showAlert = function(title, message){
        $('#modal-alert-title').html(title);
        $('#modal-alert-message').html(message);
        $('#modal-alert').modal('show');
    };

    $scope.showLoading = function(title){
        $('#modal-loading-title').html(title);
        $('#modal-loading').css('display','block');
    };

    $scope.hideLoading = function(){
        $('#modal-loading').css('display','none');
    };
  
  $scope.giveIdToChange = function(id){
    //console.log('mandeha v s ato e');
    $scope.PRODUIT.Donation_id = id;
    console.log($scope.PRODUIT.Donation_id);
    //$('#produitToChangeDonation').val(idDonation);
  };

  $scope.changeProduitToDonation = function(lien){
    console.log('mandeha ato e');
    
    var idDonation = $scope.PRODUIT.Donation_id;
    console.log(idDonation);

    var formData = new FormData();
    formData.append('donation_identity', idDonation)
    
    $http({
        url: lien, 
        method: 'POST',
        data: formData,
        headers: {'Content-Type': undefined}, 
        transformRequest: angular.identity
    })
    .then(response=>{
        var data = response.data;
        if(data.success){
            $scope.showAlert('', data.message);
            setTimeout(function(){ 
               window.location.href = BASE_URL + "produit";
            },4000);
        }
        else{
            $scope.showAlert('', data.message);
        }
    }, error=>{
        $scope.showAlert('', 'Une erreur est survenue pendant l\'opération');
    });
  };

}]);


$(window).on('load',function(){
  $('#table_donation').dataTable({

    "bStateSave": true, 

    "lengthMenu": [
        [5, 15, 20, -1],
        [5, 15, 20, "Tout"] 
    ],
    
    // set the initial value
    "pageLength": 5,
    "language": {
        "lengthMenu": "Afficher _MENU_ éléments par page",
        "zeroRecords": "Aucun élément à afficher",
        "info":"",  // "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
        "search": "Rechercher",
        "paginate":{
          "previous":"Précédent",
          "next": "Suivant"
        }
    },
//            "columnDefs": [{  // set default column settings
//                'orderable': false,
//                'targets': [5]
//            }, {
//                "searchable": false,
//                "targets": [5]
//            }],
    "order": [
        [0, "asc"]
    ] // set first column as a default sort by asc
  });
  $('#table_vente').dataTable({

    "bStateSave": true, 

    "lengthMenu": [
        [5, 15, 20, -1],
        [5, 15, 20, "Tout"] 
    ],
    
    // set the initial value
    "pageLength": 5,
    "language": {
        "lengthMenu": "Afficher _MENU_ éléments par page",
        "emptyTable": "Aucun élément à afficher",
        "info":"",  // "Affichage de l'élément _START_ à _END_ sur _TOTAL_ éléments",
        "search": "Rechercher",
        "paginate":{
          "previous":"Précédent",
          "next": "Suivant"
        }
    },
//            "columnDefs": [{  // set default column settings
//                'orderable': false,
//                'targets': [5]
//            }, {
//                "searchable": false,
//                "targets": [5]
//            }],
    "order": [
        [0, "asc"]
    ] // set first column as a default sort by asc
  });

  var choice = $('#choix_active').val();
  console.log(choice);
    if(choice == 1){
        
        $("#tabDon").removeAttr("class");
        $("#tab_1").removeAttr("class");
        $('#tabDon').attr("class","nav-link active show");
        $('#tab_1').attr("class","tab-pane active show");

        $("#tabVente").removeAttr("class");
        $("#tab_2").removeAttr("class");
        $('#tabVente').attr("class","nav-link");
        $('#tab_2').attr("class","tab-pane");

    }else if(choice == 2){
        
        $("#tabDon").removeAttr("class");
        $("#tab_1").removeAttr("class");
        $('#tabDon').attr("class","nav-link ");
        $('#tab_1').attr("class","tab-pane");

        $("#tabVente").removeAttr("class");
        $("#tab_2").removeAttr("class");
        $('#tabVente').attr("class","nav-link active show");
        $('#tab_2').attr("class","tab-pane active show");

    }else{
        
        $("#tabDon").removeAttr("class");
        $("#tab_1").removeAttr("class");
        $('#tabDon').attr("class","nav-link active show");
        $('#tab_1').attr("class","tab-pane active show");

        $("#tabVente").removeAttr("class");
        $("#tab_2").removeAttr("class");
        $('#tabVente').attr("class","nav-link");
        $('#tab_2').attr("class","tab-pane");
    }
        

});