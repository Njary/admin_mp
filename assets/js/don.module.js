var app = angular.module('DonApp', []);

app.controller('DonCtrl', ["$scope", "$http", "$compile", function($scope, $http, $compile) {

    $scope.pushDate = null;
    $scope.images = [];
    $scope.excelImages = []; // contient les images pour les produit depuis import excle
    $scope.productFromExcel = []; // contient les produits depuis import excel
    $scope.photoExcelIndex = 0; // savoir a quel index du produit (depuis import excel) sont les photos a importer

    $scope.formList = [
        {
            id: '1',
            images: []
        }
    ];
    $scope.formClicked = ''; // alalana ny formulaire miasa, mcontenir ny id ny element ny formulaire

    $scope.showAlert = function(title, message){
        $('#modal-alert-title').html(title);
        $('#modal-alert-message').html(message);
        $('#modal-alert').modal('show');
    };

    $scope.showLoading = function(title){
        $('#modal-loading-title').html(title);
        $('#modal-loading').css('display','block');
    };

    $scope.hideLoading = function(){
        $('#modal-loading').css('display','none');
    };

    $scope.formClick = function(id){
        $scope.formClicked = id;
    };

    $scope.getNumber = function(value){
        var result = parseFloat(value.toString().replace(',', '.'));
        if(!isNaN(result) && result > 0){
            return result;
        }
        else{
            return 0;
        }
    };

    $scope.mileSeparator = function(nStr) {
        nStr += '';
        var x = nStr.split('.');
        var x1 = x[0];
        var x2 = x.length > 1 ? '.' + x[1] : '';
        var rgx = /(\d+)(\d{3})/;
        while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ' ' + '$2');
        }
        return x1 + x2;
    }

    $scope.formatNumberToMileSep = function(selector){
        var value = $(selector).val().trim().replace(/\s/g,"");
        var newVal = $scope.mileSeparator(value);
        $(selector).val(newVal);
    }

    $scope.addNewType = function(lien ){


        $http({
            url: lien, 
            method: 'POST',
            data: 'type='+$('#type-name-modal').val().trim(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(response=>{
            var data = response.data;
            if(data.success){
                $('#type-name-modal').val("");
                var select = $('#select-type'+$scope.formClicked);
                select.append('<option value="'+data.id_type_produit+'" >'+data.label+'</option>');
                select.select2('val', data.id_type_produit);
                for(var i=0; i<$scope.formList.length; i++){
                    if($scope.formList[i].id != $scope.formClicked){
                        $('#select-type'+$scope.formList[i].id).append('<option value="'+data.id_type_produit+'" >'+data.label+'</option>');
                    }
                }
                $scope.initDonSelect2($scope.formClicked);
            }
            else{
                $scope.showAlert('', data.message);
            }
        }, error=>{
            $scope.showAlert('', 'Une erreur est survenue pendant l\'ajout du nouveau type');
        });
    };

    $scope.addNewInfoNutris = function(lien){
        $http({
            url: lien, 
            method: 'POST',
            data: 'infoNutris='+$('#info-nutris-name-modal').val(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(response=>{
            var data = response.data;
            if(data.success){
                $('#info-nutris-name-modal').val("");
                var select = $('#select-info-nutris'+$scope.formClicked);
                select.append('<option value="'+data.id_information_nutritionnelle+'" >'+data.label+'</option>');
                select.select2("val", select.select2("val").concat(data.id_information_nutritionnelle));
                for(var i=0; i<$scope.formList.length; i++){
                    if($scope.formList[i].id != $scope.formClicked){
                        $('#select-info-nutris'+$scope.formList[i].id).append('<option value="'+data.id_information_nutritionnelle+'" >'+data.label+'</option>');
                    }
                }
                $('#select-type-info-nutri-modal').append('<option value="'+data.id_information_nutritionnelle+'" >'+data.label+'</option>');
                $scope.initDonSelect2($scope.formClicked);
            }
            else{
                $scope.showAlert('', data.message);
            }
        }, error=>{
            $scope.showAlert('', 'Une erreur est survenue pendant l\'ajout de la nouvelle information nutritionnelle');
        });
    };

    $scope.addNewRaisonSurplus = function(lien){
        $http({
            url: lien, 
            method: 'POST',
            data: 'raisonSurplus='+$('#raison-surplus-name-modal').val(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(response=>{
            var data = response.data;
            if(data.success){
                $('#raison-surplus-name-modal').val("");
                var select = $('#select-raison-surplus'+$scope.formClicked);
                select.append('<option value="'+data.id_raison_surplus+'" >'+data.label+'</option>');
                select.select2("val", select.select2("val").concat(data.id_raison_surplus));
                for(var i=0; i<$scope.formList.length; i++){
                    if($scope.formList[i].id != $scope.formClicked){
                        $('#select-raison-surplus'+$scope.formList[i].id).append('<option value="'+data.id_raison_surplus+'" >'+data.label+'</option>');
                    }
                }
                $scope.initDonSelect2($scope.formClicked);
            }
            else{
                $scope.showAlert('', data.message);
            }
        }, error=>{
            $scope.showAlert('', 'Une erreur est survenue pendant l\'ajout du raison de surplus');
        });
    };

    $scope.clickGroupement = function(arg){
        if(arg==1){
            $('#input-groupement-total').prop('checked', false);
        }
        else{
            $('#input-groupement-unite').prop('checked', false);
        }
        console.log('click');
    };

    $scope.onFileImageChange = function(element){
        var files = $('#input-file-image'+$scope.formClicked).prop('files');
        for(var i = 0 ; i < files.length ; i++){
            if( $scope.testeImage(files[i].type)!=1 ){
                toastr.error("Erreur","Veuillez choisir des fichiers de type image");
                return;
            }

        }
        var divFilePreview = $('#div-file-preview'+$scope.formClicked);
        $scope.showImage(divFilePreview, files, 0);
    };

    $scope.getImgExt = function(imgName){
        var strs = imgName.split('.');
        if(strs.length > 1){
            return strs[strs.length-1];
        }
        return 'png';
    };

    $scope.imageExist = function(dataURL, array){
        var existe = false;
        for(var i in array){
            if(!array[i].fromServer){
                if(array[i].dataURL == dataURL){
                    existe = true;
                    break;
                }
            }
        }
        return existe;
    };
    $scope.testeImage = function(typeFile){
        var strs = typeFile.split('/');
        if(strs[0]=='image'){
            return 1;
        }
        return 0;
    };

    $scope.showImage = function(divFilePreview, files, index, array){
        if(index < files.length){
            var reader = new FileReader();
            var arrayName = 1;
            var _index = Date.now() + index;
            var idForm;
            reader.onloadend = function () {
                var newPhoto = {
                    extension: $scope.getImgExt(files[index].name),
                    dataURL: reader.result,
                    index: _index,
                    fromServer: false
                };
                var existe = false;
                if(array != undefined){
                    arrayName = 2;
                    existe = $scope.imageExist(newPhoto.dataURL, array);
                    if(!existe){
                        array.push(newPhoto);
                    }
                }
                else{
                    var arrayForm;
                    for(var i=0; i<$scope.formList.length; i++){
                        if($scope.formList[i].id == $scope.formClicked){
                            arrayForm = $scope.formList[i].images;
                            idForm = $scope.formClicked;
                            break;
                        }
                    }
                    existe = $scope.imageExist(newPhoto.dataURL, arrayForm);
                    if(!existe){
                        arrayForm.push(newPhoto);
                    }
                }
                if(!existe){
                    divFilePreview.append($compile('<div class="col-6 div-img-preview" ><img src="'+reader.result+'" /><button ng-click="removeImage('+arrayName+', '+_index+', '+idForm+')" class="btn btn-secondary"><i class="fa fa-trash" ></i></button></div>')($scope));
                }
                $scope.showImage(divFilePreview, files, index+1, array);
            }
            if (files[index]) {
                reader.readAsDataURL(files[index]);
            }
        }
    };

    $scope.removeImage = function(arrayName, index, idForm){
        var divFilePreview;
        var array;
        if(arrayName == 1){
            divFilePreview = $('#div-file-preview'+idForm);
            for(var i=0; i<$scope.formList.length; i++){
                if($scope.formList[i].id == idForm){
                    array = $scope.formList[i].images;
                    break;
                }
            }
        }
        else{
            divFilePreview = $('#div-image-preview-excel');
            array = $scope.excelImages;
        }

        for(var i=0; i<array.length; i++){
            if(array[i].index == index){
                array.splice(i, 1);
                break;
            }
        }

        divFilePreview.html('');
        for(var i=0; i<array.length; i++){
            if(array[i].fromServer){
                divFilePreview.append($compile('<div class="col-6 div-img-preview" ><img src="'+array[i].url+'" /><button ng-click="removeImage('+arrayName+', '+array[i].index+', '+idForm+')" class="btn btn-secondary"><i class="fa fa-trash" ></i></button></div>')($scope));
            }
            else{
                divFilePreview.append($compile('<div class="col-6 div-img-preview" ><img src="'+array[i].dataURL+'" /><button ng-click="removeImage('+arrayName+', '+array[i].index+', '+idForm+')" class="btn btn-secondary"><i class="fa fa-trash" ></i></button></div>')($scope));
            }
        }
    };

     $scope.removeImageProduct = function(id_photo_produit){
        $("#div-img-product"+id_photo_produit).remove();
    };

    $scope.showPhotoPicker = function(idForm, baseUrl, lien, forExcel){
        $('#modal-photo-picker').modal('show');
        var divPreview = $('#div-file-preview-modal');
        var loading = $('#lds-roller-photo-modal');
        divPreview.html('');
        loading.show();
        $scope.formClicked = idForm;
        $http({
            url: lien, 
            method: 'POST',
            data: 'nom='+$('#input-photo-modal').val().trim(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(
            response=>{
                loading.hide();
                var liste = response.data;
                for(var i=0; i<liste.length; i++){
                    if(forExcel == undefined){
                        divPreview.append($compile('<div class="col-3 div-img-preview" ><img id="img-photo-'+liste[i].id_photo_produit+'" class="photo-in-picker" ng-click="clickPhotoFromServer('+idForm+','+liste[i].id_photo_produit+')" src="'+baseUrl+liste[i].url+'" /><i ng-show="photoIsChoosing('+idForm+','+liste[i].id_photo_produit+')" class="fa fa-check"></i></div>')($scope));
                    }
                    else{
                        divPreview.append($compile('<div class="col-3 div-img-preview" ><img id="img-photo-'+liste[i].id_photo_produit+'" class="photo-in-picker" ng-click="clickPhotoFromServer('+idForm+','+liste[i].id_photo_produit+', '+forExcel+')" src="'+baseUrl+liste[i].url+'" /><i ng-show="photoIsChoosing('+idForm+','+liste[i].id_photo_produit+', '+forExcel+')" class="fa fa-check"></i></div>')($scope));
                    }
                }
            },
            error=>{
                loading.hide();
                divPreview.html("Pas encore d'image du côté du serveur");
            }
        );
    };

    $scope.searchPhoto = function(baseUrl, lien, forExcel){
        var divPreview = $('#div-file-preview-modal');
        var loading = $('#lds-roller-photo-modal');
        loading.show();
        $http({
            url: lien, 
            method: 'POST',
            data: 'nom='+$('#input-photo-modal').val().trim(),
            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
        })
        .then(
            response=>{
                loading.hide();
                divPreview.html('');
                var liste = response.data;
                for(var i=0; i<liste.length; i++){
                    if(forExcel == undefined){
                        divPreview.append($compile('<div class="col-3 div-img-preview" ><img id="img-photo-'+liste[i].id_photo_produit+'" class="photo-in-picker" ng-click="clickPhotoFromServer('+$scope.formClicked+','+liste[i].id_photo_produit+')" src="'+baseUrl+liste[i].url+'" /><i ng-show="photoIsChoosing('+$scope.formClicked+','+liste[i].id_photo_produit+')" class="fa fa-check"></i></div>')($scope));
                    }
                    else{
                        divPreview.append($compile('<div class="col-3 div-img-preview" ><img id="img-photo-'+liste[i].id_photo_produit+'" class="photo-in-picker" ng-click="clickPhotoFromServer('+$scope.formClicked+','+liste[i].id_photo_produit+','+forExcel+')" src="'+baseUrl+liste[i].url+'" /><i ng-show="photoIsChoosing('+$scope.formClicked+','+liste[i].id_photo_produit+','+forExcel+')" class="fa fa-check"></i></div>')($scope));
                    }
                }
            },
            error=>{
                loading.hide();
                divPreview.html("Pas encore d'image du côté du serveur");
            }
        );
    };

    $scope.photoIsChoosing = function(idForm, id_photo_produit, forExcel){
        var array;
        if(forExcel == undefined){
            for(var i=0; i<$scope.formList.length; i++){
                if($scope.formList[i].id == idForm){
                    array = $scope.formList[i].images;
                    break;
                }
            }
        }
        else{
            array = $scope.excelImages;
        }
        var existe = false;
        for(var i=0; i<array.length; i++){
            if(array[i].fromServer){
                if(array[i].idPhoto == id_photo_produit){
                    return true;
                }
            }
        }
        return false;
    };

    $scope.clickPhotoFromServer = function(idForm, id_photo_produit, forExcel){
        var divFilePreview;
        var arrayName = 1;
        if(forExcel == undefined){
            divFilePreview = $('#div-file-preview'+idForm);
        }
        else{
            divFilePreview = $('#div-image-preview-excel');
            arrayName = 2;
        }
        var url = $('#img-photo-'+id_photo_produit).prop('src');
        var _index = Date.now();
        var newPhoto = {
            url: url,
            idPhoto: id_photo_produit,
            index: _index,
            fromServer: true
        };
        var array;
        if(forExcel == undefined){
            for(var i=0; i<$scope.formList.length; i++){
                if($scope.formList[i].id == idForm){
                    array = $scope.formList[i].images;
                    break;
                }
            }
        }
        else{
            array = $scope.excelImages;
        }
        var existe = false;
        for(var i=0; i<array.length; i++){
            if(array[i].fromServer){
                if(array[i].idPhoto == id_photo_produit){
                    existe = true;
                    _index = array[i].index;
                    break;
                }
            }
        }
        if (existe) {
            $scope.removeImage(arrayName, _index, idForm);
        }
        else{
            array.push(newPhoto);
            divFilePreview.append($compile('<div class="col-6 div-img-preview" ><img src="'+url+'" /><button ng-click="removeImage('+arrayName+', '+_index+', '+idForm+')" class="btn btn-secondary"><i class="fa fa-trash" ></i></button></div>')($scope));
        }
    };

    $scope.dateIsGood = function(date){
        if(date == ""){
            return false;
        }
        else{
            var strs = date.split('/');
            if(strs.length != 3){
                return false;
            }
            else{
                if(parseInt(strs[2]) < 2018){
                    return false;
                }
                else{
                    return true;
                }
            }
        }
    };

    // maka ny datetime au format mysql yyyy-mm-jj hh:mm:ss
    $scope.getDatetimeString = function(){
        var datetime = new Date(Date.now());
        var date = datetime.getFullYear()+'-'+(datetime.getMonth()+1)+'-'+datetime.getDate();
        var time = datetime.getHours()+':'+datetime.getMinutes()+':'+datetime.getSeconds();
        return date+' '+time;
    }

    $scope.formIsGood = function(formIndex, idForm){
        //console.log('aty e');

        var nom = $("#input-nom"+idForm).val().trim();
        var poids = $scope.getNumber($("#input-poids"+idForm).val().trim().replace(/\s/g,""));
        var prix = $scope.getNumber($("#input-prix"+idForm).val().trim().replace(/\s/g,""));
        //var groupement = $("#input-groupement-unite"+idForm).prop('checked') ? 1 : ($("#input-groupement-portion"+idForm).prop('checked') ? 3 : 2);
        var groupement = $("#input-groupement-unite"+idForm).prop('checked') ? 1 : 2;
        if(groupement == 2){
            var quantite = 1;
        }else{
            var quantite = parseInt($scope.getNumber($("#input-quantite"+idForm).val().trim().replace(/\s/g,"")));
        }
        //var quantite = parseInt($scope.getNumber($("#input-quantite"+idForm).val().trim()));
        var typeDate = $("#input-choix-date-peremption"+idForm).prop('checked') ? 1 : 2;
        var datePeremption = $('#input-date-peremption'+idForm).val();
        var categorie = $("#select-categorie"+idForm).val();
        var type = $("#select-type"+idForm).val();
        /*var infoNutris = $("#select-info-nutris"+idForm).select2('val').toString().split(",");
        var raisonSurplus = $("#select-raison-surplus"+idForm).select2('val').toString().split(",");*/

        //facultatif
        var volume = $("#input-volume"+idForm).val().trim();

        //fin facultatif
        var errorMsg = "";
        var isValide = true;

        if(volume != '' && $scope.getNumber( volume.replace(/\s/g,"") )==0){
           console.log("volume = "+volume);
            $("#input-volume"+idForm).addClass("erreur_input");
            isValide = false;
            var html = '<div class="message-erreur"><span class="message_erreur">Le volume doit être un nombre positif<span></div>'
            $("#div-input-volume"+idForm).append(html);
        }
        

        
        if(nom == ""){
            errorMsg = "Veuillez definir le nom du produit";
            isValide = false;
        }
        if(nom == ""){
            $("#input-nom"+idForm).addClass("erreur_input");
            var html = '<div class="message-erreur"><span class="message_erreur">Le nom doit être défini<span></div>'
            $("#div-input-nom"+idForm).append(html);
        }

        if(isValide && poids <= 0){
            errorMsg = "Le poids du produit est invalide";
            isValide = false;
        }
         if( poids <= 0){
            $("#input-poids"+idForm).addClass("erreur_input");
            var html = '<div class="message-erreur"><span class="message_erreur">Le poids doit être un nombre positif<span></div>'
            $("#div-input-poids"+idForm).append(html);
        }

        if(isValide && prix <= 0){
            errorMsg = "Le prix du produit est invalide";
            isValide = false;
        }
        if( prix <= 0){
            $("#input-prix"+idForm).addClass("erreur_input");
             var html = '<div class="message-erreur"><span class="message_erreur">Le prix doit être un nombre positif<span></div>'
            $("#div-input-prix"+idForm).append(html);
        }

        if(isValide && quantite <= 0){
            errorMsg = "La quantité du produit est invalide";
            isValide = false;
        }
        if( quantite <= 0) {
            $("#input-quantite"+idForm).addClass("erreur_input");
             var html = '<div class="message-erreur"><span class="message_erreur">La quantité doit être un nombre positif<span></div>'
            $("#div-input-quantite"+idForm).append(html);
        }

        if(isValide && !$scope.dateIsGood(datePeremption)){
            errorMsg = "La date de péremption doit être définie";
            isValide = false;
        }
        if( !$scope.dateIsGood(datePeremption)){
            $("#input-date-peremption-production"+idForm).addClass("erreur_input");
             var html = '<div class="message-erreur"><span class="message_erreur">La date de péremption ou production est invalide<span></div>'
            $("#div-input-date-peremption-production"+idForm).append(html);
        }
        if( $scope.dateIsGood(datePeremption)){
            
            if(typeDate == 1){
                var toDay = new Date(Date.now());
                //console.log('newDate / '+toDay);
                var dd = toDay.getDate();
                var mm = toDay.getMonth()+1;
                var yyyy = toDay.getFullYear();
                var dayLimite = dd + 5;

                if(dd<10) {
                    dd = '0'+dd
                } 
                if(mm<10) {
                    mm = '0'+mm
                } 
                toDay = mm + '/' + dd + '/' + yyyy;

                if(dayLimite<10){
                    dayLimite = '0'+dayLimite
                }

                //console.log('datePeremption / '+datePeremption);
                var dateLimite = mm + '/' + dayLimite + '/' + yyyy;
                /*console.log('dayLimite / '+dayLimite);
                console.log('dateLimite / '+dateLimite);*/
                
                var dateDePeremption = datePeremption.split("/");
                datePeremption = dateDePeremption[1]+'/'+dateDePeremption[0]+'/'+dateDePeremption[2];

                var dLimite = new Date(dateLimite);
                var dPeremption = new Date(datePeremption);

                /*console.log('dLimite / '+dLimite);
                console.log('dPeremption / '+dPeremption);*/
                
                if(dPeremption.getTime() <= dLimite.getTime()){
                    errorMsg = "La date de péremption est invalide, les dates qui sont moins de 1 à 5 jours avant la date de péremption ne sont pas acceptées";
                    isValide = false;
                    $("#input-date-peremption-production"+idForm).addClass("erreur_input");
                    var html = '<div class="message-erreur"><span class="message_erreur">La date de péremption ou production est invalide<span></div>'
                    $("#div-input-date-peremption-production"+idForm).append(html);
                } 
                
            }
        }

        if(categorie == ""){
            errorMsg = "La catégorie du produit doit être définie";
            isValide = false;
             $("#select-categorie"+idForm).select2({ containerCssClass : "erreur_input" });
            var html = '<div class="message-erreur"><span class="message_erreur">La catégorie du produit doit être définie<span></div>'
            $("#div-select-categorie"+idForm).append(html);
        }
        
        var categorieNonValide = [5,6,7];
        if(categorieNonValide.includes( parseInt($scope.getNumber(categorie)) ) ){
            errorMsg = "Les catégories de produit Aliments cuits, Aliments préparé et Produits laitiers ne sont pas possible en donation car DHL ne peuvent pas transporter ces dons. La catégorie du produit est invalide ";
            isValide = false;
            $("#select-categorie"+idForm).select2({ containerCssClass : "erreur_input" });
            var html = '<div class="message-erreur"><span class="message_erreur">Les catégories de produit Aliments cuits, Aliments préparé et Produits laitiers ne sont pas possible en donation car DHL ne peuvent pas transporter ces dons. La catégorie du produit est invalide<span></div>'
            $("#div-select-categorie"+idForm).append(html);
        }

        if( type == ""){
            errorMsg = "Le type du produit est invalide";
            isValide = false;
            $("#select-type"+idForm).select2({ containerCssClass : "erreur_input" });
            var html = '<div class="message-erreur"><span class="message_erreur">Le type du produit doit être défini<span></div>'
            $("#div-select-type"+idForm).append(html);
        }
        
        /*if(isValide && infoNutris.length <= 0){
            errorMsg = "Il faut choisir au moins une information nutritionnelle du produit";
            isValide = false;
        }*/
        /*if(isValide && raisonSurplus.length <= 0){
            errorMsg = "Il faut choisir au moins une raison de surplus au produit";
            isValide = false;
        }*/
        /*if(isValide && $scope.formList[formIndex].images.length <= 0){
            errorMsg = "Il faut choisir au moins une photo pour le produit";
            isValide = false;
        }*/

        return {isValide: isValide, errorMsg: errorMsg};
    };

    $scope.validateForms = function(lien){
        $scope.showLoading('Verification des données');
        var dataIsGood = true;
        $scope.removeMessageErreur();
        for(var i=0; i<$scope.formList.length; i++){
            var verification = $scope.formIsGood(i, $scope.formList[i].id);
            dataIsGood = verification.isValide;
            if(!dataIsGood){
                $scope.hideLoading();
                //$scope.showAlert('Erreur', verification.errorMsg+' dans Formulaire '+(i+1));
                //break;
            }
        }
        if(dataIsGood){
            $('#modal-loading-title').html('Enregistrement');
            if($scope.pushDate == null){
                $scope.pushDate = $scope.getDatetimeString();
            }
            $scope.pushForm(lien);
        }
    };

    $scope.pushForm = function(lien){
        var formIndex = $scope.formList.length - 1;
        var idForm = $scope.formList[formIndex].id;

        var statutDonModif = $('#statut_don_modif').val().trim();
        var id_produit = $("#id_produit").val().trim();
        var id_donation = $("#id_donation").val().trim();
        var id_etat_donation = $("#id_etat_donation").val().trim();

        var nom = $("#input-nom"+idForm).val().trim();
        //var groupement = $("#input-groupement-unite"+idForm).prop('checked') ? 1 : ($("#input-groupement-portion"+idForm).prop('checked') ? 3 : 2);
        var groupement = $("#input-groupement-unite"+idForm).prop('checked') ? 1 : 2;
        var poids = $scope.getNumber($("#input-poids"+idForm).val().trim().replace(/\s/g,""));
        var volume = $scope.getNumber($("#input-volume"+idForm).val().trim().replace(/\s/g,""));
        var prix = $scope.getNumber($("#input-prix"+idForm).val().trim().replace(/\s/g,""));
        if(groupement == 2){
            var quantite = 1;
        }else{
            var quantite = parseInt($scope.getNumber($("#input-quantite"+idForm).val().trim().replace(/\s/g,"")));
        }
        var typeDate = $("#input-choix-date-peremption"+idForm).prop('checked') ? 1 : 2;
        var datePeremption = $('#input-date-peremption'+idForm).val();
        var categorie = $("#select-categorie"+idForm).val();
        var type = $("#select-type"+idForm).val();
        var infoNutris = $("#select-info-nutris"+idForm).select2('val').toString().split(",");
        var raisonSurplus = $("#select-raison-surplus"+idForm).select2('val').toString().split(",");
        var refrigere = $("#input-refrigere"+idForm).is(":checked") ? 1 : 0; // true na false
        var congele = $("#input-congele"+idForm).is(":checked") ? 1 : 0;
        var consoDirect = $("#input-consoDirect"+idForm).is(":checked") ? 1 : 0;
        var description = $("#textarea-description"+idForm).val().trim();

        var formData = new FormData();

        formData.append('statutDonModif', statutDonModif);
        formData.append('idProduit', id_produit); 
        formData.append('idDonation', id_donation);  
        formData.append('idEtatDonation', id_etat_donation);

        formData.append('pushDate', $scope.pushDate);
        formData.append('nom', nom);
        formData.append('groupement', groupement);
        formData.append('poids', poids);
        formData.append('volume', volume);
        formData.append('quantite', quantite);
        formData.append('prix', prix);
        formData.append('refrigere', refrigere);
        formData.append('congele', congele);
        formData.append('consoDirect', consoDirect);
        formData.append('description', description);
        formData.append('typeDate', typeDate);
        formData.append('date', datePeremption);
        formData.append('categorie', categorie);
        formData.append('type', type);
        formData.append('NbInfoNutris', infoNutris.length);
        for(var i=0; i<infoNutris.length; i++){
            formData.append('infoNutris'+i, infoNutris[i]);
        }
        formData.append('NbRaisonSurplus', raisonSurplus.length);
        for(var i=0; i<raisonSurplus.length; i++){
            formData.append('raisonSurplus'+i, raisonSurplus[i]);
        }
        formData.append('NbImages', $scope.formList[formIndex].images.length);
        for(var i=0; i<$scope.formList[formIndex].images.length; i++){
            if($scope.formList[formIndex].images[i].fromServer){
                formData.append('fromServer'+i, 1);
                formData.append('idPhoto'+i, $scope.formList[formIndex].images[i].idPhoto);
            }
            else{
                formData.append('fromServer'+i, 0);
                formData.append('image'+i+'ext', $scope.formList[formIndex].images[i].extension);
                formData.append('image'+i+'data', $scope.formList[formIndex].images[i].dataURL);
            }
        }

        formData.append('isDraft', 0);

        $http({
            method: 'POST',
            url: lien,
            data: formData,
            headers: {'Content-Type': undefined},
            transformRequest: angular.identity
        })
        .then(
            response=>{
                if(response.data.success){
                    if(formIndex == 0){
                        $scope.hideLoading();
                        $scope.showAlert('',response.data.message);
                        $("#input-nom1").val('');
                        $("#input-poids1").val('');
                        $("#input-volume1").val('');
                        $("#input-prix1").val('');
                        $("#input-quantite1").val('');
                        $("#input-date-peremption1").val('');
                        $("#select-categorie1").select2('val', '');
                        $("#select-type1").select2('val', '');
                        $("#select-info-nutris1").select2('val', '');
                        $("#select-raison-surplus1").select2('val', '');
                        $("#input-congele1, #input-refrigere1, #input-consoDirect1").iCheck('uncheck');
                        $("#textarea-description1").val('');
                        $('#div-file-preview1').html('');
                        $scope.formList[formIndex].images = [];
                        $scope.pushDate = null;

                        $scope.showAlert('Merci','Votre produit est bien ajouté ! Vous allez recevoir un e-mail quand DHL passera pour la récupération de vos surplus.  Pour plus de détails regardez sur votre tableau de bord');
                    }
                    else{
                        $scope.formList.splice(formIndex, 1);
                        $('#card'+idForm).remove();
                        $scope.pushForm(lien);
                    }
                }
                else{
                    $scope.hideLoading();
                    $scope.showAlert('',response.data.message + ' dans Formulaire '+(formIndex+1));
                    $scope.pushDate = null;
                }
            },
            error=>{
                $scope.hideLoading();
                $scope.showAlert('Erreur','Une erreur est survenue');
                $scope.pushDate = null;
            }
        );
    };

     $scope.saveDraft = function(lien){
        var formIndex = $scope.formList.length - 1;
        var idForm = $scope.formList[formIndex].id;

        if($scope.pushDate == null){
            $scope.pushDate = $scope.getDatetimeString();
        }

        var statutDonModif = $('#statut_don_modif').val().trim();
        var id_produit = $("#id_produit").val().trim();
        var id_donation = $("#id_donation").val().trim();

        var nom = $("#input-nom"+idForm).val().trim();
        //var groupement = $("#input-groupement-unite"+idForm).prop('checked') ? 1 : ($("#input-groupement-portion"+idForm).prop('checked') ? 3 : 2);
        var groupement = $("#input-groupement-unite"+idForm).prop('checked') ? 1 : 2;
        var poids = $scope.getNumber($("#input-poids"+idForm).val().trim().replace(/\s/g,""));
        var volume = $scope.getNumber($("#input-volume"+idForm).val().trim().replace(/\s/g,""));
        var prix = $scope.getNumber($("#input-prix"+idForm).val().trim().replace(/\s/g,""));
        if(groupement == 2){
            var quantite = 1;
        }else{
            var quantite = parseInt($scope.getNumber($("#input-quantite"+idForm).val().trim().replace(/\s/g,"")));
        }
        var typeDate = $("#input-choix-date-peremption"+idForm).prop('checked') ? 1 : 2;
        var datePeremption = $('#input-date-peremption'+idForm).val();
        var categorie = $("#select-categorie"+idForm).val();
        var type = $("#select-type"+idForm).val();
        var infoNutris = $("#select-info-nutris"+idForm).select2('val').toString().split(",");
        var raisonSurplus = $("#select-raison-surplus"+idForm).select2('val').toString().split(",");
        var refrigere = $("#input-refrigere"+idForm).is(":checked") ? 1 : 0; // true na false
        var congele = $("#input-congele"+idForm).is(":checked") ? 1 : 0;
        var consoDirect = $("#input-consoDirect"+idForm).is(":checked") ? 1 : 0;
        var description = $("#textarea-description"+idForm).val().trim();

        var formData = new FormData();

        formData.append('statutDonModif', statutDonModif);
        formData.append('idProduit', id_produit); 
        formData.append('idDonation', id_donation); 
        
        formData.append('pushDate', $scope.pushDate);
        formData.append('nom', nom);
        formData.append('groupement', groupement);
        formData.append('poids', poids);
        formData.append('volume', volume);
        formData.append('quantite', quantite);
        formData.append('prix', prix);
        formData.append('refrigere', refrigere);
        formData.append('congele', congele);
        formData.append('consoDirect', consoDirect);
        formData.append('description', description);
        formData.append('typeDate', typeDate);
        formData.append('date', datePeremption);
        formData.append('categorie', categorie);
        formData.append('type', type);
        formData.append('NbInfoNutris', infoNutris.length);
        for(var i=0; i<infoNutris.length; i++){
            formData.append('infoNutris'+i, infoNutris[i]);
        }
        formData.append('NbRaisonSurplus', raisonSurplus.length);
        for(var i=0; i<raisonSurplus.length; i++){
            formData.append('raisonSurplus'+i, raisonSurplus[i]);
        }

        var allPhotoInBase = $('.photoInBase');
        formData.append('NbPhotoAlreadyInBase', allPhotoInBase.length);
        for(var i=0; i<allPhotoInBase.length; i++){
            var element = allPhotoInBase.get(i);
            var idPhoto = element.value.trim();
            console.log('pictures'+i+" = "+ idPhoto);
            formData.append('pictures'+i, idPhoto);
        }

        formData.append('NbImages', $scope.formList[formIndex].images.length);
        for(var i=0; i<$scope.formList[formIndex].images.length; i++){
            if($scope.formList[formIndex].images[i].fromServer){
                formData.append('fromServer'+i, 1);
                formData.append('idPhoto'+i, $scope.formList[formIndex].images[i].idPhoto);
            }
            else{
                formData.append('fromServer'+i, 0);
                formData.append('image'+i+'ext', $scope.formList[formIndex].images[i].extension);
                formData.append('image'+i+'data', $scope.formList[formIndex].images[i].dataURL);
            }
        }

        formData.append('isDraft', 1);

        $http({
            method: 'POST',
            url: lien,
            data: formData,
            headers: {'Content-Type': undefined},
            transformRequest: angular.identity
        })
        .then(
            response=>{
                if(response.data.success){
                    if(formIndex == 0){
                        $scope.hideLoading();
                        $scope.showAlert('',response.data.message);
                        $scope.pushDate = null;

                        $scope.showAlert('Notification','Les informations ont bien été enregistrées dans le brouillon');
                    }
                    else{
                        $scope.formList.splice(formIndex, 1);
                        $('#card'+idForm).remove();
                        $scope.saveDraft(lien);
                    }
                }
                else{
                    $scope.hideLoading();
                    $scope.showAlert('',response.data.message + ' dans Formulaire '+(formIndex+1));
                    $scope.pushDate = null;
                }
            },
            error=>{
                $scope.hideLoading();
                $scope.showAlert('Erreur','Une erreur est survenue');
                $scope.pushDate = null;
            }
        );
    };
 

    $scope.resetAllField = function(){
        $("#input-nom1").val('');
        $("#input-poids1").val('');
        $("#input-volume1").val('');
        $("#input-prix1").val('');
        $("#input-quantite1").val('');
        $("#input-date-peremption1").val('');
        $("#select-categorie1").select2('val', '');
        $("#select-type1").select2('val', '');
        $("#select-info-nutris1").select2('val', '');
        $("#select-raison-surplus1").select2('val', '');
        $("#input-congele1, #input-refrigere1, #input-consoDirect1").iCheck('uncheck');
        $("#textarea-description1").val('');
        $('#div-file-preview1').html('');
        
        $scope.pushDate = null;
    };


    $scope.validateForm = function(lien){
        $('#btn-validate').hide();
        $('#lds-roller').show();

        var nom = $("#input-nom").val().trim();
        var poids = $scope.getNumber($("#input-poids").val().trim());
        var volume = $scope.getNumber($("#input-volume").val().trim());
        var prix = $scope.getNumber($("#input-prix").val().trim());
        var quantite = parseInt($scope.getNumber($("#input-quantite").val().trim()));
        var datePeremption = $('#input-date-peremption').val();
        var categorie = $("#select-categorie").val();
        var type = $("#select-type").val();
        var infoNutris = $("#select-info-nutris").select2('val').toString().split(",");
        var refrigere = $("#input-refrigere").is(":checked") ? 1 : 0; // true na false
        var congele = $("#input-congele").is(":checked") ? 1 : 0;
        var description = $("#textarea-description").val().trim();

        var isValide = true;
        var errorMsg = "";
        if(nom == ""){
            errorMsg = "Veuillez definir le nom du produit";
            isValide = false;
        }
        if(isValide && poids <= 0){
            errorMsg = "Le poids du produit est invalide";
            isValide = false;
        }
        if(isValide && prix <= 0){
            errorMsg = "Le prix du produit est invalide";
            isValide = false;
        }
        if(isValide && quantite <= 0){
            errorMsg = "La quantité du produit est invalide";
            isValide = false;
        }
        /*if(isValide && volume <= 0){
            errorMsg = "Le volume du produit est invalide";
            isValide = false;
        }*/
        if(isValide && categorie == ""){
            errorMsg = "La catégorie du produit est invalide";
            isValide = false;
        }
        if(isValide && type == ""){
            errorMsg = "Le type du produit est invalide";
            isValide = false;
        }
        if(isValide && !$scope.dateIsGood(datePeremption)){
            errorMsg = "La date de péremption est invalide";
            isValide = false;
        }
        if(isValide && infoNutris.length <= 0){
            errorMsg = "Il faut choisir au moins une information nutritionnelle du produit";
            isValide = false;
        }
        /*if(isValide && description == ""){
            errorMsg = "La description du produit est vide";
            isValide = false;
        }*/
        if(isValide && $scope.images.length <= 0){
            errorMsg = "Il faut choisir au moins une photo pour le produit";
            isValide = false;
        }

        if(isValide) {
            var formData = new FormData();
            formData.append('nom', nom);
            formData.append('poids', poids);
            formData.append('volume', volume);
            formData.append('quantite', quantite);
            formData.append('prix', prix);
            formData.append('refrigere', refrigere);
            formData.append('congele', congele);
            formData.append('description', description);
            formData.append('date', datePeremption);
            formData.append('categorie', categorie);
            formData.append('type', type);
            formData.append('NbInfoNutris', infoNutris.length);
            for(var i=0; i<infoNutris.length; i++){
                formData.append('infoNutris'+i, infoNutris[i]);
            }
            formData.append('NbImages', $scope.images.length);
            for(var i=0; i<$scope.images.length; i++){
                if($scope.images[i].fromServer){
                    formData.append('fromServer'+i, 1);
                    formData.append('idPhoto'+i, $scope.images[i].idPhoto);
                }
                else{
                    formData.append('fromServer'+i, 0);
                    formData.append('image'+i+'ext', $scope.images[i].extension);
                    formData.append('image'+i+'data', $scope.images[i].dataURL);
                }
            }
            $http({
                method: 'POST',
                url: lien,
                data: formData,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            })
            .then(
                response=>{
                    $scope.showAlert('',response.data.message);
                    $('#btn-validate').show();
                    $('#lds-roller').hide();
                    if(response.data.success){
                        $("#input-nom").val('');
                        $("#input-poids").val('');
                        $("#input-volume").val('');
                        $("#input-prix").val('');
                        $("#input-quantite").val('');
                        $("#input-date-peremption").val('');
                        $("#select-categorie").select2('val', '');
                        $("#select-type").select2('val', '');
                        $("#select-info-nutris").select2('val', '');
                        $("#input-congele, #input-refrigere").iCheck('uncheck');
                        $("#textarea-description").val('');
                        $('#div-file-preview').html('');
                        $scope.images = [];
                    }
                },
                error=>{
                    $scope.showAlert('Erreur','Une erreur est survenue');
                    $('#btn-validate').show();
                    $('#lds-roller').hide();
                }
            );
        }
        else{
            $scope.showAlert('Erreur',errorMsg);
            $('#btn-validate').show();
            $('#lds-roller').hide();
        }
    };

    $scope.import = function(lien){
        var files = $('#input-file-excel').prop('files');
        if(files.length < 1){
            $scope.showAlert("", "Veuillez choisir un fichier excel");
        }
        else{
            $('#btn-import').hide();
            $('#lds-roller-import').show();

            var reader = new FileReader();
            var list = [];
            reader.onload = function(e) {
                var data = reader.result;
                var workbook = XLSX.read(data, {type: 'binary'});
                var pageIndex = 0;
                workbook.SheetNames.forEach(function(sheetName) {
                    if(pageIndex > 0){
                        var XL_row_object = XLSX.utils.sheet_to_json(workbook.Sheets[sheetName], {raw: true});
                        for(var i=0; i<XL_row_object.length; i++){
                            var obj = XL_row_object[i];
                            var date = XLSX.SSF.parse_date_code(obj['Date de péremption'], {date1904:false});
                            obj['Date de péremption'] = date.y+'-'+date.m+'-'+date.d;
                            list.push(obj);
                        }
                    }
                    pageIndex++;
                });
                $scope.beginImport(list, lien);
            };
            reader.onerror = function(e){
                $('#btn-import').show();
                $('#lds-roller-import').hide();
                $scope.showAlert("","Une erreur est survenue pendant la lecture du fichier");
            }
            reader.readAsBinaryString(files[0]);
        }
    };

    $scope.beginImport = function(list, lien){
        var columns = ["Nom","Catégorie","Type","Poids par produit (kg)","Volume (m3)","Quantité(s) (nombre des produits)","Prix unitaire (MGA)","Date de péremption","Description","Informations nutritionnelles","Le produit nécessite d'être réfrigéré","Le produit nécessite d'être congelé","Raison de surplus"];
        var colsObj = ['nom','categorie','type','poids','volume','quantite','prix','datePeremption','description','infoNutris','refrigere','congele','raisonSurplus'];
        var array = [];
        if(list.length > 0){
            var valuesOk = true;
            for(var i=0; i<list.length; i++){
                var row = {};
                for(var ci=0; ci<colsObj.length; ci++){
                    if(list[i][columns[ci]] != undefined){
                        /*if(ci == 3){
                            if(!(list[i][columns[ci]].toString().trim() == 'Par unité' || list[i][columns[ci]].toString().trim() == 'Totalité')){
                                valuesOk = false;
                                break;
                            }
                        }*/
                        row[colsObj[ci]] = list[i][columns[ci]];
                    }
                    else{
                        if(ci == 4 || ci == 8){ // si volume et description (facultatif)
                            row[colsObj[ci]] = "";
                        }
                        else{
                            valuesOk = false;
                            break;
                        }
                    }
                }
                if(!valuesOk){
                    break;
                }
                array.push(row);
            }
            
            if(valuesOk){
                $scope.startImport(array, lien);
            }
            else{
                $('#btn-import').show();
                $('#lds-roller-import').hide();
                $scope.showAlert('Erreur','Le format de données du fichier ne correspond pas au modèle ou certaines valeur sont invalides');
            }
        }
        else{
            $('#btn-import').show();
            $('#lds-roller-import').hide();
            $scope.showAlert('Erreur','Il n\'y a aucune ligne dans le fichier');
        }
    };

    $scope.startImport = function(array, lien){
        var error = null;
        var formData = new FormData();
        formData.append('moment', $scope.getDatetimeString());
        formData.append('nbRow', array.length);
        for(var i=0; i<array.length; i++){
            formData.append('nom'+i, array[i]['nom']);
            formData.append('categorie'+i, array[i]['categorie']);
            formData.append('type'+i, array[i]['type']);
            //formData.append('groupement'+i, array[i]['groupement']);
            formData.append('volume'+i, array[i]['volume']);
            formData.append('datePeremption'+i, array[i]['datePeremption']);
            formData.append('description'+i, array[i]['description']);
            formData.append('infoNutris'+i, array[i]['infoNutris']);
            formData.append('raisonSurplus'+i, array[i]['raisonSurplus']);
            formData.append('refrigere'+i, array[i]['refrigere']);
            formData.append('congele'+i, array[i]['congele']);
            if($scope.getNumber(array[i]['poids']) > 0){
                formData.append('poids'+i, array[i]['poids']);
            }
            else{
                error = "Le poids de <strong>'"+array[i]['nom']+"'</strong> est invalide";
                break;
            }
            if($scope.getNumber(array[i]['prix']) > 0){
                formData.append('prix'+i, array[i]['prix']);
            }
            else{
                error = "Le prix de <strong>'"+array[i]['nom']+"'</strong> est invalide";
                break;
            }
            if($scope.getNumber(array[i]['quantite']) > 0){
                formData.append('quantite'+i, array[i]['quantite']);
            }
            else{
                error = "La quantité de <strong>'"+array[i]['nom']+"'</strong> est invalide";
                break;
            }
        }
        if(error == null){
            $http({
                method: 'POST',
                url: lien,
                data: formData,
                headers: {'Content-Type': undefined},
                transformRequest: angular.identity
            })
            .then(
                response=>{
                    $scope.showAlert('',response.data.message);
                    if(response.data.success){
                        $scope.productFromExcel = response.data.list;
                        $("#label-file-excel").hide();
                        $("#input-file-excel").val('');
                        $('#label-file-excel').html("Veuiller choisir le fichier Excel contenant les données");
                        $('#lds-roller-import').hide();
                        $('#btn-import').hide();
                        $('#btn-download-model').hide();

                        $('#div-photo-excel').show();
                        $('#td-nom-excel').html($scope.productFromExcel[0]['nom']);
                        $('#td-type-excel').html($scope.productFromExcel[0]['type']);
                        $('#div-image-preview-excel').html('');
                    }
                    else{
                        $('#btn-import').show();
                        $('#lds-roller-import').hide();
                    }
                },
                error=>{
                    $scope.showAlert('Erreur','Une erreur est survenue');
                    $('#btn-import').show();
                    $('#lds-roller-import').hide();
                }
            );  
        }
        else{
            $scope.showAlert('Oops', error);
            $('#btn-import').show();
            $('#lds-roller-import').hide();
        }
    };

    $scope.onFileImageExcelChange = function(input){
        var files = $('#input-image-excel').prop('files');
        var divFilePreview = $('#div-image-preview-excel');
        $scope.showImage(divFilePreview, files, 0, $scope.excelImages);
    };

    $scope.importExcelPhoto = function(lien){
        if($scope.excelImages.length > 0){
            if($scope.photoExcelIndex < $scope.productFromExcel.length){
                $('#div-photo-excel').hide();
                $('#lds-roller-import').show();
                var formData = new FormData();
                formData.append('id_produit', $scope.productFromExcel[$scope.photoExcelIndex]['id_produit']);
                formData.append('nbImage', $scope.excelImages.length);
                for(var i=0; i<$scope.excelImages.length; i++){
                    if($scope.excelImages[i].fromServer){
                        formData.append('fromServer'+i, 1);
                        formData.append('idPhoto'+i, $scope.excelImages[i].idPhoto);
                    }
                    else{
                        formData.append('fromServer'+i, 0);
                        formData.append('image'+i+'ext', $scope.excelImages[i].extension);
                        formData.append('image'+i+'data', $scope.excelImages[i].dataURL);
                    }
                }
                $http({
                    method: 'POST',
                    url: lien,
                    data: formData,
                    headers: {'Content-Type': undefined},
                    transformRequest: angular.identity
                })
                .then(
                    response=>{
                        $scope.photoExcelIndex++;
                        if($scope.photoExcelIndex < $scope.productFromExcel.length){
                            $('#td-nom-excel').html($scope.productFromExcel[$scope.photoExcelIndex]['nom']);
                            $('#td-type-excel').html($scope.productFromExcel[$scope.photoExcelIndex]['type']);
                            $('#div-photo-excel').show();
                        }
                        else{
                            $scope.photoExcelIndex = 0;
                            $scope.productFromExcel = [];
                            $scope.showAlert('Terminé', "Votre produit est bien ajouté ! Vous allez recevoir un e-mail quand DHL passera pour la récupération de vos surplus. Pour plus de détails regardez sur votre tableau de bord");
                            $('#div-photo-excel').hide();
                            $('#btn-import').show();
                            $("#label-file-excel").show();
                        }
                        $scope.excelImages = [];
                        $('#lds-roller-import').hide();
                        $('#div-image-preview-excel').html('');
                    },
                    error=>{
                        $('#lds-roller-import').hide();
                        $scope.showAlert('Erreur','Une erreur est survenue');
                    }
                );
            }
            else{
                $scope.showAlert('Terminé', "Votre produit est bien ajouté ! Vous allez recevoir un e-mail quand DHL passera pour la récupération de vos surplus. Pour plus de détails regardez sur votre tableau de bord");
            }
        }
        else{
            $scope.showAlert('', 'Vous devez choisir au moins une photo');
        }
    };
    $scope.initDonSelect2 = function(idForm){
         $('.select-categorie').select2({
                width: '100%',
                placeholder: 'Choisisser ou ajouter',
                language: {
                  noResults: function() {
                    return '<button id="no-results-btn" class="btn btn-primary btn-block" onclick="ajouterCategorie('+idForm+')">Ajouter catégorie</a>';
                  },
                },
                escapeMarkup: function(markup) {
                  return markup;
                },
            });
            $('.select-type').select2({
                width: '100%',
                placeholder: 'Choisisser ou ajouter',
                language: {
                  noResults: function() {
                    return '<button id="no-results-btn" class="btn btn-primary btn-block" onclick="ajouterType('+idForm+')">Ajouter type</a>';
                  },
                },
                escapeMarkup: function(markup) {
                  return markup;
                },
            });
            $('.select-info-nutris').select2({
                width: '100%',
                placeholder: 'Choisisser ou ajouter',
                language: {
                  noResults: function() {
                    return '<button id="no-results-btn" class="btn btn-primary btn-block" onclick="ajouterInfoNutris('+idForm+')">Ajouter information</a>';
                  },
                },
                escapeMarkup: function(markup) {
                  return markup;
                },
            });
            $('.select-raison-surplus').select2({
                width: '100%',
                placeholder: 'Choisisser ou ajouter',
                language: {
                  noResults: function() {
                    return '<button id="no-results-btn" class="btn btn-primary btn-block" onclick="ajouterRaisonSurplus('+idForm+')">Ajouter raison</a>';
                  },
                },
                escapeMarkup: function(markup) {
                  return markup;
                },
            });
    }
    $scope.addForm = function(baseUrl, lien){
        var idForm = Date.now();
        $scope.formList.push(
            {
                id: idForm,
                images: []
            }
        );

        $('#div-form-list').append($compile($scope.getForm(idForm, baseUrl, lien))($scope));

         //Information bulle
        new Tippy('.info_date_peremption',{
            position:'top',
            arrow:'true'
        });

        new Tippy('.info_necessite_produit',{
            position:'bottom',
            arrow:'true'
        });
        
        new Tippy('.info_prix',{
            position:'right',
            arrow:'true'
        });

        $('.datepicker').datepicker({
            weekStart: 1,
            language: 'fr',
            format: 'dd/mm/yyyy'
        });

        $(function () {
            $scope.initDonSelect2(idForm);
        });

        
        //iCheck for checkbox and radio inputs
        $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
            checkboxClass: 'icheckbox_minimal-blue',
            radioClass   : 'iradio_minimal-blue'
        });
        //Red color scheme for iCheck
        $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
            checkboxClass: 'icheckbox_minimal-red',
            radioClass   : 'iradio_minimal-red'
        });
        //Flat red color scheme for iCheck
        $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
            checkboxClass: 'icheckbox_flat-green',
            radioClass   : 'iradio_flat-green'
        });
        // radio button
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass   : 'iradio_square-blue',
            increaseArea : '20%' // optional
        });

        // radio type de groupement
        $(".radio-groupement-unite").on('ifChanged', function(event){
            var input = $(this);
            var idForm = input.attr('id').toString().replace('input-groupement-unite', '');
            if(input.prop('checked') == true){
                //<span class="info_vente_facultatif">(facultatif)</span>
                 //console.log('ato unite e');
                $('#input-poids'+idForm).attr('placeholder', 'Poids de l\'unité');
                $('#input-volume'+idForm).attr('placeholder', 'Volume de l\'unité');
                $('.info_don_facultatif'+idForm).show();
                $('#input-prix'+idForm).attr('placeholder', 'Prix de l\'unité');
                var quantite = $('#input-quantite'+idForm);
                quantite.parent().parent().show();

            }
        });


        /*$(".radio-groupement-portion").on('ifChanged', function(event){
            var input = $(this);
            var idForm = input.attr('id').toString().replace('input-groupement-portion', '');
            if(input.prop('checked') == true){
                //<span class="info_vente_facultatif">(facultatif)</span>
                console.log('ato portion e');
                $('#input-poids'+idForm).attr('placeholder', 'Poids du portion');
                $('#input-volume'+idForm).attr('placeholder', 'Volume du portion');
                $('.info_vente_facultatif'+idForm).show();
                $('#input-prix'+idForm).attr('placeholder', 'Prix du portion');
                var quantite = $('#input-quantite'+idForm);
                quantite.parent().parent().show();

            }
        });
        */

        $(".radio-groupement-totalite").on('ifChanged', function(event){
            var input = $(this);
            var idForm = input.attr('id').toString().replace('input-groupement-total', '');
            if(input.prop('checked') == true){
                //<span class="info_vente_facultatif">(facultatif)</span>
                console.log('ato groupement e');
                $('#input-poids'+idForm).attr('placeholder', 'Poids');
                $('#input-volume'+idForm).attr('placeholder', 'Volume');
                $('.info_vente_facultatif'+idForm).hide();
                $('#input-prix'+idForm).attr('placeholder', 'Prix');
                var quantite = $('#input-quantite'+idForm);
                quantite.parent().parent().hide();

            }
        });
    }; 
    $scope.removeMessageErreur = function(){
        var div_erreur = $('.message-erreur');
        for(var i = 0 ; i < div_erreur.length ; i++){
            $( div_erreur.get(i) ).remove();
        }
    }
    $scope.removeForm = function(idForm){
        for(var i=1; i<$scope.formList.length; i++){
            if($scope.formList[i].id == idForm){
                $scope.formList.splice(i, 1);
                break;
            }
        }
        for(var i=1; i<$scope.formList.length; i++){
            $('#card-title-'+$scope.formList[i].id).html('Formulaire '+(i+1));
        }
    };

    $scope.getForm = function(idForm, baseUrl, lien){

        /*'<label style="margin-right: 10px;">'+
                            '<input class="type-groupement radio-groupement-portion" id="input-groupement-portion'+idForm+'" type="radio" name="unite'+idForm+'" />'+
                         ' Par portion'+
                        '</label>'+
        */

        return '<div class="card col-md-12" id="card'+idForm+'">'+
        '<div class="card-header" style="background-color: #00AFAA; color: #fff;">'+
            '<h3 class="card-title" id="card-title-'+idForm+'">Formulaire '+$scope.formList.length+'</h3>'+
            '<div class="card-tools">'+
                '<button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>'+
                '<button ng-click="removeForm('+idForm+')" type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-remove"></i></button>'+
            '</div>'+
        '</div>'+
        '<div class="card-body">'+
            '<div class="row">'+

                '<div class="col-md-6">'+
                    '<div class="form-group" id="div-input-nom'+idForm+'">'+
                        '<label>Nom</label>'+
                        '<input id="input-nom'+idForm+'" class="form-control" type="text" placeholder="Nom du produit"/>'+
                    '</div>'+
                    '<div class="form-group" id="div-input-groupement'+idForm+'">'+
                        '<label>Groupement</label><br>'+
                        '<label style="margin-right: 10px;">'+
                            '<input class="type-groupement radio-groupement-unite" id="input-groupement-unite'+idForm+'" type="radio" checked="true" name="unite'+idForm+'"/>'+
                         ' Par unité'+
                        '</label>'+
                        '<label style="margin-right: 10px;">'+
                            '<input class="type-groupement radio-groupement-totalite" id="input-groupement-total'+idForm+'" type="radio" name="unite'+idForm+'"/>'+
                         ' Totalité des produits'+
                        '</label>'+
                    '</div>'+
                    '<div class="form-group" id="div-input-poids'+idForm+'">'+
                        '<label>Poids</label>'+
                        '<div class="input-group mb-3">'+
                            '<input id="input-poids'+idForm+'" class="form-control" type="text" ng-keyup="formatNumberToMileSep(\'#input-poids'+idForm+'\')" placeholder="Poids de l\'unité">'+
                            '<div class="input-group-append">'+
                                '<span class="input-group-text">Kg</span>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group" id="div-input-volume'+idForm+'">'+
                        '<label>Volume <span class="info_don_facultatif'+idForm+'">(facultatif)</span></label>'+
                        '<div class="input-group mb-3">'+
                            '<input id="input-volume'+idForm+'" class="form-control" type="text" ng-keyup="formatNumberToMileSep(\'#input-volume'+idForm+'\')" placeholder="Volume de l\'unité ">'+
                            '<div class="input-group-append">'+
                                '<span class="input-group-text"><var>m<sup>3</sup></var></span>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group" id="div-input-prix'+idForm+'">'+
                        '<label>Prix <sup style="cursor: pointer !important;" id="info_prix" class="info_prix tippy" title=" Ecrire les chiffres avec un espace et sans virgule, Ex : 10 000 000"> ( i ) </sup></label>'+
                        '<div class="input-group mb-3">'+
                            '<input id="input-prix'+idForm+'" class="form-control" type="text" ng-keyup="formatNumberToMileSep(\'#input-prix'+idForm+'\')" placeholder="Prix de l\'unité">'+
                            '<div class="input-group-append">'+
                                '<span class="input-group-text">MGA</span>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group" id="div-input-quantite'+idForm+'">'+
                        '<label>Quantité(s)</label>'+
                        '<div class="input-group mb-3">'+
                            '<input id="input-quantite'+idForm+'" class="form-control" type="text" ng-keyup="formatNumberToMileSep(\'#input-quantite'+idForm+'\')" placeholder="Quantité">'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group" id="div-input-date-peremption-production'+idForm+'">'+
                         '<label style="margin-right: 10px;">'+
                                '<input class="radio-choix-type-date" id="input-choix-date-peremption'+idForm+'" type="radio"  name="type-date'+idForm+'" checked="true" />'+
                            'Date de péremption <sup style="cursor: pointer !important;" class="info_date_peremption tippy" title=" Date de péremtion ou Date limite de consommation'+
                                    'Pour assurer la logistique de la donation l’ajout du produit doit se faire entre 1 à  5 jours avant la donation dépendant de la journée de demande" > ( i ) </sup>'+
                            '</label>'+
                            '<label>'+
                                '<input id="input-choix-date-production'+idForm+'" type="radio" name="type-date'+idForm+'" />'+
                            'Date de production'+
                            '</label>'+
                        '<div class="input-group mb-3">'+
                            '<input id="input-date-peremption'+idForm+'" class="form-control datepicker" type="text" placeholder="Date de péremption">'+
                            '<div class="input-group-append">'+
                                '<span class="input-group-text">'+
                                    '<i class="fa fa-calendar"></i>'+
                                '</span>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label class="check" style="font-weight: bolder !important;">'+
                            '<input id="input-refrigere'+idForm+'" type="checkbox" class="flat-red"> Le produit nécessite d\'être réfrigéré'+
                        '</label>'+
                        '<label class="check" style="font-weight: bolder !important;">'+
                            '<input id="input-congele'+idForm+'" type="checkbox" class="flat-red"> Le produit nécessite d\'être congelé'+
                        '</label>'+
                        '<label class="check" style="font-weight: bolder !important;">'+
                            '<input id="input-consoDirect'+idForm+'" type="checkbox" class="flat-red" /> Le produit nécessite ni d\'être réfrigéré ni d\'être congelé  <sup style="cursor: pointer !important;" id="info_necessite_produit" class="info_necessite_produit tippy" title=" Pour savoir si votre produit doit être réfrigéré ou congelé, consultez notre guide « Règlementations de donation »"> ( i ) </sup>'+
                        '</label>'+
                    '</div>'+
                '</div>'+

                '<div class="col-md-6">'+
                    '<div class="form-group">'+
                        '<label>Catégorie</label>'+
                        '<div class="row" id="div-select-categorie'+idForm+'">'+
                            '<div class="col-12">'+
                                '<select onchange="angular.element(this).scope().TYPE.onValueChanged('+idForm+')" id="select-categorie'+idForm+'" data-placeholder="Catégorie du produit" class="select-categorie form-control select2" style="width: 100%;">'+
                                    $('#select-categorie1').html()+
                                '</select>'+
                                '<div >'+
                                     '<a href="#" id="bouton-form-ajouter-categorie'+idForm+'" ng-click="formClick(\''+idForm+'\')"  data-toggle="modal" data-target="#modal-new-categorie" title="Ajouter une nouvelle catégorie" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label>Type</label>'+
                        '<div class="row" id="div-select-type'+idForm+'">'+
                            '<div class="col-12">'+
                                '<select id="select-type'+idForm+'" data-placeholder="Type du produit" class="select-type form-control select2" style="width: 100%;">'+
                                $('#select-type1').html()+
                                '</select>'+
                                '<div style="display: none ;">'+
                                    '<a href="#" id="bouton-form-ajouter-type'+idForm+'"  ng-click="formClick(\''+idForm+'\')"  ><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label>Informations nutritionnelles (facultatif)</label>'+
                        '<div class="row">'+
                            '<div class="col-12" id="div-select-info-nutris'+idForm+'">'+
                                '<select id="select-info-nutris'+idForm+'" class="select-info-nutris form-control select2 select2-hidden-accessible" multiple="" data-placeholder="Informations nutritionnelles" style="width: 100%;" tabindex="-1" aria-hidden="true">'+
                                    $('#select-info-nutris1').html()+
                                '</select>'+
                                '<div style="display : none ;">'+
                                    '<a href="#" id="bouton-form-ajouter-info-nutri'+idForm+'" ng-click="formClick(\''+idForm+'\')" ><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label>Raisons des surplus (facultatif)</label>'+
                        '<div class="row">'+
                            '<div class="col-12" id="div-select-raison-surplus'+idForm+'">'+
                                '<select id="select-raison-surplus'+idForm+'" class="select-raison-surplus form-control select2 select2-hidden-accessible" multiple="" data-placeholder="Raison des surplus" style="width: 100%;" tabindex="-1" aria-hidden="true">'+
                                    $('#select-raison-surplus1').html()+
                                '</select>'+
                                '<div style="display : none">'+
                                    '<a href="#" id="bouton-form-ajouter-raison-surplus'+idForm+'"  ng-click="formClick(\''+idForm+'\')"  ><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>'+
                                '</div>'+
                            '</div>'+
                        '</div>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label>Description</label>'+
                        '<textarea id="textarea-description'+idForm+'" class="form-control" rows="3" placeholder="Description du produit"></textarea>'+
                    '</div>'+
                    '<div class="form-group">'+
                        '<label>Photos</label><br>'+
                        '<div class="row">'+
                            '<div class="col-6">'+
                                '<label ng-click="formClick(\''+idForm+'\')" for="input-file-image'+idForm+'" class="form-control btn btn-secondary">Ajouter depuis mon ordinateur<i class="fa fa-picture-o" style="margin-left:8px;"></i></label>'+
                                '<input id="input-file-image'+idForm+'" onchange="angular.element(this).scope().onFileImageChange(this)" type="file" accept="image/*" style="display: none;" multiple />'+
                            '</div>'+
                            '<div class="col-6">'+
                                '<button ng-click="showPhotoPicker(\''+idForm+'\', \''+baseUrl+'\',\''+lien+'\')" class="form-control btn btn-secondary"><strong style="color: white;">Ajouter depuis le serveur</strong><i class="fa fa-picture-o" style="margin-left:8px;"></i></button>'+
                            '</div>'+
                        '</div>'+
                        '<div class="row" id="div-file-preview'+idForm+'">'+
                            
                        '</div>'+
                    '</div>'+
                '</div>'+
            '</div>'+
        '</div>'+
    '</div>';
    };

    /* ---------- GESTION DE CATEGORIE ---------- */

    $scope.TYPE = {};

    $scope.TYPE.addNewCategorie = function(lien){
        var infoNutris = $("#select-type-info-nutri-modal").select2('val').toString();
        var formData = new FormData();
        formData.append('categorie', $('#categorie-name-modal').val().trim());
        formData.append('infoNutris', infoNutris);
        $http({
            url: lien, 
            method: 'POST',
            data: formData,
            headers: {'Content-Type': undefined},
            transformRequest: angular.identity
        })
        .then(response=>{
            var data = response.data;
            if(data.success){
                $('#categorie-name-modal').val("");
                $("#select-type-info-nutri-modal").select2('val', '');
                var select = $('#select-categorie'+$scope.formClicked);
                select.append('<option value="'+data.id_type_aliment+'" >'+data.label+'</option>');
                select.select2('val', data.id_type_aliment);
                for(var i=0; i<$scope.formList.length; i++){
                    if($scope.formList[i].id != $scope.formClicked){
                        $('#select-categorie'+$scope.formList[i].id).append('<option value="'+data.id_type_aliment+'" >'+data.label+'</option>');
                    }
                }
                // auto select
                infoNutris = infoNutris.split(',');
                var select = $('#select-info-nutris'+$scope.formClicked);
                for(var i=0; i<infoNutris.length; i++)
                    select.select2("val", select.select2("val").concat(infoNutris[i]));
            }
            else{
                $scope.showAlert('', data.message);
            }
        }, error=>{
            $scope.showAlert('', 'Une erreur est survenue pendant l\'ajout du nouveau catégorie');
        });
    };

    $scope.TYPE.onValueChanged = function(formIndex){
        var formData = new FormData();
        formData.append('idCategorie', $('#select-categorie'+formIndex).val());
        $http({
            method: 'POST',
            url: base_url+'produit/don/getInfoNutrisLinked',
            data: formData,
            headers: {'Content-Type': undefined},
            transformRequest: angular.identity
        })
        .then(
            response=>{
                var data = response.data;
                if(data.length > 0){
                    var select = $('#select-info-nutris'+formIndex);
                    select.select2("val", "");
                    for(var i=0; i<data.length; i++){
                        select.select2("val", select.select2("val").concat(data[i]));
                    }
                }
            },
            error=>{
                console.log(error);
            });
    };

}]);

$(window).on('load',function(){
    if(profilOk==0 || protocoleOk==0 || contratAccepter != 1){

        $('#modal-block-donation').modal('show');

        $('#modal-block-donation').on('hide.bs.modal', function () {
            // do something…
            window.location.href = BASE_URL + "admin/dashboard";
        });

    }
    else{
        var d = new Date();
        $('.datepicker').datepicker({
            todayHighlight: true,
            weekStart: 1,
            language: 'fr',
            format: 'dd/mm/yyyy'
            
        });
        $(".datepicker").datepicker("beforeShowDay", d);
        /*$("input[type=number]").on('mousewheel.disableScroll', function (e) {
            e.preventDefault();
        });*/ 
        $(':input[type=number]').on('mousewheel',function(e){ $(this).blur(); });

        //Information bulle
        new Tippy('.info_date_peremption',{
            position:'top',
            arrow:'true'
        });

        new Tippy('.info_necessite_produit',{
            position:'bottom',
            arrow:'true'
        });
        
        new Tippy('.info_prix',{
            position:'right',
            arrow:'true'
        });

         // radio type de groupement
        $(".radio-groupement-unite").on('ifChanged', function(event){
            var input = $(this);
            var idForm = input.attr('id').toString().replace('input-groupement-unite', '');
            if(input.prop('checked') == true){
                //<span class="info_vente_facultatif">(facultatif)</span>
                 console.log('ato unite e');
                $('#input-poids'+idForm).attr('placeholder', 'Poids de l\'unité');
                $('#input-volume'+idForm).attr('placeholder', 'Volume de l\'unité');
                $('.info_don_facultatif'+idForm).show();
                $('#input-prix'+idForm).attr('placeholder', 'Prix de l\'unité');
                var quantite = $('#input-quantite'+idForm);
                quantite.parent().parent().show();

            }
        });

        /*
        $(".radio-groupement-portion").on('ifChanged', function(event){
            var input = $(this);
            var idForm = input.attr('id').toString().replace('input-groupement-portion', '');
            if(input.prop('checked') == true){
                //<span class="info_vente_facultatif">(facultatif)</span>
                console.log('ato portion e');
                $('#input-poids'+idForm).attr('placeholder', 'Poids du portion');
                $('#input-volume'+idForm).attr('placeholder', 'Volume du portion');
                $('.info_vente_facultatif'+idForm).show();
                $('#input-prix'+idForm).attr('placeholder', 'Prix du portion');
                var quantite = $('#input-quantite'+idForm);
                quantite.parent().parent().show();

            }
        });
        */

        $(".radio-groupement-totalite").on('ifChanged', function(event){
            var input = $(this);
            var idForm = input.attr('id').toString().replace('input-groupement-total', '');
            if(input.prop('checked') == true){
                //<span class="info_vente_facultatif">(facultatif)</span>
                console.log('ato groupement e');
                $('#input-poids'+idForm).attr('placeholder', 'Poids');
                $('#input-volume'+idForm).attr('placeholder', 'Volume');
                $('.info_vente_facultatif'+idForm).hide();
                $('#input-prix'+idForm).attr('placeholder', 'Prix');
                var quantite = $('#input-quantite'+idForm);
                quantite.parent().parent().hide();

            }
        });

        //Radio type de date
        $('.radio-choix-type-date').on('ifChanged',function(event){
            var input = $(this);
            var idForm = input.attr('id').toString().replace('input-choix-date-peremption','');
            if(input.prop('checked') == true){
                $('#input-date-peremption'+idForm).attr('placeholder', 'Date de péremption');
            }
            else{
                $('#input-date-peremption'+idForm).attr('placeholder', 'Date de production');
            }
        });
    }
});
/*$(window).ready(function(){
    /*$('#modal-block-donation').on('hide.bs.modal', function () {
            // do something…
        console.log("mety an");
    });
   
});*/