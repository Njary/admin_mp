<?php
if ( ! defined('BASEPATH')) exit('No direct script access allowed');

	function sendMailToDHLGMAIL($entreprise_id,$mail,$data){
    $CI = &get_instance();

    $CI->load->model("entreprise/Entreprise_model","entreprise");
    $CI->load->model("produit/Produit_model","produit");

    $config = Array(
          'protocol' => 'smtp',
          'smtp_host' => 'ssl://smtp.googlemail.com',
          'smtp_port' => 465,
          'smtp_user' => 'teste.appli.cedric@gmail.com', 
          'smtp_pass' => 'teste.appli.password', 
          'mailtype' => 'html',
          'charset' => 'utf-8',
          'wordwrap' => TRUE
        );


        
        $CI->load->library('email', $config);
    $subject = "RAMASSAGE DE PRODUITS";
    $from = "teste.appli.cedric@gmail.com";
    $CI->name = "Foodwise";
    $to = $mail;

    $adresses = $CI->entreprise->getAdressesEntreprise($entreprise_id);
    $responsables = $CI->entreprise->getResponsablesEntreprise($entreprise_id);
    $responsable = $responsables[0];
    
    $CI->email->set_newline("\r\n");
        $CI->email->from($from, $CI->name);
        $CI->email->to($to);
        $CI->email->subject($subject);

        $categories = $CI->produit->getCategorieById($data["categorie"]);
        $types = $CI->produit->getTypeById($data["type"]);
        
        $str_volume = ($data["volume"]==0)?"Non défini":$data["volume"]." m<sup>3</sup>";
        
        $CI->email->message("
          <p>Bonjour, </p>
          <br>
          <p> <b>VOICI LES INFORMATIONS CONCERNANT LE RAMASSAGE :  </b></p>

          <p>LIEU : ".$adresses[0]->label."</p>

          <p>PERSONNE DE CONTACT DE L'ENTREPRISE : </p>
          <p>Nom : <b>".$responsable->nom."</b></p>
          <p>Poste : <b>".$responsable->poste."</b></p>
          <p>Email : <b>".$responsable->email."</b></p>
          <p>Téléphone : <b>".$responsable->telephone."</b></p>


          <p>LIEU DE LIVRAISON : </p>

          <p>DETAIL DU CONTENU DE L'ENVOI: </p>
          <p>Categorie du contenu : <b>".$categories[0]->label."</b></p>
          <p>Type de contenu : <b>".$types[0]->label."</b></p>
          <p>Poids total : <b>".$data["poidsTotal"]." Kg</b></p>
          <p>Volume : <b>".$str_volume."</b></p>
          <br>
          <p>Cordialement,</p>
          <p>L'équipe Foodwise</p>");    

    
      
        return $CI->email->send() ? true : false;
  }

  function sendMailToDHL($entreprise_id,$mail,$data){
		$CI = &get_instance();

		$CI->load->model("entreprise/Entreprise_model","entreprise");
		$CI->load->model("produit/Produit_model","produit");

		
		$subject = "RAMASSAGE DE PRODUITS";
		$from = "teste.appli.cedric@gmail.com";
		$CI->name = "Foodwise";
		$to = $mail;

		$adresses = $CI->entreprise->getAdressesEntreprise($entreprise_id);
		$responsables = $CI->entreprise->getResponsablesEntreprise($entreprise_id);
		$responsable = $responsables[0];

    $categories = $CI->produit->getCategorieById($data["categorie"]);
    $types = $CI->produit->getTypeById($data["type"]);
    
    $str_volume = ($data["volume"]==0)?"Non défini":$data["volume"]." m<sup>3</sup>";
    
    $message="
      <p>Bonjour, </p>
      <br>
      <p> <b>VOICI LES INFORMATIONS CONCERNANT LE RAMASSAGE :  </b></p>

      <p>LIEU : ".$adresses[0]->label."</p>

      <p>PERSONNE DE CONTACT DE L'ENTREPRISE : </p>
      <p>Nom : <b>".$responsable->nom."</b></p>
      <p>Poste : <b>".$responsable->poste."</b></p>
      <p>Email : <b>".$responsable->email."</b></p>
      <p>Téléphone : <b>".$responsable->telephone."</b></p>


      <p>LIEU DE LIVRAISON : </p>

      <p>DETAIL DU CONTENU DE L'ENVOI: </p>
      <p>Categorie du contenu : <b>".$categories[0]->label."</b></p>
      <p>Type de contenu : <b>".$types[0]->label."</b></p>
      <p>Poids total : <b>".$data["poidsTotal"]." Kg</b></p>
      <p>Volume : <b>".$str_volume."</b></p>
      <br>
      <p>Cordialement,</p>
      <p>L'équipe Foodwise</p>";    

    /*// In case any of our lines are larger than 70 characters, we should use wordwrap()
    $message = wordwrap($message, 70, "\r\n");*/

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
  
    return mail($to,$subject, $message,$headers)? true : false;
	}

  function sendMailToNewEA($profil_link,$data){
    
    $subject = "Bienvenue chez Foodwise !";
    $to = $data["responsable1_email"];
    
    $message="
      <p>Bienvenue ".$data["nom_entreprise"]."</p>
      <br>
      <p>Merci pour votre inscription sur la plateforme Foodwise &amp; pour lutter contre le gaspillage alimentaire avec nous !</p>

      <p>Pour commencer à partager et valoriser vos invendus 1) complétez votre <a href='".$profil_link."' >profil</a> et 2) mettez en ligne vos produits avec ce super simple <a href='' >vidéo-guide</a> !</p>
      <br>

      <p>Avec Foodwise vous :</p>

      <ul>
        <li>Diminuez vos coûts liés à la gestion des invendus</li>
        <li>Atteignez des nouveaux clients &amp; augmentez vos ventes</li>
        <li>Réduisez votre empreinte CO2</li>
        <li>Apportez un aide alimentaire vers les plus défavorisés</li>
      </ul>

      <p>Merci & bon partage !</p>
      <p>L'équipe Foodwise</p>
      <br>
      <p>Contact Madagascar :</p>
      <p>Info.mg@manzerpartazer.org</p>
      <p>fb : www.facebook.com/manzerpartazer/</p>
      <p>www.manzerpartazer.org</p>";    

    /*// In case any of our lines are larger than 70 characters, we should use wordwrap()
    $message = wordwrap($message, 70, "\r\n");*/

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
  
    return mail($to,$subject, $message,$headers)? true : false;
  }

  function sendMailToNewBen($data){
    
    $subject = "Inscription sur la plateforme Foodwise - Receveur d'Aliment";
    $to = $data["responsable1_email"];
    
    $message="
      <p>Bienvenue ".$data["responsable1_prenom"]." ".$data["responsable1_nom"]."</p>
      <br>
      <p>Merci de l'inscription avec ".$data["nom_organisation"]." à notre plateforme de partage des surplus: \"Foodwise\"! </p>

      <p>MERCI!</p>
      <p>L'équipe Foodwise</p>";    

    /*// In case any of our lines are larger than 70 characters, we should use wordwrap()
    $message = wordwrap($message, 70, "\r\n");*/

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
  
    return mail($to,$subject, $message,$headers)? true : false;
  }


  function sendMailRappelForAddProduct($data){
    
    $subject = "Besoin d'aide pour ajouter un produit ?";
    $to = $data["responsable1_email"];
    
    $message="
      <p>Bonjour ".$data["nom_entreprise"]."</p>
      <br>
      <p>Vous avez recemment commencé d'ajouter un produit sur la plateforme Foodwise afin de vendre vos surplus.<br/> 
          Avez-vous besoin d'aide pour terminer votre ajout du produit et mettre votre vente / dons en ligne?<br>
          Contactez info.mg@manzerpartazer.org / Tel : 034 42 177 00 ou regardez notre catalogue des questions fréquentes \"FAQ\". 
      </p>

      <p>Cordialement,</p>
      <p>L'équipe Foodwise</p>";

    /*// In case any of our lines are larger than 70 characters, we should use wordwrap()
    $message = wordwrap($message, 70, "\r\n");*/

    $headers  = 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
  
    return mail($to, $subject, $message, $headers) ? true : false;
  }
