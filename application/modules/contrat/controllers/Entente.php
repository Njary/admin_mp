<?php

defined('BASEPATH') OR exit('No direct script access allowed');
require_once APPPATH . "/libraries/PHPWord/autoload.php";

class Entente extends MY_Controller {

    private $idUser;
    private $curEntreprise;

    public function __construct() {
        parent::__construct();
        $this->load->model("Entente_model", "entente");
        $this->load->model("entreprise/Entreprise_model", "entreprise");
        $this->load->model("employe/Employe_model", "employe");
        $this->checkPrivilege();
        
        $this->idUser = $this->session->userdata("idUser");
        $this->curEntreprise = $this->entente->getEntreprise($this->idUser);
    }


    public function test(){
        echo "tes";
        $target = APPPATH . "../uploads/contrats/";
        $template_dir = APPPATH . '../templates/protocole_entente.docx';
        $document = new Gears\Pdf($template_dir);
        $document->converter = function()
        {
            return new Gears\Pdf\Docx\Converter\Unoconv();
        };
        $document->save($target. 'document.pdf');
    }

    protected function checkPrivilege() {
        $this->idUser = $this->session->userdata("idUser");

        if (empty($this->idUser) || $this->idUser < 0)
            redirect(site_url("login"));
    }

    public function index($validate = False) {
        $entreprise = $this->entente->getEntreprise($this->idUser);
        $data['entreprise'] = $entreprise;
        $contrat = $this->entente->getContrat($entreprise['id_entreprise']);
        $data['contrat'] = $contrat;
        $data['postes'] = $this->employe->getAllPoste();
        if ($validate) {
            $data['contrat_to_valid'] = $validate;
        }

        $this->layout->addView("entente_modal", $data);
        $this->layout->view();
    }

    public function submit() {
        $_POST = json_decode(file_get_contents('php://input'), true);

        $data = array("status" => "error");
        $entreprise = $this->input->post("entreprise");
        $employe1 = $this->input->post("employe1");
       // $employe2 = $this->input->post("employe2");
        $contrat = $this->input->post("contrat");
        $logoDonateur = $this->input->post("logoDonateur");
        //$this->session->set_userdata("contrat_validity", $contrat['validity']);

        if ($entreprise && $contrat) {
            $target = APPPATH . "../uploads/contrats/" . $entreprise['id_entreprise'];
            $template_dir = APPPATH . '../templates/protocole_entente.docx';
            $logo_dir = APPPATH . "../";
            $file = date('Y').date('m').date('d').date('H').date('i').date('s')."_protocole_entente_MG_".$entreprise['nom_entreprise']."_MP.docx";
            if (!file_exists($target)) {
                if (!mkdir($target, 0777, true)) {
                    $data["message"] = "Erreur de lors de création du fichier";
                    echo json_encode($data);
                    exit();
                }
            }

            $template = new \PhpOffice\PhpWord\TemplateProcessor($template_dir);
            $template->setValue('nom_donateur', $entreprise['nom_entreprise']);
            setlocale(LC_TIME, "fr_FR");
            $template->setValue('date', strftime("%d/%m/%Y"));
            $template->setValue('pays', $this->entente->getPays($entreprise['id_entreprise'])[0]->pays);
            //$template->setValue('validity', $contrat['validity']);

            if (!empty($logoDonateur['dataURL'])) {
                $newLogoFile = $this->saveLogo($logoDonateur['dataURL'], $logoDonateur['ext'], $entreprise['id_entreprise']);
                $template->setImg('logo_donateur', array('src' => $newLogoFile, 'size' => array(50,50)));

                //$this->entreprise->update($entreprise['id_entreprise'], array("logo" => $newLogoFile));
            }
            else{
                if (isset($entreprise['logo']) && !empty($entreprise['logo'])) {
                    $template->setImg('logo_donateur', array('src' => $entreprise['logo'], 'size' => array(50,50)));
                } else {
                    $template->setValue('logo_donateur', "[LOGO DONATEUR]");
                }
            }

            if (file_exists($logo_dir . "uploads/logo/transporteur/DHL.png")) {
                $template->setImg('logo_transport', array('src' => $logo_dir . "uploads/logo/transporteur/DHL.png", 'size' => array(50,50)));
            } else {
                $template->setValue('logo_transport', "[LOGO TRANSPORT]");
            }

            $template->setValue('responsable1_nom', $employe1['nom']);
            $template->setValue('responsable1_prenom', $employe1['prenom']);
            $template->setValue('responsable1_telephone', $employe1['telephone']);
            $template->setValue('responsable1_email', $employe1['email']);
            $template->setValue('responsable1_poste', $employe1['label']);

            /*$template->setValue('responsable2_nom', $employe2['nom']);
            $template->setValue('responsable2_prenom', $employe2['prenom']);
            $template->setValue('responsable2_telephone', $employe2['telephone']);
            $template->setValue('responsable2_email', $employe2['email']);
            $template->setValue('responsable2_poste', $employe2['label']);*/

            $template->saveAs($target . '/' . $file);
            $this->session->set_userdata("contrat_file_url", "uploads/contrats/" . $entreprise['id_entreprise'] . "/" . $file);

            //responsable 1
            $id_poste = $employe1['id_poste'];
            $this->employe->update($employe1["employe_id"], array("nom" => $employe1["nom"], "prenom" => $employe1["prenom"], "sexe" => $employe1["sexe"], "email" => $employe1["email"], "telephone" => $employe1["telephone"], "poste_id" => $id_poste));
            $this->session->set_userdata("charge_employe_id", $employe1["employe_id"]);

            //responsable 2, charge de la collecte d'emballage
            /*if (array_key_exists("employe_id", $employe2)) {
                $id_poste = $employe2['id_poste'];

                $this->employe->update($employe2["employe_id"], array("nom" => $employe2["nom"], "prenom" => $employe2["prenom"], "sexe" => $employe2["sexe"], "email" => $employe2["email"], "telephone" => $employe2["telephone"], "poste_id" => $id_poste));
                $this->session->set_userdata("charge_employe_id", $employe2["employe_id"]);
            } else {
                $id_poste = $employe2['id_poste'];

                $employe_id = $this->employe->save(array("nom" => $employe2["nom"], "prenom" => $employe2["prenom"], "sexe" => $employe2["sexe"], "email" => $employe2["email"], "telephone" => $employe2["telephone"], "poste_id" => $id_poste));
                $this->session->set_userdata("charge_employe_id", $employe_id);
            }*/
        }
    }

    public function saveLogo($dataURL, $ext, $idEA){
        $imgData = str_replace(' ','+', $dataURL);
        $imgData =  substr($imgData, strpos($imgData,",")+1);
        $imgData = base64_decode($imgData);
        $filePath = 'uploads/logos_entreprises/logo_'.$idEA.'.'.$ext;
        file_put_contents($filePath, $imgData);

        return $filePath;
    }

    public function saveOnly(){
        $contrat = $this->entente->getContrat($this->curEntreprise['id_entreprise']);
        if (!$contrat) {
            $data = array(
                "fichier" => $this->session->userdata('contrat_file_url'), 
                "type" => "entente", 
                //"validity"=>$this->session->userdata("contrat_validity"), 
                "entreprise_id" => $this->curEntreprise['id_entreprise'],
                "employe_charge_id" => $this->session->userdata("charge_employe_id"),
                "etat"=>0
            );
            $this->entente->save($data);
        }
        else{
            $data = array(
                "fichier" => $this->session->userdata('contrat_file_url'), 
                //"validity"=>$this->session->userdata("contrat_validity"),
                "etat"=>0
            );
            $this->entente->update($data, $contrat['id_contrat']);
        }
        //$this->session->unset_userdata('contrat_validity');
        $this->session->unset_userdata('contrat_file_url');
        $this->session->unset_userdata('charge_employe_id');
    }

    public function validate($redirect=0) {
        $contrat = $this->entente->getContrat($this->curEntreprise['id_entreprise']);
        if (!$contrat) {
            $data = array(
                "fichier" => $this->session->userdata('contrat_file_url'), 
                "type" => "entente", 
                //"validity"=>$this->session->userdata("contrat_validity"), 
                "entreprise_id" => $this->curEntreprise['id_entreprise'],
                "employe_charge_id" => $this->session->userdata("charge_employe_id"),
                "etat"=>1
            );
            $this->entente->save($data);
        }
        else{
            $contrat_file_url = $this->session->userdata('contrat_file_url');
            $data = array("etat" => 1);
            if(isset($contrat_file_url) && !empty($contrat_file_url)){
                $data["fichier"] = $contrat_file_url;
                //$data["validity"] = $this->session->userdata("contrat_validity");
            }
            $this->entente->update($data, $contrat['id_contrat']);
        }
       //$this->session->unset_userdata('contrat_validity');
        $this->session->unset_userdata('contrat_file_url');
        $this->session->unset_userdata('charge_employe_id');

        if($redirect == 1){
            redirect(site_url("entreprise/profil"));
        }
    }

    public function contrat() {
        $data['url'] = '/1/protocole_entente.pdf';
        $this->load->view("contrat", $data);
    }

}
