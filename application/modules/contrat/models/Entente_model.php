<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Entente_model extends CI_Model{

    private $_table = "contrat";
    private $tableEmploye = "employe";
    private $tablePoste = "poste";
    private $tableAdresse = "adresse";
    private $tableAdresseEntreprise = "adresse_entreprise";
    private $tablePays = "pays";

	public function getContrat($entreprise_id){
        $this->db->select('*');

        $this->db->from($this->_table)
                ->join("entreprise", "entreprise.id_entreprise = contrat.entreprise_id")
                ->join("utilisateur", "utilisateur.id_utilisateur = entreprise.utilisateur_id")
                ->join("entreprise_employe", "entreprise_employe.entreprise_id = entreprise.id_entreprise")
                ->join("employe", "employe.id_employe = entreprise_employe.employe_id");

        $this->db->where($this->_table.'.entreprise_id', $entreprise_id);
        $this->db->where($this->_table.'.type', "entente");

        $query = $this->db->get();
        return  ($query->num_rows() > 0) ? $query->result_array()[0] : false;
    }

    /* maka ny employe en charge de la collecte de l'embellage */
    public function getEmployeEnCharge($idContrat){
        $this->db->select($this->tableEmploye.'.*, '.$this->tablePoste.'.*');
        $this->db->from($this->tableEmploye)
        ->join($this->_table, $this->_table.'.employe_charge_id='.$this->tableEmploye.'.id_employe')
        ->join($this->tablePoste, $this->tablePoste.'.id_poste='.$this->tableEmploye.'.poste_id');
        $this->db->where($this->_table.'.id_contrat', $idContrat);
        $result = $this->db->get();
        return  ($result->num_rows() > 0) ? $result->result_array()[0] : false;
    }

    public function getEntreprise($utilisateur_id){
        $this->db->select('*');
        $this->db->from("entreprise");
        $this->db->join("utilisateur", "utilisateur.id_utilisateur = entreprise.utilisateur_id");
        $this->db->where('utilisateur.id_utilisateur', $utilisateur_id);
        $query = $this->db->get();

        $res = ($query->num_rows() > 0) ? $query->result_array()[0] : false;
       
        if($res){
            $this->db->join("employe", "employe.id_employe = entreprise_employe.employe_id");
            $this->db->join("poste", "poste.id_poste = employe.poste_id");
            $this->db->where("entreprise_employe.entreprise_id",$res['id_entreprise']);
            $res['employes'] = $this->db->get("entreprise_employe")->result_array();
        }

        return $res;
    }

    public function getEntrepriseById($entreprise_id){
        $this->db->select('*');
        $this->db->from("entreprise");
        $this->db->join("utilisateur", "utilisateur.id_utilisateur = entreprise.utilisateur_id");
        $this->db->where('entreprise.id_entreprise', $entreprise_id);
        $query = $this->db->get();

        $res = ($query->num_rows() > 0) ? $query->result_array()[0] : false;
       
        if($res){
            $this->db->join("employe", "employe.id_employe = entreprise_employe.employe_id");
            $this->db->join("poste", "poste.id_poste = employe.poste_id");
            $this->db->where("entreprise_employe.entreprise_id",$res['id_entreprise']);
            $res['employes'] = $this->db->get("entreprise_employe")->result_array();
        }
        
        return $res;
    }

    public function save($data) {
        $this->db->insert($this->_table, $data);
        return $this->db->insert_id();
    }

    public function update($data, $idContrat){
        $this->db->update($this->_table, $data, array($this->_table.'.id_contrat'=>$idContrat));
    }

    public function saveInTable($table, $data){
        $this->db->insert($table, $data);
        return $this->db->insert_id();
    }

    public function updateEntrepriseLogo($data, $utilisateur_id){
        $this->db->update("entreprise", $data, array('entreprise.utilisateur_id' => $utilisateur_id));
    }

    public function getPays($idEA){
        $query = "SELECT ".$this->tableAdresse.".*,".$this->tablePays.".label as pays FROM ".$this->tableAdresse." JOIN ".$this->tableAdresseEntreprise." ON ".$this->tableAdresse.".id_adresse=".$this->tableAdresseEntreprise.".adresse_id JOIN ".$this->tablePays." ON ".$this->tableAdresse.".pays_id=".$this->tablePays.".id_pays WHERE ".$this->tableAdresseEntreprise.".entreprise_id=".$idEA;
        $result = $this->db->query($query)->result();
        return $result;
    }

}