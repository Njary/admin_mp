<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Utilisateur_model extends CI_Model{

    protected $table = 'utilisateur';
    protected $table_url = 'url_temp_param';
    protected $table_adresse = 'adresse';
    protected $tableEmploye = 'employe';
    protected $tableEntrepriseEmploye = 'entreprise_employe';
    protected $tableDonation = "donation";

    public function updatePassword($utilisateur_id,$password,$encryptor){
        return $this->db->set(array("password"=>$encryptor->encrypt($password)))
                    ->where(array("id_utilisateur"=>$utilisateur_id))
                    ->update($this->table);
    }

    public function isAuthorized($username,$password,$encryptor){
    	$result = $this->db->query('SELECT id_utilisateur,password FROM '.$this->table.' WHERE pseudo="'.$username.'"')->result();
    	
    	if(count($result)==1){
    		$user = $result[0];
    		return ($encryptor->decrypt($user->password) == $password)?$user->id_utilisateur:-1;
    	}
    	
    	return -1;
    }

    public function checkPseudo($pseudo){
        $pseudo = trim($pseudo);
        $result = $this->db->query("SELECT id_utilisateur FROM ".$this->table." WHERE LOWER(pseudo)='".strtolower($pseudo)."'")->result();

        if(count($result)>0){
            return 0;
        }
        
        return 1;

    }
    
    
    public function mailExist($mail){
        $mail = trim($mail);
        $result = $this->db->query('SELECT email FROM '.$this->tableEmploye.' WHERE LOWER(email)="'.strtolower($mail).'"')->result();
        $exist = array('exist'=>false);
        if(count($result)>0){
            $exist['exist'] = true;
        }
        
        return $exist;
    }

    public function userPseudoExist($pseudo){
        $pseudo = trim($pseudo);
        $result = $this->db->query('SELECT pseudo FROM '.$this->table.' WHERE LOWER(pseudo)="'.strtolower($pseudo).'"')->result();
        $exist = array('exist'=>false);
        if(count($result)>0){
            $exist['exist'] = true;
        }
        
        return $exist;
    }

    public function getEmail($idEmploye){
        $result = $this->db->query('SELECT email FROM '.$this->tableEmploye.' WHERE id_employe="'.$idEmploye.'"')->result();
        if(count($result)>0){
            return $result[0]->email;
        }
        
        return '';
    }

    public function getEmailByIdEntreprise($idEtreprise){
        $query = "SELECT ".$this->tableEmploye.".email FROM ".$this->tableEntrepriseEmploye." JOIN ".$this->tableEmploye." ON ".$this->tableEmploye.".id_employe=".$this->tableEntrepriseEmploye.".employe_id WHERE ".$this->tableEntrepriseEmploye.".entreprise_id=".$idEtreprise." AND ".$this->tableEntrepriseEmploye.".position=3";
        $result = $this->db->query($query)->result();
        if(count($result)>0){
            return $result[0]->email;
        }
        
        return '';
    }

    public function save($data){
        $this->db->insert($this->table,$data);

        return $this->db->insert_id();
    }

    public function addUrlKey($key){
        $query = "INSERT INTO ".$this->table_url." (`param`,`moment`) VALUES ('".$key."',NOW())";

        $this->db->query($query);

        return $this->db->insert_id();
    }

    public function checkUrlKey($key,$ttl=30){
        $query = "SELECT * FROM ".$this->table_url." WHERE param=\"".$key."\" AND TIMESTAMPDIFF(MINUTE,moment,NOW())<".$ttl;

        $result = $this->db->query($query)->result();

        if(count($result)>0){
            $this->removeUrlKey($key);
            return 1;
        }
        
        return 0;
    }

    public function removeUrlKey($key){
        return $this->db->delete($this->table_url,array("param"=>$key));
    }

    public function update($utilisateur_id,$utilisateur){
        $this->db->where("id_utilisateur",$utilisateur_id);
        $this->db->update($this->table,$utilisateur);
    }

    public function removeAdresse($id_adresse){
        $this->db->where("id_adresse",$id_adresse);
        $this->db->delete($this->table_adresse);
    }
    public function checkIfUseInDonation($id_adresse){
        $query = "SELECT id_donation FROM ".$this->tableDonation." WHERE adresse_ramassage_id = ".$id_adresse;
        $result = $this->db->query($query)->result();
        
        return $result;
    }

}