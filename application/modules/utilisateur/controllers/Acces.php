<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Acces extends MY_Controller {

	public function __construct(){
		parent::__construct();
		$this->load->model("Utilisateur_model","user");
		$this->load->model("entreprise/Entreprise_model","entreprise");
		$this->load->model("organisation/Organisation_model","organisation");
		$this->load->model("employe/Employe_model","employe");
		$this->load->model("produit/Produit_model","produit");
	}

	public function checkSession(){
		$idUser=$this->session->userdata("idUser");

		if(!empty($idUser) && $idUser>=0)
			redirect(site_url("accueil"));
	}

	public function index(){
		$idUser=$this->session->userdata("idUser");

		if(empty($idUser) || $idUser<0)
			$this->login();
		else
			redirect(site_url("accueil"));
	}

	public function login($error=-1){
		$idUser=$this->session->userdata("idUser");

		if(!empty($idUser) && $idUser>=0)
			redirect(site_url("accueil"));
		
		$this->layout->setError($error);
		$this->layout->view("login");
	}

	public function authenticate(){
		if($this->input->post('valider_login')!== null){
			$username = $this->input->post("username");
			$password = $this->input->post("password");

			$idUser = $this->user->isAuthorized($username,$password,$this->encryption);

			$cur_entreprise = $this->entreprise->getEntrepriseByUtilisateurId($idUser);

			if($idUser>0){
				//go to apps
				$this->session->set_userdata("idUser",$idUser);
				$this->session->set_userdata("username",$username);
				$this->session->set_userdata("idEntreprise",$cur_entreprise->id_entreprise);
				$this->session->set_userdata("nomEntreprise",$cur_entreprise->nom_entreprise);
				$this->session->set_userdata("logoEntreprise",$cur_entreprise->logo);
				redirect(site_url("accueil"));
			}else{
				//erreur login ou mot de passe incorrect
				redirect(site_url("login/0"));	
			}
		}else{
			//erreur bouton invalide
			redirect(site_url("login"));
		}
	}

	public function exceptAuthenticate($key){
		$status = $this->user->checkUrlKey($key,15);

		if($status==1){
			$entreprise_id = (int) explode("#", decode_url($key))[1];
			if($entreprise_id>0){
				$cur_entreprise = $this->entreprise->getEntrepriseById($entreprise_id);
				$this->session->set_userdata("idUser",$cur_entreprise->utilisateur_id);
				$this->session->set_userdata("username",$cur_entreprise->nom_entreprise);
				$this->session->set_userdata("idEntreprise",$cur_entreprise->id_entreprise);
				$this->session->set_userdata("nomEntreprise",$cur_entreprise->nom_entreprise);
				$this->session->set_userdata("logoEntreprise",$cur_entreprise->logo);
				redirect(site_url("entreprise/profil/3"));
			}else{
				redirect(site_url("login/0"));
			}
		}else{
			redirect(site_url("login"));
		}
	}

	public function inscription(){
		$this->checkSession();
		$this->layout->setTypeCompte(1,"Entreprise agro-alimentaire");

		$types_entreprise = $this->entreprise->getTypes();

		$types_aliment = $this->entreprise->getTypesAliment();

		$postes = $this->employe->getPostes(1);

		$pays = $this->entreprise->getPays();

		$data = array(
			"types_entreprise"=>$types_entreprise,
			"types_aliment"=>$types_aliment,
			"postes"=>$postes,
			"pays"=>$pays
		);

		$this->layout->view("registration",$data);
	}

	public function available_pseudo(){
		$pseudo = $this->input->post('pseudo');
		header('Content-Type: application/json');
		echo json_encode(array("available"=>$this->user->checkPseudo($pseudo)));
	}

	public function mailExist(){
		$mail = $this->input->post('email');
		header('Content-Type: application/json');
		echo json_encode($this->user->mailExist($mail));
	}

	public function register(){
		if($this->input->post('creer_compte')!== null){
			$data = $this->input->post();
			// var_dump($data);

			$utilisateur_data = array(
				"pseudo"=>$data["nom_entreprise"],
				"password"=>$this->encryption->encrypt($data["password"])
			);

			//créer utilisateur
			$utilisateur_id = $this->user->save($utilisateur_data);

			$entreprise_data = array(
				"nom_entreprise" => $data["nom_entreprise"],
				"ca_entreprise" => 0,
				"qte_kg" => -1,
				"qte_mga" => -1,
				"utilisateur_id" => $utilisateur_id,
				"logo"=>"user_placeholder.png"
			);

			//créer entreprise
			$entreprise_id = $this->entreprise->save($entreprise_data);
			// mis a jour proprietaire poste si cree par user
			$idPoste = (int)$data["responsable1_poste"];
			if($this->employe->getPosteOwner($idPoste) < 0){
				$this->employe->updatePoste($idPoste, $entreprise_id);
			}

			//Construct number phone Format
			$id_pays = $data["responsable1_pays"];
			$labelPhone = "";
			$pays = $this->entreprise->getPays();

			foreach ($pays as$p) {
				if($p->id_pays == $id_pays){
					$labelPhone = "".$p->prefixe_tel;
				}
			}

			$data["responsable1_phone"] = $labelPhone.$data["responsable1_phone"];
			///

			$employe_data = array(
				"nom"=>$data["responsable1_nom"],
				"prenom"=>$data["responsable1_prenom"],
				"telephone"=>$data["responsable1_phone"],
				"email"=>$data["responsable1_email"],
				"poste_id"=>$data["responsable1_poste"],
				"sexe"=>$data["sexe"]
			);

			$employe_id = $this->employe->save($employe_data);
			
			$adresse_id = $this->entreprise->savePaysEntreprise($id_pays);

			$adresse_entreprise_id = $this->entreprise->setAdresseEntreprise($entreprise_id,$adresse_id);

			//joindre entreprise et responsable 1
			$this->entreprise->setEmployeEntreprise($entreprise_id,$employe_id,1);

			if(sendMailToNewEA($this->createTempLinkProfil($entreprise_id),$data)){
				$this->session->set_flashdata('inscription_reussie', 1);
				redirect(site_url("login"));
			}
		}else{
			$this->layout->setTypeCompte(1,"Entreprise");
			$this->layout->view("registration");
		}
	}

	public function logout(){
		$this->session->sess_destroy();
		redirect(site_url("login"));
	}

	public function forget_password(){
		$this->layout->view("reset_password");
	}
	
	public function reset_password($key){
		//vérifier la validité de la clé et c'est utilisable qu'une fois
		$utilisateur_id = (int) explode("#", decode_url($key))[1];
		$status = $this->user->checkUrlKey($key);

		if($status==1){
			$this->layout->view("reset_password",array("reset"=>1,"utilisateur_id"=>$utilisateur_id));
		}else{
			redirect(site_url("forget_password"));
		}
	}

	public function update_password(){
		$password = $this->input->post("password");
		$utilisateur_id = $this->input->post("utilisateur_id");

		header('Content-Type: application/json');

		$status = 0;

		if($this->user->updatePassword($utilisateur_id,$password,$this->encryption)){
			$status = 1;
		}

		echo json_encode(array("status"=>$status));
	}

	public function checkMailResponsable(){
		$mail = $this->input->post("mail");

		$status = $this->employe->checkMail($mail);

		if($status==1){
			//get entreprise id
			$cur_entreprise = $this->entreprise->getEntrepriseByResponsableMail($mail);
			
			//générer lien valable 30min
			$key = encode_url("".time()."crmbl#".$cur_entreprise->utilisateur_id);
			$this->user->addUrlKey($key);

			//envoie mail à $mail du lien site_url()."utilisateur/acces/reset_password/".$key
			if(!$this->sendMailResetPassword($mail,site_url()."utilisateur/acces/reset_password/".$key)){
				$status = -1;
			}

		}

		header('Content-Type: application/json');
		
		echo json_encode(array("status"=>$status));
	}

	public function enregistrementReceveur(){
		$this->layout->setTypeCompte(2,"Receveur d'aliments");

		$type_organisation = $this->organisation->getTypes();

		$info_nutri = $this->produit->getInfosNutri();

		$tranches_age = $this->organisation->getTranchesAge();

		$postes = $this->employe->getPostes(2);

		$beneficiaire_organisations = $this->organisation->getBeneficiaireOrganisation();

		$pays = $this->entreprise->getPays();

		$data = array(
			"types_organisation"=>$type_organisation,
			"info_nutri"=>$info_nutri,
			"tranches_age"=>$tranches_age,
			"postes"=>$postes,
			"beneficiaires"=>$beneficiaire_organisations, 
			"pays"=>$pays
		);

		$this->layout->view("registration",$data);
	}

	public function registerReceiver(){
		if($this->input->post('creer_compte_ben')!== null){
			$data = $this->input->post();

			$address = array(
				"label"=>$data['adresse_organisation'],
				"code_postal"=>$data['code_postal_organisation'],
				"ville"=>$data['ville_organisation'],
				"pays_id"=>$data['pays_organisation']
			);

			//Construct number phone Format
			$id_pays = $data["pays_organisation"];
			$labelPhone = "";
			$pays = $this->entreprise->getPays();

			foreach ($pays as$p) {
				if($p->id_pays == $id_pays){
					$labelPhone = "".$p->prefixe_tel;
				}
			}

			

			$adresse_id = $this->organisation->addAdresse($address);

			$organisation = array(
				"nom_organisation"=>$data["nom_organisation"],
				"adresse_id"=>$adresse_id,
				"numero"=>$data["numero"],
				"type_organisation_id"=>(int)$data["type_organisation"]
			);

			$organisation_id = $this->organisation->save($organisation);

			$data["responsable1_phone"] = $labelPhone.$data["responsable1_phone"];

			$employe_data = array(
				"nom"=>$data["responsable1_nom"],
				"prenom"=>$data["responsable1_prenom"],
				"telephone"=>$data["responsable1_phone"],
				"email"=>$data["responsable1_email"],
				"poste_id"=>$data["responsable1_poste"],
				"sexe"=>$data["responsable1_sexe"]
			);

			$employe_id = $this->employe->save($employe_data);

			//beneficiaire
			foreach ($data["beneficiaire"] as $key => $value) {
				$this->organisation->addOrganisationBeneficiaireOrganisation($organisation_id,(int)$value);
			}

			//joindre entreprise et responsable 1
			$this->organisation->setEmployeOrganisation($organisation_id,$employe_id,1);

			//responsable 2
			if(array_key_exists("responsable2_nom",$data) && !empty($data["responsable2_nom"])){

				$data["responsable2_phone"] = $labelPhone.$data["responsable2_phone"];
				
				$employe_data = array(
					"nom"=>$data["responsable2_nom"],
					"prenom"=>$data["responsable2_prenom"],
					"telephone"=>$data["responsable2_phone"],
					"email"=>$data["responsable2_email"],
					"poste_id"=>$data["responsable2_poste"],
					"sexe"=>$data["responsable2_sexe"]
				);

				$employe_id = $this->employe->save($employe_data);

				//joindre entreprise et responsable 1
				$this->organisation->setEmployeOrganisation($organisation_id,$employe_id,2);
			}

			
			foreach ($data as $key => $value) {
				//age
				if(strpos($key,"age_") !== false){
					$tab_key = explode("_",$key);
					$tranche_age_id = $tab_key[1];

					$this->organisation->setEffectifAge($organisation_id,$tranche_age_id,(int)$value);
				}

				//information nutritionnelle
				if(strpos($key,"info_nutri_") !== false){
					if($value=="on"){
						$tab_key = explode("_",$key);
						$info_nutri_id = $tab_key[2];

						$this->organisation->setInfoNutri($organisation_id,$info_nutri_id);
					}
				}
			}

			$this->organisation->setEquipement($organisation_id,1,$data["frigo"]);
			$this->organisation->setEquipement($organisation_id,2,$data["refrigerateur"]);
			$this->organisation->setEquipement($organisation_id,3,$data["transport"]);

			// var_dump($data);

			if(sendMailToNewBen($data)==false){
				echo "Echec de l'enregsitrement !";
			}
		}
		
		$this->layout->setTypeCompte(2,"Receveur d'aliments");
		$this->layout->view("registration",array("succes_ben"=>1));
	}

	public function sendMailResetPassword($mail,$link){

		$subject = "Réinitialisation mot de passe - Accès Foodwise";

		$message='Pour réinitialiser votre mot de passe d\'accès à votre compte entreprise Foodwise, voici un lien accessible une seule fois et valide durant les 30 minutes qui viennent :'.$link;

		return mail($mail,$subject, $message);
	}

	/** -------------- RAVO -------------- */
	public function addNewPoste(){
		$typePoste = (int)$this->input->post('typePoste');
		$nomPoste = trim($this->input->post('nomPoste'));
		$result = array(
			"success" => false,
			"idPoste" => -1
		);
		if(!empty($nomPoste)){
			$idEA = $this->session->userdata("idEntreprise");
			if(empty($idEA)){
				$idEA = -1;
			}
			$result['idPoste'] = $this->employe->addPoste($nomPoste,$typePoste,$idEA);
			$result['success'] = true;
		}

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function getResponsable(){
		$result = array(
			"success"=>false,
			"postes" => array(),
			"responsable" => array(
				"nom" => "",
				"prenom" => "",
				"sexe" => "",
                "poste_id" => "",
                "email" => "",
                "telephone" => ""
            )
		);

		$idUser = $this->session->userdata("idUser");
		if(!empty($idUser) && $idUser>=0){
			$idEntreprise = $this->employe->getIdEntreprise($idUser);
			if($idEntreprise != null){
				$idEmpl3 = $this->employe->getEmploye3ID($idEntreprise);
				if($idEmpl3 != null){
					$employe = $this->employe->getEmploye($idEmpl3);
					$result['success'] = true;
					$result['responsable'] = $employe;
				}
			}
		}
		$result['postes'] = $this->employe->getAllPoste();

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function updateResponsable(){
		$idUser = $this->session->userdata("idUser");
		$idEntreprise = $this->session->userdata("idEntreprise");
		$result = array(
			"success"=>false,
			"message"=>"Certaines données sont invalides",
			"result"=>null
		);
		if(!empty($idUser) && $idUser > 0 && $idEntreprise > 0){
			$magasin = trim($this->input->post('magasin'));
			$nom = trim($this->input->post('nom'));
			$prenom = trim($this->input->post('prenom'));
			$sexe = trim($this->input->post('sexe'));
			$email = strtolower(trim($this->input->post('email')));
			$telephone = trim($this->input->post('telephone'));
			$poste = (int)($this->input->post('poste'));
			$id_magasin = is_numeric($this->input->post('id_magasin')) ? (int)$this->input->post('id_magasin') : -1;
			$employe_responsable_id = is_numeric($this->input->post('employe_responsable_id')) ? (int)$this->input->post('employe_responsable_id') : -1;

			if(!$this->user->mailExist($email)['exist'] || strcmp($this->user->getEmailByIdEntreprise($idEntreprise), $email)==0){
				if(!empty($nom) && (strcmp($sexe, 'M') == 0 || strcmp($sexe, 'F') == 0)){
					if(!empty($magasin)){
						if(!empty($email)){
							if(!empty($telephone)){
								if(!empty($poste) && $poste > 0){
									// Ajout si n'existe pas encore
									if($id_magasin <= 0){
										$newEmploye = $this->employe->addResponsableMagasin($idEntreprise, $nom, $prenom, $sexe, $email, $telephone, $poste);
										$newMag = $this->employe->addMagasin($idEntreprise, $newEmploye['id_employe'], $magasin);
										$newMag['responsable'] = $newEmploye;
										$result['result'] = $newMag;
									}
									// sinon mis a jour
									else{
										$newEmploye = $this->employe->updateResponsableMagasin($employe_responsable_id, $nom, $prenom, $sexe, $email, $telephone, $poste);
										$newMag = $this->employe->updateMagasin($id_magasin, $magasin);
										$newMag['responsable'] = $newEmploye;
										$result['result'] = $newMag;
									}
									$result['success'] = true;
									$result['message'] = "Le responsable de votre magasin est enregistré";
								}
							}
						}
					}
				}
			}
		}

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function deleteResponsable(){
		$idEA = $this->session->userdata("idEntreprise");
		if(!empty($idEA) && $idEA > 0){
			$id_magasin = is_numeric($this->input->post('id_magasin')) ? (int)$this->input->post('id_magasin') : -1;
			$employe_responsable_id = is_numeric($this->input->post('employe_responsable_id')) ? (int)$this->input->post('employe_responsable_id') : -1;
			if($id_magasin > 0){
				$this->employe->deleteMagasin($id_magasin);
			}
			if($employe_responsable_id > 0){
				$this->employe->deleteResponsableMagasin($employe_responsable_id);	
			}
		}
	}

	public function create_info_nutri(){
		$label = trim($this->input->post("info_nutri"));
		$type_id = $this->produit->addInfoNutri($label);

		header('Content-Type: application/json');
		echo json_encode(array("info_id"=>$type_id));
	}

	public function addNewTypeOrganisation(){
		$label = trim($this->input->post("typeOrganisation"));
	
		$result = $this->organisation->addType($label);

		header('Content-Type: application/json');
		echo json_encode(array("type_organisation_id"=>$result['id'], "exist"=>$result['exist']));
	}

	public function addNewBeneficiaireOrganisation(){
		$label = trim($this->input->post("benOrg"));
	
		$result = $this->organisation->addBeneficiaireOrganisation($label);

		header('Content-Type: application/json');
		echo json_encode(array("beneficiaire_organisation_id"=>$result['id'], "exist"=>$result['exist']));
	}

	public function createTempLinkProfil($entreprise_id){
		$key = encode_url("".time()."crmbl#".$entreprise_id);
		$this->user->addUrlKey($key);
		return site_url("utilisateur/acces/exceptAuthenticate/".$key);
	}
}
