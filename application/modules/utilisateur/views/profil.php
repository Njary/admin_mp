<?php
	//$pays = array('Madagascar','La Réunion','Maurice','Comore');
	//$pays = $pays;
	function typeSelected($type, $allTypes){
		foreach($allTypes as $t){
			if($t->id_type_entreprise == $type->id_type_entreprise){
				return true;
			}
		}
		return false;
	}
	function typeAlimentSelected($typeAl, $allTypesAl){
		foreach($allTypesAl as $t){
			if($t->id_type_aliment == $typeAl->id_type_aliment){
				return true;
			}
		}
		return false;
	}


?>
<?php if($erreurUploadImage==1): ?>
	<div class="alert alert-danger alert-dismissible">
	  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	  <h5><i class="icon fa fa-ban"></i> Erreur!</h5>
	  Une erreur inconnue est survenue lors de la mise à jour du logo.
	</div>
<?php elseif($erreurUploadImage==2): ?>
	<div class="alert alert-success alert-dismissible">
      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
      <h5><i class="icon fa fa-check"></i> Succès!</h5>
      Vos informations ont été mise à jour correctement.
    </div>
<?php endif; ?>
<input type="hidden" id="paramProfil" value="<?php echo $erreurUploadImage; ?>" name="">
<div ng-app="profilApp" ng-controller="profilCtrl" ng-init="CONTRAT.ngInit(); listPays = <?php  echo htmlspecialchars(json_encode($pays)); ?> ; listCompte = <?php  echo htmlspecialchars(json_encode($all_types_compte)); ?>">	
	<input type="hidden" id="pseudo_hidden" value="<?php echo $pseudo; ?>"/>
	<div class="row">
		<div class="col-md-3">

		<!-- Profile Image -->
		<div class="card card-primary card-outline">
		  <div class="card-body box-profile">
		    <div class="text-center">

		      <img class="logo-profil profile-user-img img-fluid img-circle"
							 src="<?php
								if(empty($entreprise->logo) || strcmp($entreprise->logo, "user_placeholder.png")==0){
									echo base_url('assets/img/user_placeholder.png');
								}
								else{
									echo base_url($entreprise->logo);
								}
								 ?>"
		           alt="User profile picture">
		    </div>

		    <h3 class="profile-username text-center"><?php echo $entreprise->nom_entreprise; ?>
		    	
		    </h3>

		    <p class="text-muted text-center">Compte Entreprise Agro-alimentaire</p>

		   	<!--<button class="btn btn-secondary btn-block" ng-click="update_logo()" style="white-space: nowrap;">
					<b style="color: #fff;">Changer le logo</b>
					<i class="fa fa-pencil mr-1" style="margin-left:8px;"></i>
			</button>
			-->

		    <button class="btn btn-secondary btn-block" ng-click="edit()" style="white-space: nowrap;">
					<b style="color: #fff;">Mettre à jour</b>
					<i class="fa fa-pencil mr-1" style="margin-left:8px;"></i>
			</button>

			<button class="btn btn-secondary btn-block" ng-click="MAGASIN.openModalMagasins()" style="white-space: nowrap;">
					<b style="color: #fff;">Gérer les magasins</b>
					<i class="fa fa-home mr-1" style="margin-left:8px;"></i>
			</button>
		  </div>
		  <!-- /.card-body -->
		</div>
		<!-- /.card -->

		<!-- /.card -->
		</div>
		<!-- /.col -->
		<div class="col-md-9">
		<div class="card">
		  <div class="card-header p-2">
		    <ul class="nav profilPage nav-pills">
		      <li class="nav-item"><a class="nav-link active" href="#apropos" data-toggle="tab">A propos</a></li>
		      <li class="nav-item"><a class="nav-link" href="#activite" data-toggle="tab">Activités</a></li>
		    </ul>
		  </div><!-- /.card-header -->
		  <div class="card-body">
		    <div class="tab-content">
		      <div class="active tab-pane" id="apropos">
		      	<div class="row">
			    	<div class="col-6">
			    		<strong>
			    			<i class="fa fa-address-card mr-1"></i>
			    			 Immatriculation
		    			 	<i ng-click="editField('#', 'input-nif-entreprise', '#scroll-immatriculation-entreprise')" class="fa fa-pencil mr-1 icon-edit-in-profil"></i>
		    			</strong>
				    	<div>
				    		<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
					    		<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">NIF</div>
					    		<div class="col-8"><?php echo (!empty($entreprise->nif_entreprise))?$entreprise->nif_entreprise:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
					    	</div>
					    	<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
					    		<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">STAT</div>
					    		<div class="col-8"><?php echo (!empty($entreprise->stat_entreprise))?$entreprise->stat_entreprise:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
					    	</div>
						</div>
					    <hr>
			    		<strong>
			    			<i class="fa fa-building mr-1"></i>
			    			 Type de l'entreprise 
		    			 	<i ng-click="editField('.', 'select2-search__field', '#scroll-type-entreprise')" class="fa fa-pencil mr-1 icon-edit-in-profil"></i>
		    			</strong>
				    	<div>
				    		<?php if(empty($types_entreprise)): ?>
				    			<span style="margin-left: 16px;font-style: italic;margin-top: 8px;display: inline-block;">(Non défini)</span>
				    		<?php endif; ?>
					    	<?php foreach ($types_entreprise as $key => $type) : ?>
						      <div class="tag-mp"><?php echo $type->label; ?></div>
							<?php endforeach; ?>
						</div>
					    <hr>
					    <strong>
					    	<i class="fa fa-user mr-1"></i> 
						    	Responsable numéro 1
						    <i ng-click="editField('#', 'input-responsable1_nom', '#scroll-resp1-nom')" class="fa fa-pencil mr-1 icon-edit-in-profil"></i>
						</strong>
					    <div>
					    	<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
					    		<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom</div>
					    		<div class="col-8"><?php echo $responsables[0]->nom." ".$responsables[0]->prenom; ?></div>
					    	</div>
					    	<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
					    		<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Poste</div>
					    		<div class="col-8"><?php echo (!empty($responsables[0]->poste))?$responsables[0]->poste:'<span style="font-style: italic;">(Non définie)</span>'; ?></div>
					    	</div>
					    	<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
					    		<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Email</div>
					    		<div class="col-8"><?php echo $responsables[0]->email; ?></div>
					    	</div>
					    	<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
					    		<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Téléphone</div>
					    		<div class="col-8"><?php echo $responsables[0]->telephone; ?></div>
					    	</div>
					    </div>
					    <hr>
					    <strong>
					    	<i class="fa fa-user mr-1"></i> 
						    	Responsable numéro 2
						    <i ng-click="editField('#', 'input-responsable2_nom', '#scroll-resp2-nom')" class="fa fa-pencil mr-1 icon-edit-in-profil"></i>
						</strong>
					    <?php if(count($responsables)==2): ?>
						    <div>
						    	<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
						    		<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Nom</div>
						    		<div class="col-8"><?php echo $responsables[1]->nom." ".$responsables[1]->prenom; ?></div>
						    	</div>
						    	<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
						    		<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Poste</div>
						    		<div class="col-8"><?php echo $responsables[1]->poste; ?></div>
						    	</div>
						    	<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
						    		<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Email</div>
						    		<div class="col-8"><?php echo $responsables[1]->email; ?></div>
						    	</div>
						    	<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
						    		<div class="col-4" style="padding-left:20px;font-weight: bolder;color: #646369;">Téléphone</div>
						    		<div class="col-8"><?php echo $responsables[1]->telephone; ?></div>
						    	</div>
						    </div>
						<?php else: ?>
							<br>
							<span style="margin-left: 16px;font-style: italic;">Aucun</span>
						<?php endif; ?>

			    	</div>

			    	<div class="col-6">
			    		<strong>
			    			<i class="fa fa-map-marker mr-1"></i> 
				    			Adresse
				    		<i ng-click="editField('#', 'input-adresse_entreprise', '#scroll-adresse-entreprise')" class="fa fa-pencil mr-1 icon-edit-in-profil"></i>
				    	</strong>
			    		<?php if(empty($adresses)): ?>
			    			<br>
			    			<span style="margin-left: 16px;font-style: italic;margin-top: 8px;display: inline-block;">(Non défini)</span>
			    		<?php endif; ?>
					    <ul style="margin-top: 13px;">
					    	<?php foreach ($adresses as $key => $value) : ?>
						      	<li>
						      		<?php if($value->label){ echo $value->label.", ".$value->code_postal." - ".$value->ville.", ".$value->pays; }else{ echo "<span style='font-style: italic;display: inline-block;'>(Non défini)</span>"; } ?>
						    	</li>
						    <?php endforeach; ?>
					    </ul>
					    <hr>
					    <strong>
					    	<i class="fa fa-gift mr-1"></i> 
						    	Type d'aliments
						    <i ng-click="editField('.', 'check', '#scroll-type-aliment')" class="fa fa-pencil mr-1 icon-edit-in-profil"></i>
						</strong>
					    <div>
					    	<?php if(empty($types_aliment)): ?>
				    			<span style="margin-left: 16px;font-style: italic;margin-top: 8px;display: inline-block;">(Non défini)</span>
				    		<?php endif; ?>
					    	<?php foreach ($types_aliment as $key => $type) : ?>
						      <div class="tag-mp"><?php echo $type->label; ?></div>
							<?php endforeach; ?>
					    </div>
					    <hr>
					    <strong>
			    			<i class="fa fa-credit-card mr-1"></i>
			    			 Information paiement
		    			 	<i ng-click="editField('#', 'select-compte-paiement', '#scroll-compte-entreprise')" class="fa fa-pencil mr-1 icon-edit-in-profil"></i>
		    			</strong>
				    	<div>
				    		<?php if(empty($info_paiement2)): ?>
				    			<span style="margin-left: 16px;font-style: italic;margin-top: 8px;display: inline-block;">(Non défini)</span>
				    		<?php endif; ?>
				    		<?php foreach ($info_paiement2 as $key => $paiement) : ?>
						      	<div class="row" style="margin-top: 4px;margin-bottom: 4px;">
						    		<div class="col-6" style="padding-left:20px;font-weight: bolder;color: #646369;"><?php echo $paiement->label; ?></div>
						    		<div class="col-6"><?php echo $paiement->numero_compte; ?></div>
					    		</div>
							<?php endforeach; ?>
						</div>
					    <hr>


					    <strong>
					    	<i class="fa fa-file-text-o mr-1"></i>
						    	Protocole d'entente
					    	<?php if(count($rapport)>0){ ?>
						    	<i title="Télécharger le fichier" onclick="window.location='<?= base_url($rapport[0]->fichier); ?>'" class="fa fa-download mr-1 icon-edit-in-profil"></i>
						    <?php } ?>
						</strong>
				    	<p>Ce document vous protège juridiquement en enlevant/renonçant votre responsabilité envers les produits alimentaires donnés.</p>
					    <?php $btnProtocole = 'Compléter';
				     		if(count($rapport)>0): ?>
						    <p class="text-muted">
						      <!--<a href="<?php //echo base_url($rapport[0]->fichier); ?>" target="_blank"> Protocole d'entente <?php //echo $entreprise->nom_entreprise; ?> - Equipe Manzer partazer</a> ')"-->
						      <span ng-click="CONTRAT.downloadProtocole()" style="cursor:pointer !important; color: #00AFAA !important;"> Protocole d'entente <?php echo $entreprise->nom_entreprise; ?> - Equipe Manzer partazer </span>
						      <input type="hidden" id="contrat_file_url" value="<?php echo base_url($rapport[0]->fichier); ?>" />
						    </p>
						<?php $btnProtocole = 'Modifier';
						 endif; ?>
							
						 	<div style="text-align: right;">
								<button ng-click="completeProtocole()" class="btn btn-primary btn-flat" style="font-style: italic; border-radius: 4px;"><?= $btnProtocole; ?></button>
							</div>	
							<?php if(count($rapport)>0 && $CONTRAT['contrat']['etat'] == 0): ?>
							<a href="<?= base_url(''); ?>contrat/entente/validate/1" class="btn btn-primary btn-flat" style="font-style: italic; border-radius: 4px;">Accepter</a>
							<?php endif; ?>
			    	</div>
			    </div>
		      </div>
		      <!-- /.tab-pane -->
		      <div class="tab-pane" id="activite">
		        <!-- The timeline -->
		        <ul class="timeline timeline-inverse">
		          <!-- timeline time label -->
		          <li class="time-label">
		            <span class="bg-danger">
		              10 Feb. 2014
		            </span>
		          </li>
		          <!-- /.timeline-label -->
		          <!-- timeline item -->
		          <li>
		            <i class="fa fa-envelope bg-primary"></i>

		            <div class="timeline-item">
		              <span class="time"><i class="fa fa-clock-o"></i> 12:05</span>

		              <h3 class="timeline-header"><a href="#">Support Team</a> sent you an email</h3>

		              <div class="timeline-body">
		                Etsy doostang zoodles disqus groupon greplin oooj voxy zoodles,
		                weebly ning heekya handango imeem plugg dopplr jibjab, movity
		                jajah plickers sifteo edmodo ifttt zimbra. Babblely odeo kaboodle
		                quora plaxo ideeli hulu weebly balihoo...
		              </div>
		              <div class="timeline-footer">
		                <a href="#" class="btn btn-primary btn-sm">Read more</a>
		                <a href="#" class="btn btn-danger btn-sm">Delete</a>
		              </div>
		            </div>
		          </li>
		          <!-- END timeline item -->
		          <!-- timeline item -->
		          <li>
		            <i class="fa fa-user bg-info"></i>

		            <div class="timeline-item">
		              <span class="time"><i class="fa fa-clock-o"></i> 5 mins ago</span>

		              <h3 class="timeline-header no-border"><a href="#">Sarah Young</a> accepted your friend request
		              </h3>
		            </div>
		          </li>
		          <!-- END timeline item -->
		          <!-- timeline item -->
		          <li>
		            <i class="fa fa-comments bg-warning"></i>

		            <div class="timeline-item">
		              <span class="time"><i class="fa fa-clock-o"></i> 27 mins ago</span>

		              <h3 class="timeline-header"><a href="#">Jay White</a> commented on your post</h3>

		              <div class="timeline-body">
		                Take me to your leader!
		                Switzerland is small and neutral!
		                We are more like Germany, ambitious and misunderstood!
		              </div>
		              <div class="timeline-footer">
		                <a href="#" class="btn btn-warning btn-flat btn-sm">View comment</a>
		              </div>
		            </div>
		          </li>
		          <!-- END timeline item -->
		          <!-- timeline time label -->
		          <li class="time-label">
		            <span class="bg-success">
		              3 Jan. 2014
		            </span>
		          </li>
		          <!-- /.timeline-label -->
		          <!-- timeline item -->
		          <li>
		            <i class="fa fa-camera bg-purple"></i>

		            <div class="timeline-item">
		              <span class="time"><i class="fa fa-clock-o"></i> 2 days ago</span>

		              <h3 class="timeline-header"><a href="#">Mina Lee</a> uploaded new photos</h3>

		              <div class="timeline-body">
		                <!--<img src="http://placehold.it/150x100" alt="..." class="margin">
		                <img src="http://placehold.it/150x100" alt="..." class="margin">
		                <img src="http://placehold.it/150x100" alt="..." class="margin">
		                <img src="http://placehold.it/150x100" alt="..." class="margin"> -->
		              </div>
		            </div>
		          </li>
		          <!-- END timeline item -->
		          <li>
		            <i class="fa fa-clock-o bg-gray"></i>
		          </li>
		        </ul>
		      </div>
		      <!-- /.tab-pane -->

		      <!-- /.tab-pane -->
		    </div>
		    <!-- /.tab-content -->
		  </div><!-- /.card-body -->
		</div>
		<!-- /.nav-tabs-custom -->
		</div>
		<!-- /.col -->
	</div>

	<!--
	<div id="modal_edit_logo" class="modal fade" tabindex="-1" role="dialog">
		<div class="modal-dialog modal-lg" >
			<div class="modal-content">
				<div class="modal-header">
			        <h5 class="modal-title">Mettre à jour la logo de l'entreprise</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          <span aria-hidden="true">&times;</span>
			        </button >
			     </div>
			     <div class="modal-body" >
			     	<form action="<?php //echo site_url('entreprise/editLogoProfil'); ?>" method="POST" id="formRegister" enctype="multipart/form-data">
				      	 <div class="form-group">
		                    <label>Veuillez importer votre logo</label><br>
		                    <label for="input-file-image" class="col-6 form-control btn btn-secondary">Parcourir sur mon ordinateur<i class="fa fa-picture-o" style="margin-left:8px;"></i></label>
		                    <!-// input type file onchange : onchange="angular.element(this).scope().onFileImageChange(this)" //->
		                    <input type="hidden" id="x1" name="x1" />
			        		<input type="hidden" id="y1" name="y1" />
			        		<input type="hidden" id="x2" name="x2" />	
			        		<input type="hidden" id="y2" name="y2" />
		                    <!-//input id="input-file-image" name="logo_entreprise"  type="file" accept="image/*" style="display: none;" /> //->
		                    <input type="file" name="logo_entreprise" id="input-file-image" style="display: none;" onchange="angular.element(this).scope().fileSelectHandler(this)" />
		                    <div class="info_pour_logo" ><label>Veuillez choisir une région pour votre logo. </label></div>
		                    <div class="error" style="color: #f00 !important;"></div>
		                    <div class="row" id="div-file-preview" style="overflow: auto !important;">
		                        <img id="preview"/>  
		                    </div>
		                    <div class="info" style="display: none !important;">
				                <label>File size</label> <input type="text" id="filesize" name="filesize" />
				                <label>Type</label> <input type="text" id="filetype" name="filetype" />
				                <label>Image dimension</label> <input type="text" id="filedim" name="filedim" />
				                <label>W</label> <input type="text" id="w" name="w" />
				                <label>H</label> <input type="text" id="h" name="h" />
			            	</div>
		                </div>
		                <div class="modal-footer">
		                	<div class="row">
				              <div class="col-6">
				                <a class="btn btn-secondary" data-dismiss="modal" style="color: #fff;">Annuler</a>
				              </div>
				              <div class="col-6">
				                <span ng-click="validLogoEdit()" class="btn btn-primary">Valider</span>
				              </div>
				                <button id="btn-modifier_logo" name="modifier_logo" type="submit" class="btn btn-primary" style="display: none;">Valider</button>
				             
				            </div>
		                </div> 
		            </form>
			     </div>
			</div>
		</div>
	</div>
-->

	<div id="modal_edit" class="modal fade" tabindex="-1" role="dialog">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">Mettre à jour les informations</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        <form action="<?php echo site_url('entreprise/editProfil'); ?>" method="POST" id="formRegister" enctype="multipart/form-data">
	        	
	        	<div class="form-group">
                    <label>Logo</label><br>
                    <div class="thumbnail">
                    	<img class="logo-profil profile-user-img col-4" id="logo_to_edit" style="margin-left: 3px !important;"
							 src="<?php
								if(empty($entreprise->logo) || strcmp($entreprise->logo, "user_placeholder.png")==0){
									echo base_url('assets/img/user_placeholder.png');
								}
								else{
									echo base_url($entreprise->logo);
								}
								 ?>"
		           alt="User profile picture"><br>
		           		 <label for="input-file-image" class="col-4 form-control btn btn-secondary">Modifier<i class="fa fa-picture-o" style="margin-left:8px;"></i></label>
		           		  <input type="file" name="logo_entreprise" id="input-file-image" style="display: none;" onchange="angular.element(this).scope().onLogoEntrepriseChange(this)" />
                    </div>

                   
                    <!--// input type file onchange : onchange="angular.element(this).scope().onFileImageChange(this)" //-->
                    <!--<input type="hidden" id="x1" name="x1" />
	        		<input type="hidden" id="y1" name="y1" />
	        		<input type="hidden" id="x2" name="x2" />	
	        		<input type="hidden" id="y2" name="y2" />-->
                    <!--//input id="input-file-image" name="logo_entreprise"  type="file" accept="image/*" style="display: none;" /> //-->
                    <!--<input type="file" name="logo_entreprise" id="input-file-image" style="display: none;" onchange="angular.element(this).scope().fileSelectHandler(this)" />-->
                   
                    <!--<div class="error" style="color: #f00 !important;"></div>-->

                    <div class="row" id="div-file-preview" style="margin-left: auto; margin-right: auto;">
                    	<div id="upload-demo" style="margin-left: auto;  margin-right: auto;">
                    		
                    	</div>
                        <!--<img id="preview"/> --> 
                    </div>
                    <input type="hidden" id="imagebase64" name="imagebase64">
                    <!--<div class="info" style="display: none !important;">
		                <label>File size</label> <input type="text" id="filesize" name="filesize" />
		                <label>Type</label> <input type="text" id="filetype" name="filetype" />
		                <label>Image dimension</label> <input type="text" id="filedim" name="filedim" />
		                <label>W</label> <input type="text" id="w" name="w" />
		                <label>H</label> <input type="text" id="h" name="h" />
	            	</div>-->
                </div>
            	

	        	<label id="scroll-type-entreprise">Type d'entreprise</label>
	        	<div class="loader-default col-1" ng-if="loadType"><img src="<?php echo img_url('spin.svg'); ?>"></div>
	            <div class="form-group row">
	              <div class="col-12" id="div_select_types_entreprise">
						<select id="select_types_entreprise" name="type_entreprise[]" class="form-control select2 select2-hidden-accessible" multiple="multiple" data-placeholder="Choisir le type de votre entreprise*" style="width: 100%;" tabindex="-1" aria-hidden="true">
	                  		<option></option>
						<?php foreach($all_types_entreprise as $type): ?>
	                    	<option value="<?php echo $type->id_type_entreprise; ?>" <?php if(typeSelected($type, $types_entreprise)) echo "selected"; ?>><?php echo $type->label; ?></option>
	                  <?php endforeach; ?>
	                </select>
	              </div>
	              <!--<div class="col-2">
	                <a class="btn btn-primary btn-block" ng-click="addTypeEntreprise()" title="Ajouter un autre type"><span>Ajouter</span></a>
	              </div>-->
	            </div>

	            <label>Nom de l'entreprise</label>
	            <div class="form-group has-feedback">
	              <input name="nom_entreprise" type="text" class="form-control form-to-test" placeholder="Nom de votre entreprise*" value="<?php echo $entreprise->nom_entreprise; ?>" required>
	              <span class="fa fa-building form-control-feedback"></span>
	            </div>
	            <hr/>
	            <label id="scroll-immatriculation-entreprise">Numéro d'Identité Fiscale (NIF)</label>        
	            <div class="form-group has-feedback">	
	              <input id="input-nif-entreprise" name="nif_entreprise" type="text" class="form-control form-to-test" placeholder="NIF de votre entreprise" value="<?php echo $entreprise->nif_entreprise; ?>" title="Numéro d'identité fiscale"> 
	              <span class="fa fa-address-card form-control-feedback"></span>
	            </div>

	           <label>Stat</label>
	            <div class="form-group has-feedback">
	              <input name="stat_entreprise" type="text" class="form-control form-to-test" placeholder="STAT de votre entreprise" value="<?php echo $entreprise->stat_entreprise; ?>" title="Stat de l'entreprise"> 
	              <span class="fa fa-address-card form-control-feedback"></span>
	            </div>
	            
	            <hr/>

				<label id="scroll-adresse-entreprise">Adresse de votre entreprise</label>
	            <div class="form-group row">
	              <div class="col-12">
	                <input id="input-adresse_entreprise" name="adresse_entreprise" type="text" class="form-control form-to-test" placeholder="Adresse de votre entreprise*" value="<?php echo (empty($adresses))?'':$adresses[0]->label; ?>" required="" title="Adresse de l'entreprise">
	              </div>
	            </div>
				<div class="form-group " style="margin-top: -10px;">
					<input name="code_postal_entreprise" value="<?php echo (empty($adresses))?'':$adresses[0]->code_postal; ?>" type="text" class="form-control form-to-test" placeholder="Code postal*" require="" title="Code postal"/>
				</div>
				<div class="form-group ">
					<input name="ville_entreprise" value="<?php echo (empty($adresses))?'':$adresses[0]->ville; ?>" type="text" class="form-control form-to-test" placeholder="Ville*" require="" title="Ville"/>
				</div>

				<div class="form-group ">

					<?php foreach($pays as $p): ?>

	              	 <?php if(count($adresses)>0){if( $adresses[0]->pays_id==$p->id_pays){ echo '<input id="paysValue0" type="hidden" name="model" value="'.$adresses[0]->pays_id.'">';}} ?>
	                    	
	                <?php endforeach; ?>

					<select  name="pays_entreprise" id="pays_entreprise" data-placeholder="Pays*" class="form-control select2 form-to-test" style="width: 100%;" require="" title="Pays" 
					ng-model="selectPays[0]" ng-change="changePays(0)" ng-init="initValPays(0)">
						<option></option>
						<?php for($i=0; $i<count($pays); $i++){ ?>
							<option value="<?php echo $pays[$i]->id_pays; ?>" <?php echo (!empty($adresses) && ($adresses[0]->pays_id == $pays[$i]->id_pays))?'selected': ''; ?>><?php echo $pays[$i]->label ?></option>
						<?php } ?>
					</select>
				</div>

				
				<h6 id="scroll-coordonnee-entreprise">Coordonnée géographique (Latitude, Longitude)</h6>
				
				<div class="form-group row">
		            <div class="form-group col-md-5">
						<input id="infoLatitude9990" name="latitude_entreprise" value="<?php echo (empty($adresses))?'':$adresses[0]->latitude; ?>" type="text" class="form-control form-to-test lat-to-test" placeholder="Latitude*" require="" title="Latitude"/>
					</div>
					<div class="form-group col-md-5">
						<input id="infoLongitude9990" name="longitude_entreprise" value="<?php echo (empty($adresses))?'':$adresses[0]->longitude; ?>" type="text" class="form-control form-to-test long-to-test " placeholder="Longitude*" require="" title="Longitude"/>
					</div>
					<div class="col-md-2">
						<a href="#"  class="btn btn-primary btn-block" ng-click="initShowMap(9990)" data-toggle="modal" data-target="#modal-show-map" title="Sélectionner les coordonnées sur une carte" ><i class="fa fa-map"></i> </a>
					</div>
				
				</div>

	            <div id="otherAdress">
	        		<?php foreach ($adresses as $key => $value) : ?>
	        			<?php if($key!=0): ?>
	        				<div class="form-group input-group adr<?php echo $value->id_adresse; ?>">
	        					<input name="adresse_entreprise<?php echo $value->id_adresse; ?>" type="text" class="form-control form-to-test" placeholder="Autre adresse de votre entreprise" value="<?php echo $value->label; ?>">
	        					<div class="input-group-append" style="cursor: pointer;" title="Supprimer" ng-click="removeHtml('.adr<?php echo $value->id_adresse; ?>')">
	        						<span class="input-group-text"><i class="fa fa-minus"></i></span>
	        					</div>
	        				</div>
									<div class="form-group adr<?php echo $value->id_adresse; ?>">
										<input name="code_postal_entreprise<?php echo $value->id_adresse; ?>" value="<?php echo $value->code_postal; ?>" type="text" class="form-control form-to-test" placeholder="Code postal*" require=""/>
									</div>
									<div class="form-group adr<?php echo $value->id_adresse; ?>">
										<input name="ville_entreprise<?php echo $value->id_adresse; ?>" value="<?php echo $value->ville; ?>" type="text" class="form-control form-to-test" placeholder="Ville*" require=""/>
									</div>
									<div class="form-group adr<?php echo $value->id_adresse; ?>">
										<select id="pays_entreprise_1" name="pays_entreprise<?php echo $value->id_adresse; ?>" data-placeholder="Pays*" class="form-control select2 form-to-test" style="width: 100%;" require="">
											<option></option>
											<?php for($i=0; $i<count($pays); $i++){ ?>
												<option value="<?php echo $pays[$i]->id_pays; ?>" <?php //echo (strcmp($value->pays, $pays[$i])==0)?'selected': ''; ?>
												<?php if($value->pays_id == $pays[$i]->id_pays){ echo "selected";} ?>><?php echo $pays[$i]->label ?></option>
											<?php } ?>
										</select>
									</div>
									<h6 id="scroll-coordonnee-entreprise" class="adr<?php echo $value->id_adresse; ?>"">Coordonnée géographique (Latitude, Longitude)</h6>
									<div class="form-group row adr<?php echo $value->id_adresse; ?>">
							            <div class="form-group col-md-5 adr<?php echo $value->id_adresse; ?>">
											<input id="infoLatitude<?php echo $value->id_adresse; ?>" name="latitude_entreprise<?php echo $value->id_adresse; ?>" value="<?php echo $value->latitude; ?>" type="text" class="form-control form-to-test lat-to-test" placeholder="Latitude*" require="" title="Latitude"/>
										</div>
										<div class="form-group col-md-5 adr<?php echo $value->id_adresse; ?>">
											<input id="infoLongitude<?php echo $value->id_adresse; ?>" name="longitude_entreprise<?php echo $value->id_adresse; ?>" value="<?php echo $value->longitude; ?>" type="text" class="form-control form-to-test long-to-test" placeholder="Longitude*" require="" title="Longitude"/> 	
										</div>
										<div class="col-md-2 adr<?php echo $value->id_adresse; ?>">
											<a href="#"  class="btn btn-primary btn-block" ng-click="initShowMap(<?php echo $value->id_adresse; ?>)" data-toggle="modal" data-target="#modal-show-map" title="Sélectionner les coordonnées sur une carte" ><i class="fa fa-map"></i> </a>
										</div>
									</div>
	        			<?php endif; ?>
	        		<?php endforeach; ?>
	            </div>
	            <div class="form-group row col-12">
	                <a class="btn btn-primary btn-block" ng-click="addAdresse()" title="Ajouter une autre adresse de l’entreprise"><span>Ajouter une autre adresse</span></a>
	              </div>
	            <hr/>
	            <div class="form-group has-feedback">
	            	<label>Responsable numéro 1 <i>(ex : Directeur Général)</i></label><br/>
					<input name="responsable1_id" value="<?php echo $responsables[0]->id_employe; ?>" type="hidden" class="form-control" placeholder="Nom*" required="">
					<br/><label>Nom</label>
					<div class="input-group input-group-md">
					  <div class="input-group-prepend" id="sexe-select">
					    <select  id="select_responsable_1" class="form-control select2" name="responsable1_sexe">
					      <option value="M" <?php if($responsables[0]->sexe=="M") echo "selected"; ?>>Mr</option>
					      <option value="F" <?php if($responsables[0]->sexe=="F") echo "selected"; ?>>Mme</option>
					    </select>
					  </div>

					  <input id="input-responsable1_nom" name="responsable1_nom" type="text" class="form-control form-to-test" placeholder="Nom*" required="" value="<?php echo $responsables[0]->nom; ?>">
					</div>
	            </div>

	            <label>Prénom</label>
	            <div class="form-group has-feedback">
	              <input name="responsable1_prenom" type="text" class="form-control form-to-test" placeholder="Prénom(s)*" required="" value="<?php echo $responsables[0]->prenom; ?>">
	            </div>
	            <label>Poste</label>
	            <div class="loader-default col-1" ng-if="loadType"><img src="<?php echo img_url('spin.svg'); ?>"></div>
	            <div class="form-group row">
	              <div class="col-12">
	                <select id="select_poste1" name="responsable1_poste" data-placeholder="Poste à l'enceinte de l'entreprise*" class="form-control select2 form-to-test" style="width: 100%;">
										<option></option>
	                  <?php foreach($postes as $poste): ?>
	                    <option value="<?php echo $poste->id_poste; ?>" <?php if($responsables[0]->poste_id==$poste->id_poste) echo "selected"; ?>><?php echo $poste->label; ?></option>
	                  <?php endforeach; ?>
	                </select>
	              </div>
	              <!--<div class="col-2">
	                <a class="btn btn-primary btn-block" ng-click="addPoste('#select_poste1')" title="Ajouter une autre poste"><span>Ajouter</span></a>
	              </div>-->
	            </div>

	            <label>Email</label>
	            <div class="form-group has-feedback">
	              <input ng-keyup="verifMail('1')" id="responsable1_email" name="responsable1_email" value="<?php echo $responsables[0]->email; ?>" type="email" class="form-control form-to-test" placeholder="Email*" required="">
								<div class="loader-default col-2" id="loader-email-resp1" style="display: none; position: absolute;right: -9px;top: -14px;"><img src="<?php echo img_url('spin.svg'); ?>"></div>
              	<span class="form-error" id="msg-email-resp1" style="display: none;">cette adresse e-mail est déjà utilisée par un membre du plateforme</span>
            	</div>

            	<label>Téléphone</label>
	            <div class="form-group has-feedback">
	            	<div class="row">
			            <div class="col-md-2">
		                  <span class="input-group-text labelTelephone0" > - </span>
		                </div>
		                <div class="col-md-10">
		              		<input name="responsable1_phone" id="responsable1_phone" value="<?php echo substr($responsables[0]->telephone, 4); ?>" type="text" class="form-control form-to-test" placeholder="Téléphone*" required="">
		              	</div>
		            </div>
	            </div>

	            <?php if(count($responsables)==2) : ?>
	            	<div>
						<div class="form-group has-feedback">
							<label>Responsable numéro 2 <i>(ex : Directeur Marketing)</i></label>
							<input name="responsable2_id" value="<?php echo $responsables[1]->id_employe; ?>" type="hidden" class="form-control" placeholder="Nom*" required="">

							<div class="input-group input-group-md">
							  <div class="input-group-prepend" id="sexe-select">
							    <select id="select_responsable_2" class="form-control select2" name="responsable2_sexe">
							      <option value="M" <?php if($responsables[1]->sexe=="M") echo "selected"; ?>>Mr</option>
							      <option value="F" <?php if($responsables[1]->sexe=="F") echo "selected"; ?>>Mme</option>
							    </select>
							  </div>
							  <input id="input-responsable2_nom" name="responsable2_nom" type="text" class="form-control form-to-test" placeholder="Nom*" required="" value="<?php echo $responsables[1]->nom; ?>">
							</div>
						</div>

						<div class="form-group has-feedback">
			              <input name="responsable2_prenom" type="text" class="form-control form-to-test" placeholder="Prénom(s)*" required="" value="<?php echo $responsables[1]->prenom; ?>">
			            </div>

					<div class="form-group row">
						<div class="loader-default col-1" ng-if="loadType"><img src="<?php echo img_url('spin.svg'); ?>"></div>
						<div class="col-12">
							<select id="select_poste2A" name="responsable2_poste" class="form-control select2 form-to-test" style="width: 100%;">
								<option disabled selected value>Poste à l'enceinte de l'entreprise*</option>
							  <?php foreach($postes as $poste): ?>
							    <option value="<?php echo $poste->id_poste; ?>" <?php if($responsables[1]->poste_id==$poste->id_poste) echo "selected"; ?>><?php echo $poste->label; ?></option>
							  <?php endforeach; ?>
							</select>
						</div>
						<!--<div class="col-2">
							<a class="btn btn-primary btn-block" ng-click="addPoste('#select_poste2A')" title="Ajouter un autre type"><span>Ajouter</span></a>
						</div>-->
					</div>

						<div class="form-group has-feedback">
							<input ng-keyup="verifMail('2')" id="responsable2_email" name="responsable2_email" value="<?php echo $responsables[1]->email; ?>" type="email" class="form-control" placeholder="Email*">
							<div class="loader-default col-2" id="loader-email-resp2" style="display: none; position: absolute;right: -9px;top: -14px;"><img src="<?php echo img_url('spin.svg'); ?>"></div>
							<span class="form-error" id="msg-email-resp2" style="display: none;">cette adresse e-mail est déjà utilisée par un membre du plateforme</span>
						</div>
		              <div class="form-group has-feedback">
		              	<div class="row">
				            <div class="col-md-2">
			                  <span class="input-group-text labelTelephone0" > - </span>
			                </div>
			                <div class="col-md-10">
		                		<input name="responsable2_phone" id="responsable2_phone" value="<?php echo substr($responsables[1]->telephone,4); ?>" type="text" class="form-control form-to-test" title="Numéro téléphone du second responsable" placeholder="Téléphone*">
		                	</div>
		                </div>
		              </div>
		            </div>
	            <?php else: ?>
		            <div id="scroll-resp2-nom" class="row" ng-if="!twoResponsable">
		              <div class="col-10 offset-1" style="margin-bottom: 10px;">
		                <a class="btn btn-default btn-block btn-sm" ng-click="addResponsable()">Ajouter un deuxième responsable</a>
		              </div>
		            </div>

		            <div ng-if="twoResponsable">
									<label>Responsable numéro 2 <i>(ex : Directeur Marketing)</i></label>
									<div class="input-group input-group-md has-feedback">
										<div class="input-group-prepend" id="sexe-select">
											<select id="select-sexe2B" name="responsable2_sexe">
												<option value="M">Mr</option>
												<option value="F">Mme</option>
											</select>
										</div>
										<input name="responsable2_nom" type="text" class="form-control form-to-test" placeholder="Nom*" />
									</div>

		              <div class="form-group has-feedback" style="margin-top: 16px;">
			              <input name="responsable2_prenom" type="text" class="form-control form-to-test" placeholder="Prénom(s)*" />
			            </div>
		              
		              <div class="form-group row">
		              	<div class="loader-default col-1" ng-if="loadType"><img src="<?php echo img_url('spin.svg'); ?>"></div>
							<div class="col-12">
								<select id="select_poste2B" class="form-to-test" name="responsable2_poste" data-placeholder="Poste à l'enceinte de l'entreprise*" style="width: 100%;">
								<option></option>
									<?php foreach($postes as $poste): ?>
								    <option value="<?php echo $poste->id_poste; ?>"><?php echo $poste->label; ?></option>
								  <?php endforeach; ?>
								</select>
							</div>
							<!--<div class="col-2">
								<a class="btn btn-primary btn-block" ng-click="addPoste('#select_poste2B')" title="Ajouter une autre poste"><span>Ajouter</span></a>
							</div>-->
						</div>

		              <div class="form-group has-feedback">
		                <input ng-keyup="verifMail('2')" id="responsable2_email" name="responsable2_email" type="email" class="form-control form-to-test" placeholder="Email*">
										<div class="loader-default col-2" id="loader-email-resp2" style="display: none; position: absolute;right: -9px;top: -14px;"><img src="<?php echo img_url('spin.svg'); ?>"></div>
										<span class="form-error" id="msg-email-resp2" style="display: none;">cette adresse e-mail est déjà utilisée par un membre du plateforme</span>
									</div>
		              <div class="form-group has-feedback">
		              	<div class="row">
				            <div class="col-md-2">
			                  <span class="input-group-text labelTelephone0"> - </span>
			                </div>
			                <div class="col-md-10">
		                		<input name="responsable2_phone" id="responsable2_phone" type="text" class="form-control form-to-test" title="Numéro téléphone su second responsable" placeholder="Téléphone*">
		                	</div>
		                </div>
		              </div>
		            </div>
		        <?php endif; ?>


		        <hr/>
		        <div id="select-compte-container">
					 <select id="select_compte" placeholder="Autre compte de paiement" style="width: 100%;">
					 	<option></option>
	                  <?php foreach($all_types_compte as $compte): ?>
	                    	<option class="optionCompte"  value="<?php echo $compte->id_compte; ?>"><?php echo $compte->label; ?>
	                    	</option>
	                  <?php endforeach; ?>
	                </select>
				</div>

		        <label id="scroll-compte-entreprise"> Compte entreprise <i>(Ex: Compte bancaire,Mobile banking,...)</i> </label>
		        <div class="form-group row">
	              <div class="col-10">
	              	 <?php foreach($all_types_compte as $compte): ?>

	              	 <?php if(count($info_paiement)>0){if( $info_paiement[0]->compte_id==$compte->id_compte){ echo '<input id="compteValue0" type="hidden" name="model" value="'.$info_paiement[0]->compte_id.'">';}} ?>
	                    	
	                  <?php endforeach; ?>

	                <select id="select-compte-paiement0" class="select_compte_entreprise form-control select2" name="information_compte0" data-placeholder="Compte de paiement de l'entreprise" ng-init="initCompteValue(0)" ng-model="selectCompte[0]" ng-change="changeTypeCompte(0)" style="width: 100%;">
					 	<option></option>
	                  <?php foreach($all_types_compte as $compte): ?>
	                    	<option ng-if="typeCompteInPays == <?php echo $compte->pays_id; ?>" value="<?php echo $compte->id_compte; ?>" <?php if(count($info_paiement)>0){if( $info_paiement[0]->compte_id==$compte->id_compte){ echo "selected";}} ?> ><?php echo $compte->label; ?>
	                    	</option>
	         
	                    	
	                  <?php endforeach; ?>
	                </select>
	              </div>
	              <div class="form-group col-2">
	                <a class="btn btn-primary btn-block" ng-click="addCompte()" title="Ajouter une autre compte"><span>Ajouter</span></a>
	              </div>
	            </div>
    
	            <!--label>Numéro de compte</label-->
				<div class="form-group has-feedback">
					<div class="row">
						<div class="col-md-3">
							<span class="input-group-text labelCompte" id="labelCompte0"> Compte n° </span>
						</div>
						<div class="col-md-9">
							<input id="numero-compte-paiement0" name="numero_compte_entreprise0" value="<?php echo (empty($info_paiement))?'':$info_paiement[0]->numero_compte; ?>" type="text" class="form-control numero_compte_entreprise" placeholder="Numéro de compte"/>
							<span class="loader-default col-1" id="loader-numCompte-resp0" style="display: none; position: absolute;right: 0px;top: 5px;"><img src="<?php echo img_url('spin.svg'); ?>"></span>
							<input type="hidden" id="loader_checking" val="<?php echo img_url('spin.svg'); ?>"/>
						</div>
					</div>
					
					
				</div>

				<div id="otherCompte"> 

					<br/>
					<?php foreach ($info_paiement as $key => $value) : ?>


	        			<?php if($key!=0): ?>


	        				<?php for($i=0;$i<count($all_types_compte);$i++){ ?>
	        					<?php if($value->compte_id==$all_types_compte[$i]->id_compte){ echo '<input id="compteValue'.$value->id_entreprise_compte.'" type="hidden" name="model" value="'.$value->compte_id.'">';} ?>
	        				 <?php } ?>

	        				<div class="form-group row compteADD compte<?php echo $value->id_entreprise_compte; ?>">
	        					<div class="col-10">
		        					<select id="select-compte-paiement<?php echo $value->id_entreprise_compte; ?>" class="form-control select2 select_compte_entreprise addCompte" name="information_compte<?php echo $value->id_entreprise_compte; ?>" data-placeholder="Autre compte de paiement" ng-init="initCompteValue(<?php echo $value->id_entreprise_compte; ?>)" ng-model="selectCompte[<?php echo $value->id_entreprise_compte; ?>]" ng-change="changeTypeCompte(<?php echo $value->id_entreprise_compte; ?>)" style="width: 100%;">
										 	<option></option>
						                  <?php for($i=0;$i<count($all_types_compte);$i++){ ?>
						                    	<option value="<?php echo $all_types_compte[$i]->id_compte; ?>" <?php if($value->compte_id==$all_types_compte[$i]->id_compte) echo "selected"; ?> > <?php echo $all_types_compte[$i]->label; ?>
						                    	</option>
						                  <?php } ?>
						            </select>
					        	</div>
					        	<div class="col-2">
		        					<a class="btn btn-default btn-block" style="cursor: pointer;" ng-click="removeHtml('.compte<?php echo $value->id_entreprise_compte; ?>')" title="Supprimer"><i class="fa fa-minus"></i></a>
	        					</div>
	        				</div>
							<div class="form-group has-feedback compteADD compte<?php echo $value->id_entreprise_compte; ?>">
								<div class="row">
									<div class="col-md-3">
										<span class="input-group-text labelCompte" id="labelCompte<?php echo $value->id_entreprise_compte; ?>"> Compte n° </span>
									</div>
									<div class="col-md-9">
										<!-- ng-blur="verifyCompte(<?php echo $value->id_entreprise_compte; ?>)" -->
										<input id="numero-compte-paiement<?php echo $value->id_entreprise_compte; ?>" name="numero_compte_entreprise<?php echo $value->id_entreprise_compte; ?>" value="<?php echo $value->numero_compte; ?>" type="text" class="form-control numero_compte_entreprise" placeholder="Numéro de compte" />
										<span class="loader-default col-1" id="loader-numCompte-resp<?php echo $value->id_entreprise_compte; ?>" style="display: none; position: absolute;right: 0px;top: 5px;"><img src="<?php echo img_url('spin.svg'); ?>"></span>
									</div>
								</div>
							</div>

						<?php endif; ?>
	        		<?php endforeach; ?>

				</div>
				<br/>
				<!--
	            <div class="has-feedback">
	              <label>Chiffre d'affaires annuel</label>
	            </div>

	            <div class="form-group has-feedback input-group">
	              <input name="ca_entreprise" value="<?php echo $entreprise->ca_entreprise; ?>" type="number" min="0" class="form-control" placeholder="Saisir le montant">
	              <div class="input-group-append">
	                <span class="input-group-text">MGA</span>
	              </div>
	            </div>-->

	            <div class="form-group" id="typesAliment">
	              <label id="scroll-type-aliment">Type d'aliments (facultatif)</label><br>
	              <?php foreach($all_types_aliment as $type): ?>
	                <label class="check">
	                  <input name="type_aliment_<?php echo $type->id_type_aliment; ?>" type="checkbox" class="flat-red" <?php if(typeAlimentSelected($type, $types_aliment)) echo "checked"; ?>>
	                  <?php echo $type->label; ?>
	                </label>
	              <?php endforeach; ?>
	              </label>
	            </div>

	            <div class="row">
	              <div class="col-4" style="margin-top: -14px;margin-bottom: 15px;">
	                <a class="btn btn-default btn-block btn-sm" ng-click="addTypeAliment()">Autre</a>
	              </div>
	            </div>

	            <div class="row">
	              <!-- /.col -->
	              <div class="col-4 offset-2">
	                <a class="btn btn-secondary btn-block btn-flat" data-dismiss="modal" style="color: #fff;">Annuler</a>
	              </div>
	              <div class="col-6">
	                <span ng-click="validFormEdit()" class="btn btn-primary btn-block btn-flat">Mettre à jour</span>
	              </div>
	                <button id="btn-modifier_compte" name="modifier_compte" type="submit" class="btn btn-primary btn-block btn-flat" style="display: none;">Mettre à jour</button>
	              <!-- /.col -->
	            </div>
	          </form>
	      </div>
	    </div>
	  </div>
	</div>

	<div id="modal_type_entreprise" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Type d'entreprise</h5>

            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Ajouter nouveau type</label>
              <input type="text" class="form-control" ng-model="new_type" placeholder="Saisir un libellé pour le nouveau type">
            </div>
            <div class="form-error" ng-if="notValidTypeEntreprise">
              Veuilez remplir correctement le champs ci-dessus
            </div>
          </div>
          <div class="modal-footer">
            <div class="loader-default col-1" ng-if="loadType"><img src="<?php echo img_url('spin.svg'); ?>"></div>
            <button type="button" class="btn btn-primary" ng-click="createTypeEntreprise()">Enregistrer</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
          </div>
        </div>
      </div>
    </div>

    <div id="modal_type_aliment" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Type d'aliments</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Ajouter nouveau type</label>
              <input type="text" class="form-control" ng-model="new_type_aliment" placeholder="Saisir un libellé pour le nouveau type">
            </div>
            <div class="form-error" ng-if="notValidTypeEntreprise">
              Veuilez remplir correctement le champs ci-dessus
            </div>
          </div>
          <div class="modal-footer">
            <div class="loader-default col-1" ng-if="loadType"><img src="<?php echo img_url('spin.svg'); ?>"></div>
            <button type="button" class="btn btn-primary" ng-click="createTypeAliment()">Enregistrer</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
          </div>
        </div>
      </div>
	</div>
	
	<div id="modal_new_poste" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Titre ou poste au sein de l'entreprise</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <div class="form-group">
              <label for="exampleInputEmail1">Poste</label>
              <input type="text" class="form-control" id="new-poste-name" ng-model="new_type" placeholder="Saisir un libellé pour votre poste">
            </div>
            <div class="form-error" ng-if="notValidTypeEntreprise">
              Veuilez remplir correctement le champs ci-dessus
            </div>
          </div>
          <div class="modal-footer">
            <div class="loader-default col-1" ng-if="loadType"><img src="<?php echo img_url('spin.svg'); ?>"></div>
            <button type="button"  id="bouton_ajouter_poste" class="btn btn-primary" ng-click="createPoste()">Enregistrer</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
          </div>
        </div>
      </div>
    </div>

    <div id="modal-manage-magasin" class="modal fade" tabindex="-1" role="dialog">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">Liste de magasins de votre entreprise</h5>
            <button title="Ajouter un nouveau magasin" type="button" class="close" ng-click="MAGASIN.openModalAddMagasin()">
              <span>Ajouter</span>
            </button>
          </div>
          <div class="modal-body">
            <table class="table table-striped">
            	<thead>
            		<tr>
            			<th>Magasin</th>
            			<th>Responsable</th>
            			<th></th>
            		</tr>
            	</thead>
            	<tbody>
	    			<tr ng-repeat="mag in MAGASIN.magasins">
	    				<td>{{mag.nom}}</td>
	    				<td>{{mag.responsable.nom}} {{mag.responsable.prenom}}</td>
	    				<td class="td-icon">
	    					<i ng-click="MAGASIN.openModalEdit(mag.id_magasin)" title="Modifier le magasin" class="fa fa-pencil"></i>
	    					<i ng-click="MAGASIN.openConfirmDelete(mag.id_magasin)" title="Supprimer le magasin" class="fa fa-remove" style="margin-left: 40px;"></i>
	    				</td>
	    			</tr>
            	</tbody>
            </table>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>

	<!-- MODAL POUR ENREGISTREMENT DE RESPONSABLE DE MAGASIN -->
	<div id="modal-add-magasin" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="">{{MAGASIN.modalTitle}}</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label for="input-nom-magasin" class="col-form-label">Magasin:</label>
						<input id="input-nom-magasin" type="text" placeholder="Nom du magasin" class="form-control mag-input-to-clear">
					</div>
					<label>Responsable</label>
					<div class="form-group">
						<label for="input-nom-resp" class="col-form-label">Nom:</label>
						<div class="row">
							<div class="col-md-3">
								<select id="select-sexe-resp" style="appearance: none; -moz-appearance: none; -webkit-appearance: none;" class="custom-select form-control">
									<option value="M">Mr</option>
									<option value="F">Mme</option>
								</select>
							</div>
							<div class="col-md-9">
								<input type="text" placeholder="Nom du responsable" class="form-control mag-input-to-clear" id="input-nom-resp">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="input-prenom-resp" class="col-form-label">Prénom:</label>
						<input type="text" placeholder="Prénom" class="form-control mag-input-to-clear" id="input-prenom-resp">
					</div>
					<div class="form-group">
						<label for="input-email-resp" class="col-form-label">E-mail:</label>
						<input ng-keyup="verifMailRespMag()" type="email" placeholder="E-mail" class="form-control mag-input-to-clear" id="input-email-resp">
						<div class="loader-default col-2" id="loader-email-resp" style="display: none; position: absolute;right: 0px;top: 120px;"><img src="<?php echo img_url('spin.svg'); ?>"></div>
						<span class="form-error" id="msg-email-resp" style="display: none;">cette adresse e-mail est déjà utilisée par un membre du plateforme</span>
					</div>
					<div class="form-group">

						<label for="input-tel-resp" class="col-form-label">Téléphone:</label>
						<div class="row">
				            <div class="col-md-2">
			                  <span class="input-group-text labelTelephone0" > - </span>
			                </div>
                			<div class="col-md-10">
								<input type="text" placeholder="Numéro de téléphone" class="form-control mag-input-to-clear" id="input-tel-resp">
							</div>
						</div>
					</div>
					<div class="form-group">
						<label for="select-post-resp" class="col-form-label">Poste:</label>
						<div class="loader-default col-1" ng-if="loadType"><img src="<?php echo img_url('spin.svg'); ?>"></div>
						<div class="row">
							<div class="col-12">
								<select id="select-post-resp" data-placeholder="Poste du responsable" class="form-control select2" style="width: 100%;">
									<option></option>
									<?php foreach($postes as $poste): ?>
	                    			<option value="<?php echo $poste->id_poste; ?>"><?php echo $poste->label; ?></option>
	                  				<?php endforeach; ?>
								</select>
							</div>
							<!--<div class="col-2" style="margin-left: -7.5px;">
								<button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal-new-poste"><span>Ajouter</span></button>
							</div>-->
						</div>
					</div>
				</div>
				<div class="modal-footer" id="footer-resp-modal">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button ng-disabled="respMagInvalid" type="button" class="btn btn-primary" ng-click="MAGASIN.updateResponsable('<?php echo base_url('') ?>u-r-m-e')">Valider</button>
				</div>
				<div id="lds-roller-responsable" class="lds-roller" style="margin: 0 auto;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
			</div>
		</div>
	</div>

	<!-- MODAL POUR AJOUT DE POSTE -->
	<div class="modal fade" id="modal-new-poste">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="">Ajout d'un nouveau poste</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form>
						<div class="form-group">
							<label class="col-form-label">Nom du poste:</label>
							<input type="text" class="form-control" id="input-nom-post-new">
						</div>
					</form>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
					<button type="button" class="btn btn-primary" ng-click="addNewPoste('<?php echo base_url(''); ?>a-n-p')">Ajouter</button>
				</div>
			</div>
		</div>
	</div>

	<!-- MODAL CONFIRMATIOM SUPPRESSION MAGASIN -->
	<div class="modal fade" id="modal-confirm-delete" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" >Attention !</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
	        {{MAGASIN.deleteMessage}}
	      </div>
	      <div class="modal-footer modal-footer-delete">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal">Non</button>
	        <button ng-click="MAGASIN.delete('<?php echo base_url(''); ?>d-r-m-e')" type="button" class="btn btn-danger">Oui</button>
	      </div>
	      <div id="lds-roller-delete" class="lds-roller" style="margin: 0 auto;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
	    </div>
	  </div>
	</div>

	<!-- MODAL POUR LES ALERT -->
	<div class="modal fade" id="modal-alert">
	  <div class="modal-dialog modal-dialog-centered" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="modal-alert-title">Oops</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body" style="text-align: center;">
	        <p id="modal-alert-message"><p>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-primary" data-dismiss="modal">OK</button>
	      </div>
	    </div>
	  </div>
	</div>

	<?php $_contratToValid = isset($contrat_to_valid) ? $contrat_to_valid : 0; ?>
    <!-- MODAL POUR LE CONTRAT -->
    <div id="contratModal" class="modal fade">
        <div class="modal-dialog" ng-class="CONTRAT.modalSize">
            <div class="modal-content">
                <form role="form" ng-submit="CONTRAT.submit()">
                    <div class="modal-header">
                    </div>
                    <div class=" modal-body loader" ng-show="CONTRAT.loading"></div>
                    <?php if($_contratToValid == 1){ 
                    	$frameSRC = 'https://view.officeapps.live.com/op/embed.aspx?src='.base_url('').$contrat_file_url; ?>
                    <div class="modal-body" ng-show="!CONTRAT.loading">
                        <h4 class="modal-title">Protocole d'entente : </h4>  
                        <div class="card-body">
                            <iframe class="embed-responsive-item" src="<?= $frameSRC; ?>" width='100%' height='400' frameborder='0'></iframe>
                        </div> 
                        <div class="modal-footer">
                        	<div class="row">
	                            <div class="col-md-6 float-left" style="font-size:10px !important; padding-right: 50px !important;">
	                            	En cliquant « Accepter » vous acceptez les conditions du  protocole d’entente entre FoodWise et votre entité. Pour poursuivre notre collaboration vous devriez signer le document et télécharger le document signé sur notre page. Une fois téléchargé notre équipe va valider votre profil après avoir vérifier votre scan et vous pouvez désormais commencer la donation des produits !
	                            </div>
	                            <div class="col-md-6">
		                            <button type="button" class="btn btn-default pull-left" onclick="$('#contratModal').modal('hide')">Annuler</button>
		                            <button style="margin-left: 5px !important;" class="btn btn-primary" ng-click="CONTRAT.saveOnly($event, '<?= base_url('').$contrat_file_url; ?>')">Enregistrer seulement</button>
		                            <button class="btn btn-primary" ng-click="CONTRAT.validate($event, '<?= base_url('').$contrat_file_url; ?>')">Accepter</button >
	                        	</div>
	                        </div>
                        </div>
                    </div>
                    <?php } ?>
                    <div class="modal-body" <?php if($_contratToValid == 0){ ?> ng-show="!CONTRAT.loading" <?php } else { ?> style="display: none;" <?php } ?>>
                        <h4 class="modal-title">Protocole d'entente (obligatoire) : </h4>
                        <div class="card-body">
                            <div class="form-group">
                                <label for="inputDonateur">Nom donateur *: </label>
                                <input type="text" class="form-control" disabled="true" ng-model="CONTRAT.contrat.entreprise.nom_entreprise"  required>									
                            </div>
                            <!--<div class="form-group">
                                <label for="contrat-select-validite">Validité *: </label>
                                <select class="form-control form-contrat-to-test select2" style="width: 100%;" id="contrat-select-validite" data-placeholder="Durée*" title="Validité" ng-model="CONTRAT.contrat.contrat.validity" required>
                                    <option></option>
                                        <option value="12 mois, renouvelable" >12 mois, renouvelable</option>
                                        <option value="24 mois, renouvelable" >24 mois, renouvelable</option>
                                </select>
                            </div>-->
                            <div class="form-group">
                                <label>Logo Donateur</label>
                                <div class="input-group">
                                    <div>
                                        <div class="thumbnail">
                                        	<?php 
                                        		$logoToShow = $CONTRAT['entreprise']['logo'];
                                        		if(empty($logoToShow) || strcmp('user_placeholder.png', $logoToShow)==0){
                                        			$logoToShow = base_url('assets/img/no-image.png');
                                        		}
                                        		else{
                                        			$logoToShow = base_url($logoToShow);
                                        		}
                                        	 ?>
                                            <img id="img-logo-donateur" src="<?= $logoToShow; ?>" class="img-thumbnail preview"/>
                                        </div>
                                        
                                        <div>
                                        	
                                            <label for="input-logo-donateur" class="btn btn-default">Choisir un logo</label>
                                           
                                            <input onchange="angular.element(this).scope().CONTRAT.onLogoDonateurChange(this)" id="input-logo-donateur" type="file" accept="image/*" style="display: none;">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Personne dument habilité pour la conclusion et pour la signature de ce protocole d’entente </label>
                                <div class="row">
                                    <div class="col-md-3">
                                        <select ng-model="CONTRAT.contrat.employe1.sexe" style="appearance: none; -moz-appearance: none; -webkit-appearance: none;" class="custom-select form-control">
                                            <option value="M">Mr</option>
                                            <option value="F">Mme</option>
                                        </select>
                                    </div>
                                    <div class="col-md-9">
                                        <input ng-model="CONTRAT.contrat.employe1.nom" type="text" class="form-control form-contrat-to-test" placeholder="Nom*" title="Nom du responsable de la communication" required>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <input ng-model="CONTRAT.contrat.employe1.prenom" type="text" class="form-control form-contrat-to-test" placeholder="Prénom*" title="Prenom du responsable de la communication"  required />
                            </div>
                            <div class="form-group">
                            	<div class="loader-default col-1" ng-if="loadType"><img src="<?php echo img_url('spin.svg'); ?>"></div>
                                <div class="row">
                                    <div class="col-12">
                                        <select  onchange="angular.element(this).scope().CONTRAT.posteEmployeChange(this, 1)" id="select-contrat-poste-resp1" data-placeholder="Poste du responsable*" class="form-control custom-select select2 form-contrat-to-test" title="Poste du responsable de la communication"  style="width: 100%;">
                                            <option></option>
                                            <?php
                                                foreach($postes as $poste): ?>
                                                <option value="<?= $poste->id_poste; ?>"> <?= $poste->label; ?></option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                    <!--<div class="col-2" style="margin-left: -7.5px;">
                                        <button ng-click="CONTRAT.selectChoosing = 'select-contrat-poste-resp1'" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal-contrat-new-poste"><span>Ajouter</span></i></button>
                                    </div>-->
                                </div>
                            </div>
                            <div class="form-group">
                                <input ng-keyup="CONTRAT.verifMail('1')" id="contrat-responsable1_email" ng-model="CONTRAT.contrat.employe1.email" type="email" class="form-control form-to-test" title="Email du responsable de la communication"  placeholder="Email*" required>
                                <div class="loader-default col-2" id="loader-email-resp1" style="display: none; position: absolute;right: 20px;top: 766px;"><img src="<?php echo img_url('spin.svg'); ?>"></div>
                                <span class="form-error" id="contrat-msg-email-resp1" style="display: none;">cette adresse e-mail est déjà utilisée par un membre du plateforme</span>
                            </div>
                            <div class="form-group">
                            	<div class="row">
						            <div class="col-md-2">
					                  <span class="input-group-text labelTelephone0" > - </span>
					                </div>
		                			<div class="col-md-10">
                                		<input ng-model="CONTRAT.contrat.employe1.telephone" id="resp_com_direct_phone" type="text" class="form-control form-contrat-to-test" placeholder="Téléphone*" title="Numéro téléphone du responsable de la communication"  required>
                                	</div>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" onclick="$('#contratModal').modal('hide')">Fermer</button>
                            <!--button ng-click="CONTRAT.formIsBad()" type="button" class="btn btn-primary">Valider</button-->
                            <button ng-click="CONTRAT.submit()" type="button" class="btn btn-primary">Valider</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

	<!-- MODAL POUR AJOUT DE POSTE -->
    <div class="modal fade" id="modal-contrat-new-poste" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="">Ajout d'un nouveau poste</h5>
                    <button type="button" class="close" onclick="$('#modal-contrat-new-poste').modal('hide')" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group">
                            <label class="col-form-label">Nom du poste:</label>
                            <input type="text" class="form-control" id="input-contrat-nom-post-new">
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" onclick="$('#modal-contrat-new-poste').modal('hide')">Annuler</button>
                    <button type="button" class="btn btn-primary" ng-click="CONTRAT.addNewPoste('<?php echo base_url(''); ?>a-n-p')">Ajouter</button>
                </div>
            </div>
        </div>
    </div>

    <!-- MODAL POUR TELECHARGER LE PROTOCOLE EN PDF -->
    <div class="modal fade" id="modal-download-protocole-pdf" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="">Téléchargement du protocole d'entente</h5>
                    <button ng-show="CONTRAT.askDownload" type="button" class="close" onclick="$('#modal-download-protocole-pdf').modal('hide')" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
                </div>
                <div class="modal-body">
                   	<div ng-show="CONTRAT.askDownload">Voulez vous téléchargez le fichier PDF du protocole d'entente?</div>
                   	<div class=" modal-body loader" ng-show="CONTRAT.loading"></div>
                   	<input id="downPdf" type="hidden" value="">
                </div>
                <div ng-show="CONTRAT.askDownload" class="modal-footer" >
                    <button type="button" class="btn btn-secondary" onclick="$('#modal-download-protocole-pdf').modal('hide')">Annuler</button>
                    <button type="button" class="btn btn-primary" ng-click="CONTRAT.launchDownloadPdf()">Télécharger</button>
                </div>
            </div>
        </div>
    </div>


    <!-- MODAL POUR AFFICHER LA CARTE -->
    <div class="modal fade" id="modal-show-map" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
	            <div class="modal-header">
	                <h5 class="modal-title">Choisir les coordonnées sur la carte </h5>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body">
	            	<div class="contenu_carte">
			        	<div class="row"> 			
	        				<div class="col-6">
        						<div class="row">
        							<div class="col-4">
        								<label>Latitude : </label><br/><label>Longitude : </label>
        							</div>
        							<div class="col-8">
        								<div class="lat_value_in_map" style="margin-bottom: .5rem !important;" > (Non défini) </div>
			        					<div class="long_value_in_map" style="margin-bottom: .5rem !important;" > (Non défini) </div>
        							</div>
        						</div>
	        					<span id="info"></span><br>
			        			<input id="identifiantMere" type="hidden" value="">
		        			</div>
		        			<div class="col-2"></div>
		        			<div class="col-4">
								<div id="autocomplete_list" class="ui-widget">
								    <input id="search_in_carte" class="form-control" placeholder="Rechercher" autocomplete="on">
								    <!--<datalist id="json-datalist"></datalist>-->
								    <!--<div class="input-group-append">
								      <button class="btn btn-primary btn-navbar " type="submit">
								        <i class="fa fa-search"></i>
								      </button>
								    </div> -->
								</div>
							</div>
			        	</div>
			        	<div class="row">
			        		<div id='map' style="width: 100%!important; height: 250px;"></div>
			        	</div>
		        	</div>
		        	<div class="loader load_map">
		        		<div id="lds-roller-responsable" class="lds-roller" style=""><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
		        		<br/>
		        	</div>
		        	<div class="load_map" style="text-align: center !important; margin-top: 10px !important;">Chargement de la carte . . . </div>
	            </div>
	            <div class="modal-footer">
            		<div class="contenu_carte" >
            			<button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
	                	<button ng-click="putCoordonneChoisi()" type="button" class="btn btn-primary" data-dismiss="modal">Choisir</button>
        			</div>
        		</div>
            </div>
        </div>
    </div>
    

</div>

<script type="text/javascript">
	var countRapport = <?php echo count($rapport); ?>;
	var paysEntreprise = <?php 
	if(!empty($adresses)){ 
		if(empty($adresses[0]->pays)){
			echo 0;
		}
		else{
			echo 1;
		}
 	}
 	else{
 		echo 0;
 	} ?>; 
	var twoResponsable = false;
	var responsables = {
		responsable1_email: '<?php echo $responsables[0]->email; ?>'
	};
	<?php if(count($responsables)==2){ ?>
	responsables['responsable2_email'] = '<?php echo $responsables[1]->email; ?>';
	twoResponsable = true;
	<?php } ?>

    var contrat_to_valid = <?php echo $_contratToValid; ?>;
    var contrat = <?php echo isset($CONTRAT['contrat']) ? json_encode($CONTRAT['contrat']) : "false"; ?>;
    var entreprise = <?php echo isset($CONTRAT['entreprise']) ? json_encode($CONTRAT['entreprise']) : "false"; ?>;
    var employe1 = <?php echo isset($CONTRAT['entreprise']['employes'][0]) ? json_encode($CONTRAT['entreprise']['employes'][0]) : "false"; ?>;
    var employe2 = <?php echo isset($CONTRAT['employe2_contrat']) ? json_encode($CONTRAT['employe2_contrat']) : "false"; ?>;
    var responsables = {
    	responsable1_email: '',
        responsable2_email: ''
    };
    responsables.responsable1_email = employe1.email;
    if (employe2.email != undefined) responsables.responsable2_email = employe2.email;
    var erreurUploadImage = <?php echo isset($erreurUploadImage) ? $erreurUploadImage : -1; ?>;

    var magasins = <?php echo json_encode($magasins); ?>;
</script>
