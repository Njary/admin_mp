<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Entreprise extends MY_Controller {

	private $contrat;
	private $_entreprise;
	private $_employe2_contrat;
	private $idUser;

	public function __construct(){
		parent::__construct();
		$this->checkPrivilege();
		$this->load->model("contrat/Entente_model","entente");
		$this->load->model("Entreprise_model","entreprise");
		$this->load->model("utilisateur/Utilisateur_model","utilisateur");
		$this->load->model("employe/Employe_model","employe");
		$this->load->model("produit/Don_model", "don");
		$this->checkContrat();
	}

	public function create_type(){
		$label = trim($this->input->post("type_label"));
	
		$result = $this->entreprise->addType($label, $this->session->userdata("idEntreprise"));

		header('Content-Type: application/json');
		echo json_encode(array("type_id"=>$result['id'], "exist"=>$result['exist']));
	}

	public function create_type_aliment(){
		$label = trim($this->input->post("type_label"));
	
		$result = $this->entreprise->addTypeAliment($label, $this->session->userdata("idEntreprise"));

		header('Content-Type: application/json');
		echo json_encode(array("type_id"=>$result['id'], 'exist'=>$result['exist']));
	}

	public function checkContrat(){
		$this->_entreprise = $this->entente->getEntreprise($this->session->userdata("idUser"));
		$this->contrat = $this->entente->getContrat($this->session->userdata("idEntreprise"));
		if($this->contrat){
			$this->_employe2_contrat = $this->entente->getEmployeEnCharge($this->contrat['id_contrat']);
		}
	}

	public function profil($erreurUploadImage=0, $validate = 0){
		
		$this->layout->ajouter_css('../plugins/iCheck/all');
		$this->layout->ajouter_css('../plugins/jquery-ui/jquery-ui');
		$this->layout->ajouter_css('../plugins/select2/select2.min');

		$this->layout->ajouter_js('../plugins/iCheck/icheck.min');
		$this->layout->ajouter_js('../plugins/select2/select2.full.min');
		$this->layout->ajouter_js('profil');

		$this->action("Mon profil");

		$idEntreprise = $this->session->userdata("idEntreprise");
		$entreprises = $this->entreprise->getById($idEntreprise);
		if(count($entreprises)!=1){
			$this->session->set_userdata("idEntreprise",-1);
			$this->session->set_userdata("idUser",-1);
			redirect(site_url(''));
			return;
		}

		$entreprise = $entreprises[0];
		$types_entreprise = $this->entreprise->getTypesEntreprise($idEntreprise);
		$types_aliment = $this->entreprise->getTypesAlimentEntreprise($idEntreprise);
		$responsables = $this->entreprise->getResponsablesEntreprise($idEntreprise);
		$adresses = $this->entreprise->getAdressesEntreprise($idEntreprise);
		$rapport = $this->entreprise->getRapportDEntente($idEntreprise);
		$postes = $this->employe->getPostes(1, $idEntreprise);
		$magasins = $this->entreprise->getMagasins($idEntreprise);
		$information_paiement = $this->entreprise->getInformationPaiement($idEntreprise);
		$information_paiement2 = $this->entreprise->getInformationPaiement($idEntreprise);
		$pays = $this->entreprise->getPays();

		$id_mobile_banking = array(1,2,3);
		foreach ($information_paiement as $key => $value) {

			/*if($value->id_compte != 4){*/
			if( in_array( $value->id_compte, $id_mobile_banking) ){
				$value->numero_compte = substr($value->numero_compte, -7, 7);
			}
		
		}

		$all_types_entreprise = $this->entreprise->getTypes($idEntreprise);

		$all_types_aliment = $this->entreprise->getTypesAliment($idEntreprise);

		$all_types_compte = $this->entreprise->getTypeCompte();

		$toView = array(
			"idEA"=>$idEntreprise,
			"entreprise"=>$entreprise,
			"types_entreprise"=>$types_entreprise,
			"types_aliment"=>$types_aliment,
			"all_types_entreprise"=>$all_types_entreprise,
			"all_types_aliment"=>$all_types_aliment,
			"responsables"=>$responsables,
			"adresses"=>$adresses,
			"rapport"=>$rapport,
			"pseudo"=>$this->session->userdata("username"),
			"erreurUploadImage"=>$erreurUploadImage,
			"postes"=>$postes,
			"magasins"=>$magasins,
			"info_paiement"=>$information_paiement,
			"info_paiement2"=>$information_paiement2,
			"all_types_compte"=>$all_types_compte,
			"CONTRAT"=>-1,
			"contrat_to_valid"=>$validate,
			"contrat_file_url"=>$this->session->userdata("contrat_file_url"), 
			"pays"=>$pays,
			"need_map"=>true
		);

		$data['entreprise'] = $this->_entreprise;
		$data['contrat'] = $this->contrat;
		$data['employe2_contrat'] = $this->_employe2_contrat;
		$toView['CONTRAT'] = $data;


		$this->layout->addView("utilisateur/profil",$toView);
		$this->layout->view();
	}


	public function editProfil(){
		$entreprise_id = $this->session->userdata("idEntreprise");
		
		if($this->input->post('modifier_compte')!== null){
			$data = $this->input->post();

			if( $this->input->post('imagebase64') != '' ) {
		        $image = $this->input->post('imagebase64');

		        list($type, $image) = explode(';', $image);
		        list(, $image)      = explode(',', $image);
		        $image = base64_decode($image);

		        $fileLocation = './uploads/logos_entreprises';
		        $fileName = 'logo_'.$entreprise_id.'.png';

		        $fileNameWithDirectory = './uploads/logos_entreprises/logo_'.$entreprise_id.'.png';
		        //"$fileLocation/$fileName"

		        if( file_put_contents( $fileNameWithDirectory , $image ) !== false ){

		        	$uploadData = array();
		        	$uploadData["file_name"] = $fileName;
		        	$this->doUpdateProfil($data, $uploadData);
					redirect("entreprise/profil/2");

		        }else{
		        	redirect("entreprise/profil/1");
		        }
		    }else{
		    	$this->doUpdateProfil($data);
				redirect("entreprise/profil/2");
		    }

			/*$config['upload_path']='./uploads/logos_entreprises/';
			$config['allowed_types']='jpg|png';
			$config['overwrite'] = TRUE;
			
			if($_FILES["logo_entreprise"]["size"]!=0)
				$config['file_name'] = 'logo_'.$entreprise_id;
			
			$this->load->library('upload',$config);

			if(!$this->upload->do_upload("logo_entreprise")){
				if(empty($this->upload->data("file_name"))){
					$this->doUpdateProfil($data);
					redirect("entreprise/profil/2");
				}else
					redirect("entreprise/profil/1");
			}else{
				$this->doUpdateProfil($data,$this->upload->data());
				redirect("entreprise/profil/2");
			}*/
		}
	}

	public function doUpdateProfil($data,$uploadData=NULL){
		
		/*var_dump($data);
		die();*/

		$utilisateur_data = array(
			"pseudo"=>$data["nom_entreprise"],
		);

		$utilisateur_id = $this->session->userdata("idUser");

		$this->utilisateur->update($utilisateur_id,$utilisateur_data);

		$this->session->set_userdata("username",$data["nom_entreprise"]);

		$entreprise_id = $this->session->userdata("idEntreprise");

		$entreprise_data = array(
			"nom_entreprise" => $data["nom_entreprise"],
			"nif_entreprise" => $data["nif_entreprise"],
			"stat_entreprise" => $data["stat_entreprise"],
			"ca_entreprise" => $data["ca_entreprise"],
			"qte_kg" => -1,
			"qte_mga" => -1,
			"utilisateur_id" => $utilisateur_id
		);

		if($uploadData!=NULL){
			$entreprise_data["logo"]="uploads/logos_entreprises/".$uploadData["file_name"];
		}

		$this->entreprise->update($entreprise_id,$entreprise_data);

		//mise à jour adresse
		$adresses = $this->entreprise->getAdressesEntreprise($this->session->userdata("idEntreprise"));

		$this->entreprise->removeAdresse($entreprise_id);

		$list_id_donation = [];

		foreach ($adresses as $key => $value) {
			$isUse = $this->utilisateur->checkIfUseInDonation($value->id_adresse);
			
			if(count($isUse) == 0){
				$this->utilisateur->removeAdresse($value->id_adresse);
			}else{
				foreach ($isUse as $key => $idDonation) {
					
					$id = (int)$idDonation->id_donation;
					array_push($list_id_donation, $id);
					$this->don->updateAdresseRamassage( $id, null);
				}
				$this->utilisateur->removeAdresse($value->id_adresse);			
			}
				
		}

		foreach ($data as $key => $value) {
			if(strpos($key,"adresse_entreprise") !== false && !empty($value)){
				$nb = str_replace('adresse_entreprise','',$key);
				$address = array(
					"label"=>$value,
					"latitude"=>$data['latitude_entreprise'.$nb],
					"longitude"=>$data['longitude_entreprise'.$nb],
					"code_postal"=>$data['code_postal_entreprise'.$nb],
					"ville"=>$data['ville_entreprise'.$nb],
					"pays_id"=>$data['pays_entreprise'.$nb]
				);

				//créer adresse
				$adresse_id = $this->entreprise->addAdresse($address);
				//joindre adresse et entreprise
				$this->entreprise->setAdresseEntreprise($entreprise_id,$adresse_id);
			}
		}

		$adresse_entreprise = $this->entreprise->getAdressesEntreprise($this->session->userdata("idEntreprise"));
		$adresse_entreprise = $adresse_entreprise[0];


		for ($i=0; $i<count($list_id_donation); $i++) {
			
			$this->don->updateAdresseRamassage( $list_id_donation[$i], $adresse_entreprise->id_adresse );
		}
		

		//mise à jour information compte
		$this->entreprise->removeCompte($entreprise_id);

		foreach ($data as $key => $value) {
			if(strpos($key,"information_compte") !== false && !empty($value)){
				$nb = str_replace('information_compte','',$key);
				$info_paiement =  array(
					"entreprise_id" => $entreprise_id,
					"compte_id" => $value,
					"numero_compte" => $data['numero_compte_entreprise'.$nb]
				);

				//Créer information compte
				$this->entreprise->setInformationCompte($info_paiement);
			}
		}


		$employe_data = array(
			"nom"=>$data["responsable1_nom"],
			"prenom"=>$data["responsable1_prenom"],
			"telephone"=>$data["responsable1_phone"],
			"email"=>strtolower($data["responsable1_email"]),
			"poste_id"=>$data["responsable1_poste"],
			"sexe"=>$data["responsable1_sexe"]
		);

		if(!$this->utilisateur->mailExist($employe_data["email"])['exist']){
			$this->employe->update($data["responsable1_id"],$employe_data);
		}
		else{
			if(strcmp($this->utilisateur->getEmail($data["responsable1_id"]), $employe_data["email"])==0){
				$this->employe->update($data["responsable1_id"],$employe_data);
			}
		}
		
		//responsable 2
		if(array_key_exists("responsable2_nom",$data) && !empty($data["responsable2_nom"])){
			$employe_data = array(
				"nom"=>$data["responsable2_nom"],
				"prenom"=>$data["responsable2_prenom"],
				"telephone"=>$data["responsable2_phone"],
				"email"=>strtolower($data["responsable2_email"]),
				"poste_id"=>$data["responsable2_poste"],
				"sexe"=>$data["responsable2_sexe"]
			);
			
			if(!$this->utilisateur->mailExist($employe_data["email"])['exist']){
				if(array_key_exists("responsable2_id", $data)){
					$this->employe->update($data["responsable2_id"],$employe_data);
				}else{
					$employe_id = $this->employe->save($employe_data);
					$this->entreprise->setEmployeEntreprise($entreprise_id,$employe_id,2);
				}
			}
			else{
				if(strcmp($this->utilisateur->getEmail($data["responsable2_id"]), $employe_data['email'])==0){
					if(array_key_exists("responsable2_id", $data)){
						$this->employe->update($data["responsable2_id"],$employe_data);
					}else{
						$employe_id = $this->employe->save($employe_data);
						$this->entreprise->setEmployeEntreprise($entreprise_id,$employe_id,2);
					}
				}
			}
		}

		//type entreprise
		$this->entreprise->removeEntrepriseType($entreprise_id);

		foreach ($data["type_entreprise"] as $key => $value) {
			$this->entreprise->setTypeEntreprise($entreprise_id,(int)$value);
		}

		//types aliments
		$this->entreprise->removeEntrepriseTypeAliment($entreprise_id);

		foreach ($data as $key => $value) {
			if(strpos($key,"type_aliment") !== false){
				if($value=="on"){
					$tab_key = explode("_",$key);
					$type_aliment_id = $tab_key[2];

					$this->entreprise->setTypeAlimentEntreprise($entreprise_id,$type_aliment_id);
				}
			}
		}

		$this->session->set_userdata("nomEntreprise",$data["nom_entreprise"]);

		if($uploadData!=NULL)
			$this->session->set_userdata("logoEntreprise",$entreprise_data["logo"]);

		redirect("entreprise/profil/2");
	}

	/*public function uploadImageFile() { // Note: GD library is required for this function
	    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	        $iWidth = $iHeight = 200; // desired image result dimensions
	        $iJpgQuality = 90;
	        if ($_FILES) {
	            // if no errors and size less than 250kb
	        	var_dump($_FILES);
	            //if (! $_FILES['image_file']['error'] && $_FILES['image_file']['size'] < 250 * 1024) {
	        	if (! $_FILES['image_file']['error']){
	                if (is_uploaded_file($_FILES['image_file']['tmp_name'])) {
	                    // new unique filename
	                    $sTempFileName = 'doc/' . md5(time().rand());
	                    // move uploaded file into cache folder
	                    move_uploaded_file($_FILES['image_file']['tmp_name'], $sTempFileName);
	                    // change file permission to 644
	                    @chmod($sTempFileName, 0644);
	                    if (file_exists($sTempFileName) && filesize($sTempFileName) > 0) {
	                        $aSize = getimagesize($sTempFileName); // try to obtain image info
	                        if (!$aSize) {
	                            @unlink($sTempFileName);
	                            return;
	                        }
	                        // check for image type
	                        switch($aSize[2]) {
	                            case IMAGETYPE_JPEG:
	                                $sExt = '.jpg';
	                                // create a new image from file
	                                $vImg = @imagecreatefromjpeg($sTempFileName);
	                                break;
	                            case IMAGETYPE_PNG:
	                               $sExt = '.png';
	                                // create a new image from file
	                                $vImg = @imagecreatefrompng($sTempFileName);
	                                break;
	                            default:
	                                @unlink($sTempFileName);
	                                return;
	                        }
	                        // create a new true color image
	                        $vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );
	                        // copy and resize part of an image with resampling
	                        imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$_POST['x1'], (int)$_POST['y1'], $iWidth, $iHeight, (int)$_POST['w'], (int)$_POST['h']);
	                        // define a result image filename
	                        $sResultFileName = $sTempFileName . $sExt;
	                        // output image to file
	                        imagejpeg($vDstImg, $sResultFileName, $iJpgQuality);
	                        @unlink($sTempFileName);
	                        return $sResultFileName;
	                    }
	                }
	            }
	        }
	    }
	}*/
	
	/*public function editProfil(){
		$entreprise_id = $this->session->userdata("idEntreprise");
		if($this->input->post('modifier_compte')!== null){
			$data = $this->input->post();
			$sResultFileName = '';
			$updateLogo = false;
			$iWidth = $iHeight = 256; // desired image result dimensions
	        $iJpgQuality = 100; // 0 : pire qualité , 100 : meilleur qualité, default : 75
	        if ($_FILES) {
	        	if (! $_FILES['logo_entreprise']['error']) {
	        		if (is_uploaded_file($_FILES['logo_entreprise']['tmp_name'])) {
	        			$updateLogo = true;
	        			// new unique filename
	        			$fileNameOfficial = 'logo_' .$entreprise_id; //Nom fichier seulement
	                    $sTempFileName = 'uploads/logos_entreprises/logo_' .$entreprise_id; // Nom fichier avec chemin
	                    // move uploaded file into cache folder
	                    move_uploaded_file($_FILES['logo_entreprise']['tmp_name'], $sTempFileName);
	                    // change file permission to 644
	                    @chmod($sTempFileName, 0644);
	                    if (file_exists($sTempFileName) && filesize($sTempFileName) > 0) {
	                        $aSize = getimagesize($sTempFileName); // try to obtain image info
	                        if (!$aSize) {
	                            @unlink($sTempFileName);
	                            return;
	                        }

	                        // check for image type
	                        switch($aSize[2]) {
	                            case IMAGETYPE_JPEG:
	                                $sExt = '.jpg';
	                                // create a new image from file
	                                $vImg = @imagecreatefromjpeg($sTempFileName);
	                                break;
	                            case IMAGETYPE_PNG:
	                               $sExt = '.png';
	                                // create a new image from file
	                                $vImg = @imagecreatefrompng($sTempFileName);
	                                break;
	                            default:
	                                @unlink($sTempFileName);
	                                return;
	                        }
	                        //Regler la ratio
	                        $ratio_origine = (int)$this->input->post('w')/(int)$this->input->post('h');
	                        if ($iWidth/$iHeight > $ratio_origine) {
							   $iWidth = $iHeight*$ratio_origine;
							} else {
							   $iHeight = $iWidth/$ratio_origine;
							}
	                        // create a new true color image
	                        $vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );
	                        // copy and resize part of an image with resampling
	                        imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$this->input->post('x1'), (int)$this->input->post('y1'), $iWidth, $iHeight, (int)$this->input->post('w'), (int)$this->input->post('h'));
	                        // define a result image filename
	                        $sResultFileName = $sTempFileName . $sExt; // Nom fichier avec chemin 
	                        $sResultFileNameOfficial = $fileNameOfficial . $sExt; //Nom fichier seulement
	                        // output image to file
	                        imagejpeg($vDstImg, $sResultFileName, $iJpgQuality);
	                        @unlink($sTempFileName);
	                        
	                    }
	        		}else{
	        			$updateLogo = false;
	        		}
	        	}
	        }

	       

	        if($updateLogo){
	        	if($sResultFileName != ''){
	        		$uploadData = array();
	        		$uploadData["file_name"] = $sResultFileNameOfficial;
		        	$this->doUpdateProfil($data, $uploadData);
		        	redirect("entreprise/profil/2");
	        	}else{
	        		redirect("entreprise/profil/1");
	        	}
	        }else{	
				$this->doUpdateProfil($data);
				redirect("entreprise/profil/2");
	        }

		}

	}

	public function editLogoProfil(){
		$entreprise_id = $this->session->userdata("idEntreprise");
		if($this->input->post('modifier_logo')!== null){
			$data = $this->input->post();
			$sResultFileName = '';
			$updateLogo = false;
			$iWidth = $iHeight = 256; // desired image result dimensions
	        $iJpgQuality = 100; // 0 : pire qualité , 100 : meilleur qualité, default : 75
	        if ($_FILES) {
	        	if (! $_FILES['logo_entreprise']['error']) {
	        		if (is_uploaded_file($_FILES['logo_entreprise']['tmp_name'])) {
	        			$updateLogo = true;
	        			// new unique filename
	        			$fileNameOfficial = 'logo_' .$entreprise_id; //Nom fichier seulement
	                    $sTempFileName = 'uploads/logos_entreprises/logo_' .$entreprise_id; // Nom fichier avec chemin
	                    // move uploaded file into cache folder
	                    move_uploaded_file($_FILES['logo_entreprise']['tmp_name'], $sTempFileName);
	                    // change file permission to 644
	                    @chmod($sTempFileName, 0644);
	                    if (file_exists($sTempFileName) && filesize($sTempFileName) > 0) {
	                        $aSize = getimagesize($sTempFileName); // try to obtain image info
	                        if (!$aSize) {
	                            @unlink($sTempFileName);
	                            return;
	                        }

	                        // check for image type
	                        switch($aSize[2]) {
	                            case IMAGETYPE_JPEG:
	                                $sExt = '.jpg';
	                                // create a new image from file
	                                $vImg = @imagecreatefromjpeg($sTempFileName);
	                                break;
	                            case IMAGETYPE_PNG:
	                               $sExt = '.png';
	                                // create a new image from file
	                                $vImg = @imagecreatefrompng($sTempFileName);
	                                break;
	                            default:
	                                @unlink($sTempFileName);
	                                return;
	                        }
	                        //Regler la ratio
	                        $ratio_origine = (int)$this->input->post('w')/(int)$this->input->post('h');
	                        if ($iWidth/$iHeight > $ratio_origine) {
							   $iWidth = $iHeight*$ratio_origine;
							} else {
							   $iHeight = $iWidth/$ratio_origine;
							}
	                        // create a new true color image
	                        $vDstImg = @imagecreatetruecolor( $iWidth, $iHeight );
	                        // copy and resize part of an image with resampling
	                        imagecopyresampled($vDstImg, $vImg, 0, 0, (int)$this->input->post('x1'), (int)$this->input->post('y1'), $iWidth, $iHeight, (int)$this->input->post('w'), (int)$this->input->post('h'));
	                        // define a result image filename
	                        $sResultFileName = $sTempFileName . $sExt; // Nom fichier avec chemin 
	                        $sResultFileNameOfficial = $fileNameOfficial . $sExt; //Nom fichier seulement
	                        // output image to file
	                        imagejpeg($vDstImg, $sResultFileName, $iJpgQuality);
	                        @unlink($sTempFileName);
	                        
	                    }
	        		}else{
	        			$updateLogo = false;
	        		}
	        	}
	        }

	       

	        if($updateLogo){
	        	if($sResultFileName != ''){
	        		$uploadData = array();
	        		$uploadData["file_name"] = $sResultFileNameOfficial;
					$entreprise_data["logo"]="uploads/logos_entreprises/".$uploadData["file_name"];
					$this->session->set_userdata("logoEntreprise",$entreprise_data["logo"]);
		        	redirect("entreprise/profil/2");
	        	}else{
	        		redirect("entreprise/profil/1");
	        	}
	        }else{	
				redirect("entreprise/profil/1");
	        }
		}
	}*/

}