<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Entreprise_model extends CI_Model{

	protected $table = 'entreprise';
    protected $table_type = 'type_entreprise';
    protected $table_type_aliment = 'type_aliment';
    protected $table_adresse = 'adresse';
    protected $table_adresse_entreprise = 'adresse_entreprise';
    protected $table_entreprise_employe = 'entreprise_employe';
    protected $table_entreprise_type = 'entreprise_type';
    protected $table_entreprise_type_aliment = 'entreprise_type_aliment';
    protected $table_employe = "employe";
    protected $table_poste = "poste";
    protected $table_contrat = "contrat";
    protected $table_magasin = "magasin";
    protected $table_compte = "compte";
    protected $table_entreprise_compte = "entreprise_compte";
    protected $table_pays = "pays";
    protected $table_donation = "donation";

    public function getById($id_entreprise){
        $query = $this->db->get_where($this->table,array("id_entreprise"=>(int)$id_entreprise));
        return $query->result();
    }

    public function getEmployeEntreprieByPosition($id_entreprise,$position){
        $sql = "SELECT id_employe,nom,poste_id,email,telephone,sexe FROM employe e JOIN entreprise_employe ee ON ee.employe_id=e.id_employe WHERE ee.entreprise_id = ".$id_entreprise." AND ee.position = ".$position;
        $query = $this->db->query($sql);
        return $query->result();
    }

    public function getTypesEntreprise($id_entreprise){
        $query = "SELECT id_type_entreprise,label FROM ".$this->table_type." t JOIN ".$this->table_entreprise_type." et ON t.id_type_entreprise=et.type_entreprise_id WHERE et.entreprise_id=".(int)$id_entreprise;
        return $this->db->query($query)->result();
    }

    public function getTypesAlimentEntreprise($id_entreprise){
        $query = "SELECT id_type_aliment,label FROM ".$this->table_type_aliment." ta JOIN ".$this->table_entreprise_type_aliment." eta ON ta.id_type_aliment=eta.type_aliment_id WHERE eta.entreprise_id=".(int)$id_entreprise;
        return $this->db->query($query)->result();
    }

    public function getResponsablesEntreprise($id_entreprise){
        $query = "SELECT id_employe,nom,prenom,p.label as poste,p.id_poste as poste_id,email,telephone,position,sexe FROM ".$this->table_employe." e JOIN ".$this->table_entreprise_employe." ee ON ee.employe_id=e.id_employe LEFT JOIN ".$this->table_poste." p ON p.id_poste = e.poste_id WHERE ee.entreprise_id=".(int)$id_entreprise." ORDER BY position";
        return $this->db->query($query)->result();
    }

    public function getAdressesEntreprise($id_entreprise){
        $query = "SELECT a.*, p.label as pays, p.prefixe_tel FROM ".$this->table_adresse." a JOIN ".$this->table_adresse_entreprise." ae ON ae.adresse_id=a.id_adresse JOIN ".$this->table_pays." p ON p.id_pays = a.pays_id WHERE ae.entreprise_id=".(int)$id_entreprise;
        return $this->db->query($query)->result();
    }

    public function getRapportDEntente($id_entreprise){
        return $this->db->get_where($this->table_contrat,array("entreprise_id"=>$id_entreprise))->result();
    }

    public function getTypes($id_entreprise=0){
        $query = "SELECT * FROM ".$this->table_type." WHERE proprietaire=0 OR proprietaire=".$id_entreprise;
        $result = $this->db->query($query)->result();
        return $result;
    }

    public function save($data){
        $this->db->insert($this->table,$data); 

        return $this->db->insert_id();
    }

    public function addType($type, $proprietaire=0){
        $query = "SELECT * FROM ".$this->table_type." WHERE LOWER(label)='".strtolower($type)."' AND (proprietaire=0 OR proprietaire=".$proprietaire.")";
        $result = $this->db->query($query)->result();
        $toReturn = array('id'=>-1, 'exist'=>false);
        if(count($result) <= 0){
            $toInsert = array(
                'label'=>$type,
                'proprietaire'=>$proprietaire
            );
            $this->db->insert($this->table_type,$toInsert); 
            $toReturn['id'] = $this->db->insert_id();
        }
        else{
            $toReturn['id'] = $result[0]->id_type_entreprise;
            $toReturn['exist'] = true;
        }
        return $toReturn;
    }

    public function getTypesAliment($idEA=0){
        $query = "SELECT * FROM ".$this->table_type_aliment." WHERE proprietaire=0 OR proprietaire=".$idEA;
        $result = $this->db->query($query)->result();
        return $result;
    }

    public function addTypeAliment($type, $idEA=0){
        $query = "SELECT * FROM ".$this->table_type_aliment." WHERE LOWER(label)='".strtolower($type)."' AND (proprietaire=0 OR proprietaire=".$idEA.")";
        $result = $this->db->query($query)->result();
        $toReturn = array('id'=>-1, 'exist'=>false);
        if(count($result) <= 0){
            $this->db->insert($this->table_type_aliment,array('label'=>$type)); 
            $toReturn['id'] = $this->db->insert_id();
        }
        else{
            $toReturn['id'] = $result[0]->id_type_aliment;
            $toReturn['exist'] = true;
        }
        return $toReturn;
    }

    public function addAdresse($adresse){
        $this->db->insert($this->table_adresse,$adresse); 

        return $this->db->insert_id();
    }

    public function updateAdresse($adresse_id, $address){
        $this->db->where("id_adresse", $adresse_id);
        $this->db->update($this->table_adresse, $address);
    }

    public function setAdresseEntreprise($entreprise_id,$adresse_id){
        $this->db->insert($this->table_adresse_entreprise,array("entreprise_id"=>$entreprise_id,"adresse_id"=>$adresse_id)); 

        return $this->db->insert_id();
    }

    public function setInformationCompte($info_paiement){
        $this->db->insert($this->table_entreprise_compte,$info_paiement); 
        
        return $this->db->insert_id();
    }

    public function setEmployeEntreprise($entreprise_id,$employe_id,$position){
        $this->db->insert($this->table_entreprise_employe,array("entreprise_id"=>$entreprise_id,"employe_id"=>$employe_id,"position"=>$position)); 

        return $this->db->insert_id();
    }

    public function setTypeEntreprise($entreprise_id,$type_entreprise_id){
        $this->db->insert($this->table_entreprise_type,array("entreprise_id"=>$entreprise_id,"type_entreprise_id"=>$type_entreprise_id)); 

        return $this->db->insert_id();
    }

    public function removeEntrepriseType($entreprise_id){
        return $this->db->delete($this->table_entreprise_type,array("entreprise_id"=>$entreprise_id));
    }

    public function removeEntrepriseTypeAliment($entreprise_id){
        return $this->db->delete($this->table_entreprise_type_aliment,array("entreprise_id"=>$entreprise_id));
    }

    public function setTypeAlimentEntreprise($entreprise_id,$type_aliment_id){
        $this->db->insert($this->table_entreprise_type_aliment,array("entreprise_id"=>$entreprise_id,"type_aliment_id"=>$type_aliment_id)); 

        return $this->db->insert_id();
    }

    public function getEntrepriseByResponsableMail($mail){
        $query = "SELECT * FROM ".$this->table." e JOIN ".$this->table_entreprise_employe." ee ON e.id_entreprise = ee.entreprise_id JOIN ".$this->table_employe." em ON ee.employe_id = em.id_employe WHERE em.email = \"".$mail."\"";

        $result = $this->db->query($query)->result();

        if(count($result)>0)
            return $result[0];

        return null;
    }

    public function getEntrepriseByUtilisateurId($utilisateur_id){
        $result = $this->db->get_where($this->table, array('utilisateur_id' => $utilisateur_id))->result();

        if(count($result)==1){
            return $result[0];
        }

        return null;
    }

    public function getEntrepriseById($entreprise_id){
        $result = $this->db->get_where($this->table, array('id_entreprise' => $entreprise_id))->result();

        if(count($result)==1){
            return $result[0];
        }

        return null;
    }

    public function update($entreprise_id,$entreprise){
        $this->db->where("id_entreprise",$entreprise_id);
        $this->db->update($this->table,$entreprise);
    }

    public function removeAdresse($entreprise_id){
        $this->db->where("entreprise_id",$entreprise_id);
        $this->db->delete($this->table_adresse_entreprise);
    }

     public function removeCompte($entreprise_id){
        $this->db->where("entreprise_id",$entreprise_id);
        $this->db->delete($this->table_entreprise_compte);
    }

    public function getMagasins($idEA){
        $this->db->select('*');
        $this->db->from($this->table_magasin);
        $this->db->where('entreprise_id', $idEA);
        $arrayMag = $this->db->get()->result_array();
        for($i=0; $i<count($arrayMag); $i++){
            $query = "SELECT * FROM ".$this->table_employe." e JOIN ".$this->table_poste." p ON e.poste_id=p.id_poste WHERE e.id_employe=".$arrayMag[$i]['employe_responsable_id'];
            $resp = $this->db->query($query)->result_array()[0];
            $arrayMag[$i]['responsable'] = $resp;
        }
        return $arrayMag;
    }
    //$table_compte = "compte";
    //protected $table_entreprise_compte = "entreprise_compte";

    public function getInformationPaiement($id_entreprise){
        $query = "SELECT * FROM ".$this->table_entreprise_compte." tec JOIN ".$this->table_compte." tc ON tc.id_compte=tec.compte_id WHERE tec.entreprise_id=".(int)$id_entreprise;
        return $this->db->query($query)->result();
    }

    public function getTypeCompte(){
        $query = "SELECT * FROM ".$this->table_compte;
        $result = $this->db->query($query)->result();
        return $result;
    }

    public function getPays(){
        $query = "SELECT * FROM ".$this->table_pays;
        $result = $this->db->query($query)->result();
        return $result;
    }

    public function savePaysEntreprise($id_pays){
        $data = array(
            "label"=>"",
            "latitude"=>"",
            "longitude"=>"",
            "code_postal"=>"",
            "ville"=>"",
            "pays_id"=>$id_pays
        );

        $this->db->insert($this->table_adresse,$data); 

        return $this->db->insert_id();
    }

    public function wsGetAllEntrepriseWithVente(){
        $query = "SELECT e.id_entreprise as id, e.nom_entreprise as label FROM ".$this->table." e JOIN ".$this->table_donation." d ON d.entreprise_id = e.id_entreprise WHERE d.action = 2 GROUP BY e.id_entreprise";
        $result = $this->db->query($query)->result();
        return $result;
    }

    public function wsGetAllEntrepriseWithVenteAProximity(){

        /*$query = "SELECT e.id_entreprise as id_entreprise, e.nom_entreprise as nom_entreprise, a.label as label_adresse, a.latitude as latitude, a.longitude as longitude, a.code_postal as code_postal, a.ville as ville, p.label as pays FROM ".$this->table." e JOIN ".$this->table_donation." d ON d.entreprise_id = e.id_entreprise JOIN ".$this->table_adresse_entreprise." ae ON ae.entreprise_id = e.id_entreprise JOIN ".$this->table_adresse." a ON a.id_adresse = ae.adresse_id JOIN ".$this->table_pays." p ON p.id_pays = a.pays_id WHERE d.action = 2";*/

        $query = "SELECT DISTINCT e.id_entreprise as id_entreprise, e.nom_entreprise as nom_entreprise, a.label as label_adresse, a.latitude as latitude, a.longitude as longitude, a.code_postal as code_postal, a.ville as ville, p.label as pays FROM ".$this->table." e JOIN ".$this->table_donation." d ON d.entreprise_id = e.id_entreprise JOIN ".$this->table_adresse_entreprise." ae ON ae.entreprise_id = e.id_entreprise JOIN ".$this->table_adresse." a ON a.id_adresse = ae.adresse_id JOIN ".$this->table_pays." p ON p.id_pays = a.pays_id WHERE d.action = 2";

        $result = $this->db->query($query)->result();
        return $result;

    }


}