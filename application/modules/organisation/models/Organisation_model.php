<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Organisation_model extends CI_Model{

    protected $table = 'organisation';
    protected $table_type = 'type_organisation';
    protected $table_tranche_age = 'tranche_age';
    protected $table_adresse = 'adresse';
    protected $table_organisation_employe = 'organisation_employe';
    protected $table_nombre_enfant = 'nombre_enfant';
    protected $table_organisation_info_nutri = 'organisation_information_nutritionnelle';
    protected $table_organisation_equipement = 'organisation_equipement';
    protected $table_beneficiaire_organisation = 'beneficiaire_organisation';
    protected $table_organisation_beneficiaire_organisation = 'organisation_beneficiaire_organisation';

    public function save($data){
        $this->db->insert($this->table,$data); 

        return $this->db->insert_id();
    }

    public function getBeneficiaireOrganisation(){
        $query = $this->db->get($this->table_beneficiaire_organisation);
        return $query->result();
    }

    public function addBeneficiaireOrganisation($label, $proprietaire=-1){
        $query = "SELECT * FROM ".$this->table_beneficiaire_organisation." WHERE LOWER(label)='".strtolower($label)."' AND (proprietaire=0 OR proprietaire=".$proprietaire.")";
        $result = $this->db->query($query)->result();
        $toReturn = array('exist'=>false, 'id'=>-1);
        if(count($result) <= 0){
            $this->db->insert($this->table_beneficiaire_organisation,array('label'=>$label,'proprietaire'=>$proprietaire)); 
            $toReturn['id'] = $this->db->insert_id();
        }
        else{
            $toReturn['id'] = $result[0]->id_beneficiaire_organisation;
            $toReturn['exist'] = true;
        }
        return $toReturn;
    }

    public function addOrganisationBeneficiaireOrganisation($organisation_id,$beneficiaire_organisation_id){
        $this->db->insert($this->table_organisation_beneficiaire_organisation,array("beneficiaire_organisation_id"=>$beneficiaire_organisation_id,"organisation_id"=>$organisation_id)); 

        return $this->db->insert_id();
    }

    public function getTypes(){
    	$query = $this->db->get($this->table_type);
        return $query->result();
    }

    public function addType($label, $proprietaire=-1){
        $query = "SELECT * from ".$this->table_type." WHERE LOWER(label)='".strtolower($label)."' AND (proprietaire=0 OR proprietaire=".$proprietaire.")";
        $result = $this->db->query($query)->result();
        $toReturn = array('exist'=>false, 'id'=>-1);
        if(count($result) <= 0){
            $this->db->insert($this->table_type,array('label'=>$label,'proprietaire'=>$proprietaire)); 
            $toReturn['id'] = $this->db->insert_id();
        }
        else{
            $toReturn['id'] = $result[0]->id_type_organisation;
            $toReturn['exist'] = true;
        }
        return $toReturn;
    }

    public function getTranchesAge(){
    	$query = $this->db->get($this->table_tranche_age);
        return $query->result();
    }

    public function addAdresse($adresse){
        $this->db->insert($this->table_adresse,$adresse); 

        return $this->db->insert_id();
    }

    public function setEmployeOrganisation($organisation_id,$employe_id,$position){
        $this->db->insert($this->table_organisation_employe,array("organisation_id"=>$organisation_id,"employe_id"=>$employe_id,"position"=>$position)); 

        return $this->db->insert_id();
    }

    public function setEffectifAge($organisation_id,$tranche_age_id,$effectif){
        $this->db->insert($this->table_nombre_enfant,array("organisation_id"=>$organisation_id,"tranche_age"=>$tranche_age_id,"nombre"=>$effectif)); 

        return $this->db->insert_id();
    }

    public function setInfoNutri($organisation_id,$info_nutri_id){
        $this->db->insert($this->table_organisation_info_nutri,array("organisation_id"=>$organisation_id,"information_nutritionnelle_id"=>$info_nutri_id)); 

        return $this->db->insert_id();
    }

    public function setEquipement($organisation_id,$equipement_id,$etat){
        $this->db->insert($this->table_organisation_equipement,array("organisation_id"=>$organisation_id,"equipement_id"=>$equipement_id,"etat"=>$etat)); 

        return $this->db->insert_id();
    }

    public function getOrganisationsInfoNutri(){
        $query = "SELECT o.id_organisation,oin.information_nutritionnelle_id FROM organisation o JOIN organisation_information_nutritionnelle oin ON o.id_organisation = oin.organisation_id";

        return $this->db->query($query)->result();
    }

    public function getOrganisationsEquipement(){
        $query = "SELECT o.id_organisation,oe.equipement_id,oe.etat FROM organisation o JOIN organisation_equipement oe ON o.id_organisation = oe.organisation_id";

        return $this->db->query($query)->result();
    }

}