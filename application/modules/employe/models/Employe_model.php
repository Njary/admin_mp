<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Employe_model extends CI_Model{
	protected $table = "employe";
    protected $table_poste = "poste";
    protected $table_entreprise = "entreprise";
	protected $table_entreprise_employe = "entreprise_employe";
    protected $table_magasin = "magasin";

	public function addPoste($poste_label,$type_poste,$proprietaire){
        $poste_id = $this->getPoste($poste_label);
        if($poste_id==false){
            $data = array(
                'label'=>$poste_label,
                'type'=>$type_poste,
                'proprietaire'=>$proprietaire
            );
            $this->db->insert($this->table_poste, $data);

            return $this->db->insert_id();
        }else{
            return $poste_id;
        }
	}

	public function save($data){
        $this->db->insert($this->table,$data); 

        return $this->db->insert_id();
    }

    public function checkMail($mail){
    	$query = "SELECT id_employe FROM ".$this->table." e JOIN ".$this->table_entreprise_employe." ee ON e.id_employe = ee.employe_id WHERE ee.position = 1 AND e.email = \"".$mail."\"";

    	$result = $this->db->query($query)->result();

    	if(count($result)>0){
    		return 1;
    	}

    	return 0;
    }

    public function getPosteOwner($id_poste){
        $this->db->select('proprietaire');
        $this->db->from("poste");
        $this->db->where('id_poste', $id_poste);
        $query = $this->db->get();
        return  ($query->num_rows() > 0) ? $query->result_array()[0]['proprietaire'] : 0;
    }

    public function getPoste($poste){
        $this->db->select('*');
        $this->db->from("poste");
        $this->db->where('LOWER(poste.label)', strtolower($poste));
        $query = $this->db->get();
        return  ($query->num_rows() > 0) ? $query->result_array()[0]['id_poste'] : false;
    }

    public function getPostes($type, $proprietaire=0){
        $query = "SELECT * FROM ".$this->table_poste." WHERE type=".$type." AND (proprietaire=0 OR proprietaire=".$proprietaire.")";
        return $this->db->query($query)->result();
    }

    public function updatePoste($idPoste, $idEA){
        $this->db->set('proprietaire', $idEA);
        $this->db->where('id_poste', $idPoste);
        $this->db->update($this->table_poste);
    }

    public function update($employe_id,$employe){
        $this->db->where("id_employe",$employe_id);
        $this->db->update($this->table,$employe);
    }

    /** maka ny ID employe izay manana position = 3 */
    public function getEmploye3ID($idEA){
        $this->db->from($this->table_entreprise_employe);
        $this->db->where('entreprise_id', $idEA);
        $this->db->where('position', 3);
        $data = $this->db->get()->result();
        if(count($data) > 0){
            return $data[0]->employe_id;
        }
        else{
            return null;
        }
    }

    public function getEmploye($idEmpl){
        $this->db->from($this->table);
        $this->db->where('id_employe', $idEmpl);
        $data = $this->db->get()->result();
        if(count($data) > 0){
            return array(
                "nom" => $data[0]->nom,
                "prenom" => $data[0]->prenom,
                "sexe" => $data[0]->sexe,
                "poste_id" => $data[0]->poste_id,
                "email" => $data[0]->email,
                "telephone" => $data[0]->telephone
            );
        }
        else{
            return null;
        }
    }

    public function getIdEntreprise($idUser){
        $this->db->from($this->table_entreprise);
        $this->db->where('utilisateur_id', $idUser);
        $data = $this->db->get()->result();
        if(count($data) > 0){
            return $data[0]->id_entreprise;
        }
        else{
            return null;
        }
    }

    public function getAllPoste(){
        $data = $this->db->get($this->table_poste)->result();
        $result = array();
        for($i=0; $i<count($data); $i++){
            $result[] = array(
                "id_poste"=>$data[$i]->id_poste,
                "label"=>$data[$i]->label
            );
        }
        return $result;
    }

    public function addMagasin($idEA, $idEmploye, $magasin){
        $data = array(
            "nom"=>$magasin,
            "entreprise_id"=>$idEA,
            "employe_responsable_id"=>$idEmploye
        );
        $this->db->insert($this->table_magasin, $data);
        $idMag = $this->db->insert_id();
        $this->db->select('*');
        $this->db->from($this->table_magasin);
        $this->db->where("id_magasin",$idMag);
        return $this->db->get()->result_array()[0];
    }

    public function addResponsableMagasin($idEA, $nom, $prenom, $sexe, $email, $telephone, $poste){
        $data = array(
            "nom"=>$nom,
            "prenom"=>$prenom,
            "email"=>$email,
            "telephone"=>$telephone,
            "sexe"=>$sexe,
            "poste_id"=>$poste
        );
        $this->db->insert($this->table, $data);
        $idEmploye = $this->db->insert_id();
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join($this->table_poste, $this->table.".poste_id=".$this->table_poste.".id_poste");
        $this->db->where("id_employe",$idEmploye);
        return $this->db->get()->result_array()[0];
    }

    public function updateMagasin($idMagasin, $magasin){
        $this->db->where('id_magasin',$idMagasin);
        $this->db->update($this->table_magasin, array('nom'=>$magasin));
        $this->db->select('*');
        $this->db->from($this->table_magasin);
        $this->db->where("id_magasin",$idMagasin);
        return $this->db->get()->result_array()[0];
    }

    public function updateResponsableMagasin($idEmploye, $nom, $prenom, $sexe, $email, $telephone, $poste){
        $data = array(
            "nom"=>$nom,
            "prenom"=>$prenom,
            "email"=>$email,
            "telephone"=>$telephone,
            "sexe"=>$sexe,
            "poste_id"=>$poste
        );
        $this->db->where('id_employe',$idEmploye);
        $this->db->update($this->table, $data);
        $this->db->select('*');
        $this->db->from($this->table);
        $this->db->join($this->table_poste, $this->table.".poste_id=".$this->table_poste.".id_poste");
        $this->db->where("id_employe",$idEmploye);
        return $this->db->get()->result_array()[0];
    }

    public function deleteMagasin($id_magasin){
        $this->db->where('id_magasin', $id_magasin);
        $this->db->delete($this->table_magasin);
    }

    public function deleteResponsableMagasin($id_employe){
        $this->db->where('id_employe', $id_employe);
        $this->db->delete($this->table);
    }
    
}