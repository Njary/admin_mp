<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends MY_Controller {

	protected $limit = 5;
	private $entreprise;
	private $idUser;
	private $idEA ;
	private $actionDonation ;
	private $actionVente ;
	private $poidsTotalDonation ;
	private $poidsTotalVente ;
	
	public function __construct(){
		parent::__construct();
		$this->checkPrivilege();
		$this->load->model("Admin_model");
		$this->idEA = $this->session->userdata("idEntreprise");		
		$this->actionDonation = 1;
		$this->actionVente = 2;

		if(  $this->input->post('teste') != null ){ 
			
			$this->poidsTotalDonation = $this->Admin_model->getPoidsTotal($this->idEA, $this->actionDonation , $this->input->post('mois') , $this->input->post('annee') );
			
			$this->poidsTotalVente = $this->Admin_model->getPoidsTotal($this->idEA, $this->actionVente , $this->input->post('mois') , $this->input->post('annee') );

		}

	}

	public function index(){
		$this->layout->view();
	}

	public function dashboard(){
		$this->action("Tableau de bord");
		$this->layout->ajouter_css('dashboard');
		$this->layout->ajouter_js('../plugins/chart.js/Chart.min');
		$this->layout->ajouter_js('../js/admin.module');
		$idEA = $this->session->userdata("idEntreprise");
		$offset = 0;
		$donations = $this->Admin_model->getDonation($idEA, $this->limit, $offset);
		$ventes = $this->Admin_model->getVente($idEA, $this->limit, $offset);

		$actionDonation = 1;
		$actionVente = 2;

		$selectGlobal = array(
			array( "name"=>"Nourriture sauvé", "value"=>"getNourritureSauve"),
			array( "name"=>"Nombre de repas total", "value"=>"getNombreRepasTotal"),
			array( "name"=>"Total de CO2 évité", "value"=>"getTotalCOEvit"),
			array( "name"=>"Poids total des dons", "value"=>"getPoidsTotalDon"),
			array( "name"=>"Total de repas en donation", "value"=>"getTotalRepasDonation"),
			array( "name"=>"Montant total des dons", "value"=>"getMontantTotalDons"),
			array( "name"=>"Poids total des produits vendus", "value"=>"getPoidsTotalProduitsVendu"),
			array( "name"=>"Nombre total de repas vendu", "value"=>"getNombreTotalRepasVendu"),
			array( "name"=>"Montant total des ventes", "value"=>"getMontantTotalVente"),
			
		);
		$selectVente = array(
			array( "name"=>"Poids de nourriture", "value"=>"getPoidsNourriture"),
			array( "name"=>"Nombre total de repas", "value"=>"getNombreTotalRepas"),
			array( "name"=>"Chiffre d'affaires", "value"=>"getChiffreAffaires"),
			
		);

		$selectDon = array(
			array( "name"=>"Poids total", "value"=>"getPoidsTotal"),
			array( "name"=>"Nombre de repas", "value"=>"getNombreRepas"),
			array( "name"=>"Couts du surplus", "value"=>"getCoutSurplus"),
			array( "name"=>"Nombre bénéficiaires", "value"=>"getNombreBénéficiaires"),
			
		);


		$arg = array(
			"donations"=>$donations,
			"vente"=>$ventes,
			"poidsTotalDonation"=>$this->Admin_model->getPoidsTotal($idEA, $actionDonation),
			"poidsTotalVente"=>$this->Admin_model->getPoidsTotal($idEA, $actionVente),
			"nombreTotalDonation"=>$this->Admin_model->getNombreTotal($idEA, $actionDonation),
			"nombreTotalVente"=>$this->Admin_model->getNombreTotal($idEA, $actionVente),
			"qte_mga_don"=>$this->Admin_model->getQteMga($idEA, $actionDonation),
			"qte_mga_vente"=>$this->Admin_model->getQteMga($idEA, $actionVente),
			"nbTotalBeneficiaireDon"=> $this->Admin_model->getNbTotalBeneficiaire($idEA, $actionDonation),
			"selectDon"=> $selectDon,
			"selectVente"=> $selectVente,
			"selectGlobal"=> $selectGlobal,
			
		);
		

		$this->layout->addView("dashboard", $arg);
		$this->layout->view();
	}

	///don
	public function getPoidsTotal(){
		return number_format($this->poidsTotalDonation, 2, ',', ' ');
	}
	public function getNombreRepas(){
		return number_format($this->poidsTotalDonation/0.25, 2, ',', ' ');
	}

	public function getCoutSurplus(){
		$qte_mga_don= $this->Admin_model->getQteMga($this->idEA, $this->actionDonation);
		return number_format($qte_mga_don, 2, ',', ' ') ;
	}

	public function getNombreBénéficiaires(){
		$nbTotalBeneficiaireDon = $this->Admin_model->getNbTotalBeneficiaire($this->idEA, $this->actionDonation);
		return number_format($nbTotalBeneficiaireDon, 0, ',', ' ');
	}

	///fin don

	///vente
	public function getPoidsNourriture(){
		
		return number_format($this->poidsTotalVente, 2, ',', ' ');
	}

	public function getNombreTotalRepas(){
		
		return number_format($this->poidsTotalVente/0.25, 2, ',', ' ');
	}

	public function getChiffreAffaires(){
		
		$qte_mga_vente = $this->Admin_model->getQteMga($this->idEA, $this->actionVente);
		return number_format($qte_mga_vente, 2, ',', ' '); 
	}

	///fin vente

	////general
	public function getNourritureSauve(){
		
		return number_format($this->poidsTotalDonation + $this->poidsTotalVente, 2, ',', ' ') ;
	}

	public function getNombreRepasTotal(){
		
		return number_format(($this->poidsTotalDonation/0.25) + ($this->poidsTotalVente/0.25), 2, ',', ' ');
	}

	public function getTotalCOEvit(){
		
		return number_format(($this->poidsTotalDonation*1.9)+($this->poidsTotalVente*1.9) , 2, ',', ' '); 
	}

	public function getPoidsTotalDon(){
	
		return number_format($this->poidsTotalDonation, 2, ',', ' ');
	}
	public function getTotalRepasDonation(){
		return number_format($this->poidsTotalDonation/0.25, 2, ',', ' ');
	}

	public function getMontantTotalDons(){
		$qte_mga_don= $this->Admin_model->getQteMga($this->idEA, $this->actionDonation);
		return number_format($qte_mga_don, 2, ',', ' ');
	}

	public function getPoidsTotalProduitsVendu(){

		return number_format($this->poidsTotalVente, 2, ',', ' ');
	}

	public function getNombreTotalRepasVendu(){

		return number_format($this->poidsTotalVente/0.25, 2, ',', ' ');
	}

	public function getMontantTotalVente(){

		$qte_mga_vente= $this->Admin_model->getQteMga($this->idEA, $this->actionVente);
		return number_format($qte_mga_vente, 2, ',', ' ');
	}

	///fin general

	//Sur les donations
	public function getRecentDonation(){
		$idEA = $this->session->userdata("idEntreprise");
		$offset = $this->input->post('offset');
		
		header('Content-Type: application/json');
		echo json_encode($this->Admin_model->getDonation($idEA, $this->limit, $offset));
	}

	public function getQuantityPerYear(){
		$year = (int) $this->input->post('year');
		$idEA = $this->session->userdata("idEntreprise");
		$action = 1; //Donation

		header('Content-Type: application/json');
		echo json_encode($this->Admin_model->getQuantityPerYear($year, $idEA, $action));
	}

	public function getMontantPerYear(){
		$year = (int) $this->input->post('year');
		$idEA = $this->session->userdata("idEntreprise");
		$action = 1; //Donation

		header('Content-Type: application/json');
		echo json_encode($this->Admin_model->getMontantPerYear($year, $idEA, $action));
	}

	public function getNombrePerGenre(){
		$genre = $this->input->post('genre');
		$idEA = $this->session->userdata("idEntreprise");
		$action = 1; //Donation

		header('Content-Type: application/json');
		echo json_encode($this->Admin_model->getNombrePerGenre($genre, $idEA, $action));
	}

	public function getQtePerGenre(){
		$genre = $this->input->post('genre');
		$idEA = $this->session->userdata("idEntreprise");
		$action = 1; //Donation

		header('Content-Type: application/json');
		echo json_encode($this->Admin_model->getQtePerGenre($genre, $idEA, $action));
	}
	//Fin sur les donations

	//Début sur les ventes
	public function getRecentVente(){
		$idEA = $this->session->userdata("idEntreprise");
		$offset = $this->input->post('offset');
		
		header('Content-Type: application/json');
		echo json_encode($this->Admin_model->getVente($idEA, $this->limit, $offset));
	}

	public function getQuantitySalesPerYear(){
		$year = (int) $this->input->post('year');
		$idEA = $this->session->userdata("idEntreprise");
		$action = 2; //Donation

		header('Content-Type: application/json');
		echo json_encode($this->Admin_model->getQuantityPerYear($year, $idEA, $action));
	}

	public function getMontantSalesPerYear(){
		$year = (int) $this->input->post('year');
		$idEA = $this->session->userdata("idEntreprise");
		$action = 2; //Donation

		header('Content-Type: application/json');
		echo json_encode($this->Admin_model->getMontantPerYear($year, $idEA, $action));
	}

	public function getNombreSalesPerGenre(){
		$genre = $this->input->post('genre');
		$idEA = $this->session->userdata("idEntreprise");
		$action = 2; //Donation

		header('Content-Type: application/json');
		echo json_encode($this->Admin_model->getNombrePerGenre($genre, $idEA, $action));
	}

	public function getQteSalesPerGenre(){
		$genre = $this->input->post('genre');
		$idEA = $this->session->userdata("idEntreprise");
		$action = 2; //Donation

		header('Content-Type: application/json');
		echo json_encode($this->Admin_model->getQtePerGenre($genre, $idEA, $action));
	}

	//Fin sur les ventes

	public function getNombreDonVentePerYear(){
		$year = (int) $this->input->post('year');
		$idEA = $this->session->userdata("idEntreprise");

		header('Content-Type: application/json');
		echo json_encode($this->Admin_model->getNombreDonVentePerYear($year, $idEA));
	}

	public function nePlusAfficherBienvenu(){

		$etat = $this->input->post('etat');
		
		$this->session->set_userdata("first_time", $etat);

		$result = array(
			"etat"=>$this->session->userdata('first_time')
		);

		header('Content-Type: application/json');
		echo json_encode($this->session->userdata("first_time"));
		
	}

	// create xlsx
    public function generateXls() {
        $fileName = 'data-'.time().'.xlsx'; 

		// load excel library
        $this->load->library('excel');



        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0);

        $liste_rapport = $this->input->post('rapport');

        $entete = 'A';
		
        $class_object = $this;
        for($i=0 ; $i < count( $liste_rapport ) ; $i++){

        	$name_value = explode("&&",$liste_rapport[$i]);// method&&name

        	$objPHPExcel->getActiveSheet()->SetCellValue($entete.'1', $name_value[1]);

        	$reflectionMethod = new ReflectionMethod('Admin', $name_value[0]);
			$val = $reflectionMethod->invoke($class_object);
			
			$objPHPExcel->getActiveSheet()->SetCellValue($entete.'2', $val);

			$entete++;
        }


		
		
		
		
        // set Header
        /*$objPHPExcel->getActiveSheet()->SetCellValue('A1', 'Nourriture sauvé');
        $objPHPExcel->getActiveSheet()->SetCellValue('B1', 'Nombre de repas total');
        $objPHPExcel->getActiveSheet()->SetCellValue('C1', 'Total de CO2');
        $objPHPExcel->getActiveSheet()->SetCellValue('D1', 'Poids total des dons');
        $objPHPExcel->getActiveSheet()->SetCellValue('E1', 'Total de repas en donation');       
        $objPHPExcel->getActiveSheet()->SetCellValue('F1', 'Montant total des dons');       
        $objPHPExcel->getActiveSheet()->SetCellValue('G1', 'Poids total des produits vendu');       
        $objPHPExcel->getActiveSheet()->SetCellValue('H1', 'Nombre total de repas vendu');       
        $objPHPExcel->getActiveSheet()->SetCellValue('I1', 'Montant total des ventes');*/       
        /*$objPHPExcel->getActiveSheet()->SetCellValue('J1', 'Poids de nourriture');       
        $objPHPExcel->getActiveSheet()->SetCellValue('K1', 'Nombre total de repas');       
        $objPHPExcel->getActiveSheet()->SetCellValue('L5', 'Chiffre d\'affaires');*/       
        // set Row
       /* $rowCount = 2;
            $objPHPExcel->getActiveSheet()->SetCellValue('A' . $rowCount, number_format( $poidsTotalDonation + $poidsTotalVente , 2, ',', ' '));
            $objPHPExcel->getActiveSheet()->SetCellValue('B' . $rowCount, number_format( ($poidsTotalDonation/0.25) + ($poidsTotalVente/0.25) , 2, ',', ' ') );
            $objPHPExcel->getActiveSheet()->SetCellValue('C' . $rowCount, number_format(($poidsTotalDonation*1.9)+($poidsTotalVente*1.9) , 2, ',', ' '));
            $objPHPExcel->getActiveSheet()->SetCellValue('D' . $rowCount, number_format($poidsTotalDonation, 2, ',', ' '));
            $objPHPExcel->getActiveSheet()->SetCellValue('E' . $rowCount, number_format($poidsTotalDonation/0.25, 2, ',', ' '));
            $objPHPExcel->getActiveSheet()->SetCellValue('F' . $rowCount, number_format($qte_mga_don, 2, ',', ' '));
            $objPHPExcel->getActiveSheet()->SetCellValue('G' . $rowCount, number_format($poidsTotalVente, 2, ',', ' '));
            $objPHPExcel->getActiveSheet()->SetCellValue('H' . $rowCount, number_format($poidsTotalVente/0.25, 2, ',', ' '));
            $objPHPExcel->getActiveSheet()->SetCellValue('I' . $rowCount, number_format($qte_mga_vente, 2, ',', ' '));*/
            /*$objPHPExcel->getActiveSheet()->SetCellValue('J' . $rowCount, number_format($poidsTotalVente, 2, ',', ' '));
            $objPHPExcel->getActiveSheet()->SetCellValue('K' . $rowCount, number_format($poidsTotalVente/0.25, 2, ',', ' '));
            $objPHPExcel->getActiveSheet()->SetCellValue('M' . $rowCount, number_format($qte_mga_vente, 2, ',', ' '));*/
            
        $filename = "tutsmake". date("Y-m-d-H-i-s").".csv";
		header('Content-Type: application/vnd.ms-excel'); 
		header('Content-Disposition: attachment;filename="'.$filename.'"');
		header('Cache-Control: max-age=0'); 
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV'); 
		$objWriter->setDelimiter(';');
		$objWriter->save('php://output'); 
    }
}