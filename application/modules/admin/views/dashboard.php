<div class="container-section" ng-app="AdminApp" ng-controller="AdminCtrl" ng-init="init('<?php echo base_url(''); ?>g-n-d-v-p-y' )">

    <div style="width: 100%;" >
      
      <button id="download" ng-click="ouvrirModalChoix()" class="bouton-sticky " title="Exporter en pdf"><img src="<?php echo base_url('assets/img/dashboard/downloads-icon.png') ?>" class="exportImage" alt="exporter"></button>
      <div class="card card-primary card-outline">
        <div class="card-header d-flex p-0">
            <h3 class="card-title p-3"></h3>
            <ul class="nav dashB nav-pills mr-auto p-2">
                <li class="nav-item"><a class="nav-link active show" href="#tab_total" data-toggle="tab">Total</a></li>

                <li class="nav-item"><a class="nav-link" href="#tab_vente" ng-click="loadVenteInfo('<?php echo base_url(''); ?>g-q-s-p-y' , '<?php echo base_url(''); ?>g-m-s-p-y' , '<?php echo base_url(''); ?>g-n-s-p-g' , '<?php echo base_url(''); ?>g-q-s-p-g' )" data-toggle="tab">Vente</a></li>

                <li class="nav-item"><a class="dashB nav-link" href="#tab_donation" ng-click="loadDonInfo('<?php echo base_url(''); ?>g-q-p-y','<?php echo base_url(''); ?>g-m-p-y','<?php echo base_url(''); ?>g-n-p-g','<?php echo base_url(''); ?>g-q-p-g')" data-toggle="tab">Donation</a></li>
            </ul>
        </div>

        <div class="card-body">
          <div class="tab-content">
              <!-- START TAB TOTAL -->
              <div id="tab_total" class="tab-pane active show">
                  <!-- Info boxes -->
          
                  <div class="row">
                    <div class="col-12 col-sm-6 col-md-4">
                      <!--div class="small-box bg-warning">
                        <div class="inner">
                          <h3><?php echo number_format($poidsTotalDonation + $poidsTotalVente, 2, ',', ' ').' '; ?> <sup style="font-size: 20px">Kg</sup></h3>

                          <p>Poids total</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-magnet"></i>
                        </div>
                        <span class="small-box-footer"></span>
                      </div-->

                      <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header " style="background-color: #94D5EB;">
                          <div class="widget-user-image">
                            <img class="img-icon-style" src="<?php echo base_url('assets/img/dashboard/diet.png');?>" alt="User Avatar">
                          </div>
                          <!-- /.widget-user-image -->
                          <h5 class="widget-user-username" style="font-size: 17px !important;">Nourriture sauvé</h5>
                          <h3 class="widget-user-desc" style="font-size: 19px !important;"><?php echo number_format($poidsTotalDonation + $poidsTotalVente, 2, ',', ' ').' '; ?> Kg</h3>
                        </div>
                  
                      </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                      <!--div class="small-box bg-success">
                        <div class="inner">
                          <h3><?php echo number_format(($poidsTotalDonation/0.25) + ($poidsTotalVente/0.25), 2, ',', ' ').' '; ?> </h3>

                          <p>Nombre de repas total</p>
                        </div>
                        <div class="icon">
                          <i class="fa fa-birthday-cake"></i>
                        </div>
                        <span class="small-box-footer"></span>
                      </div-->

                      <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header " style="background-color: #94D5EB;">
                          <div class="widget-user-image">
                            <img class="img-icon-style" src="<?php echo base_url('assets/img/dashboard/soup.png');?>" alt="User Avatar" >
                          </div>
                          <!-- /.widget-user-image -->
                          <h5 class="widget-user-username" style="font-size: 17px !important;">Nombre de repas total</h5>
                          <h3 class="widget-user-desc" style="font-size: 19px !important;"><?php echo number_format(($poidsTotalDonation/0.25) + ($poidsTotalVente/0.25), 2, ',', ' ').' '; ?></h3>
                        </div>
                  
                      </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                      <!--div class="small-box bg-danger">
                        <div class="inner">
                          <h3><?php echo number_format(($poidsTotalDonation*1.9)+($poidsTotalVente*1.9) , 2, ',', ' ').' '; ?> <sup style="font-size: 20px">Kg</sup></h3>

                          <p>Total de CO<sub>2</sub> évité</p>
                        </div>
                        <div class="icon">
                          <!-=-<i class="fa fa-flask"></i> ->
                          <i><img scr="<?php echo base_url('assets/img/dashboard/co2.png');?>" alt="Image CO2" width="70" height="64"/></i>
                        </div>
                        <span class="small-box-footer"></span>
                      </div-->

                      <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header " style="background-color: #94D5EB;">
                          <div class="widget-user-image ">
                            <img class="img-icon-style" src="<?php echo base_url('assets/img/dashboard/co2.png');?>" alt="User Avatar">
                          </div>
                          <!-- /.widget-user-image -->
              
                          <h5 class="widget-user-username" style="font-size: 17px !important;">Total de CO<sub>2</sub> évité</h5>
                          <h3 class="widget-user-desc" style="font-size: 19px !important;"><?php echo number_format(($poidsTotalDonation*1.9)+($poidsTotalVente*1.9) , 2, ',', ' ').' '; ?> Kg</h3>
                        </div>
                  
                      </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12">
                      <div class="card">
                          <div class="card-header no-border">
                            <div class="d-flex justify-content-between">
                              <h3 class="card-title">Nombre de Don/Vente </h3>
                              <div class="d-flex">
                                <p class="d-flex flex-column">
                                  <select id="select-year-don-vente-per-year" onchange="angular.element(this).scope().yearNbDonVenteChange('<?php echo base_url(''); ?>g-n-d-v-p-y')" style="appearance: none; -moz-appearance: none; -webkit-appearance: none;" class="text-muted custom-select ">
                                    <?php 
                                    $currentYear = (int)date("Y");
                                    for($date=(int)date("Y"); $date>2016; $date--){
                                      if($currentYear == $date){
                                        echo '<option class="text-muted" value="'.$date.'">Cette année</option>';
                                      }
                                      else{
                                        echo '<option class="text-muted" value="'.$date.'">Année '.$date.'</option>';
                                      }
                                    } ?>
                                  </select>
                                </p>
                              </div>
                            </div>
                          </div>
                          <div class="card-body">
                            <!-- /.d-flex -->

                            <div class="position-relative mb-4" id="graph-nb-don-vente-per-year">
                              <div class="lds-roller" style="display: block; margin: 0 auto;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                            </div>

                            <div class="d-flex flex-row justify-content-end">
                              <span class="mr-2">
                                <i class="fa fa-square " style="color: #F05423;"></i> Donation
                              </span>

                              <span>
                                <i class="fa fa-square " style="color: #FEC745;"></i> Vente
                              </span>
                            </div>
                          </div>
                        </div>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12 col-sm-6 col-md-4">
                      <!--div class="info-box">
                          <span class="info-box-icon bg-success elevation-1">
                            <i class="fa fa-magnet"></i>
                          </span>

                          <div class="info-box-content">
                            <span class="info-box-text">Poids total des dons</span>
                            <span class="info-box-number">
                              <?php echo number_format($poidsTotalDonation, 2, ',', ' '); ?> Kg
                            </span>
                          </div>
                          <!- /.info-box-content ->
                        </div-->
                        <!-- /.info-box -->

                        <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                          <div class="widget-user-header bg-light">
                            <div class="widget-user-image">
                              <img class="img-icon-style" src="<?php echo base_url('assets/img/dashboard/diet.png');?>" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h5 class="widget-user-username" style="font-size: 17px !important;">Poids total des dons</h5>
                            <h3 class="widget-user-desc" style="font-size: 17px !important;"><?php echo number_format($poidsTotalDonation, 2, ',', ' '); ?> Kg </h3>
                          </div>
                    
                        </div>

                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                      <!--div class="info-box">
                          <span class="info-box-icon bg-success elevation-1">
                            <i class="fa fa-birthday-cake"></i>
                          </span>

                          <div class="info-box-content">
                            <span class="info-box-text">Nombre total de repas en donation</span>
                            <span class="info-box-number">
                              <?php echo number_format($poidsTotalDonation/0.25, 2, ',', ' '); ?> 
                            </span>
                          </div>
                          <!- /.info-box-content ->
                        </div>
                        <!- /.info-box -->

                        <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                          <div class="widget-user-header bg-light">
                            <div class="widget-user-image">
                              <img class="img-icon-style" src="<?php echo base_url('assets/img/dashboard/soup.png');?>" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h5 class="widget-user-username" style="font-size: 17px !important;">Total de repas en donation</h5>
                            <h3 class="widget-user-desc" style="font-size: 17px !important;"><?php echo number_format($poidsTotalDonation/0.25, 2, ',', ' '); ?> </h3>
                          </div>
                    
                        </div>

                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                      <!--div class="info-box">
                          <span class="info-box-icon bg-success elevation-1">
                            <i class="fa fa-usd"></i>
                          </span>

                          <div class="info-box-content">
                            <span class="info-box-text">Montant total des dons</span>
                            <span class="info-box-number">
                              <?php echo number_format($qte_mga_don, 2, ',', ' '); ?> MGA
                            </span>
                          </div>
                          <!- /.info-box-content ->
                        </div>
                        <!- /.info-box -->

                        <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                          <div class="widget-user-header bg-light">
                            <div class="widget-user-image">
                              <img class="img-icon-style" src="<?php echo base_url('assets/img/dashboard/analytics.png');?>" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h5 class="widget-user-username" style="font-size: 17px !important;">Montant total des dons</h5>
                            <h3 class="widget-user-desc" style="font-size: 17px !important;"><?php echo number_format($qte_mga_don, 2, ',', ' '); ?> MGA </h3>
                          </div>
                    
                        </div>

                    </div>
                  </div>
                  <div class="row">
                    <div class="col-12 col-sm-6 col-md-4">
                      <!--div class="info-box">
                          <span class="info-box-icon bg-dark elevation-1">
                            <i class="fa fa-magnet"></i>
                          </span>

                          <div class="info-box-content">
                            <span class="info-box-text">Poids total des produit vendu</span>
                            <span class="info-box-number">
                              <?php echo number_format($poidsTotalVente, 2, ',', ' '); ?> Kg
                            </span>
                          </div>
                          <!- /.info-box-content ->
                        </div>
                        <!- /.info-box -->

                        <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                          <div class="widget-user-header bg-light">
                            <div class="widget-user-image">
                              <img class="img-icon-style" src="<?php echo base_url('assets/img/dashboard/diet.png');?>" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h5 class="widget-user-username" style="font-size: 17px !important;">Poids total des produits vendus</h5>
                            <h3 class="widget-user-desc" style="font-size: 17px !important;"><?php echo number_format($poidsTotalVente, 2, ',', ' '); ?> Kg </h3>
                          </div>
                    
                        </div>

                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                      <!--div class="info-box">
                          <span class="info-box-icon bg-dark elevation-1">
                            <i class="fa fa-birthday-cake"></i>
                          </span>

                          <div class="info-box-content">
                            <span class="info-box-text">Nombre total de repas vendu</span>
                            <span class="info-box-number">
                              <?php echo number_format($poidsTotalVente/0.25, 2, ',', ' '); ?> 
                            </span>
                          </div>
                          <!- /.info-box-content ->
                        </div>
                        <!- /.info-box -->

                        <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                          <div class="widget-user-header bg-light">
                            <div class="widget-user-image">
                              <img class="img-icon-style" src="<?php echo base_url('assets/img/dashboard/soup.png');?>" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h5 class="widget-user-username" style="font-size: 17px !important;">Nombre total de repas vendu</h5>
                            <h3 class="widget-user-desc" style="font-size: 17px !important;" ><?php echo number_format($poidsTotalVente/0.25, 2, ',', ' '); ?>  </h3>
                          </div>
                    
                        </div>

                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                      <!--div class="info-box">
                          <span class="info-box-icon bg-dark elevation-1">
                            <i class="fa fa-usd"></i>
                          </span>

                          <div class="info-box-content">
                            <span class="info-box-text">Montant total des ventes</span>
                            <span class="info-box-number">
                              <?php echo number_format($qte_mga_vente, 2, ',', ' '); ?> MGA
                            </span>
                          </div>
                          <!- /.info-box-content ->
                        </div>
                        <!- /.info-box -->

                        <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                          <div class="widget-user-header bg-light">
                            <div class="widget-user-image">
                              <img class="img-icon-style" src="<?php echo base_url('assets/img/dashboard/analytics.png');?>" alt="User Avatar">
                            </div>
                            <!-- /.widget-user-image -->
                            <h5 class="widget-user-username" style="font-size: 17px !important;">Montant total des ventes</h5>
                            <h3 class="widget-user-desc" style="font-size: 17px !important;"><?php echo number_format($qte_mga_vente, 2, ',', ' '); ?> MGA </h3>
                          </div>
                    
                        </div>


                    </div>
                  </div>
                  
                  <!-- ssssss -->   
                  <!-- /.row -->
              </div>

              <!-- END TAB TOTAL -->
              <!-- START TAB VENTE -->
              <div id="tab_vente" class="tab-pane">
                  <!-- Info boxes -->
                  <div class="row">
                    <div class="col-12 col-sm-6 col-md-4">
                      <!--div class="info-box">
                        <span class="info-box-icon bg-info elevation-1">
                          <i class="fa fa-magnet"></i>
                        </span>

                        <div class="info-box-content">
                          <span class="info-box-text">Poids total  </span>
                          <span class="info-box-number">
                            <?php echo number_format($poidsTotalVente, 2, ',', ' '); ?> Kg
                          </span>
                        </div>
                        <!- /.info-box-content ->
                      </div-->
                      <!-- /.info-box -->

                      <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header" style="background-color: #94D5EB;">
                          <div class="widget-user-image ">
                            <img class="img-icon-style" src="<?php echo base_url('assets/img/dashboard/diet.png');?>" alt="User Avatar">
                          </div>
                          <!-- /.widget-user-image -->
              
                          <h5 class="widget-user-username" style="font-size: 17px !important;">Poids de nourriture</h5>
                          <h3 class="widget-user-desc" style="font-size: 19px !important;"><?php echo number_format($poidsTotalVente, 2, ',', ' '); ?> Kg</h3>
                        </div>
                  
                      </div>

                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                      <!--div class="info-box">
                        <span class="info-box-icon bg-success elevation-1">
                          <i class="fa fa-birthday-cake"></i>
                        </span>

                        <div class="info-box-content">
                          <span class="info-box-text">Nombre total de repas </span>
                          <span class="info-box-number">
                            <?php echo number_format($poidsTotalVente/0.25, 2, ',', ' '); ?> 
                          </span>
                        </div>
                        <!- /.info-box-content ->
                      </div-->
                      <!-- /.info-box -->

                      <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header" style="background-color: #94D5EB;">
                          <div class="widget-user-image ">
                            <img class="img-icon-style" src="<?php echo base_url('assets/img/dashboard/soup.png');?>" alt="User Avatar">
                          </div>
                          <!-- /.widget-user-image -->
              
                          <h5 class="widget-user-username" style="font-size: 17px !important;">Nombre total de repas</h5>
                          <h3 class="widget-user-desc" style="font-size: 19px !important;"><?php echo number_format($poidsTotalVente/0.25, 2, ',', ' '); ?></h3>
                        </div>
                  
                      </div>

                    </div>
                    <div class="col-12 col-sm-6 col-md-4">
                      <!--div class="info-box mb-3">
                        <span class="info-box-icon bg-warning elevation-1">
                          <i class="fa fa-usd"></i>
                        </span>

                        <div class="info-box-content">
                          <span class="info-box-text">Chiffre d'affaires </span>
                          <span class="info-box-number">
                            <?php echo number_format($qte_mga_vente, 2, ',', ' ').' '; ?> MGA
                          </span>
                        </div>
                        <!- /.info-box-content ->
                      </div-->
                      <!-- /.info-box -->
                      <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header" style="background-color: #94D5EB;">
                          <div class="widget-user-image ">
                            <img class="img-icon-style" src="<?php echo base_url('assets/img/dashboard/analytics.png');?>" alt="User Avatar">
                          </div>
                          <!-- /.widget-user-image -->
              
                          <h5 class="widget-user-username" style="font-size: 17px !important;">Chiffre d'affaires</h5>
                          <h3 class="widget-user-desc" style="font-size: 19px !important;"><?php echo number_format($qte_mga_vente, 2, ',', ' ').' '; ?> MGA</h3>
                        </div>
                  
                      </div>
                    </div>
                   
                  </div>
                  <!-- /.row -->  

                <!-- Main row -->
                <div class="row">

                  <div class="col-lg-6">
                    <div class="card">
                      <div class="card-header no-border">
                        <div class="d-flex justify-content-between">
                          <h3 class="card-title">Quantité des produits </h3>
                          <div class="d-flex">
                            <p class="d-flex flex-column">
                              <select id="select-year-qte-sales-per-year" onchange="angular.element(this).scope().yearQteSalesChange('<?php echo base_url(''); ?>g-q-s-p-y')" style="appearance: none; -moz-appearance: none; -webkit-appearance: none;" class="text-muted custom-select ">
                                <?php 
                                $currentYear = (int)date("Y");
                                for($date=(int)date("Y"); $date>2016; $date--){
                                  if($currentYear == $date){
                                    echo '<option class="text-muted" value="'.$date.'">Cette année</option>';
                                  }
                                  else{
                                    echo '<option class="text-muted" value="'.$date.'">Année '.$date.'</option>';
                                  }
                                } ?>
                              </select>
                            </p>
                          </div>
                        </div>
                      </div>
                      <div class="card-body">
                        <!-- /.d-flex -->

                        <div class="position-relative mb-4" id="graph-qte-sales-per-year">
                          <div class="lds-roller" style="display: block; margin: 0 auto;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                        </div>

                        <div class="d-flex flex-row justify-content-end">
                          <span class="mr-2">
                            <i class="fa fa-square text-gray" style="color: #F05423;"></i> Poids (Kg)
                          </span>

                          <span>
                            <i class="fa fa-square text-gray" style="color: #FEC745;"></i> Volume (<var>m<sup>3</sup></var>)
                          </span>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="col-lg-6">
                        <div class="card">
                          <div class="card-header no-border">
                            <div class="d-flex justify-content-between">
                              <h3 class="card-title">Montant des produits</h3>
                              <div class="d-flex">
                                <p class="d-flex flex-column">
                                  <select id="select-montant-sales-year" onchange="angular.element(this).scope().yearMontantSalesChange('<?php echo base_url(''); ?>g-m-s-p-y')" style="appearance: none; -moz-appearance: none; -webkit-appearance: none;" class="text-muted custom-select">
                                  <?php 
                                    $currentYear = (int)date("Y");
                                    for($date=(int)date("Y"); $date>2016; $date--){
                                      if($currentYear == $date){
                                        echo '<option class="text-muted" value="'.$date.'">Cette année</option>';
                                      }
                                      else{
                                        echo '<option class="text-muted" value="'.$date.'">L\'année '.$date.'</option>';
                                      }
                                    } ?>
                                  </select>
                                </p>
                              </div>
                            </div>
                          </div>
                          <div class="card-body">
                            <div class="d-flex">
                              <p class="d-flex flex-column">
                                <span id="montant-vente-total" class="text-bold text-lg" style="margin-top: -22px;">0 MGA</span>
                              </p>
                            </div>
                            <!-- /.d-flex -->

                            <div class="position-relative mb-4" id="graph-montant-sales-per-year">
                              <div class="lds-roller" style="display: block; margin: 0 auto;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                            </div>

                            <div class="d-flex flex-row justify-content-end">
                              
                            </div>
                          </div>
                        </div>
                        <!-- /.card -->
                      </div>
                    <!-- /.col -->


                </div>
                <!-- /.row -->  
                <div class="row">
                  <div class="col-lg-6">
                        <div class="card">
                          <div class="card-header no-border">
                            <div class="d-flex justify-content-between">
                              <h3 class="card-title">Nombre de produit vendu par : </h3>
                              <div class="d-flex">
                                <p class="d-flex flex-column">
                                  <select id="select-nombre-sales-genre" onchange="angular.element(this).scope().getNombreSalesPerGenre('<?php echo base_url(''); ?>g-n-s-p-g')" style="appearance: none; -moz-appearance: none; -webkit-appearance: none;" class="text-muted custom-select">
                                    <option value="categorie">Catégorie </option>
                                    <option value="type">Type</option>
                                  </select>
                                </p>
                              </div>
                            </div>
                          </div>
                          <div class="card-body">
                            <div class="d-flex">
                              
                            </div>
                            <!-- /.d-flex -->

                            <div class="position-relative mb-4">
                              <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-8">
                                  <div class="row">
                                    <div class="col-md-6" style="text-align: left;">0</div>
                                    <div id="nombre-total-vente" class="col-md-6" style="text-align: right;">Max</div>
                                  </div>
                                </div>
                              </div>
                              <div id="list-genre-vente" class="row">

                              </div>
                            </div>

                            <div class="d-flex flex-row justify-content-end">
                              
                            </div>
                          </div>
                        </div>
                        <!-- /.card -->
                      </div>
                      <div class="col-lg-6">
                        <div class="card">
                          <div class="card-header no-border">
                            <div class="d-flex justify-content-between">
                              <h3 class="card-title">Poids/Volume de produit vendu par :</h3>
                              <div class="d-flex">
                                <p class="d-flex flex-column">
                                  <select id="select-qte-sales-genre" onchange="angular.element(this).scope().getQteSalesPerGenre('<?php echo base_url(''); ?>g-q-s-p-g')" style="appearance: none; -moz-appearance: none; -webkit-appearance: none;" class="text-muted custom-select">
                                    <option value="categorie">Catégorie </option>
                                    <option value="type">Type</option>
                                  </select>
                                </p>
                              </div>
                            </div>
                          </div>
                          <div class="card-body">
                            <div class="d-flex">
                              
                            </div>
                            <!-- /.d-flex -->

                            <div class="position-relative mb-4">
                              <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-8">
                                  <div class="row">
                                    <div class="col-md-6" style="text-align: left;">0</div>
                                    <div id="qte-total-vente" class="col-md-6" style="text-align: right;">Max</div>
                                  </div>
                                </div>
                              </div>
                              <div id="list-qte-vente" class="row">
                                
                              </div>
                            </div>

                            <div class="d-flex flex-row justify-content-end">
                              <span class="mr-2">
                                <i class="fa fa-square text-gray" style="color: #F05423;"></i> Poids (Kg)
                              </span>

                              <span>
                                <i class="fa fa-square text-gray" style="color: #FEC745;"></i> Volume (<var>m<sup>3</sup></var>)
                              </span>
                            </div>
                          </div>
                        </div>
                        <!-- /.card -->
                      </div>
                
                </div>
                <!-- TABLE: LATEST ORDERS -->
                <div class="col-md-12">
                  <div class="card">
                    <div class="card-header border-transparent">
                      

                      <div class="d-flex justify-content-between"">
                        <h3 class="card-title">Ventes récentes</h3>
                          <p class="d-flex flex-column">
                            <select id="select-vente_etat" onchange="angular.element(this).scope().showVenteByEtat()" style="appearance: none; -moz-appearance: none; -webkit-appearance: none;" class="text-muted custom-select">
                              <option value="0" >Tout</option>
                              <option value="1">Vendu</option>
                              <option value="2">En vente</option>
                            </select>
                          </p>
                      </div>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body p-0">
                      <div class="table-responsive">
                        <table class="table m-0" id="table_vente_recente">
                          <thead>
                          <tr>
                            <th>Date</th>
                            <th>Produit</th>
                            <th>Catégorie </th>
                            <th>Quantités</th>
                            <th>Prix</th>
                            <th>Etat</th>
                          </tr>
                          </thead>
                          <tbody id="body-list-recent-vente">
                            <?php foreach($vente as $v){ ?>
                              <?php $etat = 0; 
                                    if(!empty($v['produitCommande'])){ $etat = $v['produitCommande'][0]->id_etat_commande; } 
                              ?>
                              <tr class="etat<?php echo $etat; ?>">
                                <td><?php echo $v['datetime']['date'] ?><span style="font-size: 13px; opacity: 0.9;"> à <?php echo $v['datetime']['time'] ?></span></td>
                                <td><?php echo $v['produit'] ?></td>
                                <td><?php $catArray = explode("(", $v['categorie']); echo $catArray[0]; ?></td>
                                <td><?php echo $v['quantites'] ?></td>
                                <td><?php echo $v['prix']." MGA" ?></td>
                                <!--td><?php //if(empty($v['produitCommande'])){echo $v['prix'].' MGA'; }else{ echo $v['produitCommande'][0]->qte * $v['produitCommande'][0]->prix_achat_unite.' MGA';   } ?></td-->
                                <td><?php if(!empty($v['produitCommande'])){ echo $v['produitCommande'][0]->label; }else{ echo "En vente";}?></td>
                              </tr>
                            <?php } ?>
                          </tbody>
                        </table>
                      </div>
                      <!-- /.table-responsive -->
                    </div>
                    <!-- /.card-body -->
                    <div class="card-footer clearfix">
                      <button id="btn-show-more-vente" ng-click="showMoreVente('<?php echo base_url(''); ?>g-r-v')" class="btn btn-sm float-left" style="background-color: #00AFAA; color:#fff" >Afficher plus</button>
                      <div id="lds-roller-list-recent-vente" class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                    </div>
                    <!-- /.card-footer -->
                  </div>
                </div>
                <!-- /.card -->

              </div>

              <!-- END TAB VENTE -->
              <!-- START TAB DONATION -->
              <div id="tab_donation" class="tab-pane">
                  
                  <!-- Info boxes -->
                  <div class="row">
                    <div class="col-12 col-sm-6 col-md-3">
                      <!--div class="info-box">
                        <span class="info-box-icon bg-info elevation-1">
                          <i class="fa fa-magnet"></i>
                        </span>

                        <div class="info-box-content">
                          <span class="info-box-text">Poids total  </span>
                          <span class="info-box-number">
                            <?php echo number_format($poidsTotalDonation, 2, ',', ' '); ?> Kg
                          </span>
                        </div>
                        <!- /.info-box-content ->
                      </div>
                      <!- /.info-box -->

                      <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header" style="background-color: #94D5EB;">
                          <div class="widget-user-image ">
                            <img class="img-icon-style" src="<?php echo base_url('assets/img/dashboard/diet.png');?>" alt="User Avatar">
                          </div>
                          <!-- /.widget-user-image -->
              
                          <h5 class="widget-user-username" style="font-size: 15px !important;">Poids total</h5>
                          <h3 class="widget-user-desc" style="font-size: 19px !important;"><?php echo number_format($poidsTotalDonation, 2, ',', ' '); ?> Kg </h3>
                        </div>
                  
                      </div>

                    </div>
                    <div class="col-12 col-sm-6 col-md-3">
                      <!--div class="info-box">
                        <span class="info-box-icon bg-success elevation-1">
                          <i class="fa fa-birthday-cake"></i>
                        </span>

                        <div class="info-box-content">
                          <span class="info-box-text">Nombre total de repas </span>
                          <span class="info-box-number">
                            <?php echo number_format($poidsTotalDonation/0.25, 2, ',', ' '); ?> 
                          </span>
                        </div>
                        <!- /.info-box-content ->
                      </div>
                      <!- /.info-box -->

                      <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header" style="background-color: #94D5EB;">
                          <div class="widget-user-image ">
                            <img class="img-icon-style" src="<?php echo base_url('assets/img/dashboard/soup.png');?>" alt="User Avatar">
                          </div>
                          <!-- /.widget-user-image -->
              
                          <h5 class="widget-user-username" style="font-size: 15px !important;">Nombre de repas</h5>
                          <h3 class="widget-user-desc" style="font-size: 19px !important;"><?php echo number_format($poidsTotalDonation/0.25, 2, ',', ' '); ?> </h3>
                        </div>
                  
                      </div>

                    </div>
                    <div class="col-12 col-sm-6 col-md-3">
                      <!--div class="info-box mb-3">
                        <span class="info-box-icon bg-warning elevation-1">
                          <i class="fa fa-usd"></i>
                        </span>

                        <div class="info-box-content">
                          <span class="info-box-text">Couts du surplus </span>
                          <span class="info-box-number">
                            <?php echo number_format($qte_mga_don, 2, ',', ' ').' '; ?> MGA
                          </span>
                        </div>
                        <!- /.info-box-content ->
                      </div>
                      <!- /.info-box -->

                      <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header " style="background-color: #94D5EB;">
                          <div class="widget-user-image ">
                            <img class="img-icon-style" src="<?php echo base_url('assets/img/dashboard/analytics.png');?>" alt="User Avatar">
                          </div>
                          <!-- /.widget-user-image -->
              
                          <h5 class="widget-user-username" style="font-size: 15px !important;">Couts du surplus</h5>
                          <h3 class="widget-user-desc" style="font-size: 19px !important;" style="font-size: 18px !important;"><?php echo number_format($qte_mga_don, 2, ',', ' ').' '; ?> MGA </h3>
                        </div>
                  
                      </div> 

                    </div>

                    <div class="col-12 col-sm-6 col-md-3">
                      <!--div class="info-box mb-3">
                        <span class="info-box-icon bg-danger elevation-1">
                          <i class="fa fa-users"></i>
                        </span>

                        <div class="info-box-content">
                          <span class="info-box-text">Nombre de bénéficiaires </span>
                          <span class="info-box-number">
                            <?php echo number_format($nbTotalBeneficiaireDon, 0, ',', ' ').' '; ?> 
                          </span>
                        </div>
                        <!- /.info-box-content ->
                      </div>
                      <!- /.info-box -->

                      <div class="card card-widget widget-user-2">
                        <!-- Add the bg color to the header using any of the bg-* classes -->
                        <div class="widget-user-header " style="background-color: #94D5EB;">
                          <div class="widget-user-image ">
                            <img class="img-icon-style" src="<?php echo base_url('assets/img/dashboard/multiple_users_silhouette.png');?>" alt="User Avatar">
                          </div>
                          <!-- /.widget-user-image -->
              
                          <h5 class="widget-user-username" style="font-size: 15px !important;">Nombre bénéficiaires</h5>
                          <h3 class="widget-user-desc" style="font-size: 19px !important;"><?php echo number_format($nbTotalBeneficiaireDon, 0, ',', ' ').' '; ?></h3>
                        </div>
                  
                      </div>

                    </div>
                   
                  </div>
                  <!-- /.row -->

                  <!-- Main row -->
                  <div class="row">
                      <div class="col-lg-6">
                        <div class="card">
                          <div class="card-header no-border">
                            <div class="d-flex justify-content-between">
                              <h3 class="card-title">Quantité des produits </h3>
                              <div class="d-flex">
                                <p class="d-flex flex-column">
                                  <select id="select-year-qte-per-year" onchange="angular.element(this).scope().yearQteChange('<?php echo base_url(''); ?>g-q-p-y')" style="appearance: none; -moz-appearance: none; -webkit-appearance: none;" class="text-muted custom-select ">
                                    <?php 
                                    $currentYear = (int)date("Y");
                                    for($date=(int)date("Y"); $date>2016; $date--){
                                      if($currentYear == $date){
                                        echo '<option class="text-muted" value="'.$date.'">Cette année</option>';
                                      }
                                      else{
                                        echo '<option class="text-muted" value="'.$date.'">Année '.$date.'</option>';
                                      }
                                    } ?>
                                  </select>
                                </p>
                              </div>
                            </div>
                          </div>
                          <div class="card-body">
                            <!-- /.d-flex -->

                            <div class="position-relative mb-4" id="graph-qte-per-year">
                              <div class="lds-roller" style="display: block; margin: 0 auto;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                            </div>

                            <div class="d-flex flex-row justify-content-end">
                              <span class="mr-2">
                                <i class="fa fa-square text-gray" style="color: #F05423;"></i> Poids (Kg)
                              </span>

                              <span>
                                <i class="fa fa-square text-gray" style="color: #FEC745;"></i> Volume (<var>m<sup>3</sup></var>)
                              </span>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="col-lg-6">
                        <div class="card">
                          <div class="card-header no-border">
                            <div class="d-flex justify-content-between">
                              <h3 class="card-title">Montant des produits</h3>
                              <div class="d-flex">
                                <p class="d-flex flex-column">
                                  <select id="select-year-montant" onchange="angular.element(this).scope().yearMontantChange('<?php echo base_url(''); ?>g-m-p-y')" style="appearance: none; -moz-appearance: none; -webkit-appearance: none;" class="text-muted custom-select">
                                  <?php 
                                    $currentYear = (int)date("Y");
                                    for($date=(int)date("Y"); $date>2016; $date--){
                                      if($currentYear == $date){
                                        echo '<option class="text-muted" value="'.$date.'">Cette année</option>';
                                      }
                                      else{
                                        echo '<option class="text-muted" value="'.$date.'">L\'année '.$date.'</option>';
                                      }
                                    } ?>
                                  </select>
                                </p>
                              </div>
                            </div>
                          </div>
                          <div class="card-body">
                            <div class="d-flex">
                              <p class="d-flex flex-column">
                                <span id="montant-total" class="text-bold text-lg" style="margin-top: -22px;">0 MGA</span>
                              </p>
                            </div>
                            <!-- /.d-flex -->

                            <div class="position-relative mb-4" id="graph-montant-per-year">
                              <div class="lds-roller" style="display: block; margin: 0 auto;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                            </div>

                            <div class="d-flex flex-row justify-content-end">
                              
                            </div>
                          </div>
                        </div>
                        <!-- /.card -->
                      </div>
                    <!-- /.col -->

                      <div class="col-lg-6">
                        <div class="card">
                          <div class="card-header no-border">
                            <div class="d-flex justify-content-between">
                              <h3 class="card-title">Nombre de produit donné par : </h3>
                              <div class="d-flex">
                                <p class="d-flex flex-column">
                                  <select id="select-genre-nombre" onchange="angular.element(this).scope().getNombrePerGenre('<?php echo base_url(''); ?>g-n-p-g')" style="appearance: none; -moz-appearance: none; -webkit-appearance: none;" class="text-muted custom-select">
                                    <option value="categorie">Catégorie </option>
                                    <option value="type">Type</option>
                                  </select>
                                </p>
                              </div>
                            </div>
                          </div>
                          <div class="card-body">
                            <div class="d-flex">
                              
                            </div>
                            <!-- /.d-flex -->

                            <div class="position-relative mb-4">
                              <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-8">
                                  <div class="row">
                                    <div class="col-md-6" style="text-align: left;">0</div>
                                    <div id="nombre-total" class="col-md-6" style="text-align: right;">Max</div>
                                  </div>
                                </div>
                              </div>
                              <div id="list-genre" class="row">

                              </div>
                            </div>

                            <div class="d-flex flex-row justify-content-end">
                              
                            </div>
                          </div>
                        </div>
                        <!-- /.card -->
                      </div>
                      <div class="col-lg-6">
                        <div class="card">
                          <div class="card-header no-border">
                            <div class="d-flex justify-content-between">
                              <h3 class="card-title">Poids/Volume de produit donné par :</h3>
                              <div class="d-flex">
                                <p class="d-flex flex-column">
                                  <select id="select-genre-qte" onchange="angular.element(this).scope().getQtePerGenre('<?php echo base_url(''); ?>g-q-p-g')" style="appearance: none; -moz-appearance: none; -webkit-appearance: none;" class="text-muted custom-select">
                                    <option value="categorie">Catégorie </option>
                                    <option value="type">Type</option>
                                  </select>
                                </p>
                              </div>
                            </div>
                          </div>
                          <div class="card-body">
                            <div class="d-flex">
                              
                            </div>
                            <!-- /.d-flex -->

                            <div class="position-relative mb-4">
                              <div class="row">
                                <div class="col-md-4"></div>
                                <div class="col-md-8">
                                  <div class="row">
                                    <div class="col-md-6" style="text-align: left;">0</div>
                                    <div id="qte-total" class="col-md-6" style="text-align: right;">Max</div>
                                  </div>
                                </div>
                              </div>
                              <div id="list-qte" class="row">
                                
                              </div>
                            </div>

                            <div class="d-flex flex-row justify-content-end">
                              <span class="mr-2">
                                <i class="fa fa-square text-gray" style="color: #F05423;"></i> Poids (Kg)
                              </span>

                              <span>
                                <i class="fa fa-square text-gray" style="color: #FEC745;"></i> Volume (<var>m<sup>3</sup></var>)
                              </span>
                            </div>
                          </div>
                        </div>
                        <!-- /.card -->
                      </div>

                      <!-- TABLE: LATEST ORDERS -->
                      <div class="col-md-12">
                        <div class="card">
                          <div class="card-header border-transparent">
                            <h3 class="card-title">Donations récentes</h3>
                          </div>
                          <!-- /.card-header -->
                          <div class="card-body p-0">
                            <div class="table-responsive">
                              <table class="table m-0">
                                <thead>
                                <tr>
                                  <th>Date</th>
                                  <th>Bénéficiaire</th>
                                  <th>Produit</th>
                                  <th>Type du produit</th>
                                  <th>Quantités</th>
                                  <th>Etat</th>
                                </tr>
                                </thead>
                                <tbody id="body-list-recent-don">
                                  <?php foreach($donations as $don){ ?>
                                    <tr>
                                      <td><?php echo $don['datetime']['date'] ?><span style="font-size: 13px; opacity: 0.9;"> à <?php echo $don['datetime']['time'] ?></span></td>
                                      <td><?php echo $don['beneficiaire'] ?></td>
                                      <td><?php echo $don['produit'] ?></td>
                                      <td><?php echo $don['type'] ?></td>
                                      <td><?php echo $don['quantites'] ?></td>
                                      <td><?php echo $don['etat'] ?></td>
                                    </tr>
                                  <?php } ?>
                                </tbody>
                              </table>
                            </div>
                            <!-- /.table-responsive -->
                          </div>
                          <!-- /.card-body -->
                          <div class="card-footer clearfix">
                            <button id="btn-show-more-don" ng-click="showMoreDon('<?php echo base_url(''); ?>g-r-d')" class="btn btn-sm  float-left" style="background-color: #00AFAA; color:#fff">Afficher plus</button>
                            <div id="lds-roller-list-recent-don" class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                          </div>
                          <!-- /.card-footer -->
                        </div>
                      </div>
                      <!-- /.card -->

                  </div>
                  <!-- /.row -->


              </div>
              <!-- END TAB DONATION -->
          </div>
        </div>

      </div>

    </div>

    <div class="info-bienvenu">
      <div class="titre-bienvenu"><h1>Bienvenu sur Foodwise</h1></div>
      <div class="close-info-bienvenu" ng-click="closeInfoBienvenu()"><i class="fa fa-remove"></i></div>
       <input type="hidden" value="<?php echo (isset($_SESSION['first_time']))? $_SESSION['first_time'] : ''?>" id="test-first-time">
        <div >
            <div class="section-check-info">
              <div class="loader-default col-1" ng-if="loadType"><img src="<?php echo img_url('spin.svg'); ?>"></div>
              <input type="checkbox" id="id_checkbox_bienvenu" name="teste_bienvenu"  >
              <label for="teste_bienvenu" >Ne plus afficher cette information jusqu'à une nouvelle connexion.</label>
            </div>           
            <input type="hidden" id="base_url" value="<?php echo base_url('') ?>">
        </div>
    </div>


</div>

<div class="modal" id="modal-choix" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <form action="<?php echo base_url('admin/generateXls') ?>" method="POST">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Choisir rapport</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <input name="teste" type="hidden" value="1">

          <div class="row">
            <div class="col-md-4">
              <input id="civilite" class="radio radio-select" type="radio" name="civilite" value="monsieur" checked="" row="general" onclick="getchoixRapport(this)" />
              Général

            </div>

             <div class="col-md-4">
            
              <input class="radio radio-select" type="radio" name="civilite" value="madame" row="vente" onclick="getchoixRapport(this)"/>
               Vente

            </div>

            <div class="col-md-4">
            
              <input class="radio radio-select" type="radio" name="civilite" value="madame" row="don" onclick="getchoixRapport(this)"/>
            Don

            </div>
          </div>
          <div class="row">
            <div class="col-8">
              <label for="rapport">Choisissez le rapport</label>
              <select id="select-rapport" class="form-control input_inline select2" multiple="" data-placeholder="Selectionner rapport" name="rapport[]" style="margin-top: 16px;">
                
                  <?php foreach ($selectGlobal as $select) { ?>
                   
                  <option value="<?php echo $select['value'].'&&'.$select['name'] ?>"><?php echo $select['name']?></option>
                      
                  <?php } ?>
              </select>
            </div>
          </div> 
          <div class="row">
            
            <div class="col">
              <label for="mois">Choisissez le mois (facultatif)</label>
              <select id="select-mois" class="input_inline select2" dataholder="Choisissez le mois" name="mois" style="margin-top: 16px;">
                  <option value="">Choisissez le mois</option>
                  <option value="1">Janvier</option>
                  <option value="2">Févrie</option>
                  <option value="3">Mars</option>
                  <option value="4">Avril</option>
                  <option value="5">Mai</option>
                  <option value="6">Juin</option>
                  <option value="7">Juillet</option>
                  <option value="8">Août</option>
                  <option value="9">Septembre</option>
                  <option value="10">Octobre</option>
                  <option value="11">Novembre</option>
                  <option value="12">Décembre</option>
                 
              </select>
            </div>
            <div class="col">
              <label for="rapport">Choisissez l'année</label>
              <select  id="select-annee" style="appearance: none; -moz-appearance: none; -webkit-appearance: none;" class="text-muted custom-select select2" name="annee">
                <?php 
                $currentYear = (int)date("Y");
                for($date=(int)date("Y"); $date>2016; $date--){
                  if($currentYear == $date){
                    echo '<option class="text-muted" value="'.$date.'">Cette année</option>';
                  }
                  else{
                    echo '<option class="text-muted" value="'.$date.'">Année '.$date.'</option>';
                  }
                } ?>
              </select>
            </div>

          </div>  

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Fermer</button>
          <input type="submit" class="btn btn-primary" value="Télécharger" >
          
        </div>
      </div>
    </form>
  </div>
</div>
<script type="text/javascript">

  
function getchoixRapport(element){
    var type_role = $(element).attr('row');
    var selection = document.getElementById('select-rapport');
    if(type_role == 'general'){


    //remove option
        while (selection.firstChild) {
        selection.removeChild(selection.firstChild);
        }

        var liste_selectGlobal= <?php echo json_encode($selectGlobal) ?>;
       
        var options = liste_selectGlobal.map(function(select){return '<option value="'+select.value+'&&'+select.name+'" >'+select.name+'</option>'}).join("");

        var html = options ;    

        $("#select-rapport").append(html);

    } else if(type_role == 'vente'){


    //remove option
        while (selection.firstChild) {
        selection.removeChild(selection.firstChild);
        }


        var liste_selectVente= <?php echo json_encode($selectVente) ?>;
        
        var options = liste_selectVente.map(function(select){return '<option value="'+select.value+'&&'+select.name+'" >'+select.name+'</option>'}).join("");

        var html = options ;    

        $("#select-rapport").append(html);

    }else if(type_role == 'don'){


    //remove option
        while (selection.firstChild) {
        selection.removeChild(selection.firstChild);
        }


        var liste_selectDon= <?php echo json_encode($selectDon) ?>;
        
        var options = liste_selectDon.map(function(select){return '<option value="'+select.value+'&&'+select.name+'" >'+select.name+'</option>'}).join("");

        var html = options ;    

        $("#select-rapport").append(html);

    }
    $('#select-rapport').select2();
}

</script>


