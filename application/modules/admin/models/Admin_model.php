<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin_model extends CI_Model{

    protected $tableDonation = "donation";
    protected $tableOrganisation = "organisation";
    protected $tableProduit = "produit";
    protected $tableTypeProduit = "type_produit";
    protected $tableTypeAliment = "type_aliment";
    protected $tableEntreprise = "entreprise";
    protected $tableEtat = "etat_donation";
    protected $tableProduitCommande = "produit_commande";
    protected $tableCommande = "commande";
    protected $tableEtatCommande = "etat_commande";

    public function getFormatDate($from){
        $datetime = explode(' ', $from);
        $date = explode('-', $datetime[0]);
        $time = explode(':', $datetime[1]);
        $result = array(
            "date"=>$date[2].'/'.$date[1].'/'.$date[0],
            "time"=>$time[0].'h'.$time[1]
        );
        return $result;
    }

    public function getDonation($idEA, $limit, $offset){
        $condition = "entreprise_id =" . "'" . $idEA . "' AND action = 1 AND etat_donation_id != 5";
        $this->db->from($this->tableDonation);
        $this->db->distinct('id_donation');
        $this->db->where($condition);
        $this->db->order_by("moment", "desc");
        $this->db->limit($limit, $offset);
        $donations = $this->db->get()->result();
        $result = array();
        foreach($donations as $row){
                $object = array();
                $object['datetime'] = $this->getFormatDate($row->moment);
                $beneficiaire = $this->db->query("SELECT * FROM ".$this->tableOrganisation." WHERE id_organisation=".$row->organisation_id)->result()[0];
                $object['beneficiaire'] = $beneficiaire->nom_organisation;
                $produit = $this->db->query("SELECT * FROM ".$this->tableProduit." WHERE id_produit=".$row->produit_id_produit)->result()[0];
                $object['prix'] = $produit->prix;
                $object['produit'] = $produit->nom_produit;
                $object['quantites'] = $produit->poids*$row->quantite." Kg".(($produit->volume==0)?"":" | ".$produit->volume*$row->quantite." <var>m<sup>3</sup></var>");
                $typeProduit = $this->db->query("SELECT * FROM ".$this->tableTypeProduit." WHERE id_type_produit=".$produit->type_produit_id)->result()[0];
                $object['type'] = $typeProduit->label;
                $categorieProduit = $this->db->query("SELECT * FROM ".$this->tableTypeAliment." WHERE id_type_aliment=".$produit->type_aliment_id)->result()[0];
                $object['categorie'] = $categorieProduit->label;
                $object['etat'] = $this->db->query("SELECT * FROM ".$this->tableEtat." WHERE id_etat_donation=".$row->etat_donation_id)->result()[0]->label;
                $result[] = $object;
        }

        return $result;
    }
   
    /*public function  getVente($idEA, $limit, $offset){
        $condition = "entreprise_id =" . "'" . $idEA . "' AND action = 2 AND etat_donation_id != 5";
        $this->db->from($this->tableDonation);
        $this->db->distinct('id_donation');
        $this->db->where($condition);
        $this->db->order_by("moment", "desc");
        $this->db->limit($limit, $offset);
        $ventes = $this->db->get()->result();
        $result = array();
        foreach($ventes as $row){
                $object = array();
                $object['datetime'] = $this->getFormatDate($row->moment);
                $produit = $this->db->query("SELECT * FROM ".$this->tableProduit." WHERE id_produit=".$row->produit_id_produit)->result()[0];
                $object['produit'] = $produit->nom_produit;
                $object['prix'] = $produit->prix;
                $object['pourcentage_reduction'] = $row->pourcentage_reduction;
                $object['quantites'] = $produit->poids*$row->quantite." Kg".(($produit->volume==0)?"":" | ".$produit->volume*$row->quantite." <var>m<sup>3</sup></var>");
                $typeProduit = $this->db->query("SELECT * FROM ".$this->tableTypeProduit." WHERE id_type_produit=".$produit->type_produit_id)->result()[0];
                $object['type'] = $typeProduit->label;
                $categorieProduit = $this->db->query("SELECT * FROM ".$this->tableTypeAliment." WHERE id_type_aliment=".$produit->type_aliment_id)->result()[0];
                $object['categorie'] = $categorieProduit->label;
                $object['etat'] = $this->db->query("SELECT * FROM ".$this->tableEtat." WHERE id_etat_donation=".$row->etat_donation_id)->result()[0]->label;
                $object['id_etat'] = $row->etat_donation_id;
                $produit_commande = $this->db->query("SELECT * FROM ".$this->tableProduitCommande." pc join ".$this->tableCommande." c on c.id_commande=pc.commande_id join ".$this->tableEtatCommande." ec on ec.id_etat_commande = c.etat WHERE pc.donation_id =".$row->id_donation)->result();
                $object['produitCommande'] =  $produit_commande;
                $result[] = $object;
        }

        return $result;
    }*/

    public function getVente($idEA, $limit, $offset){
        
        $query = "SELECT * FROM ".$this->tableProduitCommande." pc join ".$this->tableCommande." c on c.id_commande = pc.commande_id join ".$this->tableEtatCommande." ec on ec.id_etat_commande = c.etat join ".$this->tableDonation." d on d.id_donation = pc.donation_id WHERE d.entreprise_id=".$idEA." AND d.action = 2 AND d.etat_donation_id != 5 AND ec.id_etat_commande = 1 ORDER BY c.date DESC LIMIT ".$offset.",".$limit;
        /*$query = "SELECT * FROM ".$this->tableProduitCommande." pc join ".$this->tableCommande." c on c.id_commande = pc.commande_id ORDER BY c.date DESC LIMIT ".$offset.",".$limit;*/

        $ventes = $this->db->query($query)->result();
        
        $result = array();
        foreach($ventes as $row){
                $object = array();
                $object['datetime'] = $this->getFormatDate($row->date);
                $produit = $this->db->query("SELECT * FROM ".$this->tableProduit." WHERE id_produit=".$row->produit_id_produit)->result()[0];
                $object['produit'] = $produit->nom_produit;
                //$object['prix'] = $produit->prix;
                $object['prix'] = $row->prix_achat_unite * $row->qte;
                $object['pourcentage_reduction'] = $row->pourcentage_reduction;
                //$object['quantites'] = $produit->poids*$row->quantite." Kg".(($produit->volume==0)?"":" | ".$produit->volume*$row->quantite." <var>m<sup>3</sup></var>");
                $object['quantites'] = $row->qte;
                $typeProduit = $this->db->query("SELECT * FROM ".$this->tableTypeProduit." WHERE id_type_produit=".$produit->type_produit_id)->result()[0];
                $object['type'] = $typeProduit->label;
                $categorieProduit = $this->db->query("SELECT * FROM ".$this->tableTypeAliment." WHERE id_type_aliment=".$produit->type_aliment_id)->result()[0];
                $object['categorie'] = $categorieProduit->label;
                $object['etat'] = $this->db->query("SELECT * FROM ".$this->tableEtat." WHERE id_etat_donation=".$row->etat_donation_id)->result()[0]->label;
                $object['id_etat'] = $row->etat_donation_id;
                $produit_commande = $this->db->query("SELECT * FROM ".$this->tableProduitCommande." pc join ".$this->tableCommande." c on c.id_commande=pc.commande_id join ".$this->tableEtatCommande." ec on ec.id_etat_commande = c.etat WHERE pc.donation_id =".$row->id_donation)->result();
                $object['produitCommande'] =  $produit_commande;
                $result[] = $object;
        }

        return $result;
    }

    public function getPoidsTotal($idEA, $action , $mois=-1 , $annee=-1){
        if($action == 2){
            $query = "SELECT sum(p.poids*pc.qte) as somme FROM ".$this->tableProduit." p join ".$this->tableDonation." d on d.produit_id_produit=p.id_produit join ".$this->tableProduitCommande."  pc on pc.donation_id = d.id_donation join ".$this->tableCommande." c on c.id_commande = pc.commande_id join ".$this->tableEtatCommande." ec on ec.id_etat_commande = c.etat WHERE d.entreprise_id=".$idEA." AND d.action = ".$action." AND d.etat_donation_id != 5 AND ec.id_etat_commande = 1"; //c.id_commande = 1 ==> Payé 
        }else{
            $query = "SELECT sum(p.poids*d.quantite) as somme FROM ".$this->tableProduit." p join ".$this->tableDonation." d on d.produit_id_produit=p.id_produit WHERE d.entreprise_id=".$idEA." AND action = ".$action." AND etat_donation_id != 5";
        }
        
        $somme = $this->db->query($query)->result()[0]->somme;
        return round($somme, 2);
    }

    public function getVolumeTotal($idEA){
        $query = "SELECT sum(p.volume*d.quantite) as somme FROM ".$this->tableProduit." p join ".$this->tableDonation." d on d.produit_id_produit=p.id_produit WHERE d.entreprise_id=".$idEA;
        $somme = $this->db->query($query)->result()[0]->somme;
        return round($somme, 2);
    }

    public function getNombreTotal($idEA, $action){
        if($action == 2){
            $query = "SELECT COUNT(*) as somme FROM (SELECT DISTINCT moment FROM ".$this->tableDonation." d join ".$this->tableProduit." p on p.id_produit=d.produit_id_produit join ".$this->tableProduitCommande."  pc on pc.donation_id = d.id_donation join ".$this->tableCommande." c on c.id_commande = pc.commande_id join ".$this->tableEtatCommande." ec on ec.id_etat_commande = c.etat WHERE d.entreprise_id=".$idEA." AND d.action = ".$action." AND d.etat_donation_id != 5 AND ec.id_etat_commande = 1) AS moments";
        }else{
            $query = "SELECT COUNT(*) as somme FROM (SELECT DISTINCT moment FROM ".$this->tableDonation." WHERE entreprise_id=".$idEA." AND action = ".$action." AND  etat_donation_id != 5) AS moments";
        }
        
        $somme = $this->db->query($query)->result()[0]->somme;
        return $somme;
    }

    public function getNbTotalBeneficiaire($idEA, $action){
        $query = "SELECT COUNT(*) as nbBenef FROM (SELECT DISTINCT organisation_id FROM ".$this->tableDonation." WHERE entreprise_id=".$idEA." AND action = ".$action." AND  etat_donation_id != 5) AS organisation";
        $nombreBenef = $this->db->query($query)->result()[0]->nbBenef;
        return $nombreBenef;
    }


    public function getQteKg($idEA){
        $this->db->from($this->tableEntreprise);
        $this->db->where('id_entreprise', $idEA);
        $entreprise = $this->db->get()->result()[0];
        return $entreprise->qte_kg;
    }

    public function getQteMga($idEA, $action){
        if($action == 2){
            $query = "SELECT DISTINCT sum(pc.qte * pc.prix_achat_unite) as montant FROM ".$this->tableProduitCommande."  pc join ".$this->tableCommande." c on c.id_commande = pc.commande_id join ".$this->tableEtatCommande." ec on ec.id_etat_commande=c.etat join ".$this->tableDonation." d on d.id_donation=pc.donation_id WHERE d.entreprise_id=".$idEA." AND d.action = ".$action." AND d.etat_donation_id != 5 AND ec.id_etat_commande = 1";
        }else{
            $query = "SELECT DISTINCT sum(don.quantite*pr.prix) as montant FROM ".$this->tableDonation." don JOIN ".$this->tableProduit." pr ON pr.id_produit=don.produit_id_produit WHERE don.entreprise_id='".$idEA."' AND don.action = '".$action."' AND etat_donation_id != 5";
        }
        
        $total = $this->db->query($query)->result()[0]->montant;
        if($total < 0 || $total == null){
            $total = 0;
        }
        return $total;
    }

    public function getQuantityPerYear($year, $idEA, $action){
        $result = array();
        if($action == 2){
            for($month=1; $month<13; $month++){
                $query = "SELECT DISTINCT sum(pc.qte*pr.poids) as poids, sum(pc.qte*pr.volume) as volumes FROM ".$this->tableDonation." don JOIN ".$this->tableProduit." pr ON pr.id_produit=don.produit_id_produit join ".$this->tableProduitCommande."  pc on pc.donation_id = don.id_donation join ".$this->tableCommande." c on c.id_commande = pc.commande_id join ".$this->tableEtatCommande." ec on ec.id_etat_commande = c.etat WHERE YEAR(c.date)=".$year." AND MONTH(c.date)=".$month." AND don.entreprise_id='".$idEA."' AND don.action = '".$action."' AND don.etat_donation_id != 5 AND ec.id_etat_commande = 1";
                $data = $this->db->query($query)->result()[0];
                $result[] = array(
                    "poids" => $data->poids,
                    "volumes" => $data->volumes
                );
            }
        }else{
            for($month=1; $month<13; $month++){
                $query = "SELECT DISTINCT sum(don.quantite*pr.poids) as poids, sum(don.quantite*pr.volume) as volumes FROM ".$this->tableDonation." don JOIN ".$this->tableProduit." pr ON pr.id_produit=don.produit_id_produit WHERE YEAR(don.moment)=".$year." AND MONTH(don.moment)=".$month." AND don.entreprise_id='".$idEA."' AND don.action = '".$action."' AND don.etat_donation_id != 5";
                $data = $this->db->query($query)->result()[0];
                $result[] = array(
                    "poids" => $data->poids,
                    "volumes" => $data->volumes
                );
            }
        }
        
        return $result;
    }

    public function getMontantPerYear($year, $idEA, $action){
        $result = array();
        $total = 0;
        if($action == 2){
            for($month=1; $month<13; $month++){
                $query = "SELECT DISTINCT sum(pc.qte * pc.prix_achat_unite) as montant FROM ".$this->tableDonation." don JOIN ".$this->tableProduitCommande." pc ON pc.donation_id=don.id_donation join ".$this->tableCommande." c on c.id_commande=pc.commande_id join ".$this->tableEtatCommande." ec on ec.id_etat_commande=c.etat WHERE YEAR(c.date)=".$year." AND MONTH(c.date)=".$month." AND don.entreprise_id='".$idEA."' AND don.action = '".$action."' AND don.etat_donation_id != 5 AND ec.id_etat_commande = 1";
                $data = $this->db->query($query)->result()[0];
                $result[] = $data->montant;
                $total += $data->montant;
            }
        }else{
            for($month=1; $month<13; $month++){
                $query = "SELECT DISTINCT sum(don.quantite*pr.prix) as montant FROM ".$this->tableDonation." don JOIN ".$this->tableProduit." pr ON pr.id_produit=don.produit_id_produit WHERE YEAR(don.moment)=".$year." AND MONTH(don.moment)=".$month." AND don.entreprise_id='".$idEA."' AND don.action = '".$action."' AND don.etat_donation_id != 5 ";
                $data = $this->db->query($query)->result()[0];
                $result[] = $data->montant;
                $total += $data->montant;
            }
        }
        
        return array(
            "list"=>$result,
            "total"=>$total
        );
    }

    public function getNombrePerGenre($genre, $idEA, $action){
        $list = array();
        $total = 0;
        $this->db->where('proprietaire', 0);
        $this->db->or_where('proprietaire', $idEA);
        if(strcmp($genre, 'categorie')==0){
            $categories = $this->db->get($this->tableTypeAliment)->result();
            if($action == 2){
                for($i=0; $i<count($categories); $i++){
                    $query = "SELECT DISTINCT SUM(pc.qte) as nombre FROM ".$this->tableDonation." d JOIN ".$this->tableProduit." p ON d.produit_id_produit=p.id_produit join ".$this->tableTypeAliment." t ON t.id_type_aliment=p.type_aliment_id join ".$this->tableProduitCommande." pc on pc.donation_id = d.id_donation join ".$this->tableCommande." c on c.id_commande=pc.commande_id join ".$this->tableEtatCommande." ec on ec.id_etat_commande=c.etat WHERE d.entreprise_id=".$idEA." AND d.action = '".$action."' AND d.etat_donation_id != 5 AND ec.id_etat_commande = 1 AND t.id_type_aliment=".$categories[$i]->id_type_aliment;
                    $nombre = 0;
                    $nombre += $this->db->query($query)->result()[0]->nombre;
                    $total += $nombre;
                    $list[] = array(
                        "label"=>$categories[$i]->label,
                        "nombre"=>$nombre
                    );
                }
            }else{
                for($i=0; $i<count($categories); $i++){
                    $query = "SELECT DISTINCT SUM(d.quantite) as nombre FROM ".$this->tableDonation." d JOIN ".$this->tableProduit." p ON d.produit_id_produit=p.id_produit JOIN ".$this->tableTypeAliment." t ON t.id_type_aliment=p.type_aliment_id WHERE d.entreprise_id=".$idEA." AND d.action = '".$action."' AND d.etat_donation_id != 5 AND t.id_type_aliment=".$categories[$i]->id_type_aliment;
                    $nombre = 0;
                    $nombre += $this->db->query($query)->result()[0]->nombre;
                    $total += $nombre;
                    $list[] = array(
                        "label"=>$categories[$i]->label,
                        "nombre"=>$nombre
                    );
                }
            }
            
        }
        else{
            $types = $this->db->get($this->tableTypeProduit)->result();
            if($action == 2){
                for($i=0; $i<count($types); $i++){
                    $query = "SELECT DISTINCT SUM(pc.qte) as nombre FROM ".$this->tableDonation." d JOIN ".$this->tableProduit." p ON d.produit_id_produit=p.id_produit JOIN ".$this->tableTypeProduit." t ON t.id_type_produit=p.type_produit_id join ".$this->tableProduitCommande." pc on pc.donation_id = d.id_donation join ".$this->tableCommande." c on c.id_commande=pc.commande_id join ".$this->tableEtatCommande." ec on ec.id_etat_commande=c.etat WHERE d.entreprise_id=".$idEA." AND d.action = '".$action."' AND d.etat_donation_id != 5 AND ec.id_etat_commande = 1 AND t.id_type_produit=".$types[$i]->id_type_produit;
                    $nombre = 0;
                    $nombre += $this->db->query($query)->result()[0]->nombre;
                    $total += $nombre;
                    $list[] = array(
                        "label"=>$types[$i]->label,
                        "nombre"=>$nombre
                    );
                }
            }else{
                for($i=0; $i<count($types); $i++){
                    $query = "SELECT DISTINCT SUM(d.quantite) as nombre FROM ".$this->tableDonation." d JOIN ".$this->tableProduit." p ON d.produit_id_produit=p.id_produit JOIN ".$this->tableTypeProduit." t ON t.id_type_produit=p.type_produit_id WHERE d.entreprise_id=".$idEA." AND d.action = '".$action."' AND d.etat_donation_id != 5 AND t.id_type_produit=".$types[$i]->id_type_produit;
                    $nombre = 0;
                    $nombre += $this->db->query($query)->result()[0]->nombre;
                    $total += $nombre;
                    $list[] = array(
                        "label"=>$types[$i]->label,
                        "nombre"=>$nombre
                    );
                }
            }
            
        }
        return array(
            "list"=>$list,
            "total"=>$total
        );
    }

    public function getQtePerGenre($genre, $idEA, $action){
        $list = array();
        $totalPoids = 0;
        $totalVolumes = 0;
        $this->db->where('proprietaire', 0);
        $this->db->or_where('proprietaire', $idEA);
        if(strcmp($genre, 'categorie')==0){
            $categories = $this->db->get($this->tableTypeAliment)->result();
            if($action == 2){
                for($i=0; $i<count($categories); $i++){
                    $query = "SELECT DISTINCT SUM(pc.qte*p.poids) AS poids, SUM(pc.qte*p.volume) AS volumes FROM ".$this->tableDonation." d JOIN ".$this->tableProduit." p ON d.produit_id_produit=p.id_produit JOIN ".$this->tableTypeAliment." t ON t.id_type_aliment=p.type_aliment_id join ".$this->tableProduitCommande." pc on pc.donation_id = d.id_donation join ".$this->tableCommande." c on c.id_commande=pc.commande_id join ".$this->tableEtatCommande." ec on ec.id_etat_commande=c.etat WHERE d.entreprise_id=".$idEA." AND d.action = '".$action."' AND d.etat_donation_id != 5 AND ec.id_etat_commande = 1 AND t.id_type_aliment=".$categories[$i]->id_type_aliment;
                    $poids = 0;
                    $volumes = 0;
                    $poids += $this->db->query($query)->result()[0]->poids;
                    $volumes += $this->db->query($query)->result()[0]->volumes;
                    $totalPoids += $poids;
                    $totalVolumes += $volumes;
                    $list[] = array(
                        "label"=>$categories[$i]->label,
                        "poids"=>$poids,
                        "volumes"=>$volumes
                    );
                }
            }else{
                for($i=0; $i<count($categories); $i++){
                    $query = "SELECT DISTINCT SUM(d.quantite*p.poids) AS poids, SUM(d.quantite*p.volume) AS volumes FROM ".$this->tableDonation." d JOIN ".$this->tableProduit." p ON d.produit_id_produit=p.id_produit JOIN ".$this->tableTypeAliment." t ON t.id_type_aliment=p.type_aliment_id WHERE d.entreprise_id=".$idEA." AND d.action = '".$action."' AND d.etat_donation_id != 5 AND t.id_type_aliment=".$categories[$i]->id_type_aliment;
                    $poids = 0;
                    $volumes = 0;
                    $poids += $this->db->query($query)->result()[0]->poids;
                    $volumes += $this->db->query($query)->result()[0]->volumes;
                    $totalPoids += $poids;
                    $totalVolumes += $volumes;
                    $list[] = array(
                        "label"=>$categories[$i]->label,
                        "poids"=>$poids,
                        "volumes"=>$volumes
                    );
                }
            }
            
        }
        else{
            $types = $this->db->get($this->tableTypeProduit)->result();
            if($action == 2){
                for($i=0; $i<count($types); $i++){
                    $query = "SELECT DISTINCT SUM(pc.qte*p.poids) AS poids, SUM(pc.qte*p.volume) AS volumes FROM ".$this->tableDonation." d JOIN ".$this->tableProduit." p ON d.produit_id_produit=p.id_produit JOIN ".$this->tableTypeProduit." t ON t.id_type_produit=p.type_produit_id join ".$this->tableProduitCommande." pc on pc.donation_id = d.id_donation join ".$this->tableCommande." c on c.id_commande=pc.commande_id join ".$this->tableEtatCommande." ec on ec.id_etat_commande=c.etat WHERE d.entreprise_id=".$idEA." AND d.action = '".$action."' AND d.etat_donation_id != 5 AND ec.id_etat_commande = 1 AND t.id_type_produit=".$types[$i]->id_type_produit;
                    $poids = 0;
                    $volumes = 0;
                    $poids += $this->db->query($query)->result()[0]->poids;
                    $volumes += $this->db->query($query)->result()[0]->volumes;
                    $totalPoids += $poids;
                    $totalVolumes += $volumes;
                    $list[] = array(
                        "label"=>$types[$i]->label,
                        "poids"=>$poids,
                        "volumes"=>$volumes
                    );
                }
            }else{
                for($i=0; $i<count($types); $i++){
                    $query = "SELECT DISTINCT SUM(d.quantite*p.poids) AS poids, SUM(d.quantite*p.volume) AS volumes FROM ".$this->tableDonation." d JOIN ".$this->tableProduit." p ON d.produit_id_produit=p.id_produit JOIN ".$this->tableTypeProduit." t ON t.id_type_produit=p.type_produit_id WHERE d.entreprise_id=".$idEA." AND d.action = '".$action."' AND d.etat_donation_id != 5 AND t.id_type_produit=".$types[$i]->id_type_produit;
                    $poids = 0;
                    $volumes = 0;
                    $poids += $this->db->query($query)->result()[0]->poids;
                    $volumes += $this->db->query($query)->result()[0]->volumes;
                    $totalPoids += $poids;
                    $totalVolumes += $volumes;
                    $list[] = array(
                        "label"=>$types[$i]->label,
                        "poids"=>$poids,
                        "volumes"=>$volumes
                    );
                }
            }
            
        }
        return array(
            "list"=>$list,
            "totalPoids"=>$totalPoids,
            "totalVolumes"=>$totalVolumes
        );
    }

    public function getNombreDonVentePerYear($year, $idEA){
        $result = array();
        $resultat = array();
        $actionDon = 1;
        $actionVente = 2;
       
        for($month=1; $month<13; $month++){
            $query1 = "SELECT COUNT(*) as nombreDon FROM (SELECT DISTINCT moment FROM ".$this->tableDonation." don WHERE YEAR(don.moment)=".$year." AND MONTH(don.moment)=".$month." AND don.entreprise_id='".$idEA."' AND don.action = '".$actionDon."' AND don.etat_donation_id != 5) AS moments1";
            $data1 = $this->db->query($query1)->result()[0]; 

            /*$query2 = "SELECT COUNT(*) as nombreVente FROM (SELECT DISTINCT moment FROM ".$this->tableDonation." don join ".$this->tableProduitCommande." pc on pc.donation_id = don.id_donation join ".$this->tableCommande." c on c.id_commande=pc.commande_id join ".$this->tableEtatCommande." ec on ec.id_etat_commande=c.etat WHERE YEAR(don.moment)=".$year." AND MONTH(don.moment)=".$month." AND don.entreprise_id='".$idEA."' AND don.action = '".$actionVente."' AND don.etat_donation_id != 5 AND ec.id_etat_commande = 1) AS moments2";
            $data2 = $this->db->query($query2)->result()[0];*/
            $query2 = "SELECT SUM(quantite) as nombreVente FROM (SELECT SUM(pc.qte) as quantite FROM ".$this->tableCommande." c join ".$this->tableProduitCommande." pc on c.id_commande=pc.commande_id join ".$this->tableEtatCommande." ec on ec.id_etat_commande=c.etat join ".$this->tableDonation." don on pc.donation_id = don.id_donation WHERE YEAR(c.date)=".$year." AND MONTH(c.date)=".$month." AND don.entreprise_id='".$idEA."' AND don.action = '".$actionVente."' AND don.etat_donation_id != 5 AND ec.id_etat_commande = 1 GROUP BY c.date) AS quantite ";

            $data2 = $this->db->query($query2)->result()[0];

            $result[] = array(
                    "don" => $data1->nombreDon,
                    "vente" => $data2->nombreVente
            );
               
        }
               
        return $result;
    }

}