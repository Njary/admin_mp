<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Don extends MY_Controller {

	private $contrat;
	private $entreprise;

	public function __construct(){
		parent::__construct();
		$this->checkPrivilege();
		$this->load->model("Don_model");
		$this->load->model("contrat/Entente_model","entente");
		$this->load->model("organisation/Organisation_model","organisation");
		$this->load->model("entreprise/Entreprise_model","entreprise_model");
		$this->load->model("Produit_model","produit");
		$this->checkContrat();

	}

	protected function checkPrivilege(){
		$idUser=$this->session->userdata("idUser");

		if(empty($idUser) || $idUser<0)
			redirect(site_url("login"));
	}

	public function checkContrat(){
		$this->entreprise = $this->entente->getEntreprise($this->session->userdata("idUser"));
		$this->contrat = $this->entente->getContrat($this->session->userdata("idEntreprise"));
	}

	public function index(){
		$this->faireUnDon();
	}

	public function faireUnDon($idDon=-1){
		$this->layout->ajouter_js('../js/shim.min');
		$this->layout->ajouter_js('../js/xlsx.full.min');
		$this->layout->ajouter_js('../js/init.element');
		$this->layout->ajouter_js('../js/don.module');
		

		$donCur = null;

		$action = 1; //Car donation
		$checkDraft = $this->produit->checkIfThereAreDraft($action);
		if( $idDon == -1 && count($checkDraft)>0 ){
			$idDon = $checkDraft[0]->id_donation;
		}


		if($idDon!=-1){
			$donCur = $this->produit->getInfoProduitById($idDon,1);
			$donCur = $donCur[0];

			$id_produit = $donCur->id_produit;

			//Information nutritionnelle
			$info_nutris =  $this->produit->getProduitInfoNutris($id_produit);
			$donCur->info_nutris = $info_nutris;
			
			//Raison surplus
			$raison_surplus = $this->produit->getProduitRaisonSurplus($idDon);
			$donCur->raisonSurplus = $raison_surplus;
			
			//??Necessite de refrigeration ou congelation
			$equipement = $this->produit->getEquipementNecessaire($id_produit);
			if(count($equipement) == 3){
				$congele = $equipement[0]->etat;
				$refrigere = $equipement[1]->etat;
				$consoDirect = $equipement[2]->etat;
			}else{
				$congele = 0;
				$refrigere = 0;
				$consoDirect = 0;
			}
			$donCur->refrigere = $refrigere;
			$donCur->congele = $congele;
			$donCur->consoDirect = $consoDirect;

			$photo = $this->produit->getProductPictures($id_produit,$this->entreprise['id_entreprise']);
			$donCur->photo = $photo;
			
		}

		$profilOk = $this->Don_model->canMakeDonation($this->entreprise['id_entreprise']);
		$protocoleOk = true;
		if($profilOk){
			if(!$this->contrat){
				$protocoleOk = false;
			}
		}

		/*var_dump($donCur);
		die();*/
		
		$this->action("Donation");
		$types = $this->Don_model->getTypes($this->entreprise['id_entreprise']);
		$categories = $this->Don_model->getCategories($this->entreprise['id_entreprise']);
		$infoNutris = $this->Don_model->getInfoNutris($this->entreprise['id_entreprise']);
		$raisonSurplus = $this->Don_model->getRaisonSurplus($this->entreprise['id_entreprise']);
		$this->layout->addView(
			"don", 
			array(
				'categories'=>$categories,
				'types'=>$types, 
				'infoNutris'=>$infoNutris,
				'raisonSurplus'=>$raisonSurplus,
				'profilOk'=>$profilOk,
				'protocoleOk'=>$protocoleOk,
				'contrat'=>$this->contrat,
				'donModif'=>$donCur
			)
		);

		$this->layout->view();
	}


	public function canMakeDonation(){
		$result = array(
			"canMakeDonation"=>$this->Don_model->canMakeDonation($this->entreprise['id_entreprise'])
		);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function dashboard(){
		$this->layout->addView("dashboard");
		$this->layout->view();
	}

	public function addNewCategorie(){
		$categorie = trim($this->input->post('categorie'));
		$infoNutris = trim($this->input->post('infoNutris'));
		$result = array(
			"label"=>$categorie,
			"id_type_aliment"=>0,
			"message"=>"",
			"success"=>false
		);
		if(!empty($categorie) && isset($categorie)){
			
			$exist = $this->Don_model->addNewCategorie($categorie, $this->session->userdata("idEntreprise"));
			if(!$exist['exist']){
				$result['success'] = true;
				$result['id_type_aliment'] = $exist['id_type_aliment'];
				if(!empty($infoNutris) && isset($infoNutris)){
					$this->Don_model->linkToInfoNutris($exist['id_type_aliment'], $infoNutris);
				}
			}
			else{
				$result['message'] = "Cette catégorie de produit existe déjà";
			}
			
		}
		else{
			$result['message'] = "La catégorie du produit est vide";
		}

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	/*public function addNewCategorie(){
		$categorie = trim($this->input->post('categorie'));
		$infoNutris = trim($this->input->post('infoNutris'));
		$result = array(
			"label"=>$categorie,
			"id_type_aliment"=>0,
			"message"=>"",
			"success"=>false
		);
		if(!empty($categorie) && isset($categorie)){
			if(!empty($infoNutris) && isset($infoNutris)){
				$exist = $this->Don_model->addNewCategorie($categorie, $this->session->userdata("idEntreprise"));
				if(!$exist['exist']){
					$result['success'] = true;
					$result['id_type_aliment'] = $exist['id_type_aliment'];
					$this->Don_model->linkToInfoNutris($exist['id_type_aliment'], $infoNutris);
				}
				else{
					$result['message'] = "Cette catégorie de produit existe déjà";
				}
			}
			else{
				$result['message'] = "Vous devez préciser l'information nutritionnelle correspondant à la catégorie";
			}
		}
		else{
			$result['message'] = "La catégorie du produit est vide";
		}

		header('Content-Type: application/json');
		echo json_encode($result);
	}*/

	public function addNewType(){
		$type = trim($this->input->post('type'));
		$result = array(
			"label"=>$type,
			"id_type_produit"=>0,
			"message"=>"",
			"success"=>false
		);
		if(!empty($type) && isset($type)){
			$exist = $this->Don_model->addNewType($type, $this->session->userdata("idEntreprise"));
			if(!$exist['exist']){
				$result['success'] = true;
				$result['id_type_produit'] = $exist['id_type_produit'];
			}
			else{
				$result['message'] = "Le type de produit existe déjà";
			}
		}
		else{
			$result['message'] = "Le type de produit est vide";
		}

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function addNewInfoNutris(){
		$infoNutris = trim($this->input->post('infoNutris'));
		$result = array(
			"label"=>$infoNutris,
			"id_information_nutritionnelle"=>0,
			"message"=>"",
			"success"=>false
		);
		if(!empty($infoNutris) && isset($infoNutris)){
			$exist = $this->Don_model->addNewInfoNutris($infoNutris, $this->session->userdata("idEntreprise"));
			if(!$exist['exist']){
				$result['success'] = true;
				$result['id_information_nutritionnelle'] = $exist['id_information_nutritionnelle'];
			}
			else{
				$result['message'] = "Cette information existe déjà";
			}
		}
		else{
			$result['message'] = "Cette information est invalide";
		}
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function addNewRaisonSurplus(){
		$raisonSurplus = trim($this->input->post('raisonSurplus'));
		$result = array(
			"label"=>$raisonSurplus,
			"id_raison_surplus"=>0,
			"message"=>"",
			"success"=>false
		);
		if(!empty($raisonSurplus) && isset($raisonSurplus)){
			$exist = $this->Don_model->addNewRaisonSurplus($raisonSurplus, $this->session->userdata("idEntreprise"));
			if(!$exist['exist']){
				$result['success'] = true;
				$result['id_raison_surplus'] = $exist['id_raison_surplus'];
			}
			else{
				$result['message'] = "Cette raison de surplus existe déjà";
			}
		}
		else{
			$result['message'] = "Cette raison de surplus est invalide";
		}
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function getInfoNutrisLinked(){
		$idCategorie = $this->input->post('idCategorie');
		header('Content-Type: application/json');
		echo json_encode($this->Don_model->getInfoNutrisLinked($idCategorie));
	}

	public function saveImage($dataURL, $ext, $idProduit, $index){
		$result = array('exist'=>false,'filepath'=>'', 'id_photo_produit'=>-1);
		$imgData = str_replace(' ','+', $dataURL);
		$imgData =  substr($imgData, strpos($imgData,",")+1);
		$imgDataMIME = base64_decode($imgData);
		$dateSave = Date('YmdHis');
		$filePath = 'assets/img/produits/produit_'.$idProduit.'_'.$index.'_'.$dateSave.'.'.$ext;
		// teste si data est deja dans disque
		$dataInDisc = $this->Don_model->getPhotoPoduits('', $this->session->userdata("idEntreprise"));
		for($i=0; $i<count($dataInDisc); $i++){
			if(file_exists($dataInDisc[$i]['url'])){
				$data = base64_encode(file_get_contents($dataInDisc[$i]['url']));
				if(strcmp($imgData, $data)==0){
					$result['exist'] = true;
					$result['id_photo_produit'] = $dataInDisc[$i]['id_photo_produit'];
					break;
				}
			}
		}
		if(!$result['exist']){
			$result['filepath'] = $filePath;
			file_put_contents($filePath, $imgDataMIME);
		}

		return $result;
	}

	public function getPhotoPoduits(){
		$nom = $this->input->post('nom');
		header('Content-Type: application/json');
		echo json_encode($this->Don_model->getPhotoPoduits($nom, $this->session->userdata("idEntreprise")));
	}

	public function parseDateFrToSql($date){
		if(empty($date)){
			return null;
		}
		else{
			$strs = explode("/", $date);
			if(count($strs) != 3){
				return null;
			}
			else{
				if(((int) $strs[2]) < 2018){
					return null;
				}
				else{
					return $strs[2]."-".$strs[1]."-".$strs[0];
				}
			}
		}
	}

	public function pushFormDonation(){
		$result = array(
			'success'=>true,
			'message'=>''
		);

		$statutDonModif = (int)$this->input->post('statutDonModif');
		$idProduit = (int)$this->input->post('idProduit');
		$idDonation = (int)$this->input->post('idDonation');
		$idEtatDonation = (int)$this->input->post('idEtatDonation');

		$moment = $this->input->post('pushDate');
		$nom = $this->input->post('nom');
		$typeDate = is_numeric($this->input->post('typeDate')) ? (int) $this->input->post('typeDate') : 0;
		$datePeremption = $this->parseDateFrToSql($this->input->post('date'));
		$categorie = is_numeric($this->input->post('categorie')) ? (int) $this->input->post('categorie') : -1;
		$type = is_numeric($this->input->post('type')) ? (int) $this->input->post('type') : -1;
		$groupement = is_numeric($this->input->post('groupement')) ? (int) $this->input->post('groupement') : 0;
		$poids = is_numeric($this->input->post('poids')) ? (float) $this->input->post('poids') : 0;
		$volume = is_numeric($this->input->post('volume')) ? (float) $this->input->post('volume') : 0;
		$prix = is_numeric($this->input->post('prix')) ? (float) $this->input->post('prix') : 0;
		$quantite = is_numeric($this->input->post('quantite')) ? (int) $this->input->post('quantite') : 0;
		$refrigere = is_numeric($this->input->post('refrigere')) ? (int) $this->input->post('refrigere') : -1;
		$congele = is_numeric($this->input->post('congele')) ? (int) $this->input->post('congele') : -1;
		$consoDirect = is_numeric($this->input->post('consoDirect')) ? (int) $this->input->post('consoDirect') : -1;
		$description = $this->input->post('description');
		$NbInfoNutris = is_numeric($this->input->post('NbInfoNutris')) ? (int) $this->input->post('NbInfoNutris') : -1;
		$infoNutris = array();
		for($i=0; $i<$NbInfoNutris; $i++){
			$infoNutris[] = is_numeric($this->input->post('infoNutris'.$i)) ? (int) $this->input->post('infoNutris'.$i) : -1;
		}
		$NbRaisonSurplus = is_numeric($this->input->post('NbRaisonSurplus')) ? (int) $this->input->post('NbRaisonSurplus') : -1;
		$raisonSurplus = array();
		for($i=0; $i<$NbRaisonSurplus; $i++){
			$raisonSurplus[] = is_numeric($this->input->post('raisonSurplus'.$i)) ? (int) $this->input->post('raisonSurplus'.$i) : -1;
		}

		$NbPhotoAlreadyInBase = is_numeric($this->input->post('NbPhotoAlreadyInBase')) ? (int) $this->input->post('NbPhotoAlreadyInBase') : -1;
		$pictures = array();
		for($i=0; $i<$NbPhotoAlreadyInBase; $i++){
			$pictures[] = is_numeric($this->input->post('pictures'.$i)) ? (int) $this->input->post('pictures'.$i) : -1;
		}

		$NbImages = is_numeric($this->input->post('NbImages')) ? (int) $this->input->post('NbImages') : -1;
		$images = array();
		for($i=0; $i<$NbImages; $i++){
			$fromServer = (int) $this->input->post('fromServer'.$i);
			if($fromServer == 1){
				$images[] = array(
					"fromServer" => $fromServer,
					"idPhoto" => $this->input->post('idPhoto'.$i)
				);
			}
			else{
				$images[] = array(
					"fromServer" => 0,
					'ext'=>$this->input->post('image'.$i.'ext'),
					'dataURL'=>$this->input->post('image'.$i.'data')
				);
			}
		}

		if(empty($nom)){
			$result['success'] = false;
			$result['message'] = 'Le nom du produit est vide';
		}
		if($result['success'] && $categorie < 0){
			$result['success'] = false;
			$result['message'] = 'La catégorie du produit est invalide';
		}
		if($result['success'] && $type < 0){
			$result['success'] = false;
			$result['message'] = 'Le type du produit est invalide';
		}
		if($result['success'] && $poids <= 0){
			$result['success'] = false;
			$result['message'] = 'Le poids du produit est invalide';
		}
		if($result['success'] && $prix <= 0){
			$result['success'] = false;
			$result['message'] = 'Le prix du produit est invalide';
		}
		if($result['success'] && $quantite <= 0){
			$result['success'] = false;
			$result['message'] = 'La quantité du produit est invalide';
		}
		if($result['success'] && $datePeremption == null){
			$result['success'] = false;
			$result['message'] = 'La date de péremption est invalide';
		}
		if($result['success'] && !($refrigere == 0 || $refrigere == 1)){
			$result['success'] = false;
			$result['message'] = 'Valeur invalide';
		}
		if($result['success'] && !($congele == 0 || $congele == 1)){
			$result['success'] = false;
			$result['message'] = 'Valeur invalide';
		}
		if($result['success'] && !($consoDirect == 0 || $consoDirect == 1)){
			$result['success'] = false;
			$result['message'] = 'Valeur invalide';
		}
		/*if($result['success']){
			for($i=0; $i<$NbInfoNutris; $i++){
				if($infoNutris[$i] < 0){
					$result['success'] = false;
					$result['message'] = 'Aucune information nutritionnelle n\'a été précisée pour le produit';
					break;
				}
			}
		}*/
		/*if($result['success']){
			for($i=0; $i<$NbRaisonSurplus; $i++){
				if($raisonSurplus[$i] < 0){
					$result['success'] = false;
					$result['message'] = 'Aucune raison de surplus n\'a été précisée pour le produit';
					break;
				}
			}
		}*/

		$isDraft = is_numeric($this->input->post('isDraft')) ? (int) $this->input->post('isDraft') : 0;
		/*if($result['success'] && $NbImages <= 0){
			$result['success'] = false;
			$result['message'] = 'Il faut au moins une photo pour le produit';
		}*/

		if($result['success']){

			if($statutDonModif == 0){
			
				//recherche bénéficiaire approprié
				$produitProperty = (object) array("infoNutri"=>$infoNutris,"equipement"=>array(2=>(int)$refrigere,1=>(int)$congele));
				$organisation_id = $this->findBeneficiaire($produitProperty);

				if($organisation_id==0){
					$result['success'] = false;
					$result['message'] = 'Impossible d\'enregistrer la donation. Aucun bénéficiare n\'a encore été enregistré dans la base de donnée de Manzer Partazer';
				}else{
					//notification transporteur
					$data = array(
						"categorie"=>$categorie,
						"type"=>$type,
						"poidsTotal"=>$poids*$quantite,
						"volume"=>$volume
					);

					// if($this->sendMailToTransporter($this->entreprise['id_entreprise'],$data)==true){
						$idProduit = $this->Don_model->saveProduit($nom, $categorie, $type, $description, $infoNutris, $poids, $volume, $prix, $refrigere, $congele, $consoDirect, $groupement);
						$this->Don_model->pushDonation($this->entreprise['id_entreprise'],$organisation_id, $idProduit, $quantite, $typeDate, $datePeremption, $moment, $raisonSurplus, $isDraft);
						for($i=0; $i<$NbImages; $i++){
							if($images[$i]['fromServer'] == 0){
								$urlData = $this->saveImage($images[$i]['dataURL'], $images[$i]['ext'], $idProduit, $i);
								if($urlData['exist']){
									$this->Don_model->savePhotoProduit($idProduit, null, $urlData['id_photo_produit'], $this->entreprise['id_entreprise']);
								}
								else{
									$this->Don_model->savePhotoProduit($idProduit, $urlData['filepath'], null, $this->entreprise['id_entreprise']);
								}
							}
							else{
								$this->Don_model->savePhotoProduit($idProduit, null, $images[$i]['idPhoto'], $this->entreprise['id_entreprise']);
							}
						}
						$result['message'] = 'Enregistrement terminé';
					// }else{
					// 	$result['message'] = 'Impossible d\'enregistrer la donation. Le responsable du transport n\'a pas pu être contacter';
					// }
				}


			}else if($statutDonModif == 1){

				//recherche bénéficiaire approprié
				$produitProperty = (object) array("infoNutri"=>$infoNutris,"equipement"=>array(2=>(int)$refrigere,1=>(int)$congele));
				$organisation_id = $this->findBeneficiaire($produitProperty);

				if($organisation_id==0){
					$result['success'] = false;
					$result['message'] = 'Impossible d\'enregistrer la donation. Aucun bénéficiare n\'a encore été enregistré dans la base de donnée de Manzer Partazer';
				}else{
					//notification transporteur
					$data = array(
						"categorie"=>$categorie,
						"type"=>$type,
						"poidsTotal"=>$poids*$quantite,
						"volume"=>$volume
					);

					// if($this->sendMailToTransporter($this->entreprise['id_entreprise'],$data)==true){
						$this->Don_model->updateProduit($idProduit, $nom, $categorie, $type, $description, $infoNutris, $poids, $volume, $prix, $refrigere, $congele, $consoDirect, $groupement);
						$this->Don_model->pushUpdateDonation($idDonation, $this->entreprise['id_entreprise'],$organisation_id, $idProduit, $quantite, $typeDate,$datePeremption, $moment, $raisonSurplus, $isDraft);

						$this->Don_model->DeletePhotoProduit($idProduit, $pictures);

						for($i=0; $i<$NbImages; $i++){
							if($images[$i]['fromServer'] == 0){
								$urlData = $this->saveImage($images[$i]['dataURL'], $images[$i]['ext'], $idProduit, $i);
								if($urlData['exist']){
									$this->Don_model->updatePhotoProduit($idProduit, null, $urlData['id_photo_produit'], $this->entreprise['id_entreprise']);
								}
								else{
									$this->Don_model->updatePhotoProduit($idProduit, $urlData['filepath'], null, $this->entreprise['id_entreprise']);
								}
							}
							else{
								$this->Don_model->updatePhotoProduit($idProduit, null, $images[$i]['idPhoto'], $this->entreprise['id_entreprise']);
							}
						}
						$result['message'] = 'Enregistrement terminé';
					// }else{
					// 	$result['message'] = 'Impossible d\'enregistrer la donation. Le responsable du transport n\'a pas pu être contacter';
					// }
				}
			}else{
				$result['message'] = "Erreur d'enregistrement";
			}
		}

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function saveDonAsDraft(){
		$result = array(
			'success'=>true,
			'message'=>''
		);
		
		$moment = $this->input->post('pushDate');
		$nom = $this->input->post('nom');
		$typeDate = $this->input->post('typeDate');
		$datePeremption = $this->parseDateFrToSql($this->input->post('date'));
		$groupement = $this->input->post('groupement');
		$poids = $this->input->post('poids');
		$volume = $this->input->post('volume');
		$prix = $this->input->post('prix');
		$quantite = $this->input->post('quantite');
		$description = $this->input->post('description');


		$refrigere = is_numeric($this->input->post('refrigere')) ? (int) $this->input->post('refrigere') : null;
		$congele = is_numeric($this->input->post('congele')) ? (int) $this->input->post('congele') : null;
		$consoDirect = is_numeric($this->input->post('consoDirect')) ? (int) $this->input->post('consoDirect') : null;
		
		$categorie = is_numeric($this->input->post('categorie')) ? (int) $this->input->post('categorie') : null;
		$type = is_numeric($this->input->post('type')) ? (int) $this->input->post('type') : null;


		$NbInfoNutris = is_numeric($this->input->post('NbInfoNutris')) ? (int) $this->input->post('NbInfoNutris') : -1;
		$infoNutris = array();
		for($i=0; $i<$NbInfoNutris; $i++){
			$infoNutris[] = is_numeric($this->input->post('infoNutris'.$i)) ? (int) $this->input->post('infoNutris'.$i) : -1;
		}
		$NbRaisonSurplus = is_numeric($this->input->post('NbRaisonSurplus')) ? (int) $this->input->post('NbRaisonSurplus') : -1;
		$raisonSurplus = array();
		for($i=0; $i<$NbRaisonSurplus; $i++){
			$raisonSurplus[] = is_numeric($this->input->post('raisonSurplus'.$i)) ? (int) $this->input->post('raisonSurplus'.$i) : null;
		}

		$NbPhotoAlreadyInBase = is_numeric($this->input->post('NbPhotoAlreadyInBase')) ? (int) $this->input->post('NbPhotoAlreadyInBase') : -1;
		$pictures = array();
		for($i=0; $i<$NbPhotoAlreadyInBase; $i++){
			$pictures[] = is_numeric($this->input->post('pictures'.$i)) ? (int) $this->input->post('pictures'.$i) : -1;
		}

		$NbImages = is_numeric($this->input->post('NbImages')) ? (int) $this->input->post('NbImages') : -1;
		$images = array();
		for($i=0; $i<$NbImages; $i++){
			$fromServer = (int) $this->input->post('fromServer'.$i);
			if($fromServer == 1){
				$images[] = array(
					"fromServer" => $fromServer,
					"idPhoto" => $this->input->post('idPhoto'.$i)
				);
			}
			else{
				$images[] = array(
					"fromServer" => 0,
					'ext'=>$this->input->post('image'.$i.'ext'),
					'dataURL'=>$this->input->post('image'.$i.'data')
				);
			}
		}

		$isDraft = is_numeric($this->input->post('isDraft')) ? (int) $this->input->post('isDraft') : 0;
		
		$action = 1; // 2 car don
		$checkDraft = $this->produit->checkIfThereAreDraft($action);


		if($result['success']){

			if( count($checkDraft)>0 ){

				$idDonation = $checkDraft[0]->id_donation;
				$idProduit = $checkDraft[0]->produit_id_produit;

				//recherche bénéficiaire approprié
				$produitProperty = (object) array("infoNutri"=>$infoNutris,"equipement"=>array(2=>(int)$refrigere,1=>(int)$congele));
				$organisation_id = $this->findBeneficiaire($produitProperty);

				if($organisation_id==0){
					$result['success'] = false;
					$result['message'] = 'Impossible d\'enregistrer la donation. Aucun bénéficiare n\'a encore été enregistré dans la base de donnée de Manzer Partazer';
				}else{
					//notification transporteur
					$data = array(
						"categorie"=>$categorie,
						"type"=>$type,
						"poidsTotal"=>$poids*$quantite,
						"volume"=>$volume
					);

					// if($this->sendMailToTransporter($this->entreprise['id_entreprise'],$data)==true){
						$this->Don_model->updateProduit($idProduit, $nom, $categorie, $type, $description, $infoNutris, $poids, $volume, $prix, $refrigere, $congele, $consoDirect, $groupement);
						$this->Don_model->pushUpdateDonation($idDonation, $this->entreprise['id_entreprise'],$organisation_id, $idProduit, $quantite, $typeDate ,$datePeremption, $moment, $raisonSurplus, $isDraft);

						$this->Don_model->DeletePhotoProduit($idProduit, $pictures);

						for($i=0; $i<$NbImages; $i++){
							if($images[$i]['fromServer'] == 0){
								$urlData = $this->saveImage($images[$i]['dataURL'], $images[$i]['ext'], $idProduit, $i);
								if($urlData['exist']){
									$this->Don_model->updatePhotoProduit($idProduit, null, $urlData['id_photo_produit'], $this->entreprise['id_entreprise']);
								}
								else{
									$this->Don_model->updatePhotoProduit($idProduit, $urlData['filepath'], null, $this->entreprise['id_entreprise']);
								}
							}
							else{
								$this->Don_model->updatePhotoProduit($idProduit, null, $images[$i]['idPhoto'], $this->entreprise['id_entreprise']);
							}
						}
						$result['message'] = 'Enregistrement terminé';
					// }else{
					// 	$result['message'] = 'Impossible d\'enregistrer la donation. Le responsable du transport n\'a pas pu être contacter';
					// }
				}


			}else{

				//recherche bénéficiaire approprié
				$produitProperty = (object) array("infoNutri"=>$infoNutris,"equipement"=>array(2=>(int)$refrigere,1=>(int)$congele));
				$organisation_id = $this->findBeneficiaire($produitProperty);

				if($organisation_id==0){
					$result['success'] = false;
					$result['message'] = 'Impossible d\'enregistrer la donation. Aucun bénéficiare n\'a encore été enregistré dans la base de donnée de Manzer Partazer';
				}else{
					//notification transporteur
					$data = array(
						"categorie"=>$categorie,
						"type"=>$type,
						"poidsTotal"=>$poids*$quantite,
						"volume"=>$volume
					);

					// if($this->sendMailToTransporter($this->entreprise['id_entreprise'],$data)==true){
						$idProduit = $this->Don_model->saveProduit($nom, $categorie, $type, $description, $infoNutris, $poids, $volume, $prix, $refrigere, $congele, $consoDirect, $groupement);
						$this->Don_model->pushDonation($this->entreprise['id_entreprise'],$organisation_id, $idProduit, $quantite, $typeDate, $datePeremption, $moment, $raisonSurplus, $isDraft);
						for($i=0; $i<$NbImages; $i++){
							if($images[$i]['fromServer'] == 0){
								$urlData = $this->saveImage($images[$i]['dataURL'], $images[$i]['ext'], $idProduit, $i);
								if($urlData['exist']){
									$this->Don_model->savePhotoProduit($idProduit, null, $urlData['id_photo_produit'], $this->entreprise['id_entreprise']);
								}
								else{
									$this->Don_model->savePhotoProduit($idProduit, $urlData['filepath'], null, $this->entreprise['id_entreprise']);
								}
							}
							else{
								$this->Don_model->savePhotoProduit($idProduit, null, $images[$i]['idPhoto'], $this->entreprise['id_entreprise']);
							}
						}
						$result['message'] = 'Enregistrement terminé';
					// }else{
					// 	$result['message'] = 'Impossible d\'enregistrer la donation. Le responsable du transport n\'a pas pu être contacter';
					// }
				}
			}
			
		}

		header('Content-Type: application/json');
		echo json_encode($result);
	}


	public function import(){
		$produitAdded = array();
		$result = array(
			'success' => true,
			'message' => '',
			'list' => array()
		);
		$moment = $this->input->post('moment');
		$nbRow = is_numeric($this->input->post('nbRow')) ? (int) $this->input->post('nbRow') : -1;
		for($i=0; $i<$nbRow; $i++){
			$nom = trim($this->input->post('nom'.$i));
			$categorie = trim($this->input->post('categorie'.$i));
			$type = trim($this->input->post('type'.$i));
			$datePeremption = trim($this->input->post('datePeremption'.$i));
			$groupement = $this->input->post('groupement'.$i);
			if(strcmp($groupement, 'Par unité') == 0){
				$groupement = 1;
			}
			else{
				$groupement = 2;
			}
			$volume = is_numeric($this->input->post('volume'.$i)) ? (float) $this->input->post('volume'.$i) : 0;
			$poids = is_numeric($this->input->post('poids'.$i)) ? (float) $this->input->post('poids'.$i) : 0;
			$prix = is_numeric($this->input->post('prix'.$i)) ? (float) $this->input->post('prix'.$i) : 0;
			$quantite = is_numeric($this->input->post('quantite'.$i)) ? (float) $this->input->post('quantite'.$i) : 0;
			$description = $this->input->post('description'.$i);
			if(!isset($description) || empty($description)){
				$description = '';
			}
			else{
				$description = trim($description);
			}
			$infoNutris = $this->input->post('infoNutris'.$i);
			$raisonSurplus = $this->input->post('raisonSurplus'.$i);
			$refrigere = $this->input->post('refrigere'.$i);
			$congele = $this->input->post('congele'.$i);

			if(strlen($nom) > 0 && strlen($type) > 0 && strlen($infoNutris) > 0 && strlen($categorie)){
				if($prix > 0 && $poids > 0 && $quantite > 0){
					if(strcasecmp($refrigere, 'oui') == 0 || strcasecmp($refrigere, 'non') == 0){
						if(strcasecmp($congele, 'oui') == 0 || strcasecmp($congele, 'non') == 0){
							$infoNutris = explode('|', $infoNutris);
							$raisonSurplus = explode('|', $raisonSurplus);
							if(count($infoNutris) > 0 && count($raisonSurplus) > 0){

								//recherche bénéficiaire approprié
								$produitProperty = (object) array("infoNutri"=>$infoNutris,"equipement"=>array(2=>(int)$refrigere,1=>(int)$congele));
								$organisation_id = $this->findBeneficiaire($produitProperty);

								if($organisation_id==0){
									$result['success'] = false;
									$result['message'] = 'Impossible d\'enregistrer la donation. Aucun bénéficiare n\'a encore été enregistré dans la base de donnée de Manzer Partazer';
									break;
								}else{
									$added = $this->Don_model->saveProduitImport($nom, $categorie, $type, $description, $infoNutris, $poids, $volume, $prix, $refrigere, $congele, $this->entreprise['id_entreprise'], $groupement);
									$this->Don_model->saveDonationImport($this->entreprise['id_entreprise'], $organisation_id, $added['id_produit'], $quantite, $datePeremption, $moment, $raisonSurplus);
									$produitAdded[] = $added;
								}
							}
						}
					}
				}
			}
		}

		if(count($produitAdded) > 0){
			$result['list'] = $produitAdded;
			$result['message'] = "Voici les enrégistrements effectués, maintenant vous pouvez définir les photos correspondants";
		}
		else{
			$result['success'] = false;
			$result['message'] = "Aucune ligne n'a été ajouté car certaines valeurs sont invalides";
		}

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function importExcelPhoto(){
		$idProduit = $this->input->post('id_produit');
		$nbImage = $this->input->post('nbImage');
		for($i=0; $i<$nbImage; $i++){
			$fromServer = (int) $this->input->post('fromServer'.$i);
			if($fromServer == 1){
				$this->Don_model->savePhotoProduit($idProduit, null, $this->input->post('idPhoto'.$i), $this->entreprise['id_entreprise']);
			}
			else{
				$ext = $this->input->post('image'.$i.'ext');
				$dataURL = $this->input->post('image'.$i.'data');
				$urlData = $this->saveImage($dataURL, $ext, $idProduit, $i);
				if(!$urlData['exist']){
					$this->Don_model->savePhotoProduit($idProduit, $urlData['filepath'], null, $this->entreprise['id_entreprise']);
				}
				else{
					$this->Don_model->savePhotoProduit($idProduit, null, $urlData['id_photo_produit'], $this->entreprise['id_entreprise']);
				}
			}
		}
	}

	public function findBeneficiaire($produitProperty){
		$organisations = array();

		$coef = 1;

		$result = 0;

		//Information nutritionnelle
		$nombreInfoNutris = count($produitProperty->infoNutri);

		$infoNutriValid = array();

		$checkIfInfoNutrisIsValid = true;
		for($i=0; $i<$nombreInfoNutris; $i++){
			if($produitProperty->infoNutri[$i] > 0){
				$infoNutriValid[] = $produitProperty->infoNutri[$i];
			}
		}

		if(empty($infoNutriValid)){
			//echo "tatp";$
			$allNutri = $this->Don_model->getAllInfoNutris();
			foreach($allNutri as $key => $value){
				$infoNutriValid[] = $value->id_information_nutritionnelle;
			}
		}

		/*var_dump($infoNutriValid);
		die();*/
		// $produitProperty = (object) array("infoNutri"=>array(1,4),"equipement"=>array(2=>1,1=>0));

		$organisation_infoNutri = $this->organisation->getOrganisationsInfoNutri();
		$organisation_equipement = $this->organisation->getOrganisationsEquipement();

		foreach ($organisation_infoNutri as $value) {
			$organisations[$value->id_organisation]["infoNutri"][]=(int)$value->information_nutritionnelle_id;
		}

		foreach ($organisation_equipement as $value) {
			$organisations[$value->id_organisation]["equipement"][$value->equipement_id]=(int)$value->etat;
		}

		foreach ($organisations as $key=>$organisation) {
			$organisations[$key]["score"]=0;
		}

		if(count($organisations)>0){

			//calcul score infoNutri avec un coefficient*2
			/*foreach ($produitProperty->infoNutri as $key_i => $infoNutri)*/
			
			foreach ($infoNutriValid as $key_i => $infoNutri) {
				foreach ($organisations as $key_o => $organisation) {
					if(in_array($infoNutri,$organisation["infoNutri"])){
						$organisations[$key_o]["score"]+=$coef*2;
					}
				}
			}

			//calcul score equipement
			foreach ($produitProperty->equipement as $key_e => $etat_equipement) {
				foreach ($organisations as $key_o => $organisation) {
					if($organisation["equipement"][$key_e]==$etat_equipement){
						$organisations[$key_o]["score"]+=$coef;
					}
				}
			}

			//quel est le scrore maximal
			$max = 0;
			foreach ($organisations as $organisation) {
				$max = ($organisation["score"]>$max)?$organisation["score"]:$max; 
			}

			// echo "MAX : ".$max."\n";

			$organisationEligible = array();

			foreach ($organisations as $key=>$organisation) {
				if($organisation["score"]==$max){
					$val = array();
					$val["id"]=$key;
					$val["detail"]=$organisation;
					$organisationEligible[]=$val;
				}
			}

			if(count($organisationEligible)>0){
				//séléction aléatoire parmi les organisations éligible
				$result = $organisationEligible[rand(0,count($organisationEligible)-1)]["id"];

				/*echo "elu : ".$result."\n";

				var_dump($organisationEligible);

				var_dump($organisations);*/
			}
		}

		return $result;
	}

	public function sendMailToTransporter($entreprise_id,$data){
		$transporteur = $this->entreprise_model->getEmployeEntreprieByPosition($entreprise_id,2);
		return sendMailToDHL($entreprise_id,$transporteur[0]->email,$data);
	}
}