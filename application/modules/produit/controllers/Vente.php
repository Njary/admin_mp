<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vente extends MY_Controller {

	private $contrat;
	private $entreprise;

	public function __construct(){
		parent::__construct();
		$this->checkPrivilege();
		$this->load->model("Vente_model");
		$this->load->model("contrat/Entente_model","entente");
		$this->load->model("organisation/Organisation_model","organisation");
		$this->load->model("entreprise/Entreprise_model","entreprise_model");
		$this->load->model("Produit_model","produit");
		$this->checkContrat();

	}

	protected function checkPrivilege(){
		$idUser=$this->session->userdata("idUser");

		if(empty($idUser) || $idUser<0)
			redirect(site_url("login"));
	}

	public function checkContrat(){
		$this->entreprise = $this->entente->getEntreprise($this->session->userdata("idUser"));
		$this->contrat = $this->entente->getContrat($this->session->userdata("idEntreprise"));
	}

	public function index(){
		$this->faireUneVente();
	}

	public function faireUneVente($idVente=-1){
		$this->layout->ajouter_js('../js/shim.min');
		$this->layout->ajouter_js('../js/xlsx.full.min');
		$this->layout->ajouter_js('../js/init.element');
		$this->layout->ajouter_js('../js/vente.module');
		$this->layout->ajouter_js('../js/jquery.timepicker.min');

		$this->layout->ajouter_css('../css/jquery.timepicker.min');
		$this->layout->ajouter_css('../plugins/jquery-ui/jquery-ui');


		$venteCur = null;

		$action = 2; //Car vente
		$checkDraft = $this->produit->checkIfThereAreDraft($action);
		if( $idVente == -1 && count($checkDraft)>0 ){
			$idVente = $checkDraft[0]->id_donation;
		}

		$adresseEntreprise = $this->entreprise_model->getAdressesEntreprise($this->entreprise['id_entreprise']);

		if($idVente!=-1){

			$venteCur = $this->produit->getInfoProduitById($idVente,2);

			if(count($venteCur) > 0){

				$venteCur = $venteCur[0];

				$id_produit = $venteCur->id_produit;

				//Information nutritionnelle
				$info_nutris =  $this->produit->getProduitInfoNutris($id_produit);
				$venteCur->info_nutris = $info_nutris;
				
				//Raison surplus
				$raison_surplus = $this->produit->getProduitRaisonSurplus($idVente);
				$venteCur->raisonSurplus = $raison_surplus;
				
				//??Necessite de refrigeration ou congelation
				$equipement = $this->produit->getEquipementNecessaire($id_produit);
				

				if(count($equipement) == 3){
					$congele = $equipement[0]->etat;
					$refrigere = $equipement[1]->etat;
					$consoDirect = $equipement[2]->etat;
				}else{
					$congele = 0;
					$refrigere = 0;
					$consoDirect = 0;
				}
				$venteCur->refrigere = $refrigere;
				$venteCur->congele = $congele;
				$venteCur->consoDirect = $consoDirect;

				//Stratégie marketing par Semaine
				$strategiePerWeek = $this->Vente_model->getStrategieMarketingPerWeek($idVente);
				$venteCur->strategieMarketingPerWeek = $strategiePerWeek;	

				//Strategie marketing
				$strategie = $this->Vente_model->getStrategieMarketingNotPerWeek($idVente);
				$venteCur->strategieMarketing = $strategie;		
				
				//Adresse ramassage du produit
				$adresse = $this->produit->getAdresseRamassage($idVente);
				if(count($adresse)>0){
					if(count($adresseEntreprise)>0){
						if($adresse[0]->id_adresse == $adresseEntreprise[0]->id_adresse){
							$venteCur->adresseRamassage = null;
						}else{
							$venteCur->adresseRamassage = $adresse[0];
						}
					}else{
						$venteCur->adresseRamassage = $adresse[0];
					}
				}else{
					$venteCur->adresseRamassage = null;
				}

				//Periode Recupération
				$periodeRecuperation = $this->Vente_model->getPeriodeRecuperation($idVente);
				$venteCur->periodeRecuperation = $periodeRecuperation;

				//Photo produit
				$photo = $this->produit->getProductPictures($id_produit,$this->entreprise['id_entreprise']);
				$venteCur->photo = $photo;
			
			}

		}
		
		$profilOk = $this->Vente_model->canMakeVente($this->entreprise['id_entreprise']);
		$protocoleOk = true; // TOUJOURS OK car faire une vente ne nécessite pas de de protocole d'entente 
		/*if($profilOk){
			if(!$this->contrat){
				$protocoleOk = false;
			}
		}*/
		
		$this->action("Vente");
		$types = $this->Vente_model->getTypes($this->entreprise['id_entreprise']);
		$categories = $this->Vente_model->getCategories($this->entreprise['id_entreprise']);
		$infoNutris = $this->Vente_model->getInfoNutris($this->entreprise['id_entreprise']);
		$raisonSurplus = $this->Vente_model->getRaisonSurplus($this->entreprise['id_entreprise']);
		//$periodeReduction = $this->Vente_model->getPeriodeReduction();
		$periodeReduction = $this->Vente_model->getPeriodeReductionNotPerWeek();
		$pays = $this->entreprise_model->getPays();
		
		$this->layout->addView(
			"vente", 
			array(
				'categories'=>$categories,
				'types'=>$types, 
				'infoNutris'=>$infoNutris,
				'raisonSurplus'=>$raisonSurplus,
				'profilOk'=>$profilOk,
				'protocoleOk'=>$protocoleOk,
				'contrat'=>$this->contrat,
				'adresse'=>$adresseEntreprise,
				'periodeReduction'=>$periodeReduction,
				'pays'=>$pays,
				'venteModif'=>$venteCur,
				'need_map'=>true

			)
		);

		$this->layout->view();
	}




	public function canMakeVente(){
		$result = array(
			"canMakeVente"=>$this->Vente_model->canMakeVente($this->entreprise['id_entreprise'])
		);
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function dashboard(){
		$this->layout->addView("dashboard");
		$this->layout->view();
	}

	public function addNewCategorie(){
		$categorie = trim($this->input->post('categorie'));
		$infoNutris = trim($this->input->post('infoNutris'));
		$result = array(
			"label"=>$categorie,
			"id_type_aliment"=>0,
			"message"=>"",
			"success"=>false
		);
		if(!empty($categorie) && isset($categorie)){
			
			$exist = $this->Don_model->addNewCategorie($categorie, $this->session->userdata("idEntreprise"));
			if(!$exist['exist']){
				$result['success'] = true;
				$result['id_type_aliment'] = $exist['id_type_aliment'];
				if(!empty($infoNutris) && isset($infoNutris)){
					$this->Don_model->linkToInfoNutris($exist['id_type_aliment'], $infoNutris);
				}
			}
			else{
				$result['message'] = "Cette catégorie de produit existe déjà";
			}
			
		}
		else{
			$result['message'] = "La catégorie du produit est vide";
		}

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	/*public function addNewCategorie(){
		$categorie = trim($this->input->post('categorie'));
		$infoNutris = trim($this->input->post('infoNutris'));
		$result = array(
			"label"=>$categorie,
			"id_type_aliment"=>0,
			"message"=>"",
			"success"=>false
		);
		if(!empty($categorie) && isset($categorie)){
			if(!empty($infoNutris) && isset($infoNutris)){
				$exist = $this->Vente_model->addNewCategorie($categorie, $this->session->userdata("idEntreprise"));
				if(!$exist['exist']){
					$result['success'] = true;
					$result['id_type_aliment'] = $exist['id_type_aliment'];
					$this->Vente_model->linkToInfoNutris($exist['id_type_aliment'], $infoNutris);
				}
				else{
					$result['message'] = "Cette catégorie de produit existe déjà";
				}
			}
			else{
				$result['message'] = "Vous devez préciser l'information nutritionnelle correspondant à la catégorie";
			}
		}
		else{
			$result['message'] = "La catégorie du produit est vide";
		}

		header('Content-Type: application/json');
		echo json_encode($result);
	}*/

	public function addNewType(){
		$type = trim($this->input->post('type'));
		$result = array(
			"label"=>$type,
			"id_type_produit"=>0,
			"message"=>"",
			"success"=>false
		);
		if(!empty($type) && isset($type)){
			$exist = $this->Vente_model->addNewType($type, $this->session->userdata("idEntreprise"));
			if(!$exist['exist']){
				$result['success'] = true;
				$result['id_type_produit'] = $exist['id_type_produit'];
			}
			else{
				$result['message'] = "Le type de produit existe déjà";
			}
		}
		else{
			$result['message'] = "Le type de produit est vide";
		}

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function addNewInfoNutris(){
		$infoNutris = trim($this->input->post('infoNutris'));
		$result = array(
			"label"=>$infoNutris,
			"id_information_nutritionnelle"=>0,
			"message"=>"",
			"success"=>false
		);
		if(!empty($infoNutris) && isset($infoNutris)){
			$exist = $this->Vente_model->addNewInfoNutris($infoNutris, $this->session->userdata("idEntreprise"));
			if(!$exist['exist']){
				$result['success'] = true;
				$result['id_information_nutritionnelle'] = $exist['id_information_nutritionnelle'];
			}
			else{
				$result['message'] = "Cette information existe déjà";
			}
		}
		else{
			$result['message'] = "Cette information est invalide";
		}
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function addNewRaisonSurplus(){
		$raisonSurplus = trim($this->input->post('raisonSurplus'));
		$result = array(
			"label"=>$raisonSurplus,
			"id_raison_surplus"=>0,
			"message"=>"",
			"success"=>false
		);
		if(!empty($raisonSurplus) && isset($raisonSurplus)){
			$exist = $this->Vente_model->addNewRaisonSurplus($raisonSurplus, $this->session->userdata("idEntreprise"));
			if(!$exist['exist']){
				$result['success'] = true;
				$result['id_raison_surplus'] = $exist['id_raison_surplus'];
			}
			else{
				$result['message'] = "Cette raison de surplus existe déjà";
			}
		}
		else{
			$result['message'] = "Cette raison de surplus est invalide";
		}
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function getInfoNutrisLinked(){
		$idCategorie = $this->input->post('idCategorie');
		header('Content-Type: application/json');
		echo json_encode($this->Vente_model->getInfoNutrisLinked($idCategorie));
	}

	public function saveImage($dataURL, $ext, $idProduit, $index){
		$result = array('exist'=>false,'filepath'=>'', 'id_photo_produit'=>-1);
		$imgData = str_replace(' ','+', $dataURL);
		$imgData =  substr($imgData, strpos($imgData,",")+1);
		$imgDataMIME = base64_decode($imgData);
		$dateSave = Date('YmdHis');
		$filePath = 'assets/img/produits/produit_'.$idProduit.'_'.$index.'_'.$dateSave.'.'.$ext;
		// teste si data est deja dans disque
		$dataInDisc = $this->Vente_model->getPhotoPoduits('', $this->session->userdata("idEntreprise"));
		for($i=0; $i<count($dataInDisc); $i++){
			if(file_exists($dataInDisc[$i]['url'])){
				$data = base64_encode(file_get_contents($dataInDisc[$i]['url']));
				if(strcmp($imgData, $data)==0){
					$result['exist'] = true;
					$result['id_photo_produit'] = $dataInDisc[$i]['id_photo_produit'];
					break;
				}
			}
		}
		if(!$result['exist']){
			$result['filepath'] = $filePath;
			file_put_contents($filePath, $imgDataMIME);
		}

		return $result;
	}

	public function getPhotoPoduits(){
		$nom = $this->input->post('nom');
		header('Content-Type: application/json');
		echo json_encode($this->Vente_model->getPhotoPoduits($nom, $this->session->userdata("idEntreprise")));
	}

	public function parseDateFrToSql($date){
		if(empty($date)){
			return null;
		}
		else{
			$strs = explode("/", $date);
			if(count($strs) != 3){
				return null;
			}
			else{
				if(((int) $strs[2]) < 2018){
					return null;
				}
				else{
					return $strs[2]."-".$strs[1]."-".$strs[0];
				}
			}
		}
	}

	public function pushFormVente(){
		$result = array(
			'success'=>true,
			'message'=>'',
			'saveProduct'=> array()
		);


		$statutVenteModif = (int)$this->input->post('statutVenteModif');
		$idProduit = (int)$this->input->post('idProduit');
		$idDonation = (int)$this->input->post('idDonation');
		$idEtatDonation = (int)$this->input->post('idEtatDonation');

		$moment = $this->input->post('pushDate');
		$nom = $this->input->post('nom');
		$groupement = is_numeric($this->input->post('groupement')) ? (int) $this->input->post('groupement') : -1;
		$poids = is_numeric($this->input->post('poids')) ? (float) $this->input->post('poids') : -1;
		$volume = is_numeric($this->input->post('volume')) ? (float) $this->input->post('volume') : -1;
		$prix = is_numeric($this->input->post('prix')) ? (float) $this->input->post('prix') : -1;
		$quantite = is_numeric($this->input->post('quantite')) ? (int) $this->input->post('quantite') : -1;
		$quantiteMin = is_numeric($this->input->post('quantiteMinimum')) ? (int) $this->input->post('quantiteMinimum') : -1;
		$pourcentage = is_numeric($this->input->post('pourcentage')) ? (float) $this->input->post('pourcentage') : -1;
		$typeDate = is_numeric($this->input->post('typeDate')) ? (int) $this->input->post('typeDate') : -1;
		$datePeremptionOrProduction = $this->parseDateFrToSql($this->input->post('date'));

		$reduction4Semaine = is_numeric($this->input->post('reduction4Semaine')) ? (float) $this->input->post('reduction4Semaine') : null;
		$reduction3Semaine = is_numeric($this->input->post('reduction3Semaine')) ? (float) $this->input->post('reduction3Semaine') : null;
		$reduction2Semaine = is_numeric($this->input->post('reduction2Semaine')) ? (float) $this->input->post('reduction2Semaine') : null;
		$reduction1Semaine = is_numeric($this->input->post('reduction1Semaine')) ? (float) $this->input->post('reduction1Semaine') : null;

		$reductionParSemaine = [$reduction4Semaine, $reduction3Semaine, $reduction2Semaine, $reduction1Semaine];

		$NbPeriodeReduction = is_numeric($this->input->post('NbPeriodeReduction')) ? (int) $this->input->post('NbPeriodeReduction') : -1;
		$periodeReduction = array();
		for($i=0; $i<$NbPeriodeReduction; $i++){
			$periodeReduction[] = is_numeric($this->input->post('periodeReduction'.$i)) ? (int) $this->input->post('periodeReduction'.$i) : "";
		}

		$NbPourcentageReduction = is_numeric($this->input->post('NbPourcentageReduction')) ? (int) $this->input->post('NbPourcentageReduction') : -1;
		$pourcentageReduction = array();
		for($i=0; $i<$NbPourcentageReduction; $i++){
			$pourcentageReduction[] = is_numeric($this->input->post('pourcentageReduction'.$i)) ? (int) $this->input->post('pourcentageReduction'.$i) : "";
		}


		$refrigere = is_numeric($this->input->post('refrigere')) ? (int) $this->input->post('refrigere') : -1;
		$congele = is_numeric($this->input->post('congele')) ? (int) $this->input->post('congele') : -1;
		$consoDirect = is_numeric($this->input->post('consoDirect')) ? (int) $this->input->post('consoDirect') : -1;

		$description = $this->input->post('description');
		$categorie = is_numeric($this->input->post('categorie')) ? (int) $this->input->post('categorie') : -1;
		$type = is_numeric($this->input->post('type')) ? (int) $this->input->post('type') : -1;
		
		
		$NbInfoNutris = is_numeric($this->input->post('NbInfoNutris')) ? (int) $this->input->post('NbInfoNutris') : -1;
		$infoNutris = array();
		for($i=0; $i<$NbInfoNutris; $i++){
			$infoNutris[] = is_numeric($this->input->post('infoNutris'.$i)) ? (int) $this->input->post('infoNutris'.$i) : -1;
		}

		//$haveRaisonSurplus = is_numeric($this->input->post('haveRaisonSurplus')) ? (int) $this->input->post('haveRaisonSurplus') : 0;
		$NbRaisonSurplus = is_numeric($this->input->post('NbRaisonSurplus')) ? (int) $this->input->post('NbRaisonSurplus') : -1;
		
		/*if($haveRaisonSurplus == 1){
			$raisonSurplus = array();
			for($i=0; $i<$NbRaisonSurplus; $i++){
				$raisonSurplus[] = is_numeric($this->input->post('raisonSurplus'.$i)) ? (int) $this->input->post('raisonSurplus'.$i) : -1;
			}
		}else{
			$raisonSurplus = array();
			$raisonSurplus[0] = is_numeric($this->input->post('raisonSurplus0')) ? (int) $this->input->post('raisonSurplus0') : -1;
		}*/
		$raisonSurplus = array();
		for($i=0; $i<$NbRaisonSurplus; $i++){
			$raisonSurplus[] = is_numeric($this->input->post('raisonSurplus'.$i)) ? (int) $this->input->post('raisonSurplus'.$i) : -1;
		}
			
		$isAdresseEntreprise = is_numeric($this->input->post('isAdresseEntreprise')) ? (int) $this->input->post('isAdresseEntreprise') : 0;
		$id_AdresseEntreprise = is_numeric($this->input->post('id_adresseEntreprise')) ? (int) $this->input->post('id_adresseEntreprise') : -1;
		$adresseRmsg = $this->input->post('adresseRmsg');
		$codePostalRmsg = $this->input->post('codePostalRmsg');
		$villeRmsg = $this->input->post('villeRmsg');
		$paysRmsg = is_numeric($this->input->post('paysRmsg')) ? (int) $this->input->post('paysRmsg') : -1;
		$latitudeRmsg = is_numeric( $this->input->post('latitudeRmsg') ) ? (float) $this->input->post('latitudeRmsg') : 0;
		$longitudeRmsg = is_numeric( $this->input->post('longitudeRmsg') ) ? (float) $this->input->post('longitudeRmsg') : 0;

		/* ------ PERIODE RECUPERATION ------ */
		$NbPeriodeRecuperation = is_numeric($this->input->post('NbPeriodeRecuperation')) ? (int) $this->input->post('NbPeriodeRecuperation') : -1;
		$periodeRecuperation = array();
		for($i=0; $i<$NbPeriodeRecuperation; $i++){
			$periodeRecuperation[] = $this->input->post('periodeRecuperation'.$i);
		}

		$NbHoraireDebut = is_numeric($this->input->post('NbHoraireDebut')) ? (int) $this->input->post('NbHoraireDebut') : -1;
		$horaireDebut = array();
		for($i=0; $i<$NbHoraireDebut; $i++){
			$horaireDebut[] = $this->input->post('horaireDebut'.$i);
		}

		$NbHoraireFin = is_numeric($this->input->post('NbHoraireFin')) ? (int) $this->input->post('NbHoraireFin') : -1;
		$horaireFin = array();
		for($i=0; $i<$NbHoraireFin; $i++){
			$horaireFin[] = $this->input->post('horaireFin'.$i);
		}
		/* ------ FIN PERIODE RECUPERATION ------ */


		$NbPhotoAlreadyInBase = is_numeric($this->input->post('NbPhotoAlreadyInBase')) ? (int) $this->input->post('NbPhotoAlreadyInBase') : -1;
		$pictures = array();
		for($i=0; $i<$NbPhotoAlreadyInBase; $i++){
			$pictures[] = is_numeric($this->input->post('pictures'.$i)) ? (int) $this->input->post('pictures'.$i) : -1;
		}

		$NbImages = is_numeric($this->input->post('NbImages')) ? (int) $this->input->post('NbImages') : -1;
		$images = array();
		for($i=0; $i<$NbImages; $i++){
			$fromServer = (int) $this->input->post('fromServer'.$i);
			if($fromServer == 1){
				$images[] = array(
					"fromServer" => $fromServer,
					"idPhoto" => $this->input->post('idPhoto'.$i)
				);
			}
			else{
				$images[] = array(
					"fromServer" => 0,
					'ext'=>$this->input->post('image'.$i.'ext'),
					'dataURL'=>$this->input->post('image'.$i.'data')
				);
			}
		}

		$isDraft = is_numeric($this->input->post('isDraft')) ? (int) $this->input->post('isDraft') : 0;

		if(empty($nom)){
			$result['success'] = false;
			$result['message'] = 'Le nom du produit est vide';
		}
		if($result['success'] && $categorie < 0) {
			$result['success'] = false;
			$result['message'] = 'La catégorie du produit est invalide';
		}
		if($result['success'] && $type < 0) {
			$result['success'] = false;
			$result['message'] = 'Le type du produit est invalide';
		}
		if($result['success'] && $poids < 0) {
			$result['success'] = false;
			$result['message'] = 'Le poids du produit est invalide';
		}
		if($result['success'] && $prix < 0) {
			$result['success'] = false;
			$result['message'] = 'Le prix du produit est invalide';
		}
		if($result['success'] && $quantite < 0) {
			$result['success'] = false;
			$result['message'] = 'La quantité du produit est invalide';
		}
		if($result['success'] && $quantiteMin < 0) {
			$result['success'] = false;
			$result['message'] = 'La quantité minimum du produit est invalide';
		}
		if($result['success'] && $groupement != 2 && ($quantiteMin < 0 || $quantite < $quantiteMin ) ){
			$result['success'] = false;
			$result['message'] = 'La quantité minimum du produit est invalide, cette quantité minimum doit être inférieur à la quantité du produit';
		}
		if($result['success'] && $datePeremptionOrProduction == null){
			$result['success'] = false;
			$result['message'] = 'La date de péremption est invalide';
		}
		if($result['success'] && !($refrigere == 0 || $refrigere == 1)){
			$result['success'] = false;
			$result['message'] = 'La valeur dont le produit nécessite est invalide';
		}
		if($result['success'] && !($congele == 0 || $congele == 1)){
			$result['success'] = false;
			$result['message'] = 'La valeur dont le produit nécessite est invalide';
		}
		if($result['success'] && !($consoDirect == 0 || $consoDirect == 1)){
			$result['success'] = false;
			$result['message'] = 'La valeur dont le produit nécessite est invalide';
		}
		/*if($result['success']){
			for($i=0; $i<$NbInfoNutris; $i++){
				if($infoNutris[$i] < 0){
					$result['success'] = false;
					$result['message'] = 'Aucune information nutritionnelle n\'a été précisée pour le produit';
					break;
				}
			}
		}*/
		/*if($result['success']){
			if($haveRaisonSurplus > 0){
				for($i=0; $i<$NbRaisonSurplus; $i++){
					if($raisonSurplus[$i] < 0){
						$result['success'] = false;
						$result['message'] = 'Aucune raison de surplus n\'a été précisée pour le produit';
						break;
					}
				}
			}
			
		}*/
		if($result['success']){
			for($i=0; $i<count($reductionParSemaine); $i++){
				if( $reductionParSemaine[$i] == null ) {
					$result['success'] = false;
					$result['message'] = 'Une pourcentage de reduction est invalide';
					break;
				}
			}
		}

		if($result['success']){
			for($i=0; $i<$NbPeriodeReduction; $i++){
				if($periodeReduction[$i] < 0){
					$result['success'] = false;
					$result['message'] = 'Une période de reduction est invalide';
					break;
				}
			}
		}

		if($result['success']){
			for($i=0; $i<$NbPourcentageReduction; $i++){
				if($pourcentageReduction[$i] < 0){
					$result['success'] = false;
					$result['message'] = 'Le pourcentage de reduction est invalide';
					break;
				}
			}
		}
		
		if(empty($adresseRmsg)){
			$result['success'] = false;
			$result['message'] = "L'adresse de ramassage est invalide";
		}
		if(empty($codePostalRmsg)){
			$result['success'] = false;
			$result['message'] = "Le code postal associée au lieu de ramassage est invalide";
		}
		if(empty($villeRmsg)){
			$result['success'] = false;
			$result['message'] = "La ville associée à l'adresse est invalide";
		}
		if($result['success'] && $paysRmsg < 0){
			$result['success'] = false;
			$result['message'] = 'Le pays associée au lieu de ramassage est invalide';
		}
		if($result['success'] && ($latitudeRmsg == 0 && $longitudeRmsg == 0)){
			$result['success'] = false;
			$result['message'] = 'Les coordonnées associée au lieu de ramassage est invalide';
		}
		if($result['success']){
			for($i=0; $i<$NbPeriodeRecuperation; $i++){
				if(empty($periodeRecuperation[$i])){
					$result['success'] = false;
					$result['message'] = 'Une période de récupération est invalide';
				}
			}
		}
		if($result['success']){
			for($i=0; $i<$NbHoraireDebut; $i++){
				if(empty($horaireDebut[$i])){
					$result['success'] = false;
					$result['message'] = 'Un horaire de début de récupération est invalide';
				}
			}
		}
		if($result['success']){
			for($i=0; $i<$NbHoraireFin; $i++){
				if(empty($horaireFin[$i])){
					$result['success'] = false;
					$result['message'] = 'Un horaire de fin de récupération est invalide';
				}
			}
		}

		/*if($result['success'] && $NbImages <= 0){
			$result['success'] = false;
			$result['message'] = 'Il faut au moins une photo pour le produit';
		}*/

		if($result['success']){	

			if($statutVenteModif == 0){

				$idProduit = $this->Vente_model->saveProduit($nom, $categorie, $type, $description, $infoNutris, $poids, $volume, $prix, $refrigere, $congele, $consoDirect, $groupement);
				$this->Vente_model->pushVente($this->entreprise['id_entreprise'], $idProduit, $quantite, $quantiteMin, $pourcentage, $typeDate ,$datePeremptionOrProduction, $moment, $raisonSurplus, $reductionParSemaine, $periodeReduction, $pourcentageReduction, $isAdresseEntreprise, $id_AdresseEntreprise,$adresseRmsg, $codePostalRmsg, $villeRmsg, $paysRmsg, $latitudeRmsg, $longitudeRmsg, $periodeRecuperation, $horaireDebut, $horaireFin, $isDraft);
				for($i=0; $i<$NbImages; $i++){
					if($images[$i]['fromServer'] == 0){
						$urlData = $this->saveImage($images[$i]['dataURL'], $images[$i]['ext'], $idProduit, $i);
						if($urlData['exist']){
							$this->Vente_model->savePhotoProduit($idProduit, null, $urlData['id_photo_produit'], $this->entreprise['id_entreprise']);
						}
						else{
							$this->Vente_model->savePhotoProduit($idProduit, $urlData['filepath'], null, $this->entreprise['id_entreprise']);
						}
					}
					else{
						$this->Vente_model->savePhotoProduit($idProduit, null, $images[$i]['idPhoto'], $this->entreprise['id_entreprise']);
					}
				}
				$result['message'] = 'Enregistrement terminé';

				//Data for firestore
				$owner = "Entreprise_name";
				$nomEntreprise = $this->session->userdata('nomEntreprise');
				if(!empty($nomEntreprise) || $nomEntreprise != ''){
					$owner = $nomEntreprise;
				}

				$topic = "Product_name";
				$typeName = $this->Vente_model->getTypeProduitNameById($type);
				if(!empty($typeName)){
					$topic = $typeName->label;
				}
				
				$topic_id = $type;

				$result['saveProduct'] = array(
					'owner' => $owner,
					'topic' => $topic,
					'topic_id' => $topic_id
				);

			}else if($statutVenteModif == 1){
				$this->Vente_model->updateProduit($idProduit, $nom, $categorie, $type, $description, $infoNutris, $poids, $volume, $prix, $refrigere, $congele, $consoDirect, $groupement);
				$this->Vente_model->pushUpdateVente($idDonation, $this->entreprise['id_entreprise'], $idProduit, $quantite, $quantiteMin, $pourcentage, $typeDate ,$datePeremptionOrProduction, $moment, $raisonSurplus, $reductionParSemaine, $periodeReduction, $pourcentageReduction, $isAdresseEntreprise, $id_AdresseEntreprise,$adresseRmsg, $codePostalRmsg, $villeRmsg, $paysRmsg,$latitudeRmsg, $longitudeRmsg, $periodeRecuperation, $horaireDebut, $horaireFin, $isDraft);

				$this->Vente_model->DeletePhotoProduit($idProduit, $pictures);

				for($i=0; $i<$NbImages; $i++){
					if($images[$i]['fromServer'] == 0){
						$urlData = $this->saveImage($images[$i]['dataURL'], $images[$i]['ext'], $idProduit, $i);
						if($urlData['exist']){
							$this->Vente_model->updatePhotoProduit($idProduit, null, $urlData['id_photo_produit'], $this->entreprise['id_entreprise']);
						}
						else{
							$this->Vente_model->updatePhotoProduit($idProduit, $urlData['filepath'], null, $this->entreprise['id_entreprise']);
						}
					}
					else{
						$this->Vente_model->updatePhotoProduit($idProduit, null, $images[$i]['idPhoto'], $this->entreprise['id_entreprise']);
					}
				}
				if($idEtatDonation == 2){
					$result['message'] = "Modification enregistrée ";
				}else{
					$result['message'] = 'Vente enregistrée';
				}
				
			}
			else{
				$result['message'] = "Erreur d'enregistrement";
			}

		}
		header('Content-Type: application/json');
		echo json_encode($result);
		
	}

	public function saveAsDraft(){
		$result = array(
			'success'=>true,
			'message'=>''
		);


		$statutVenteModif = (int)$this->input->post('statutVenteModif');
		$idProduit = (int)$this->input->post('idProduit');
		$idDonation = (int)$this->input->post('idDonation');

		$moment = $this->input->post('pushDate');
		$nom = $this->input->post('nom');
		$groupement = $this->input->post('groupement');
		$poids = is_numeric($this->input->post('poids')) ? (float) $this->input->post('poids') : '';
		$volume = is_numeric($this->input->post('volume')) ? (float) $this->input->post('volume') : '';
		$prix = is_numeric($this->input->post('prix')) ? (float) $this->input->post('prix') : '';
		$quantite = is_numeric($this->input->post('quantite')) ? (float) $this->input->post('quantite') : '';
		$quantiteMin = is_numeric($this->input->post('quantiteMinimum')) ? (float) $this->input->post('quantiteMinimum') : '';
		$pourcentage = is_numeric($this->input->post('pourcentage')) ? (float) $this->input->post('pourcentage') : '';
		$typeDate = $this->input->post('typeDate');
		$datePeremptionOrProduction = $this->parseDateFrToSql($this->input->post('date'));

		$reduction4Semaine = is_numeric($this->input->post('reduction4Semaine')) ? (float) $this->input->post('reduction4Semaine') : '';
		$reduction3Semaine = is_numeric($this->input->post('reduction3Semaine')) ? (float) $this->input->post('reduction3Semaine') : '';
		$reduction2Semaine = is_numeric($this->input->post('reduction2Semaine')) ? (float) $this->input->post('reduction2Semaine') : '';
		$reduction1Semaine = is_numeric($this->input->post('reduction1Semaine')) ? (float) $this->input->post('reduction1Semaine') : '';

		$reductionParSemaine = [$reduction4Semaine, $reduction3Semaine, $reduction2Semaine, $reduction1Semaine];

		$NbPeriodeReduction = is_numeric($this->input->post('NbPeriodeReduction')) ? (int) $this->input->post('NbPeriodeReduction') : -1;
		$periodeReduction = array();
		for($i=0; $i<$NbPeriodeReduction; $i++){
			$periodeReduction[] = is_numeric($this->input->post('periodeReduction'.$i)) ? (int) $this->input->post('periodeReduction'.$i) : null;
		}

		$NbPourcentageReduction = is_numeric($this->input->post('NbPourcentageReduction')) ? (int) $this->input->post('NbPourcentageReduction') : -1;
		$pourcentageReduction = array();
		for($i=0; $i<$NbPourcentageReduction; $i++){
			$pourcentageReduction[] = is_numeric($this->input->post('pourcentageReduction'.$i)) ? (int) $this->input->post('pourcentageReduction'.$i) : -1;
		}


		$refrigere = is_numeric($this->input->post('refrigere')) ? (int) $this->input->post('refrigere') : null;
		$congele = is_numeric($this->input->post('congele')) ? (int) $this->input->post('congele') : null;
		$consoDirect = is_numeric($this->input->post('consoDirect')) ? (int) $this->input->post('consoDirect') : null;
		
		$description = $this->input->post('description');
		$categorie = is_numeric($this->input->post('categorie')) ? (int) $this->input->post('categorie') : null;
		$type = is_numeric($this->input->post('type')) ? (int) $this->input->post('type') : null;
		
		
		$NbInfoNutris = is_numeric($this->input->post('NbInfoNutris')) ? (int) $this->input->post('NbInfoNutris') : -1;
		$infoNutris = array();
		for($i=0; $i<$NbInfoNutris; $i++){
			$infoNutris[] = is_numeric($this->input->post('infoNutris'.$i)) ? (int) $this->input->post('infoNutris'.$i) : null;
		}

		//$haveRaisonSurplus = is_numeric($this->input->post('haveRaisonSurplus')) ? (int) $this->input->post('haveRaisonSurplus') : 0;
		$NbRaisonSurplus = is_numeric($this->input->post('NbRaisonSurplus')) ? (int) $this->input->post('NbRaisonSurplus') : -1;
		
		/*if($haveRaisonSurplus == 1){
			$raisonSurplus = array();
			for($i=0; $i<$NbRaisonSurplus; $i++){
				$raisonSurplus[] = is_numeric($this->input->post('raisonSurplus'.$i)) ? (int) $this->input->post('raisonSurplus'.$i) : null;
			}
		}else{
			$raisonSurplus = array();
			$raisonSurplus[0] = is_numeric($this->input->post('raisonSurplus0')) ? (int) $this->input->post('raisonSurplus0') : null;
		}*/

		$raisonSurplus = array();
		for($i=0; $i<$NbRaisonSurplus; $i++){
			$raisonSurplus[] = is_numeric($this->input->post('raisonSurplus'.$i)) ? (int) $this->input->post('raisonSurplus'.$i) : null;
		}
			
		$isAdresseEntreprise = is_numeric($this->input->post('isAdresseEntreprise')) ? (int) $this->input->post('isAdresseEntreprise') : 0;
		$id_AdresseEntreprise = is_numeric($this->input->post('id_adresseEntreprise')) ? (int) $this->input->post('id_adresseEntreprise') : -1;
		$adresseRmsg = $this->input->post('adresseRmsg');
		$codePostalRmsg = $this->input->post('codePostalRmsg');
		$villeRmsg = $this->input->post('villeRmsg');
		$paysRmsg = is_numeric($this->input->post('paysRmsg')) ? (int) $this->input->post('paysRmsg') : -1;
		$latitudeRmsg = is_numeric( $this->input->post('latitudeRmsg') ) ? (float) $this->input->post('latitudeRmsg') : 0;
		$longitudeRmsg = is_numeric( $this->input->post('longitudeRmsg') ) ? (float) $this->input->post('longitudeRmsg') : 0;

		$NbPhotoAlreadyInBase = is_numeric($this->input->post('NbPhotoAlreadyInBase')) ? (int) $this->input->post('NbPhotoAlreadyInBase') : -1;
		$pictures = array();
		for($i=0; $i<$NbPhotoAlreadyInBase; $i++){
			$pictures[] = is_numeric($this->input->post('pictures'.$i)) ? (int) $this->input->post('pictures'.$i) : -1;
		}

		/* ------ PERIODE RECUPERATION ------ */
		$NbPeriodeRecuperation = is_numeric($this->input->post('NbPeriodeRecuperation')) ? (int) $this->input->post('NbPeriodeRecuperation') : -1;
		$periodeRecuperation = array();
		for($i=0; $i<$NbPeriodeRecuperation; $i++){
			$periodeRecuperation[] = $this->input->post('periodeRecuperation'.$i);
		}

		$NbHoraireDebut = is_numeric($this->input->post('NbHoraireDebut')) ? (int) $this->input->post('NbHoraireDebut') : -1;
		$horaireDebut = array();
		for($i=0; $i<$NbHoraireDebut; $i++){
			$horaireDebut[] = $this->input->post('horaireDebut'.$i);
		}

		$NbHoraireFin = is_numeric($this->input->post('NbHoraireFin')) ? (int) $this->input->post('NbHoraireFin') : -1;
		$horaireFin = array();
		for($i=0; $i<$NbHoraireFin; $i++){
			$horaireFin[] = $this->input->post('horaireFin'.$i);
		}
		/* ------ FIN PERIODE RECUPERATION ------ */

		$NbImages = is_numeric($this->input->post('NbImages')) ? (int) $this->input->post('NbImages') : -1;
		$images = array();
		for($i=0; $i<$NbImages; $i++){
			$fromServer = (int) $this->input->post('fromServer'.$i);
			if($fromServer == 1){
				$images[] = array(
					"fromServer" => $fromServer,
					"idPhoto" => $this->input->post('idPhoto'.$i)
				);
			}
			else{
				$images[] = array(
					"fromServer" => 0,
					'ext'=>$this->input->post('image'.$i.'ext'),
					'dataURL'=>$this->input->post('image'.$i.'data')
				);
			}
		}

		
		$isDraft = is_numeric($this->input->post('isDraft')) ? (int) $this->input->post('isDraft') : 0;

		 
		$action = 2; // 2 car vente
		$checkDraft = $this->produit->checkIfThereAreDraft($action);


		if($result['success']){	

			if( count($checkDraft)>0 ){
				$idDonation = $checkDraft[0]->id_donation;
				$idProduit = $checkDraft[0]->produit_id_produit;

				$this->Vente_model->updateProduit($idProduit, $nom, $categorie, $type, $description, $infoNutris, $poids, $volume, $prix, $refrigere, $congele, $consoDirect, $groupement);
				$this->Vente_model->pushUpdateVente($idDonation, $this->entreprise['id_entreprise'], $idProduit, $quantite, $quantiteMin, $pourcentage, $typeDate ,$datePeremptionOrProduction, $moment, $raisonSurplus, $reductionParSemaine, $periodeReduction, $pourcentageReduction, $isAdresseEntreprise, $id_AdresseEntreprise,$adresseRmsg, $codePostalRmsg, $villeRmsg, $paysRmsg,$latitudeRmsg, $longitudeRmsg, $periodeRecuperation, $horaireDebut, $horaireFin, $isDraft);
				
				$this->Vente_model->DeletePhotoProduit($idProduit, $pictures);

				for($i=0; $i<$NbImages; $i++){
					if($images[$i]['fromServer'] == 0){
						$urlData = $this->saveImage($images[$i]['dataURL'], $images[$i]['ext'], $idProduit, $i);
						if($urlData['exist']){
							$this->Vente_model->updatePhotoProduit($idProduit, null, $urlData['id_photo_produit'], $this->entreprise['id_entreprise']);
						}
						else{
							$this->Vente_model->updatePhotoProduit($idProduit, $urlData['filepath'], null, $this->entreprise['id_entreprise']);
						}
					}
					else{
						$this->Vente_model->updatePhotoProduit($idProduit, null, $images[$i]['idPhoto'], $this->entreprise['id_entreprise']);
					}
				}
				$result['message'] = 'Enregistrement terminé';

			}
			else{
				$idProduit = $this->Vente_model->saveProduit($nom, $categorie, $type, $description, $infoNutris, $poids, $volume, $prix, $refrigere, $congele, $consoDirect, $groupement);
				$this->Vente_model->pushVente($this->entreprise['id_entreprise'], $idProduit, $quantite, $quantiteMin, $pourcentage, $typeDate ,$datePeremptionOrProduction, $moment, $raisonSurplus, $reductionParSemaine, $periodeReduction, $pourcentageReduction, $isAdresseEntreprise, $id_AdresseEntreprise,$adresseRmsg, $codePostalRmsg, $villeRmsg, $paysRmsg, $latitudeRmsg, $longitudeRmsg, $periodeRecuperation, $horaireDebut, $horaireFin, $isDraft);
				for($i=0; $i<$NbImages; $i++){
					if($images[$i]['fromServer'] == 0){
						$urlData = $this->saveImage($images[$i]['dataURL'], $images[$i]['ext'], $idProduit, $i);
						if($urlData['exist']){
							$this->Vente_model->savePhotoProduit($idProduit, null, $urlData['id_photo_produit'], $this->entreprise['id_entreprise']);
						}
						else{
							$this->Vente_model->savePhotoProduit($idProduit, $urlData['filepath'], null, $this->entreprise['id_entreprise']);
						}
					}
					else{
						$this->Vente_model->savePhotoProduit($idProduit, null, $images[$i]['idPhoto'], $this->entreprise['id_entreprise']);
					}
				}
				$result['message'] = 'Enregistrement terminé';
			}	

		}else{
			$result['message'] = "Erreur d'enregistrement";
		}
		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function import(){
		$produitAdded = array();
		$result = array(
			'success' => true,
			'message' => '',
			'list' => array()
		);
		$moment = $this->input->post('moment');
		$nbRow = is_numeric($this->input->post('nbRow')) ? (int) $this->input->post('nbRow') : -1;
		for($i=0; $i<$nbRow; $i++){
			$nom = trim($this->input->post('nom'.$i));
			$categorie = trim($this->input->post('categorie'.$i));
			$type = trim($this->input->post('type'.$i));
			$datePeremption = trim($this->input->post('datePeremption'.$i));
			$groupement = $this->input->post('groupement'.$i);
			if(strcmp($groupement, 'Par unité') == 0){
				$groupement = 1;
			}
			else{
				$groupement = 2;
			}
			$volume = is_numeric($this->input->post('volume'.$i)) ? (float) $this->input->post('volume'.$i) : 0;
			$poids = is_numeric($this->input->post('poids'.$i)) ? (float) $this->input->post('poids'.$i) : 0;
			$prix = is_numeric($this->input->post('prix'.$i)) ? (float) $this->input->post('prix'.$i) : 0;
			$quantite = is_numeric($this->input->post('quantite'.$i)) ? (float) $this->input->post('quantite'.$i) : 0;
			$description = $this->input->post('description'.$i);
			if(!isset($description) || empty($description)){
				$description = '';
			}
			else{
				$description = trim($description);
			}
			$infoNutris = $this->input->post('infoNutris'.$i);
			$raisonSurplus = $this->input->post('raisonSurplus'.$i);
			$refrigere = $this->input->post('refrigere'.$i);
			$congele = $this->input->post('congele'.$i);

			if(strlen($nom) > 0 && strlen($type) > 0 && strlen($infoNutris) > 0 && strlen($categorie)){
				if($prix > 0 && $poids > 0 && $quantite > 0){
					if(strcasecmp($refrigere, 'oui') == 0 || strcasecmp($refrigere, 'non') == 0){
						if(strcasecmp($congele, 'oui') == 0 || strcasecmp($congele, 'non') == 0){
							$infoNutris = explode('|', $infoNutris);
							$raisonSurplus = explode('|', $raisonSurplus);
							if(count($infoNutris) > 0 && count($raisonSurplus) > 0){

								//recherche bénéficiaire approprié
								$produitProperty = (object) array("infoNutri"=>$infoNutris,"equipement"=>array(2=>(int)$refrigere,1=>(int)$congele));
								$organisation_id = $this->findBeneficiaire($produitProperty);

								if($organisation_id==0){
									$result['success'] = false;
									$result['message'] = 'Impossible d\'enregistrer la vente. Aucun bénéficiare n\'a encore été enregistré dans la base de donnée de Manzer Partazer';
									break;
								}else{
									$added = $this->Vente_model->saveProduitImport($nom, $categorie, $type, $description, $infoNutris, $poids, $volume, $prix, $refrigere, $congele, $this->entreprise['id_entreprise'], $groupement);
									$this->Vente_model->saveVenteImport($this->entreprise['id_entreprise'], $organisation_id, $added['id_produit'], $quantite, $datePeremption, $moment, $raisonSurplus);
									$produitAdded[] = $added;
								}
							}
						}
					}
				}
			}
		}

		if(count($produitAdded) > 0){
			$result['list'] = $produitAdded;
			$result['message'] = "Voici les enrégistrements effectués, maintenant vous pouvez définir les photos correspondants";
		}
		else{
			$result['success'] = false;
			$result['message'] = "Aucune ligne n'a été ajouté car certaines valeurs sont invalides";
		}

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function importExcelPhoto(){
		$idProduit = $this->input->post('id_produit');
		$nbImage = $this->input->post('nbImage');
		for($i=0; $i<$nbImage; $i++){
			$fromServer = (int) $this->input->post('fromServer'.$i);
			if($fromServer == 1){
				$this->Vente_model->savePhotoProduit($idProduit, null, $this->input->post('idPhoto'.$i), $this->entreprise['id_entreprise']);
			}
			else{
				$ext = $this->input->post('image'.$i.'ext');
				$dataURL = $this->input->post('image'.$i.'data');
				$urlData = $this->saveImage($dataURL, $ext, $idProduit, $i);
				if(!$urlData['exist']){
					$this->Vente_model->savePhotoProduit($idProduit, $urlData['filepath'], null, $this->entreprise['id_entreprise']);
				}
				else{
					$this->Vente_model->savePhotoProduit($idProduit, null, $urlData['id_photo_produit'], $this->entreprise['id_entreprise']);
				}
			}
		}
	}

	public function findBeneficiaire($produitProperty){
		$organisations = array();

		$coef = 1;

		$result = 0;

		// $produitProperty = (object) array("infoNutri"=>array(1,4),"equipement"=>array(2=>1,1=>0));

		$organisation_infoNutri = $this->organisation->getOrganisationsInfoNutri();
		$organisation_equipement = $this->organisation->getOrganisationsEquipement();

		foreach ($organisation_infoNutri as $value) {
			$organisations[$value->id_organisation]["infoNutri"][]=(int)$value->information_nutritionnelle_id;
		}

		foreach ($organisation_equipement as $value) {
			$organisations[$value->id_organisation]["equipement"][$value->equipement_id]=(int)$value->etat;
		}

		foreach ($organisations as $key=>$organisation) {
			$organisations[$key]["score"]=0;
		}

		if(count($organisations)>0){

			//calcul score infoNutri avec un coefficient*2
			foreach ($produitProperty->infoNutri as $key_i => $infoNutri) {
				foreach ($organisations as $key_o => $organisation) {
					if(in_array($infoNutri,$organisation["infoNutri"])){
						$organisations[$key_o]["score"]+=$coef*2;
					}
				}
			}

			//calcul score equipement
			foreach ($produitProperty->equipement as $key_e => $etat_equipement) {
				foreach ($organisations as $key_o => $organisation) {
					if($organisation["equipement"][$key_e]==$etat_equipement){
						$organisations[$key_o]["score"]+=$coef;
					}
				}
			}

			//quel est le scrore maximal
			$max = 0;
			foreach ($organisations as $organisation) {
				$max = ($organisation["score"]>$max)?$organisation["score"]:$max; 
			}

			// echo "MAX : ".$max."\n";

			$organisationEligible = array();

			foreach ($organisations as $key=>$organisation) {
				if($organisation["score"]==$max){
					$val = array();
					$val["id"]=$key;
					$val["detail"]=$organisation;
					$organisationEligible[]=$val;
				}
			}

			if(count($organisationEligible)>0){
				//séléction aléatoire parmi les organisations éligible
				$result = $organisationEligible[rand(0,count($organisationEligible)-1)]["id"];

				/*echo "elu : ".$result."\n";

				var_dump($organisationEligible);

				var_dump($organisations);*/
			}
		}

		return $result;
	}

	public function sendMailToTransporter($entreprise_id,$data){
		$transporteur = $this->entreprise_model->getEmployeEntreprieByPosition($entreprise_id,2);
		return sendMailToDHL($entreprise_id,$transporteur[0]->email,$data);
	}

	public function addNewPeriodeStrategie(){
		$nbJour = is_numeric($this->input->post('nbJour')) ? (float) $this->input->post('nbJour') : 0;
		$nbSemaine = is_numeric($this->input->post('nbSemaine')) ? (float) $this->input->post('nbSemaine') : 0;
		$nbMois = is_numeric($this->input->post('nbMois')) ? (float) $this->input->post('nbMois') : 0;

		
		if(!($nbJour == 0 && $nbSemaine == 0 && $nbMois == 0)){
			$nbJourInWeek = 7;
			$nbJourInMonth = 30;

			$nbJourTotal = $nbJour + ($nbSemaine * $nbJourInWeek) + ($nbMois * $nbJourInMonth);
			$label = $this->getlabelPeriode( $nbJourTotal );
			
			$insert_id = $this->Vente_model->addNewPeriodeStrategie( $label, $nbJourTotal);

			$result['message'] = "Enregistrement effectué";
			$result['id_periode'] = $insert_id;
			$result['label'] = $label;
			$result['success'] = true;

		}
		else{
			$result['message'] = "La période de réduction est invalide";
		}

		header('Content-Type: application/json');
		echo json_encode($result);
	
	}

	public function getlabelPeriode( $nbJour ){
		$label = "";

		$nbMoisLabel = 0;
		$nbSemaineLabel = 0;
		$nbJourLabel = 0;

		if( $nbJour >= 30 ){
			
			$nbMoisLabel = floor( $nbJour/30 );
			$nbJourRestant = $nbJour % 30;

			$nbSemaineLabel = floor( $nbJourRestant/7 );
			$nbJourLabel = $nbJourRestant % 7;

		}else{
			$nbSemaineLabel = floor(  $nbJour/7 );
			$nbJourLabel = $nbJour % 7;
		}

		if( $nbMoisLabel != 0 && $nbSemaineLabel == 0 && $nbJourLabel == 0 ){
			
			$label = $nbMoisLabel." mois";

		}else if( $nbMoisLabel == 0 && $nbSemaineLabel != 0 && $nbJourLabel == 0 ){
			
			$label = $nbSemaineLabel." semaines";

		}else if( $nbMoisLabel == 0 && $nbSemaineLabel == 0 && $nbJourLabel != 0 ){
			
			$label = $nbJourLabel." jours";

		}else if( $nbMoisLabel != 0 && $nbSemaineLabel != 0 && $nbJourLabel == 0 ){
			
			$label = $nbMoisLabel." mois et ".$nbSemaineLabel." semaines";

		}else if( $nbMoisLabel != 0 && $nbSemaineLabel == 0 && $nbJourLabel != 0 ){
			
			$label = $nbMoisLabel." mois et ".$nbJourLabel." jours";

		}else if( $nbMoisLabel == 0 && $nbSemaineLabel != 0 && $nbJourLabel != 0 ){

			$label = $nbSemaineLabel." semaines et ".$nbJourLabel." jours";

		}else{
			$label = $nbMoisLabel." mois, ".$nbSemaineLabel." semaines et ".$nbJourLabel." jours";
		}

		return $label;
	}
}