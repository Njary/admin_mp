<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produit extends MY_Controller {

	private $contrat;
	private $entreprise;

	public function __construct(){
		parent::__construct();
		$this->checkPrivilege();
		$this->load->model("Produit_model","produit");
		$this->load->model("contrat/Entente_model","entente");
		$this->load->model("organisation/Organisation_model","organisation");
		$this->load->model("entreprise/Entreprise_model","entreprise_model");
		$this->checkContrat();
	}

	protected function checkPrivilege(){
		$idUser=$this->session->userdata("idUser");

		if(empty($idUser) || $idUser<0)
			redirect(site_url("login"));
	}
	public function checkContrat(){
		$this->entreprise = $this->entente->getEntreprise($this->session->userdata("idUser"));
		$this->contrat = $this->entente->getContrat($this->session->userdata("idEntreprise"));
	}

	public function index(){
		$this->getAllProduit();
	}

	public function successSave($choix){
		$this->getAllProduit($choix);
	}

	public function getAllProduit($choix_active=0){
		$this->layout->ajouter_css('../plugins/datatables/jquery.dataTables');
		$this->layout->ajouter_js('../plugins/datatables/jquery.dataTables');
		$this->layout->ajouter_css('../css/produit');
		$this->layout->ajouter_js('../js/produit.module');

		$this->action("Liste");

		// Tous les produits en donation
		$allProduitDonation = $this->produit->getAllProduitByIdEntreprise($this->entreprise['id_entreprise'], 1);
		//Tous les produits en vente
		$allProduitVente = $this->produit->getAllProduitByIdEntreprise($this->entreprise['id_entreprise'], 2);

		
		for($i=0; $i<count($allProduitDonation); $i++){
			$id_produit = $allProduitDonation[$i]->id_produit;
			$id_donation = $allProduitDonation[$i]->id_donation;

			//Get information nutritionnelle
			$info_nutris =  $this->produit->getProduitInfoNutris($id_produit);
			$listInfoNutris = "";
			if( count($info_nutris) > 0){
				for( $k=0; $k<count($info_nutris); $k++){
					$listInfoNutris .= $info_nutris[$k]->label." , ";
				}
			}else{
				$listInfoNutris = "Aucune information nutritionnelle";
			}
			
			//Get raison surplus
			$raison_surplus = $this->produit->getProduitRaisonSurplus($id_donation);
			$listRaisonSurplus = "";
			if( count($raison_surplus) > 0){
				for($k=0; $k<count($raison_surplus); $k++){
					$listRaisonSurplus .= $raison_surplus[$k]->label." , ";
				}
			}else{
				$listRaisonSurplus = "Aucune raison de surplus";
			}

			//Get strategie marketing
			$strategie = $this->produit->getStrategieMarketing($id_donation);

			$allProduitDonation[$i]->listInfoNutris = $listInfoNutris;
			$allProduitDonation[$i]->info_nutris = $info_nutris;
			$allProduitDonation[$i]->listRaisonSurplus = $listRaisonSurplus;
			$allProduitDonation[$i]->raison_surplus = $raison_surplus;
			$allProduitDonation[$i]->strategie = $strategie;


		}


		for($j=0; $j<count($allProduitVente); $j++){
			$id_produit = $allProduitVente[$j]->id_produit;
			$id_vente = $allProduitVente[$j]->id_donation;

			//Get information nutritionnelle
			$info_nutris =  $this->produit->getProduitInfoNutris($id_produit);
			$listInfoNutris = "";
			if( count($info_nutris) > 0){
				for( $k=0; $k<count($info_nutris); $k++){
					$listInfoNutris .= $info_nutris[$k]->label." , ";
				}
			}else{
				$listInfoNutris = "Aucune information nutritionnelle";
			}
			
			//Get raison surplus
			$raison_surplus = $this->produit->getProduitRaisonSurplus($id_vente);
			$listRaisonSurplus = "";
			if( count($raison_surplus) > 0){
				for($k=0; $k<count($raison_surplus); $k++){
					$listRaisonSurplus .= $raison_surplus[$k]->label." , ";
				}
			}else{
				$listRaisonSurplus = "Aucune raison de surplus";
			}

			//Get strategie marketing
			$strategie = $this->produit->getStrategieMarketing($id_vente);

			//Get adresse ramasssage
			$adresse = $this->produit->getAdresseRamassage($id_vente);

			$allProduitVente[$j]->listInfoNutris = $listInfoNutris;
			$allProduitVente[$j]->info_nutris = $info_nutris;
			$allProduitVente[$j]->listRaisonSurplus = $listRaisonSurplus;
			$allProduitVente[$j]->raison_surplus = $raison_surplus;
			$allProduitVente[$j]->strategie = $strategie;
			$allProduitVente[$j]->adresse = $adresse;
			//var_dump( $strategie);

		}

		//var_dump($allProduitVente);
		//die();
		/*var_dump($allProduitDonation);
		die();*/
		
		$this->layout->addView(
			"produit", 
			array(
				
				'listProduitDonation' => $allProduitDonation,
				'listProduitVente' => $allProduitVente,
				'choix_active' => $choix_active
			)
		);

		$this->layout->view();
	}



	public function create_info_nutri(){
		$label = $this->input->post("info_nutri");
		header('Content-Type: application/json');
	
		$type_id = $this->produit->addInfoNutri($label);

		echo json_encode(array("info_id"=>$type_id));
	}

	public function changeProduitToDonation(){
		 
		$id_donation = is_numeric($this->input->post('donation_identity')) ? (int) $this->input->post('donation_identity') : -1;

		if($id_donation > 0){

			$produit_id = $this->produit->getIdProduitByDonation($id_donation);

			/*var_dump($produit_id);
			die();*/

			if(count($produit_id)==1){
				$id_produit = $produit_id[0]->produit_id_produit;
				$infoNutriForBenef = $this->produit->getInfoNutriForBenef($id_produit);
				$infoEquipmentForBenef = $this->produit->getEquipementForBenef($id_produit);

				$infoNutris = array();
				for($i=0; $i<count($infoNutriForBenef);$i++){
					$infoNutris[] = $infoNutriForBenef[$i]->information_nutritionnelle_id;
				}
				$refrigere = 0;
				$congele = 0;
				for($j=0;$j<count($infoEquipmentForBenef);$j++){
					if($infoEquipmentForBenef[$j]->equipement_id == 1){
						$congele = $infoEquipmentForBenef[$j]->etat;
					}else if($infoEquipmentForBenef[$j]->equipement_id == 2){
						$refrigere = $infoEquipmentForBenef[$j]->etat;
					}
				}

				$produitProperty = (object) array("infoNutri"=>$infoNutris,"equipement"=>array(2=>(int)$refrigere,1=>(int)$congele));
				$organisation_id = $this->findBeneficiaire($produitProperty);

				$op_change = $this->produit->changeToDonation($id_donation, $organisation_id);


				if($op_change){
					$result['success'] = true;
					$result['message'] = "Le produit a bien été affecté en donation";
				}else{
					$result['success'] = false;
					$result['message'] = "Erreur lors de la modification du statut du produit";
				}
				/*var_dump($infoNutris);
				var_dump($congele);
				var_dump($refrigere);*/
				/*var_dump($infoNutriForBenef);
				var_dump($infoEquipmentForBenef);*/
				//die();
				
			}else{
				
				$result['success'] = false;
				$result['message'] = "Erreur lors de la recolte d'information sur le produit";
			}

		}else{
			$result['success'] = false;
			$result['message'] = "Erreur d'identifiant de produit";
		}

		header('Content-Type: application/json');
		echo json_encode($result);
	}

	public function findBeneficiaire($produitProperty){
		$organisations = array();

		$coef = 1;

		$result = 0;

		// $produitProperty = (object) array("infoNutri"=>array(1,4),"equipement"=>array(2=>1,1=>0));

		$organisation_infoNutri = $this->organisation->getOrganisationsInfoNutri();
		$organisation_equipement = $this->organisation->getOrganisationsEquipement();

		foreach ($organisation_infoNutri as $value) {
			$organisations[$value->id_organisation]["infoNutri"][]=(int)$value->information_nutritionnelle_id;
		}

		foreach ($organisation_equipement as $value) {
			$organisations[$value->id_organisation]["equipement"][$value->equipement_id]=(int)$value->etat;
		}

		foreach ($organisations as $key=>$organisation) {
			$organisations[$key]["score"]=0;
		}

		if(count($organisations)>0){

			//calcul score infoNutri avec un coefficient*2
			foreach ($produitProperty->infoNutri as $key_i => $infoNutri) {
				foreach ($organisations as $key_o => $organisation) {
					if(in_array($infoNutri,$organisation["infoNutri"])){
						$organisations[$key_o]["score"]+=$coef*2;
					}
				}
			}

			//calcul score equipement
			foreach ($produitProperty->equipement as $key_e => $etat_equipement) {
				foreach ($organisations as $key_o => $organisation) {
					if($organisation["equipement"][$key_e]==$etat_equipement){
						$organisations[$key_o]["score"]+=$coef;
					}
				}
			}

			//quel est le scrore maximal
			$max = 0;
			foreach ($organisations as $organisation) {
				$max = ($organisation["score"]>$max)?$organisation["score"]:$max; 
			}

			// echo "MAX : ".$max."\n";

			$organisationEligible = array();

			foreach ($organisations as $key=>$organisation) {
				if($organisation["score"]==$max){
					$val = array();
					$val["id"]=$key;
					$val["detail"]=$organisation;
					$organisationEligible[]=$val;
				}
			}

			if(count($organisationEligible)>0){
				//séléction aléatoire parmi les organisations éligible
				$result = $organisationEligible[rand(0,count($organisationEligible)-1)]["id"];

				/*echo "elu : ".$result."\n";

				var_dump($organisationEligible);

				var_dump($organisations);*/
			}
		}

		return $result;
	}

}