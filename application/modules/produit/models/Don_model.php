<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Don_model extends CI_Model{

    protected $tableTypeProduit = 'type_produit';
    protected $tableTypeAliment = "type_aliment";
    protected $tableInformationNutritionnelle = "information_nutritionnelle";
    protected $tableRaisonSurplus = "raison_surplus";
    protected $tableRaisonDonation = "raison_donation";
    protected $tableProduit = "produit";
    protected $tableProduitEquipement = "produit_equipement";
    protected $tableProduitInformationNutritionnelle = "produit_information_nutritionnelle";
    protected $tablePhotoProduit = "photo_produit";
    protected $tableDonation = "donation";
    protected $tableAdresseEntreprise = "adresse_entreprise";
    protected $tableAdresse = "adresse";
    protected $tableEntrepriseType = "entreprise_type";
    protected $tableAlimentType = "entreprise_type_aliment";
    protected $table_type_aliment_information_nutritionnelle = "type_aliment_information_nutritionnelle";
    protected $tableEntreprise = "entreprise";
    protected $tableStrategieReduction = "strategie_reduction";
    protected $tablePeriodeReduction = "periode_reduction";
    protected $tablePeriodeRecuperationCommande = "periode_recuperation_commande";

    public function getCategories($idEA=0){
    	$result = $this->db->query('SELECT * FROM '.$this->tableTypeAliment.' WHERE proprietaire=0 OR proprietaire='.$idEA)->result();
    	return $result;
    }

    public function getTypes($idEA=0){
    	$result = $this->db->query('SELECT * FROM '.$this->tableTypeProduit.' WHERE proprietaire=0 OR proprietaire='.$idEA)->result();
    	return $result;
    }

    public function getInfoNutris($idEA=0){
    	$result = $this->db->query('SELECT * FROM '.$this->tableInformationNutritionnelle.' WHERE proprietaire=0 OR proprietaire='.$idEA)->result();
    	return $result;
    }

    public function getRaisonSurplus($idEA=0){
    	$result = $this->db->query('SELECT * FROM '.$this->tableRaisonSurplus.' WHERE proprietaire=0 OR proprietaire='.$idEA)->result();
    	return $result;
    }

    public function addNewCategorie($categorie, $idEA=0){
        $data = array('label'=>$categorie,'proprietaire'=>$idEA);
        $result = array(
            "exist" => false,
            "id_type_aliment" => 0
        );
        $exist = $this->db->query('SELECT * FROM '.$this->tableTypeAliment.' WHERE LOWER(label)="'.strtolower($categorie).'" AND (proprietaire=0 OR proprietaire='.$idEA.')')->result();
        if(count($exist) >= 1){
            $result['exist'] = true;
        }
        else{
            $this->db->insert($this->tableTypeAliment, $data);
            $result['id_type_aliment'] = $this->db->insert_id();
        }
        return $result;
    }

    public function linkToInfoNutris($idCategorie, $infoNutris){
        $datas = explode(',', $infoNutris);
        for($i=0; $i<count($datas); $i++){
            $id = (int) $datas[$i];
            $data = array(
                "type_aliment_id"=>$idCategorie,
                "information_nutritionnelle_id"=>$id
            );
            $this->db->insert($this->table_type_aliment_information_nutritionnelle, $data);
        }
    }

    public function getInfoNutrisLinked($idCategorie){
        $this->db->select($this->table_type_aliment_information_nutritionnelle.'.information_nutritionnelle_id');
        $this->db->from($this->table_type_aliment_information_nutritionnelle);
        $this->db->join($this->tableTypeAliment, $this->tableTypeAliment.'.id_type_aliment='.$this->table_type_aliment_information_nutritionnelle.'.type_aliment_id');
        $this->db->where($this->table_type_aliment_information_nutritionnelle.'.type_aliment_id', $idCategorie);
        $result = $this->db->get()->result();
        $return = array();
        foreach($result as $r){
            $return[] = $r->information_nutritionnelle_id;
        }
        return $return;
    }

    public function addNewType($type, $idEA=0){
        $data = array('label'=>$type,'proprietaire'=>$idEA);
        $result = array(
            "exist" => false,
            "id_type_produit" => 0
        );
        $exist = $this->db->query('SELECT * FROM '.$this->tableTypeProduit.' WHERE LOWER(label)="'.strtolower($type).'" AND (proprietaire=0 OR proprietaire='.$idEA.')')->result();
        if(count($exist) >= 1){
            $result['exist'] = true;
        }
        else{
            $this->db->insert($this->tableTypeProduit, $data);
            $result['id_type_produit'] = $this->db->insert_id();
        }
        return $result;
    }

    public function addNewInfoNutris($infoNutris, $idEA=0){
        $data = array('label'=>$infoNutris,'proprietaire'=>$idEA);
        $result = array(
            "exist" => false,
            "id_information_nutritionnelle" => 0
        );
        $exist = $this->db->query('SELECT * FROM '.$this->tableInformationNutritionnelle.' WHERE LOWER(label)="'.strtolower($infoNutris).'" AND (proprietaire=0 OR proprietaire='.$idEA.')')->result();
        if(count($exist) >= 1){
            $result['exist'] = true;
        }
        else{
            $this->db->insert($this->tableInformationNutritionnelle, $data);
            $result['id_information_nutritionnelle'] = $this->db->insert_id();
        }
        return $result;
    }

    public function addNewRaisonSurplus($raisonSurplus, $idEA=0){
        $data = array('label'=>$raisonSurplus,'proprietaire'=>$idEA);
        $result = array(
            "exist" => false,
            "id_raison_surplus" => 0
        );
        $exist = $this->db->query('SELECT * FROM '.$this->tableRaisonSurplus.' WHERE LOWER(label)="'.strtolower($raisonSurplus).'" AND (proprietaire=0 OR proprietaire='.$idEA.')')->result();
        if(count($exist) >= 1){
            $result['exist'] = true;
        }
        else{
            $this->db->insert($this->tableRaisonSurplus, $data);
            $result['id_raison_surplus'] = $this->db->insert_id();
        }
        return $result;
    }

    public function canMakeDonation($idEA){
        $this->db->from($this->tableAdresseEntreprise);
        $this->db->where('entreprise_id', $idEA);
        $adresseOk = count($this->db->get()->result()) > 0 ? true : false;
        $this->db->from($this->tableEntrepriseType);
        $this->db->where('entreprise_id', $idEA);
        $type = count($this->db->get()->result()) > 0 ? true : false;
        $this->db->from($this->tableAlimentType);
        $this->db->where('entreprise_id', $idEA);
        $typeAliment = count($this->db->get()->result()) > 0 ? true : false;
        if($adresseOk && $type && $typeAliment){
            return true;
        }
        else{
            return false;
        }
    }

    public function getPhotoPoduits($nom, $idEA){
        $query = "SELECT ph.id_photo_produit, ph.url FROM photo_produit ph JOIN produit pr ON pr.id_produit=ph.produit_id WHERE pr.nom_produit LIKE '%".$nom."%' AND (ph.proprietaire=0 OR ph.proprietaire=".$idEA.")";
        if(empty($nom)){
            $query .= ' LIMIT 0, 25';
        }
        $data = $this->db->query($query)->result();
        $result = array();
        foreach($data as $row){
            if(file_exists($row->url)){
                $inArray = false;
                for($i = 0; $i<count($result); $i++){
                    if(strcmp($row->url, $result[$i]['url']) == 0){
                        $inArray = true;
                        break;
                    }
                }
                if(!$inArray){
                    $result[] = array(
                        "id_photo_produit"=>$row->id_photo_produit,
                        "url"=>$row->url
                    );
                }
            }
        }
        return $result;
    }

    public function pushDonation( $idEA,$organisation_id, $idProduit, $quantite, $typeDate, $datePeremption, $moment, $raisonSurplus, $isDraft){
        
        if($isDraft == 0){
            $etat_donation_id = 2;
        }else if($isDraft == 1){
            $etat_donation_id = 5; //Brouillon
        }else{
            $etat_donation_id = 2;
        }

        $data = array(
            "entreprise_id" => $idEA,
            "produit_id_produit" => $idProduit,
            "moment" => $moment,
            "quantite" => $quantite,
            "date_peremption_produit" => $datePeremption,
            "organisation_id"=>$organisation_id,
            "etat_donation_id"=>$etat_donation_id,
            "action"=>1, //car Donation
            "type_date" => $typeDate
        );
        $this->db->insert($this->tableDonation, $data);
        $idDonation = $this->db->insert_id();
        for($i=0; $i<count($raisonSurplus); $i++){
            if($raisonSurplus[$i] > 0){
                $data = array(
                    'raison_surplus_id' => $raisonSurplus[$i],
                    'donation_id' => $idDonation
                );
                $this->db->insert($this->tableRaisonDonation, $data);
            }
        }
    }

    public function pushUpdateDonation($id_donation, $idEA,$organisation_id, $idProduit, $quantite, $typeDate , $datePeremption, $moment, $raisonSurplus, $isDraft){
        
        if($isDraft == 0){
            $etat_donation_id = 2;
        }else if($isDraft == 1){
            $etat_donation_id = 5; //Brouillon
        }else{
            $etat_donation_id = 2;
        }

        $data = array(
            "entreprise_id" => $idEA,
            "produit_id_produit" => $idProduit,
            "moment" => $moment,
            "quantite" => $quantite,
            "date_peremption_produit" => $datePeremption,
            "organisation_id"=>$organisation_id,
            "etat_donation_id"=>$etat_donation_id,
            "action"=>1, //car Donation
            "type_date" => $typeDate
        );

        $this->db->where($this->tableDonation.'.id_donation = '.$id_donation);
        $this->db->update($this->tableDonation, $data);
        
        $this->db->where($this->tableRaisonDonation.'.donation_id = '.$id_donation);
        $this->db->delete($this->tableRaisonDonation);
        for($i=0; $i<count($raisonSurplus); $i++){
             if($raisonSurplus[$i] > 0){
                 $data = array(
                    'raison_surplus_id' => $raisonSurplus[$i],
                    'donation_id' => $id_donation
                );
                $this->db->insert($this->tableRaisonDonation, $data);
             }
        }
    }

    public function saveDonation($idEA,$organisation_id, $idProduit, $quantite, $datePeremption){
        $data = array(
            "entreprise_id" => $idEA,
            "produit_id_produit" => $idProduit,
            "moment" => date('Y-m-d H:i:s'),
            "quantite" => $quantite,
            "date_peremption_produit" => $datePeremption,
            "organisation_id"=>$organisation_id,
            "etat_donation_id"=>2
        );
        $this->db->insert($this->tableDonation, $data);
    }

    public function saveProduit($nom, $categorie, $type, $descr, $infoNutris, $poids, $volume, $prix, $refrigere, $congele, $consoDirect, $groupement){
        $data = array(
            'nom_produit' => $nom,
            'type_produit_id' => $type,
            'description' => $descr,
            'poids' => $poids,
            'type_aliment_id' => $categorie,
            'volume' => $volume,
            'prix' => $prix,
            'groupement'=>$groupement
        );
        $this->db->insert($this->tableProduit, $data);
        $idProduit = $this->db->insert_id();
        
        $data = array(
            'produit_id' => $idProduit, 
            'equipement_id' => 1, // frigo
            'etat' => $congele
        );
        $this->db->insert($this->tableProduitEquipement, $data);
        $data = array(
            'produit_id' => $idProduit, 
            'equipement_id' => 2, // Réfrigérateur
            'etat' => $refrigere
        );
        $this->db->insert($this->tableProduitEquipement, $data);
        $data = array(
            'produit_id' => $idProduit, 
            'equipement_id' => 4, // Pas d'équipement (Consommation directe)
            'etat' => $consoDirect
        );
        $this->db->insert($this->tableProduitEquipement, $data);

        for($i=0; $i<count($infoNutris); $i++){
            if($infoNutris[$i]>0){
                $data = array(
                    'produit_id' => $idProduit,
                    'information_nutritionnelle_id' => $infoNutris[$i]
                );
                $this->db->insert($this->tableProduitInformationNutritionnelle, $data);
            }
            
        }

        return $idProduit;
    }



     public function updateProduit($id_produit, $nom, $categorie, $type, $descr, $infoNutris, $poids, $volume, $prix, $refrigere, $congele, $consoDirect, $groupement){
        $data = array(
            'nom_produit' => $nom,
            'type_produit_id' => $type,
            'description' => $descr,
            'poids' => $poids,
            'type_aliment_id' => $categorie,
            'volume' => $volume,
            'prix' => $prix,
            'groupement'=>$groupement
        );

        $this->db->where($this->tableProduit.'.id_produit = '.$id_produit);
        $this->db->update($this->tableProduit, $data);
        
        $data = array(
            'equipement_id' => 1, // frigo
            'etat' => $congele
        );
        $this->db->where($this->tableProduitEquipement.'.produit_id = '.$id_produit.' and equipement_id = 1');
        $this->db->update($this->tableProduitEquipement, $data);

        $data = array(
            'equipement_id' => 2, // Réfrigérateur
            'etat' => $refrigere
        );
        $this->db->where($this->tableProduitEquipement.'.produit_id = '.$id_produit.' and equipement_id = 2');
        $this->db->update($this->tableProduitEquipement, $data);

        $data = array(
            'equipement_id' => 4, // Pas d'équipement (Consommation directe)
            'etat' => $consoDirect
        );
        $this->db->where($this->tableProduitEquipement.'.produit_id = '.$id_produit.' and equipement_id = 4');
        $this->db->update($this->tableProduitEquipement, $data);

        $this->db->where($this->tableProduitInformationNutritionnelle.'.produit_id = '.$id_produit);
        $this->db->delete($this->tableProduitInformationNutritionnelle);
        for($i=0; $i<count($infoNutris); $i++){
            if($infoNutris[$i]>0){
                $data = array(
                    'produit_id' => $id_produit,
                    'information_nutritionnelle_id' => $infoNutris[$i]
                );
                $this->db->insert($this->tableProduitInformationNutritionnelle, $data);
            }
            
        }

    }


    public function saveDonationImport($idEA,$organisation_id, $idProduit, $quantite, $datePeremption, $moment, $raisonSurplus){
        $data = array(
            "entreprise_id" => $idEA,
            "produit_id_produit" => $idProduit,
            "moment" => $moment,
            "quantite" => $quantite,
            "date_peremption_produit" => $datePeremption,
            "organisation_id"=>$organisation_id,
            "etat_donation_id"=>2
        );
        $this->db->insert($this->tableDonation, $data);
        $idDonation = $this->db->insert_id();

        for($i=0; $i<count($raisonSurplus); $i++){
            $in = trim($raisonSurplus[$i]);
            if(!empty($in)){
                $inInDb = $this->db->query('SELECT * FROM '.$this->tableRaisonSurplus.' WHERE LOWER(label)="'.$in.'" AND (proprietaire=0 OR proprietaire='.$idEA.')')->result();
                if(count($inInDb) > 0){
                    $in = $inInDb[0]->id_raison_surplus;
                }
                else{
                    $this->db->insert($this->tableRaisonSurplus, array('label'=>$in, 'proprietaire'=>$idEA));
                    $in = $this->db->insert_id();
                }
                $data = array(
                    'donation_id' => $idDonation,
                    'raison_surplus_id' => $in
                );
                $this->db->insert($this->tableRaisonDonation, $data);
            }
        }
    }

    public function saveProduitImport($nom, $categorie, $type, $descr, $infoNutris, $poids, $volume, $prix, $refrigere, $congele, $idEA, $groupement){
        $typeInDb = $this->db->query('SELECT * FROM '.$this->tableTypeProduit.' WHERE LOWER(label)="'.strtolower($type).'" AND (proprietaire=0 OR proprietaire='.$idEA.')')->result();
        $typeID = 0;
        if(count($typeInDb) > 0){
            $typeID = $typeInDb[0]->id_type_produit;
        }
        else{
            $this->db->insert($this->tableTypeProduit, array('label'=>$type, 'proprietaire'=>$idEA));
            $typeID = $this->db->insert_id();
        }
        $typeCategDb = $this->db->query('SELECT * FROM '.$this->tableTypeAliment.' WHERE LOWER(label)="'.strtolower($categorie).'" AND (proprietaire=0 OR proprietaire='.$idEA.')')->result();
        $categID = 0;
        if(count($typeCategDb) > 0){
            $categID = $typeCategDb[0]->id_type_aliment;
        }
        else{
            $this->db->insert($this->tableTypeAliment, array('label'=>$type, 'proprietaire'=>$idEA));
            $categID = $this->db->insert_id();
        }
        $data = array(
            'nom_produit' => $nom,
            'type_produit_id' => $typeID,
            'description' => $descr,
            'poids' => $poids,
            'volume' => $volume,
            'type_aliment_id' => $categID,
            'prix' => $prix,
            'groupement'=>$groupement
        );
        $this->db->insert($this->tableProduit, $data);
        $idProduit = $this->db->insert_id();

        $congele = strcasecmp($congele, 'oui') == 0 ? 1 : 0;
        $data = array(
            'produit_id' => $idProduit, 
            'equipement_id' => 1, // frigo
            'etat' => $congele
        );
        $this->db->insert($this->tableProduitEquipement, $data);
        
        $refrigere = strcasecmp($refrigere, 'oui') == 0 ? 1 : 0;
        $data = array(
            'produit_id' => $idProduit, 
            'equipement_id' => 2, // Réfrigérateur
            'etat' => $refrigere
        );
        $this->db->insert($this->tableProduitEquipement, $data);
        
        for($i=0; $i<count($infoNutris); $i++){
            $in = trim($infoNutris[$i]);
            if(!empty($in)){
                $inInDb = $this->db->query('SELECT * FROM '.$this->tableInformationNutritionnelle.' WHERE LOWER(label)="'.strtolower($in).'" AND (proprietaire=0 OR proprietaire='.$idEA.')')->result();
                if(count($inInDb) > 0){
                    $in = $inInDb[0]->id_information_nutritionnelle;
                }
                else{
                    $this->db->insert($this->tableInformationNutritionnelle, array('label'=>$in));
                    $in = $this->db->insert_id();
                }
                $data = array(
                    'produit_id' => $idProduit,
                    'information_nutritionnelle_id' => $in
                );
                $this->db->insert($this->tableProduitInformationNutritionnelle, $data);
            }
        }

        $result = array(
            'id_produit' => $idProduit,
            'nom' => $nom,
            'type' => $type
        );
        return $result;
    }

    public function savePhotoProduit($idProduit, $url, $idPhoto, $idEA){
        if($url != null){
            $data = array(
                'produit_id' => $idProduit,
                'url' => $url,
                'proprietaire' => $idEA
            );
        }
        else{
            $this->db->where('id_photo_produit', $idPhoto);
            $urlInDB = $this->db->get($this->tablePhotoProduit)->result()[0]->url;
            $data = array(
                'produit_id' => $idProduit,
                'url' => $urlInDB,
                'proprietaire' => $idEA
            );
        }
        
        $this->db->insert($this->tablePhotoProduit, $data);
    }
    public function updatePhotoProduit($idProduit, $url, $idPhoto, $idEA){
        if($url != null){

            $data = array(
                'produit_id' => $idProduit,
                'url' => $url,
                'proprietaire' => $idEA
            );
        }
        else{
            $this->db->where('id_photo_produit', $idPhoto);
            $urlInDB = $this->db->get($this->tablePhotoProduit)->result()[0]->url;

            $data = array(
                'produit_id' => $idProduit,
                'url' => $urlInDB,
                'proprietaire' => $idEA
            );
        }
        
        $this->db->insert($this->tablePhotoProduit, $data);
    }

    public function DeletePhotoProduit($idProduit, $notToDelete=[]){

        $ignore = array();
        for($i=0; $i<count($notToDelete); $i++){
            $ignore[] = $notToDelete[$i];
        }
        if(count($ignore)>0){
            $this->db->where_not_in($this->tablePhotoProduit.'.id_photo_produit', $ignore);
        }

        $this->db->where($this->tablePhotoProduit.'.produit_id = '.$idProduit);
        $this->db->delete($this->tablePhotoProduit);

    }

    public function updateAdresseRamassage( $idDonation, $value){
        $data = array(
            "adresse_ramassage_id" => $value
        );
        $this->db->where("id_donation", $idDonation);
        $this->db->update($this->tableDonation, $data);
    }


    private function getBorneCoordonneesGeo($latitude){
        $intervalle = array();
        $intervalle["borne_inf"] = $latitude-0.022996;
        $intervalle["borne_sup"] = $latitude+0.022996;
        return $intervalle;
    }

    public function findByCondition($indexStart,$resultLength,$typeMotsCle,$motCle,$type_produit_id,$type_aliment_id,$prix_min,$prix_max,$date_limite, $info_nutr, $latitude,$longitude, $type_produit, $listeEntreprise){
        
        $indexStart = ($indexStart != null)? $indexStart:0;
        $resultLength = ($resultLength != null)? $resultLength:10;

        // LES VARIABLES POUR LES CONDITIONS

        $les_id_du_produits_prix = [];
        $produit_id_par_nom = [];
        $entreprise_id_par_nom = [];
        $les_id_du_produits_date_limite = [];
        $les_id_du_produits_info_nutr = [];
        $les_id_du_type_produit = [];
        $les_id_des_entreprises = [];
        

        // CREATION DES CONDITIONS A PARTIR DES VARIABLES

        /* formatage clé info_nutr */
        $info_nutr_to_string = null;

        if($info_nutr != null){
            $info_nutr_array = explode("#", $info_nutr);
            /*var_dump($info_nutr_array);
            die();*/
            $info_nutr_to_string = "(";
            for($i=1;$i<count($info_nutr_array);$i++){
                
                if($i == count($info_nutr_array) - 1){
                    $info_nutr_to_string .= $info_nutr_array[$i]; 
                }else{
                    $info_nutr_to_string .= $info_nutr_array[$i].",";
                }
            }
            $info_nutr_to_string .= ")";

        }
        /* formatage clé info_nutritionnelle */

        /* formatage clé type_produit*/
        $type_produit_to_string = null;

        if($type_produit != null){
            $type_produit_array = explode("#", $type_produit);
            
            $type_produit_to_string = "(";
            for($i=1;$i<count($type_produit_array);$i++){
                
                if($i == count($type_produit_array) - 1){
                    $type_produit_to_string .= $type_produit_array[$i]; 
                }else{
                    $type_produit_to_string .= $type_produit_array[$i].",";
                }
            }
            $type_produit_to_string .= ")";

        }

        /* formatage clé type_produit */

        /* formatage clé Entreprise*/
        $liste_entreprise_to_string = null;

        if($listeEntreprise != null){
            $liste_entreprise_array = explode("#", $listeEntreprise);
           
            $liste_entreprise_to_string = "(";
            for($i=1;$i<count($liste_entreprise_array);$i++){
                
                if($i == count($liste_entreprise_array) - 1){
                    $liste_entreprise_to_string .= $liste_entreprise_array[$i]; 
                }else{
                    $liste_entreprise_to_string .= $liste_entreprise_array[$i].",";
                }
            }
            $liste_entreprise_to_string .= ")";

        }

        /* formatage clé Entreprise */ 

        // typeMotCle 0 => OU inclusif, 1:"Nom entreprise", 2:"Nom produit" 

        // remplir les entreprise_id_par_nom
        $query = "SELECT id_produit FROM ".$this->tableProduit. " WHERE nom_produit LIKE '%".$motCle."%'";
        $produit_ids = $this->db->query($query)->result();
        if($produit_ids != null)
        {
            foreach ($produit_ids as $produit_id) {
                array_push($produit_id_par_nom,$produit_id->id_produit);
            }
        }

        // remplir les produit_id_par_nom
        $query = "SELECT id_entreprise FROM ".$this->tableEntreprise. " WHERE nom_entreprise LIKE '%".$motCle."%'";
        $entreprise_ids = $this->db->query($query)->result();
        /*var_dump($entreprise_ids);
        die();*/
        if($entreprise_ids != null)
        {
            foreach ($entreprise_ids as $entreprise_id) {
                array_push($entreprise_id_par_nom,$entreprise_id->id_entreprise);
            }
        }

        if($date_limite != null){

            // formatage des dates
            $date_limite = str_replace("/","-",$date_limite);
            $dlim = date_create($date_limite);
            $date_limite = date_create($date_limite);

            $date_debut_limite = date_create('00-00-0000');
            $date_debut_limite = date_sub( $dlim, date_interval_create_from_date_string('14 days') ); // - 2 semaines

            $date_limite = date_format($date_limite,"Y-m-d H:i:s");
            $date_debut_limite = date_format($date_debut_limite,"Y-m-d H:i:s");

            // formatage des dates
            $this->db->select("produit_id_produit");
            $this->db->from($this->tableDonation);
            $date_condition = array('date_peremption_produit <=' => $date_limite, 'date_peremption_produit >=' => $date_debut_limite);
            $this->db->where($date_condition);
            $produit_ids = $this->db->get()->result();

            // si on'a pas trouvé le produit avec le nom d'entreprise $nom_entrepriset on retourne null
            if($produit_ids == null)
                return;
            else{
                foreach ($produit_ids as $produit_id) {
                    array_push($les_id_du_produits_date_limite,$produit_id->produit_id_produit);
                }
            }

        }

        $date_now = date_create('now')->format('Y-m-d H:i:s');
        
        $mes_conditions = $this->tableDonation.".action = 2 AND quantite > 0 AND date_peremption_produit >= '".$date_now."'";

        if($type_aliment_id != null){
            $mes_conditions .= " AND ".$this->tableProduit.".type_aliment_id = ".$type_aliment_id ; 
        }
        if($type_produit_id != null){
            $mes_conditions .= " AND ".$this->tableProduit.".type_produit_id = ".$type_produit_id ; 
        }
        
        if(count($les_id_du_produits_date_limite) > 0){
            $les_id_du_produits_date_limite_to_string = "(";
            for($i=0;$i<count($les_id_du_produits_date_limite);$i++){
                if(count($les_id_du_produits_date_limite) == 1){
                       $les_id_du_produits_date_limite_to_string .= $les_id_du_produits_date_limite[$i]. ")";
                }
                else
                {
                    if($i==count($les_id_du_produits_date_limite)-1){
                    $les_id_du_produits_date_limite_to_string .= $les_id_du_produits_date_limite[$i]. ")";                    
                }
                    else{
                         $les_id_du_produits_date_limite_to_string .= $les_id_du_produits_date_limite[$i]. ",";                      
                    }
                }
            }

            $mes_conditions .= " AND ".$this->tableDonation. ".produit_id_produit IN ".$les_id_du_produits_date_limite_to_string;
        }

        // typeMotCle 0 => OU inclusif, 1:"Nom entreprise", 2:"Nom produit" 
        if($typeMotsCle == 1){
            if(count($entreprise_id_par_nom) > 0){

                $entreprise_id_par_nom_to_string = "(";
                if(count($entreprise_id_par_nom) == 1){
                   $entreprise_id_par_nom_to_string .= $entreprise_id_par_nom[0];
                }
                else{        
                    for($i=0;$i<count($entreprise_id_par_nom);$i++){
                        if($i==count($entreprise_id_par_nom)-1){
                            $entreprise_id_par_nom_to_string .= $entreprise_id_par_nom[$i];                    
                        }
                        else{
                             $entreprise_id_par_nom_to_string .= $entreprise_id_par_nom[$i]. ",";                      
                        }
                    }
                }
                $mes_conditions .= " AND ".$this->tableDonation. ".entreprise_id IN ".$entreprise_id_par_nom_to_string. ")";
            }
            // on ne trouve pas de produit associé au nom entreprise
            else{
                return;
            }
        }
        else if($typeMotsCle == 2){
            if(count($produit_id_par_nom) > 0){

                $produit_id_par_nom_to_string = "(";  
                if(count($produit_id_par_nom) == 1){
                   $produit_id_par_nom_to_string .= $produit_id_par_nom[0];
                }
                else{      
                    for($i=0;$i<count($produit_id_par_nom);$i++){
                    
                        if($i==count($produit_id_par_nom)-1){
                            $produit_id_par_nom_to_string .= $produit_id_par_nom[$i];                    
                        }
                        else{
                             $produit_id_par_nom_to_string .= $produit_id_par_nom[$i]. ",";                      
                        }
                    }
                }

                $mes_conditions .= " AND ".$this->tableDonation. ".produit_id_produit IN ".$produit_id_par_nom_to_string. ")";
            }
            // on ne trouve pas de produit associé au nom produit
            else{
                return;
            }
        }
        else{
            // on ne trouve aucun nom produit et aucun nom entreprise
            if(count($entreprise_id_par_nom) == 0 && count($produit_id_par_nom) == 0){
                return ; 
            }
            
            else if(count($produit_id_par_nom) > 0){
                $produit_id_par_nom_to_string = "(";        
                if(count($produit_id_par_nom) == 1){
                   $produit_id_par_nom_to_string .= $produit_id_par_nom[0];
                }else{
                    for($i=0;$i<count($produit_id_par_nom);$i++){                    
                        if($i==count($produit_id_par_nom)-1){
                            $produit_id_par_nom_to_string .= $produit_id_par_nom[$i];                    
                        }
                        else{
                             $produit_id_par_nom_to_string .= $produit_id_par_nom[$i]. ",";                      
                        }
                    }
                }    
                $mes_conditions .= " AND ( ".$this->tableDonation. ".produit_id_produit IN ".$produit_id_par_nom_to_string. ")";
                
                if(count($entreprise_id_par_nom) > 0){
                    $entreprise_id_par_nom_to_string = "("; 
                    if(count($entreprise_id_par_nom) == 1){
                        $entreprise_id_par_nom_to_string .= $entreprise_id_par_nom[0];
                    }else{
                        for($i=0;$i<count($entreprise_id_par_nom);$i++){
                            if($i==count($entreprise_id_par_nom)-1){
                                $entreprise_id_par_nom_to_string .= $entreprise_id_par_nom[$i];                    
                            }
                            else{
                                 $entreprise_id_par_nom_to_string .= $entreprise_id_par_nom[$i]. ",";                      
                            }
                        }
                    }       
                    $mes_conditions .= " OR ".$this->tableDonation. ".entreprise_id IN ".$entreprise_id_par_nom_to_string. "))";
                }else{
                    $mes_conditions .= ")";
                }                
            }
            else{
            
                $entreprise_id_par_nom_to_string = "(";        
                if(count($entreprise_id_par_nom) == 1){
                   $entreprise_id_par_nom_to_string .= $entreprise_id_par_nom[0];
                }
                else{
                    for($i=0;$i<count($entreprise_id_par_nom);$i++){
                        if($i==count($entreprise_id_par_nom)-1){
                            $entreprise_id_par_nom_to_string .= $entreprise_id_par_nom[$i];                    
                        }
                        else{
                             $entreprise_id_par_nom_to_string .= $entreprise_id_par_nom[$i]. ",";                      
                        }
                    }
                }

                $mes_conditions .= " AND ".$this->tableDonation. ".entreprise_id IN ".$entreprise_id_par_nom_to_string. ")";
                            
            }

        }

        if($info_nutr_to_string != "" and $info_nutr_to_string != "()"){

            $query = "SELECT produit_id FROM ".$this->tableProduitInformationNutritionnelle." WHERE information_nutritionnelle_id IN ".$info_nutr_to_string;
            $produit_ids = $this->db->query($query)->result();
            if($produit_ids != null)
            {
                foreach ($produit_ids as $produit_id) {
                    array_push($les_id_du_produits_info_nutr,$produit_id->produit_id);
                }
            }

            if(count($les_id_du_produits_info_nutr) > 0){

                $les_id_du_produits_info_nutr_to_string = "(";        
                for($i=0;$i<count($les_id_du_produits_info_nutr);$i++){
                    if(count($les_id_du_produits_info_nutr) == 1){
                           $les_id_du_produits_info_nutr_to_string .= $les_id_du_produits_info_nutr[$i]. ")";
                    }
                    else
                    {
                        if($i==count($les_id_du_produits_info_nutr)-1){
                        $les_id_du_produits_info_nutr_to_string .= $les_id_du_produits_info_nutr[$i]. ")";                    
                    }
                        else{
                             $les_id_du_produits_info_nutr_to_string .= $les_id_du_produits_info_nutr[$i]. ",";                      
                        }
                    }
                }            

                $mes_conditions .= " AND ".$this->tableDonation. ".produit_id_produit IN ".$les_id_du_produits_info_nutr_to_string;
            }else{
                 // on ne trouve pas de produit et on retourne
                return;
            }
        }

        // Condition sur type de produit
        if($type_produit_to_string != "" and $type_produit_to_string != "()"){

            $query = "SELECT id_produit FROM ".$this->tableProduit." WHERE type_produit_id IN ".$type_produit_to_string;

            $produit_ids = $this->db->query($query)->result();
            if($produit_ids != null)
            {
                foreach ($produit_ids as $produit_id) {
                    array_push($les_id_du_type_produit,$produit_id->id_produit);
                }
            }

            if(count($les_id_du_type_produit) > 0){

                $les_id_du_type_produit_to_string = "(";        
                for($i=0;$i<count($les_id_du_type_produit);$i++){
                    if(count($les_id_du_type_produit) == 1){
                           $les_id_du_type_produit_to_string .= $les_id_du_type_produit[$i]. ")";
                    }
                    else
                    {
                        if($i==count($les_id_du_type_produit)-1){
                        $les_id_du_type_produit_to_string .= $les_id_du_type_produit[$i]. ")";                    
                    }
                        else{
                             $les_id_du_type_produit_to_string .= $les_id_du_type_produit[$i]. ",";                      
                        }
                    }
                }            

                $mes_conditions .= " AND ".$this->tableDonation. ".produit_id_produit IN ".$les_id_du_type_produit_to_string;
            }else{
                 // on ne trouve pas de produit et on retourne
                return;
            }
        }

        //Condition sur liste entreprise
        if($liste_entreprise_to_string != "" and $liste_entreprise_to_string != "()"){

            $mes_conditions .= " AND ".$this->tableDonation. ".entreprise_id IN ".$liste_entreprise_to_string;

        }

        if($latitude != null && $longitude != null){
            $latitude_pres_sup = $this->getBorneCoordonneesGeo($latitude)["borne_sup"];
            $latitude_pres_inf = $this->getBorneCoordonneesGeo($latitude)["borne_inf"];
            $longitude_pres_sup = $this->getBorneCoordonneesGeo($longitude)["borne_sup"];
            $longitude_pres_inf = $this->getBorneCoordonneesGeo($longitude)["borne_inf"];
            
            $query = "SELECT id_adresse FROM ".$this->tableAdresse." WHERE latitude BETWEEN ".$latitude_pres_inf. " AND ".$latitude_pres_sup. " AND longitude BETWEEN ".
            $longitude_pres_inf. " AND ". $longitude_pres_sup;
            $adresses_id = $this->db->query($query)->result();
            
            if(count($adresses_id)>0){
                
                /* convert adresses_id to string*/
                $les_id_des_adresse = [];    
                /* convert adresses_id to string*/    
                foreach ($adresses_id as $adresse) {
                    array_push($les_id_des_adresse,$adresse->id_adresse);
                }
                $les_id_des_adresse_to_string = "(";
                for($i=0;$i<count($les_id_des_adresse);$i++){
                    if(count($les_id_des_adresse) == 1){
                           $les_id_des_adresse_to_string .= $les_id_des_adresse[$i]. ")";
                    }
                    else
                    {
                        if($i==count($les_id_des_adresse)-1){
                        $les_id_des_adresse_to_string .= $les_id_des_adresse[$i]. ")";                    
                    }
                        else{
                             $les_id_des_adresse_to_string .= $les_id_des_adresse[$i]. ",";                      
                        }
                    }
                }
                $query2 = "SELECT entreprise_id FROM ".$this->tableAdresseEntreprise." WHERE adresse_id IN ".$les_id_des_adresse_to_string;
                $entreprises_id = $this->db->query($query2)->result();

                if(count($entreprises_id) > 0 ){
                    $entreprise_id_info_geographique = [];
                    foreach ($entreprises_id as $key) {
                        array_push($entreprise_id_info_geographique,$key->entreprise_id);
                    }

                    $entreprise_id_info_geographique_to_string = "(";
                    
                    for($i=0;$i<count($entreprise_id_info_geographique);$i++){
                        if(count($entreprise_id_info_geographique) == 1){
                               $entreprise_id_info_geographique_to_string .= $entreprise_id_info_geographique[$i]. ")";
                        }
                        else
                        {
                            if($i==count($entreprise_id_info_geographique)-1){
                            $entreprise_id_info_geographique_to_string .= $entreprise_id_info_geographique[$i]. ")";                    
                        }
                            else{
                                 $entreprise_id_info_geographique_to_string .= $entreprise_id_info_geographique[$i]. ",";                      
                            }
                        }
                    }

                    $mes_conditions .= " AND ".$this->tableDonation. ".entreprise_id IN ".$entreprise_id_info_geographique_to_string;    

                }    
            }
            else{
                // on ne trouve pas de produit et on retourne
                return;
            }

        }
        //echo $mes_conditions. "\n";
        //die();
        $field = " id_donation, moment, entreprise_id, id_produit,quantite, minimum_qte_vente, date_peremption_produit as dateLimite, type_date, pourcentage_reduction, nom_produit, type_produit_id, type_aliment_id, prix, adresse_ramassage_id as adresse_rm_id";
        $mes_conditions .= " LIMIT ".$resultLength. " OFFSET ".$indexStart;
        $requete = "SELECT ".$field."  FROM ".$this->tableDonation." INNER JOIN ".$this->tableProduit. " ON ".$this->tableDonation.".produit_id_produit = ".$this->tableProduit.".id_produit WHERE ".$mes_conditions;
        //echo $requete. "\n";
        $result =  $this->db->query($requete)->result();
        return $result;
    }


    public function findProduitByIdDonation($id_donation){
        $mes_conditions = $this->tableDonation.".action = 2 AND quantite > 0 ";
        /*$field = " id_donation, id_produit, date_peremption_produit, type_date, pourcentage_reduction, nom_produit, type_produit_id, type_aliment_id, prix";*/
        $field = "id_donation, entreprise_id,organisation_id,moment,quantite,date_peremption_produit as dateLimite,etat_donation_id,type_date,minimum_qte_vente,adresse_ramassage_id,pourcentage_reduction,id_produit,nom_produit,type_produit_id,description,poids,volume,type_aliment_id,prix as prixNormal, groupement, adresse_ramassage_id as adresse_rm_id";
        $id_donation = (int) $id_donation;
        $requete = "SELECT ".$field." FROM ".$this->tableDonation." INNER JOIN ".$this->tableProduit. " ON ".$this->tableDonation.".produit_id_produit = ".$this->tableProduit.".id_produit WHERE ".$this->tableDonation.".id_donation = ".$id_donation. " AND ".$mes_conditions ;

        $result = $this->db->query($requete)->result();
        return $result; 
    }

    public function findProduitIdDonationNomproduitByIdDonation($id_donation){
        $mes_conditions = $this->tableDonation.".action = 2 AND quantite > 0 ";
        $field = "id_donation,nom_produit,quantite";
        $id_donation = (int) $id_donation;
        $requete = "SELECT ".$field." FROM ".$this->tableDonation." INNER JOIN ".$this->tableProduit. " ON ".$this->tableDonation.".produit_id_produit = ".$this->tableProduit.".id_produit WHERE ".$this->tableDonation.".id_donation = ".$id_donation. " AND ".$mes_conditions ;

        $result = $this->db->query($requete)->result();
        return $result; 
    }



    public function findDonationById($donation_id){
        $donation_id = (int) $donation_id;
        $this->db->select("id_donation, quantite");
        $this->db->from($this->tableDonation);
        $this->db->where($this->tableDonation.'.id_donation', $donation_id);
        $result =  $this->db->get()->result();
        return $result;
    }

    public function findStrategieReductionByIdDonation($donation_id){
        $this->db->select("*");
        $this->db->from($this->tableStrategieReduction);
        $this->db->where($this->tableStrategieReduction.'.donation_id', $donation_id);
        $result =  $this->db->get()->result();
        return $result;
    }

    public function findNbJourPeriodeReductionByIdPeriodeReduction($id_periode_reduction){
        $this->db->select("nombre_jour");
        $this->db->from($this->tablePeriodeReduction);
        $this->db->where($this->tablePeriodeReduction.'.id_periode_reduction', $id_periode_reduction);
        $result =  $this->db->get()->result();
        if(count($result) == 1)
            return $result[0];
        else
            return null;    
    }


    public function findIdPeriodeReductionByNombreJour($id_donation,$intervalle){
        $mes_conditions = " WHERE ".$this->tableStrategieReduction. ".donation_id = ".$id_donation;

        $mes_conditions .= " AND nombre_jour >= ".$intervalle."  ORDER BY nombre_jour LIMIT 1";

        $query = "SELECT pourcentage_reduction FROM ".$this->tablePeriodeReduction. " INNER JOIN ".$this->tableStrategieReduction. " ON ".
        
        $this->tableStrategieReduction. ".periode_reduction_id = ".$this->tablePeriodeReduction. ".id_periode_reduction ".$mes_conditions;
        
        $result = $this->db->query($query)->result();
        if(count($result) == 1)
            return $result[0];
        else
            return null;     
    }

     public function getPeriodeRecuperationCommandeByIdDonation($id_donation){
        $this->db->select("*");
        $this->db->from($this->tablePeriodeRecuperationCommande);
        $this->db->where($this->tablePeriodeRecuperationCommande.'.donation_id', $id_donation);
        $result =  $this->db->get()->result();
        return $result; 
    }

    public function getAllInfoNutris(){
        $query = "SELECT id_information_nutritionnelle FROM ".$this->tableInformationNutritionnelle;
        $result = $this->db->query($query)->result();
        return $result;
    }

    public function updateQuantiteProduit( $idDonation, $value){
        $query = "UPDATE ".$this->tableDonation. " SET quantite = quantite - ".$value." WHERE id_donation = ".$idDonation;
        $this->db->query($query);
        return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
    }

    public function wsGetAllInfoNutris(){
        $query = "SELECT id_information_nutritionnelle as id, label FROM ".$this->tableInformationNutritionnelle;
        $result = $this->db->query($query)->result();
        return $result;
    }

}


/*public function findByCondition($indexStart,$resultLength,$typeMotsCle,$motCle,$type_produit_id,$type_aliment_id,$prix_min,$prix_max,$date_limite, $info_nutr, $latitude,$longitude, $type_produit, $listeEntreprise){
        
        $indexStart = ($indexStart != null)? $indexStart:0;
        $resultLength = ($resultLength != null)? $resultLength:10;

        // LES VARIABLES POUR LES CONDITIONS

        $les_id_du_produits_prix = [];
        $produit_id_par_nom = [];
        $entreprise_id_par_nom = [];
        $les_id_du_produits_date_limite = [];
        $les_id_du_produits_info_nutr = [];
        $les_id_du_type_produit = [];
        $les_id_des_entreprises = [];
        

        // CREATION DES CONDITIONS A PARTIR DES VARIABLES

        /* formatage clé info_nutr *
        /
        $info_nutr_to_string = null;

        if($info_nutr != null){
            $info_nutr_array = explode("#", $info_nutr);
            //var_dump($info_nutr_array);
            //die();
            $info_nutr_to_string = "(";
            for($i=1;$i<count($info_nutr_array);$i++){
                
                if($i == count($info_nutr_array) - 1){
                    $info_nutr_to_string .= $info_nutr_array[$i]; 
                }else{
                    $info_nutr_to_string .= $info_nutr_array[$i].",";
                }
            }
            $info_nutr_to_string .= ")";

        }
        /* formatage clé info_nutritionnelle /

        /* formatage clé type_produit/
        $type_produit_to_string = null;

        if($type_produit != null){
            $type_produit_array = explode("#", $type_produit);
            
            $type_produit_to_string = "(";
            for($i=1;$i<count($type_produit_array);$i++){
                
                if($i == count($type_produit_array) - 1){
                    $type_produit_to_string .= $type_produit_array[$i]; 
                }else{
                    $type_produit_to_string .= $type_produit_array[$i].",";
                }
            }
            $type_produit_to_string .= ")";

        }

        /* formatage clé type_produit */

        /* formatage clé Entreprise/
        $liste_entreprise_to_string = null;

        if($listeEntreprise != null){
            $liste_entreprise_array = explode("#", $listeEntreprise);
           
            $liste_entreprise_to_string = "(";
            for($i=1;$i<count($liste_entreprise_array);$i++){
                
                if($i == count($liste_entreprise_array) - 1){
                    $liste_entreprise_to_string .= $liste_entreprise_array[$i]; 
                }else{
                    $liste_entreprise_to_string .= $liste_entreprise_array[$i].",";
                }
            }
            $liste_entreprise_to_string .= ")";

        }

        /* formatage clé Entreprise */ 

        // typeMotCle 0 => OU inclusif, 1:"Nom entreprise", 2:"Nom produit" 

        // remplir les entreprise_id_par_nom
        /*$query = "SELECT id_produit FROM ".$this->tableProduit. " WHERE nom_produit LIKE '%".$motCle."%'";
        $produit_ids = $this->db->query($query)->result();
        if($produit_ids != null)
        {
            foreach ($produit_ids as $produit_id) {
                array_push($produit_id_par_nom,$produit_id->id_produit);
            }
        }

        // remplir les produit_id_par_nom
        $query = "SELECT id_entreprise FROM ".$this->tableEntreprise. " WHERE nom_entreprise LIKE '%".$motCle."%'";
        $entreprise_ids = $this->db->query($query)->result();
        /*var_dump($entreprise_ids);
        //die();/
        if($entreprise_ids != null)
        {
            foreach ($entreprise_ids as $entreprise_id) {
                array_push($entreprise_id_par_nom,$entreprise_id->id_entreprise);
            }
        }

        if($date_limite != null){
            // formatage des dates
            $date_limite = str_replace("/","-",$date_limite);
            $date_limite = date_create($date_limite);
            $date_limite = date_format($date_limite,"Y-m-d H:i:s");
            // formatage des dates
            $this->db->select("produit_id_produit");
            $this->db->from($this->tableDonation);
            $this->db->where("date_peremption_produit >= ", $date_limite);
            $produit_ids = $this->db->get()->result();

            // si on'a pas trouvé le produit avec le nom d'entreprise $nom_entrepriset on retourne null
            if($produit_ids == null)
                return;
            else{
                foreach ($produit_ids as $produit_id) {
                    array_push($les_id_du_produits_date_limite,$produit_id->produit_id_produit);
                }
            }

        }
        // si max=-1 et min a une valeur prendre les produits ayant prix>= min
        if($prix_max == -1 && $prix_min != -1){

            $query = "SELECT id_produit FROM ".$this->tableProduit. " WHERE prix >= ".$prix_min;
            $produit_ids = $this->db->query($query)->result();
            if($produit_ids == null)
                return;
            else{
                foreach ($produit_ids as $produit_id){
                    array_push($les_id_du_produits_prix,$produit_id->id_produit);
                }
            }   
        }
        // si min=-1 et max a une valeur prendre les produits ayant prix<= max
        else if($prix_min ==-1 && $prix_max != -1){
            $query = "SELECT id_produit FROM ".$this->tableProduit. " WHERE prix <= ".$prix_max;
            $produit_ids = $this->db->query($query)->result();
            if($produit_ids == null)
                return;
            else{
                foreach ($produit_ids as $produit_id){
                    array_push($les_id_du_produits_prix,$produit_id->id_produit);
                }
            }
        }
        // si min et max ont des valeurs prendre les produits entre ces valeurs
        else if($prix_min !=-1 && $prix_max != -1){
            $query = "SELECT id_produit FROM ".$this->tableProduit. " WHERE prix BETWEEN ".$prix_min." AND " .$prix_max;
            $produit_ids = $this->db->query($query)->result();
            if($produit_ids == null)
                return;
            else{
                foreach ($produit_ids as $produit_id){
                    array_push($les_id_du_produits_prix,$produit_id->id_produit);
                }
            }
        }
        // si min=-1 et max=-1 ne pas mettre de filtre sur le prix
        else{
            $les_id_du_produits_prix = [];
        }

        $mes_conditions = $this->tableDonation.".action = 2 AND quantite > 0";

        if($type_aliment_id != null){
            $mes_conditions .= " AND ".$this->tableProduit.".type_aliment_id = ".$type_aliment_id ; 
        }
        if($type_produit_id != null){
            $mes_conditions .= " AND ".$this->tableProduit.".type_produit_id = ".$type_produit_id ; 
        }
        if(count($les_id_du_produits_prix) > 0){

            $les_id_du_produits_prix_to_string = "(";
            for($i=0;$i<count($les_id_du_produits_prix);$i++){
                if(count($les_id_du_produits_prix) == 1){
                       $les_id_du_produits_prix_to_string .= $les_id_du_produits_prix[$i]. ")";
                }
                else
                {
                    if($i==count($les_id_du_produits_prix)-1){
                    $les_id_du_produits_prix_to_string .= $les_id_du_produits_prix[$i]. ")";                    
                }
                    else{
                         $les_id_du_produits_prix_to_string .= $les_id_du_produits_prix[$i]. ",";                      
                    }
                }
            }

            $mes_conditions .= " AND ".$this->tableDonation. ".produit_id_produit IN ".$les_id_du_produits_prix_to_string;
        }
        if(count($les_id_du_produits_date_limite) > 0){
            $les_id_du_produits_date_limite_to_string = "(";
            for($i=0;$i<count($les_id_du_produits_date_limite);$i++){
                if(count($les_id_du_produits_date_limite) == 1){
                       $les_id_du_produits_date_limite_to_string .= $les_id_du_produits_date_limite[$i]. ")";
                }
                else
                {
                    if($i==count($les_id_du_produits_date_limite)-1){
                    $les_id_du_produits_date_limite_to_string .= $les_id_du_produits_date_limite[$i]. ")";                    
                }
                    else{
                         $les_id_du_produits_date_limite_to_string .= $les_id_du_produits_date_limite[$i]. ",";                      
                    }
                }
            }

            $mes_conditions .= " AND ".$this->tableDonation. ".produit_id_produit IN ".$les_id_du_produits_date_limite_to_string;
        }

        // typeMotCle 0 => OU inclusif, 1:"Nom entreprise", 2:"Nom produit" 
        if($typeMotsCle == 1){
            if(count($entreprise_id_par_nom) > 0){

                $entreprise_id_par_nom_to_string = "(";
                if(count($entreprise_id_par_nom) == 1){
                   $entreprise_id_par_nom_to_string .= $entreprise_id_par_nom[0];
                }
                else{        
                    for($i=0;$i<count($entreprise_id_par_nom);$i++){
                        if($i==count($entreprise_id_par_nom)-1){
                            $entreprise_id_par_nom_to_string .= $entreprise_id_par_nom[$i];                    
                        }
                        else{
                             $entreprise_id_par_nom_to_string .= $entreprise_id_par_nom[$i]. ",";                      
                        }
                    }
                }
                $mes_conditions .= " AND ".$this->tableDonation. ".entreprise_id IN ".$entreprise_id_par_nom_to_string. ")";
            }
            // on ne trouve pas de produit associé au nom entreprise
            else{
                return;
            }
        }
        else if($typeMotsCle == 2){
            if(count($produit_id_par_nom) > 0){

                $produit_id_par_nom_to_string = "(";  
                if(count($produit_id_par_nom) == 1){
                   $produit_id_par_nom_to_string .= $produit_id_par_nom[0];
                }
                else{      
                    for($i=0;$i<count($produit_id_par_nom);$i++){
                    
                        if($i==count($produit_id_par_nom)-1){
                            $produit_id_par_nom_to_string .= $produit_id_par_nom[$i];                    
                        }
                        else{
                             $produit_id_par_nom_to_string .= $produit_id_par_nom[$i]. ",";                      
                        }
                    }
                }

                $mes_conditions .= " AND ".$this->tableDonation. ".produit_id_produit IN ".$produit_id_par_nom_to_string. ")";
            }
            // on ne trouve pas de produit associé au nom produit
            else{
                return;
            }
        }
        else{
            // on ne trouve aucun nom produit et aucun nom entreprise
            if(count($entreprise_id_par_nom) == 0 && count($produit_id_par_nom) == 0){
                return ; 
            }
            
            else if(count($produit_id_par_nom) > 0){
                $produit_id_par_nom_to_string = "(";        
                if(count($produit_id_par_nom) == 1){
                   $produit_id_par_nom_to_string .= $produit_id_par_nom[0];
                }else{
                    for($i=0;$i<count($produit_id_par_nom);$i++){                    
                        if($i==count($produit_id_par_nom)-1){
                            $produit_id_par_nom_to_string .= $produit_id_par_nom[$i];                    
                        }
                        else{
                             $produit_id_par_nom_to_string .= $produit_id_par_nom[$i]. ",";                      
                        }
                    }
                }    
                $mes_conditions .= " AND ( ".$this->tableDonation. ".produit_id_produit IN ".$produit_id_par_nom_to_string. ")";
                
                if(count($entreprise_id_par_nom) > 0){
                    $entreprise_id_par_nom_to_string = "("; 
                    if(count($entreprise_id_par_nom) == 1){
                        $entreprise_id_par_nom_to_string .= $entreprise_id_par_nom[0];
                    }else{
                        for($i=0;$i<count($entreprise_id_par_nom);$i++){
                            if($i==count($entreprise_id_par_nom)-1){
                                $entreprise_id_par_nom_to_string .= $entreprise_id_par_nom[$i];                    
                            }
                            else{
                                 $entreprise_id_par_nom_to_string .= $entreprise_id_par_nom[$i]. ",";                      
                            }
                        }
                    }       
                    $mes_conditions .= " OR ".$this->tableDonation. ".entreprise_id IN ".$entreprise_id_par_nom_to_string. "))";
                }else{
                    $mes_conditions .= ")";
                }                
            }
            else{
            
                $entreprise_id_par_nom_to_string = "(";        
                if(count($entreprise_id_par_nom) == 1){
                   $entreprise_id_par_nom_to_string .= $entreprise_id_par_nom[0];
                }
                else{
                    for($i=0;$i<count($entreprise_id_par_nom);$i++){
                        if($i==count($entreprise_id_par_nom)-1){
                            $entreprise_id_par_nom_to_string .= $entreprise_id_par_nom[$i];                    
                        }
                        else{
                             $entreprise_id_par_nom_to_string .= $entreprise_id_par_nom[$i]. ",";                      
                        }
                    }
                }

                $mes_conditions .= " AND ".$this->tableDonation. ".entreprise_id IN ".$entreprise_id_par_nom_to_string. ")";
                            
            }

        }

        if($info_nutr_to_string != "" and $info_nutr_to_string != "()"){

            $query = "SELECT produit_id FROM ".$this->tableProduitInformationNutritionnelle." WHERE information_nutritionnelle_id IN ".$info_nutr_to_string;
            $produit_ids = $this->db->query($query)->result();
            if($produit_ids != null)
            {
                foreach ($produit_ids as $produit_id) {
                    array_push($les_id_du_produits_info_nutr,$produit_id->produit_id);
                }
            }

            if(count($les_id_du_produits_info_nutr) > 0){

                $les_id_du_produits_info_nutr_to_string = "(";        
                for($i=0;$i<count($les_id_du_produits_info_nutr);$i++){
                    if(count($les_id_du_produits_info_nutr) == 1){
                           $les_id_du_produits_info_nutr_to_string .= $les_id_du_produits_info_nutr[$i]. ")";
                    }
                    else
                    {
                        if($i==count($les_id_du_produits_info_nutr)-1){
                        $les_id_du_produits_info_nutr_to_string .= $les_id_du_produits_info_nutr[$i]. ")";                    
                    }
                        else{
                             $les_id_du_produits_info_nutr_to_string .= $les_id_du_produits_info_nutr[$i]. ",";                      
                        }
                    }
                }            

                $mes_conditions .= " AND ".$this->tableDonation. ".produit_id_produit IN ".$les_id_du_produits_info_nutr_to_string;
            }else{
                 // on ne trouve pas de produit et on retourne
                return;
            }
        }

        // Condition sur type de produit
        if($type_produit_to_string != "" and $type_produit_to_string != "()"){

            $query = "SELECT id_produit FROM ".$this->tableProduit." WHERE type_produit_id IN ".$type_produit_to_string;

            $produit_ids = $this->db->query($query)->result();
            if($produit_ids != null)
            {
                foreach ($produit_ids as $produit_id) {
                    array_push($les_id_du_type_produit,$produit_id->id_produit);
                }
            }

            if(count($les_id_du_type_produit) > 0){

                $les_id_du_type_produit_to_string = "(";        
                for($i=0;$i<count($les_id_du_type_produit);$i++){
                    if(count($les_id_du_type_produit) == 1){
                           $les_id_du_type_produit_to_string .= $les_id_du_type_produit[$i]. ")";
                    }
                    else
                    {
                        if($i==count($les_id_du_type_produit)-1){
                        $les_id_du_type_produit_to_string .= $les_id_du_type_produit[$i]. ")";                    
                    }
                        else{
                             $les_id_du_type_produit_to_string .= $les_id_du_type_produit[$i]. ",";                      
                        }
                    }
                }            

                $mes_conditions .= " AND ".$this->tableDonation. ".produit_id_produit IN ".$les_id_du_type_produit_to_string;
            }else{
                 // on ne trouve pas de produit et on retourne
                return;
            }
        }

        //Condition sur liste entreprise
        if($liste_entreprise_to_string != "" and $liste_entreprise_to_string != "()"){

            $mes_conditions .= " AND ".$this->tableDonation. ".entreprise_id IN ".$liste_entreprise_to_string;

        }

        if($latitude != null && $longitude != null){
            $latitude_pres_sup = $this->getBorneCoordonneesGeo($latitude)["borne_sup"];
            $latitude_pres_inf = $this->getBorneCoordonneesGeo($latitude)["borne_inf"];
            $longitude_pres_sup = $this->getBorneCoordonneesGeo($longitude)["borne_sup"];
            $longitude_pres_inf = $this->getBorneCoordonneesGeo($longitude)["borne_inf"];
            
            $query = "SELECT id_adresse FROM ".$this->tableAdresse." WHERE latitude BETWEEN ".$latitude_pres_inf. " AND ".$latitude_pres_sup. " AND longitude BETWEEN ".
            $longitude_pres_inf. " AND ". $longitude_pres_sup;
            $adresses_id = $this->db->query($query)->result();
            
            if(count($adresses_id)>0){
                
                /* convert adresses_id to string/
                $les_id_des_adresse = [];    
                /* convert adresses_id to string/    
                foreach ($adresses_id as $adresse) {
                    array_push($les_id_des_adresse,$adresse->id_adresse);
                }
                $les_id_des_adresse_to_string = "(";
                for($i=0;$i<count($les_id_des_adresse);$i++){
                    if(count($les_id_des_adresse) == 1){
                           $les_id_des_adresse_to_string .= $les_id_des_adresse[$i]. ")";
                    }
                    else
                    {
                        if($i==count($les_id_des_adresse)-1){
                        $les_id_des_adresse_to_string .= $les_id_des_adresse[$i]. ")";                    
                    }
                        else{
                             $les_id_des_adresse_to_string .= $les_id_des_adresse[$i]. ",";                      
                        }
                    }
                }
                $query2 = "SELECT entreprise_id FROM ".$this->tableAdresseEntreprise." WHERE adresse_id IN ".$les_id_des_adresse_to_string;
                $entreprises_id = $this->db->query($query2)->result();

                if(count($entreprises_id) > 0 ){
                    $entreprise_id_info_geographique = [];
                    foreach ($entreprises_id as $key) {
                        array_push($entreprise_id_info_geographique,$key->entreprise_id);
                    }

                    $entreprise_id_info_geographique_to_string = "(";
                    
                    for($i=0;$i<count($entreprise_id_info_geographique);$i++){
                        if(count($entreprise_id_info_geographique) == 1){
                               $entreprise_id_info_geographique_to_string .= $entreprise_id_info_geographique[$i]. ")";
                        }
                        else
                        {
                            if($i==count($entreprise_id_info_geographique)-1){
                            $entreprise_id_info_geographique_to_string .= $entreprise_id_info_geographique[$i]. ")";                    
                        }
                            else{
                                 $entreprise_id_info_geographique_to_string .= $entreprise_id_info_geographique[$i]. ",";                      
                            }
                        }
                    }

                    $mes_conditions .= " AND ".$this->tableDonation. ".entreprise_id IN ".$entreprise_id_info_geographique_to_string;    

                }    
            }
            else{
                // on ne trouve pas de produit et on retourne
                return;
            }

        }
        //echo $mes_conditions. "\n";
        //die();
        $field = " id_donation, entreprise_id, id_produit,quantite, minimum_qte_vente, date_peremption_produit as dateLimite, type_date, pourcentage_reduction, nom_produit, type_produit_id, type_aliment_id, prix, adresse_ramassage_id as adresse_rm_id";
        $mes_conditions .= " LIMIT ".$resultLength. " OFFSET ".$indexStart;
        $requete = "SELECT ".$field."  FROM ".$this->tableDonation." INNER JOIN ".$this->tableProduit. " ON ".$this->tableDonation.".produit_id_produit = ".$this->tableProduit.".id_produit WHERE ".$mes_conditions;
        //echo $requete. "\n";
        $result =  $this->db->query($requete)->result();
        return $result;
    }*/