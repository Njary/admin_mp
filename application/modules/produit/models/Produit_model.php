<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Produit_model extends CI_Model{

	protected $tableProduit = "produit";
    protected $tableDonation = "donation";
	protected $table_info_nutri = "information_nutritionnelle";
	protected $table_categorie = "type_aliment";
	protected $table_type = "type_produit";
    protected $table_photo_produit = "photo_produit";
    protected $table_produit_info_nutri = "produit_information_nutritionnelle";
    protected $table_raison_surplus = "raison_surplus";
    protected $table_raison_donation = "raison_donation";
    protected $table_produit_equipement = "produit_equipement";
    protected $table_equipement = "equipement";
    protected $table_strategie_reduction = "strategie_reduction";
    protected $table_periode_reduction = "periode_reduction";
    protected $table_adresse = "adresse";
    protected $table_etat_donation = "etat_donation";
    protected $tablePhotoProduit = "photo_produit";

	public function addInfoNutri($label, $proprietaire=-1){
        $query = "SELECT * FROM ".$this->table_info_nutri." WHERE LOWER(label)='".strtolower($label)."' AND (proprietaire=0 OR proprietaire=".$proprietaire.")";
        $result = $this->db->query($query)->result();
        if(count($result) <= 0){
    		$this->db->insert($this->table_info_nutri,array('label'=>$label, 'proprietaire'=>$proprietaire)); 
            return $this->db->insert_id();
        }
        else{
            return $result[0]->id_information_nutritionnelle;
        }
	}

	public function getInfosNutri(){
        $query = $this->db->get($this->table_info_nutri);
        return $query->result();
    }

    public function getCategorieById($id_type_aliment){
    	return $this->db->get($this->table_categorie,array("id_type_aliment"=>$id_type_aliment))->result();
    }

    public function getTypeById($id_type_produit){
    	return $this->db->get($this->table_type,array("id_type_produit"=>$id_type_produit))->result();
    }


    public function getTypeProduitByIdTypeProduit( $id_type_produit){
        $this->db->select($this->table_type.".label as label_type_produit");
        $this->db->from($this->table_type);
        $this->db->where($this->table_type.'.id_type_produit', $id_type_produit);
        $result = $this->db->get()->result();
       return $result;
    }

    public function getAllProduitByIdEntreprise($id_entreprise, $action){
       $this->db->select($this->tableProduit.'.* ,'.$this->tableDonation.'.*,'.$this->table_categorie.'.label as categorie, '.$this->table_type.'.label as type ,'.$this->table_etat_donation.'.label as etat_donation');
       $this->db->from($this->tableProduit);
       $this->db->join($this->tableDonation, $this->tableProduit.'.id_produit = '.$this->tableDonation.'.produit_id_produit');
       $this->db->join($this->table_categorie, $this->tableProduit.'.type_aliment_id = '.$this->table_categorie.'.id_type_aliment');
       $this->db->join($this->table_type, $this->tableProduit.'.type_produit_id = '.$this->table_type.'.id_type_produit');
       $this->db->join($this->table_etat_donation, $this->tableDonation.'.etat_donation_id = '.$this->table_etat_donation.'.id_etat_donation');
       $this->db->where($this->tableDonation.'.entreprise_id = '. $id_entreprise .' and '.$this->tableDonation.'.action = '.$action.' and '.$this->tableDonation.'.etat_donation_id != 5');
       $result = $this->db->get()->result();
       return $result;
    }

    public function getProduitInfoNutris($id_produit){
        $this->db->select('*');
        $this->db->from($this->table_produit_info_nutri);
        $this->db->join($this->table_info_nutri, $this->table_info_nutri.'.id_information_nutritionnelle = '.$this->table_produit_info_nutri.'.information_nutritionnelle_id');
        $this->db->where($this->table_produit_info_nutri.'.produit_id = '.$id_produit);
        $result = $this->db->get()->result();
        return $result;
    }

    public function getTypeAlimentByIdTypeAliment( $id_type_aliment){
        $this->db->select($this->table_categorie.".label as label_type_aliment");
        $this->db->from($this->table_categorie);
        $this->db->where($this->table_categorie.'.id_type_aliment', $id_type_aliment);
        $result = $this->db->get()->result();
       return $result;
    }

    public function getProduitRaisonSurplus ($id_donation){
        $this->db->select('*');
        $this->db->from($this->table_raison_donation);
        $this->db->join($this->table_raison_surplus, $this->table_raison_donation.'.raison_surplus_id = '.$this->table_raison_surplus.'.id_raison_surplus');
        $this->db->where($this->table_raison_donation.'.donation_id = '.$id_donation);
        $result = $this->db->get()->result();
        return $result;
    }
    
    public function getInfoProduitById($id_donation,$action){
        $this->db->select('*');
        $this->db->from($this->tableDonation);
        $this->db->join($this->tableProduit, $this->tableProduit.'.id_produit = '.$this->tableDonation.'.produit_id_produit');
        //$this->db->join($this->table_categorie, $this->tableProduit.'.type_aliment_id = '.$this->table_categorie.'.id_type_aliment');
        //$this->db->join($this->table_type, $this->tableProduit.'.type_produit_id = '.$this->table_type.'.id_type_produit');
        $this->db->where($this->tableDonation.'.id_donation = '. $id_donation .' and '.$this->tableDonation.'.action = '.$action);
        $result = $this->db->get()->result();
        return $result;
    }

    /* limits results: // $this->db->limit(10, 20) Produces: LIMIT 20 (index de depart) , 10 (nb de resultat) */
    public function findAll($indexStart,$resultLength){
        $this->db->limit($resultLength,$indexStart);
        return $this->db->get($this->table)->result();
    }

    public function findByTypeProduit($indexStart,$resultLength,$id_type_produit){
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($this->table_type, $this->table_type.'.id_type_produit='.$this->table.'.type_produit_id');
        $this->db->where($this->table_type.'.id_type_produit', $id_type_produit);
         $this->db->limit($resultLength,$indexStart);
    }

    public function getEquipementNecessaire($id_produit){
        $this->db->select('*');
        $this->db->from($this->table_produit_equipement);
        $this->db->join($this->table_equipement, $this->table_produit_equipement.'. equipement_id = '.$this->table_equipement.'.id_equipement');
        $this->db->where($this->table_produit_equipement.'.produit_id = '.$id_produit);
        $result = $this->db->get()->result();
        return $result;
    }


    public function findByTypeAliment($indexStart,$resultLength,$id_type_aliment){
        $this->db->select("*");
        $this->db->from($this->table);
        $this->db->join($this->table_categorie, $this->table_categorie.'.id_type_aliment='.$this->table.'.type_aliment_id');
        $this->db->where($this->table_categorie.'.id_type_aliment', $id_type_aliment);
         $this->db->limit($resultLength,$indexStart);
    }

    public function getAdresseRamassage($id_donation){
        $this->db->select($this->table_adresse.'.*');
        $this->db->from($this->table_adresse);
        $this->db->join($this->tableDonation, $this->table_adresse.'.id_adresse = '.$this->tableDonation.'.adresse_ramassage_id');
        $this->db->where($this->tableDonation.'.id_donation = '.$id_donation);
        $result = $this->db->get()->result();
        return $result;
    }


    public function getPrixMax(){
        $this->db->select_max("prix","prix_max");
        $this->db->from($this->table);
    }

    public function getIdProduitByDonation($id_donation){
        $this->db->select($this->tableDonation.'.produit_id_produit');
        $this->db->from($this->tableDonation);
        $this->db->where($this->tableDonation.'.id_donation = '.$id_donation);
        $result = $this->db->get()->result();
        return $result;
    }

    public function getPrixMin(){
        $this->db->select_min("prix","prix_min");
        $this->db->from($this->table);
        $result = $this->db->get()->result();
        return $result;   
    }

    public function getUrlPhotoProduitByIdProduit($id_produit){
        $field = $this->table_photo_produit.".url";
        $id_produit = (int) $id_produit;
        $requete = "SELECT ".$field." FROM ".$this->table_photo_produit." INNER JOIN ".$this->tableProduit. " ON ".$this->table_photo_produit.".produit_id = ".$this->tableProduit.".id_produit WHERE ".$this->tableProduit.".id_produit = ".$id_produit ;
        $result = $this->db->query($requete)->result();
        if(count($result) == 1)
            return $result[0];
        else
            return null;   
    }

    public function getInfoNutriForBenef($id_produit){
        $this->db->select('*');
        $this->db->from($this->table_produit_info_nutri);
        $this->db->where($this->table_produit_info_nutri.'.produit_id = '.$id_produit);
        $result = $this->db->get()->result();
        return $result;
    }

    public function getEquipementForBenef($id_produit){
        $this->db->select('*');
        $this->db->from($this->table_produit_equipement);
        $this->db->where($this->table_produit_equipement.'.produit_id = '.$id_produit);
        $result = $this->db->get()->result();
        return $result;
    }

    public function changeToDonation($id_donation, $id_organisation){
        $data = array(
            'action' => 1,
            'organisation_id' => $id_organisation
        );
        return (bool) $this->db->where($this->tableDonation.'.id_donation = '.$id_donation)
                                ->update($this->tableDonation, $data);
        
    }
    public function checkIfThereAreDraft($action){
        $this->db->select("*");
        $this->db->from($this->tableDonation);
        $this->db->where($this->tableDonation.'.etat_donation_id = 5 AND '.$this->tableDonation.'.action = '.$action);
        $result = $this->db->get()->result();
        return $result;
    }


    public function getProduitInfoNutrByIdInfoNutr($id_info_nutr){
        $query = "SELECT produit_id FROM ".$this->table_produit_info_nutri." WHERE information_nutritionnelle_id = ".$id_info_nutr;
        $result = $this->db->query($$query)->result();
        if(count($result) == 1)
            return $result[0];
        else
            return null; 
    }

    public function getProductPictures($id_produit, $id_entreprise){
         $query = "SELECT ph.id_photo_produit, ph.url FROM photo_produit ph WHERE ph.produit_id = ".$id_produit." AND (ph.proprietaire=0 OR ph.proprietaire=".$id_entreprise.")";
        $data = $this->db->query($query)->result();
        $result = array();
        foreach($data as $row){
            if(file_exists($row->url)){
                $inArray = false;
                for($i = 0; $i<count($result); $i++){
                    if(strcmp($row->url, $result[$i]['url']) == 0){
                        $inArray = true;
                        break;
                    }
                }
                if(!$inArray){
                    $result[] = array(
                        "id_photo_produit"=>$row->id_photo_produit,
                        "url"=>$row->url
                    );
                }
            }
        }
        return $result;
    }

     public function getStrategieMarketing($id_donation){
        $this->db->select('*');
        $this->db->from($this->table_strategie_reduction);
        $this->db->join($this->table_periode_reduction, $this->table_strategie_reduction.'.periode_reduction_id = '.$this->table_periode_reduction.'.id_periode_reduction');
        $this->db->where($this->table_strategie_reduction.'.donation_id = '.$id_donation);
        $result = $this->db->get()->result();
        return $result;
    }

    public function wsGetTypeProduit(){
        $query = "SELECT id_type_produit as id, label FROM ".$this->table_type;
        $result = $this->db->query($query)->result();
        return $result;
    }
}