<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Vente_model extends CI_Model{

    protected $tableTypeProduit = 'type_produit';
    protected $tableTypeAliment = "type_aliment";
    protected $tableInformationNutritionnelle = "information_nutritionnelle";
    protected $tableRaisonSurplus = "raison_surplus";
    protected $tableRaisonDonation = "raison_donation";
    protected $tableProduit = "produit";
    protected $tableProduitEquipement = "produit_equipement";
    protected $tableProduitInformationNutritionnelle = "produit_information_nutritionnelle";
    protected $tablePhotoProduit = "photo_produit";
    protected $tableDonation = "donation";
    protected $tableAdresseEntreprise = "adresse_entreprise";
    protected $tableAdresse = "adresse";
    protected $tableEntrepriseType = "entreprise_type";
    protected $tableAlimentType = "entreprise_type_aliment";
    protected $table_type_aliment_information_nutritionnelle = "type_aliment_information_nutritionnelle";
    protected $tablePeriodeReduction = "periode_reduction";
    protected $tableStrategieReduction = "strategie_reduction";
    protected $tablePeriodeRecuperation = "periode_recuperation_commande";

    public function getCategories($idEA=0){
    	$result = $this->db->query('SELECT * FROM '.$this->tableTypeAliment.' WHERE proprietaire=0 OR proprietaire='.$idEA)->result();
    	return $result;
    }

    public function getTypes($idEA=0){
    	$result = $this->db->query('SELECT * FROM '.$this->tableTypeProduit.' WHERE proprietaire=0 OR proprietaire='.$idEA)->result();
    	return $result;
    }

    public function getInfoNutris($idEA=0){
    	$result = $this->db->query('SELECT * FROM '.$this->tableInformationNutritionnelle.' WHERE proprietaire=0 OR proprietaire='.$idEA)->result();
    	return $result;
    }

    public function getRaisonSurplus($idEA=0){
    	$result = $this->db->query('SELECT * FROM '.$this->tableRaisonSurplus.' WHERE proprietaire=0 OR proprietaire='.$idEA)->result();
    	return $result;
    }

    public function addNewCategorie($categorie, $idEA=0){
        $data = array('label'=>$categorie,'proprietaire'=>$idEA);
        $result = array(
            "exist" => false,
            "id_type_aliment" => 0
        );
        $exist = $this->db->query('SELECT * FROM '.$this->tableTypeAliment.' WHERE LOWER(label)="'.strtolower($categorie).'" AND (proprietaire=0 OR proprietaire='.$idEA.')')->result();
        if(count($exist) >= 1){
            $result['exist'] = true;
        }
        else{
            $this->db->insert($this->tableTypeAliment, $data);
            $result['id_type_aliment'] = $this->db->insert_id();
        }
        return $result;
    }

    public function linkToInfoNutris($idCategorie, $infoNutris){
        $datas = explode(',', $infoNutris);
        for($i=0; $i<count($datas); $i++){
            $id = (int) $datas[$i];
            $data = array(
                "type_aliment_id"=>$idCategorie,
                "information_nutritionnelle_id"=>$id
            );
            $this->db->insert($this->table_type_aliment_information_nutritionnelle, $data);
        }
    }

    public function getInfoNutrisLinked($idCategorie){
        $this->db->select($this->table_type_aliment_information_nutritionnelle.'.information_nutritionnelle_id');
        $this->db->from($this->table_type_aliment_information_nutritionnelle);
        $this->db->join($this->tableTypeAliment, $this->tableTypeAliment.'.id_type_aliment='.$this->table_type_aliment_information_nutritionnelle.'.type_aliment_id');
        $this->db->where($this->table_type_aliment_information_nutritionnelle.'.type_aliment_id', $idCategorie);
        $result = $this->db->get()->result();
        $return = array();
        foreach($result as $r){
            $return[] = $r->information_nutritionnelle_id;
        }
        return $return;
    }

    public function addNewType($type, $idEA=0){
        $data = array('label'=>$type,'proprietaire'=>$idEA);
        $result = array(
            "exist" => false,
            "id_type_produit" => 0
        );
        $exist = $this->db->query('SELECT * FROM '.$this->tableTypeProduit.' WHERE LOWER(label)="'.strtolower($type).'" AND (proprietaire=0 OR proprietaire='.$idEA.')')->result();
        if(count($exist) >= 1){
            $result['exist'] = true;
        }
        else{
            $this->db->insert($this->tableTypeProduit, $data);
            $result['id_type_produit'] = $this->db->insert_id();
        }
        return $result;
    }

    public function addNewInfoNutris($infoNutris, $idEA=0){
        $data = array('label'=>$infoNutris,'proprietaire'=>$idEA);
        $result = array(
            "exist" => false,
            "id_information_nutritionnelle" => 0
        );
        $exist = $this->db->query('SELECT * FROM '.$this->tableInformationNutritionnelle.' WHERE LOWER(label)="'.strtolower($infoNutris).'" AND (proprietaire=0 OR proprietaire='.$idEA.')')->result();
        if(count($exist) >= 1){
            $result['exist'] = true;
        }
        else{
            $this->db->insert($this->tableInformationNutritionnelle, $data);
            $result['id_information_nutritionnelle'] = $this->db->insert_id();
        }
        return $result;
    }

    public function addNewRaisonSurplus($raisonSurplus, $idEA=0){
        $data = array('label'=>$raisonSurplus,'proprietaire'=>$idEA);
        $result = array(
            "exist" => false,
            "id_raison_surplus" => 0
        );
        $exist = $this->db->query('SELECT * FROM '.$this->tableRaisonSurplus.' WHERE LOWER(label)="'.strtolower($raisonSurplus).'" AND (proprietaire=0 OR proprietaire='.$idEA.')')->result();
        if(count($exist) >= 1){
            $result['exist'] = true;
        }
        else{
            $this->db->insert($this->tableRaisonSurplus, $data);
            $result['id_raison_surplus'] = $this->db->insert_id();
        }
        return $result;
    }

    public function canMakeVente($idEA){
        $this->db->from($this->tableAdresseEntreprise);
        $this->db->where('entreprise_id', $idEA);
        $adresseOk = count($this->db->get()->result()) > 0 ? true : false;
        $this->db->from($this->tableEntrepriseType);
        $this->db->where('entreprise_id', $idEA);
        $type = count($this->db->get()->result()) > 0 ? true : false;
        $this->db->from($this->tableAlimentType);
        $this->db->where('entreprise_id', $idEA);
        $typeAliment = count($this->db->get()->result()) > 0 ? true : false;
        if($adresseOk && $type && $typeAliment){
            return true;
        }
        else{
            return false;
        }
    }

    public function getPhotoPoduits($nom, $idEA){
        $query = "SELECT ph.id_photo_produit, ph.url FROM photo_produit ph JOIN produit pr ON pr.id_produit=ph.produit_id WHERE pr.nom_produit LIKE '%".$nom."%' AND (ph.proprietaire=0 OR ph.proprietaire=".$idEA.")";
        if(empty($nom)){
            $query .= ' LIMIT 0, 25';
        }
        $data = $this->db->query($query)->result();
        $result = array();
        foreach($data as $row){
            if(file_exists($row->url)){
                $inArray = false;
                for($i = 0; $i<count($result); $i++){
                    if(strcmp($row->url, $result[$i]['url']) == 0){
                        $inArray = true;
                        break;
                    }
                }
                if(!$inArray){
                    $result[] = array(
                        "id_photo_produit"=>$row->id_photo_produit,
                        "url"=>$row->url
                    );
                }
            }
        }
        return $result;
    }

    public function pushVente($idEA, $idProduit, $quantite, $quantiteMin, $pourcentage, $typeDate, $datePeremptionOrProduction, $moment, $raisonSurplus, $reductionParSemaine, $periodeReduction, $pourcentageReduction, $isAdresseEntreprise, $id_AdresseEntreprise,$adresseRmsg, $codePostalRmsg, $villeRmsg, $paysRmsg, $latitudeRmsg, $longitudeRmsg, $periodeRecuperation, $horaireDebut, $horaireFin, $isDraft){
        // Enregistrement adresse ramassage
        $id_adresse_ramassage = -1;

        if($isAdresseEntreprise == 1 && $id_AdresseEntreprise >= 0){

            $id_adresse_ramassage = $id_AdresseEntreprise;

        }else{

            $data = array(
                "label" => $adresseRmsg,
                "latitude"=> $latitudeRmsg,
                "longitude"=> $longitudeRmsg,
                "code_postal"=> $codePostalRmsg,
                "ville" => $villeRmsg,
                "pays_id" => $paysRmsg,

            );

            $this->db->insert($this->tableAdresse, $data);
            $idAdresse= $this->db->insert_id();
            $id_adresse_ramassage = $idAdresse;

            $data = array(
                "entreprise_id" => $idEA,
                "adresse_id" => $idAdresse
            );

            $this->db->insert($this->tableAdresseEntreprise, $data);
        }

        $etat_donation_id = 2;// Mail envoyé à DHL par défaut
        
        if($isDraft == 0){
            $etat_donation_id = 2;
        }else if($isDraft == 1){
            $etat_donation_id = 5; //Brouillon
        }

        $data = array(
            "entreprise_id" => $idEA,
            "produit_id_produit" => $idProduit,
            "moment" => $moment,
            "quantite" => $quantite,
            "date_peremption_produit" => $datePeremptionOrProduction,
            "etat_donation_id"=> $etat_donation_id, // ?? plus d'information
            "action" => 2, 
            "type_date" => $typeDate,
            "minimum_qte_vente" => $quantiteMin,
            "adresse_ramassage_id" => $id_adresse_ramassage,
            "pourcentage_reduction" => $pourcentage
        );
        //action = 2 car vente;
        $this->db->insert($this->tableDonation, $data);
        $id_donation = $this->db->insert_id();

        /*if($haveRaisonSurplus == 1){
            for($i=0; $i<count($raisonSurplus); $i++){
                $data = array(
                    'raison_surplus_id' => $raisonSurplus[$i],
                    'donation_id' => $id_donation
                );
                $this->db->insert($this->tableRaisonDonation, $data);
            }
        }*/

        for($i=0; $i<count($raisonSurplus); $i++){
            if( $raisonSurplus[$i]>0 ){
                $data = array(
                    'raison_surplus_id' => $raisonSurplus[$i],
                    'donation_id' => $id_donation
                );
                $this->db->insert($this->tableRaisonDonation, $data);
            }
        }

        for($i=0 ; $i<count($reductionParSemaine);$i++){  
            if($reductionParSemaine[$i] != '' && $reductionParSemaine[$i] >= 0){ 
                $data = array(
                    'periode_reduction_id' => $i+1,
                    'pourcentage_reduction' => $reductionParSemaine[$i],
                    'donation_id' => $id_donation
                );      
                $this->db->insert($this->tableStrategieReduction, $data);
            }
        }
        
        for($i=0 ; $i<count($periodeReduction);$i++){
            if($periodeReduction[$i] != '' && $pourcentageReduction[$i] != ''){
                 $data = array(
                    'periode_reduction_id' => $periodeReduction[$i],
                    'pourcentage_reduction' => $pourcentageReduction[$i],
                    'donation_id' => $id_donation
                );
                $this->db->insert($this->tableStrategieReduction, $data);
            }  
        }

        for($i=0 ; $i<count($periodeRecuperation);$i++){  
            if($periodeRecuperation[$i] != '' && $horaireDebut[$i] != '' && $horaireFin[$i] != ''){ 
                $data = array(
                    'jour' => $periodeRecuperation[$i],
                    'horaire_debut' => $horaireDebut[$i],
                    'horaire_fin' => $horaireFin[$i],
                    'donation_id' => $id_donation
                );      
                $this->db->insert($this->tablePeriodeRecuperation, $data);
            }
        }

    }

    public function pushUpdateVente($id_donation, $idEA, $idProduit, $quantite, $quantiteMin, $pourcentage, $typeDate, $datePeremptionOrProduction, $moment, $raisonSurplus, $reductionParSemaine, $periodeReduction, $pourcentageReduction, $isAdresseEntreprise, $id_AdresseEntreprise,$adresseRmsg, $codePostalRmsg, $villeRmsg, $paysRmsg, $latitudeRmsg, $longitudeRmsg, $periodeRecuperation, $horaireDebut, $horaireFin, $isDraft){
        
        // Enregistrement adresse ramassage

        $id_adresse_ramassage = -1;

        if($isAdresseEntreprise == 1 && $id_AdresseEntreprise >= 0){
            $id_adresse_ramassage = $id_AdresseEntreprise;
        }else{

            if( $id_AdresseEntreprise >= 0 ){
                
                $data = array(
                    "label" => $adresseRmsg,
                    "latitude"=> $latitudeRmsg,
                    "longitude"=> $longitudeRmsg,
                    "code_postal"=> $codePostalRmsg,
                    "ville" => $villeRmsg,
                    "pays_id" => $paysRmsg
                );

                $this->db->where($this->tableAdresse.'.id_adresse = '.$id_AdresseEntreprise);
                $this->db->update($this->tableAdresse, $data);

                $id_adresse_ramassage = $id_AdresseEntreprise;

            }else{

                $data = array(
                    "label" => $adresseRmsg,
                    "code_postal"=> $codePostalRmsg,
                    "ville" => $villeRmsg,
                    "pays_id" => $paysRmsg
                );

                $this->db->insert($this->tableAdresse, $data);
                $idAdresse= $this->db->insert_id();
                $id_adresse_ramassage = $idAdresse;

                $data = array(
                    "entreprise_id" => $idEA,
                    "adresse_id" => $idAdresse
                );

                $this->db->insert($this->tableAdresseEntreprise, $data);

            }
            
        }

        /*if($id_adresse_ramassage < 0){
            $adresseEntreprise = $this->entreprise_model->getAdressesEntreprise( $idEA );
            if( count($adresseEntreprise)>0 ){
                $id_adresse_ramassage = $adresseEntreprise[0]->id_adresse;
            }
        }*/
        
        $etat_donation_id = 2;// Mail envoyé à DHL par défaut
        
        if($isDraft == 0){
            $etat_donation_id = 2;
        }else if($isDraft == 1){
            $etat_donation_id = 5; //Brouillon
        }

        $data = array(
            "entreprise_id" => $idEA,
            "produit_id_produit" => $idProduit,
            "moment" => $moment,
            "quantite" => $quantite,
            "date_peremption_produit" => $datePeremptionOrProduction,
            "etat_donation_id"=> $etat_donation_id, // ?? plus d'information
            "action" => 2, 
            "type_date" => $typeDate,
            "minimum_qte_vente" => $quantiteMin,
            "adresse_ramassage_id" => $id_adresse_ramassage,
            "pourcentage_reduction" => $pourcentage
        );

        //action = 2 car vente;
        $this->db->where($this->tableDonation.'.id_donation = '.$id_donation);
        $this->db->update($this->tableDonation, $data);

        /*if($haveRaisonSurplus == 1){
            $this->db->where($this->tableRaisonDonation.'.donation_id = '.$id_donation);
            $this->db->delete($this->tableRaisonDonation);

            for($i=0; $i<count($raisonSurplus); $i++){
                $data = array(
                    'raison_surplus_id' => $raisonSurplus[$i],
                    'donation_id' => $id_donation
                );
                $this->db->insert($this->tableRaisonDonation, $data);
            }
        }*/

        $this->db->where( $this->tableRaisonDonation.'.donation_id = '.$id_donation );
        $this->db->delete($this->tableRaisonDonation);

        for($i=0; $i<count($raisonSurplus); $i++){
            if( $raisonSurplus[$i]>0 ){
                $data = array(
                    'raison_surplus_id' => $raisonSurplus[$i],
                    'donation_id' => $id_donation
                );
                $this->db->insert($this->tableRaisonDonation, $data);
            }
        }
        
        $this->db->where($this->tableStrategieReduction.'.donation_id = '.$id_donation);
        $this->db->delete($this->tableStrategieReduction);

        for($i=0 ; $i<count($reductionParSemaine);$i++){  
            if($reductionParSemaine[$i] != '' && $reductionParSemaine[$i] >= 0 ){ 
                $data = array(
                    'periode_reduction_id' => $i+1,
                    'pourcentage_reduction' => $reductionParSemaine[$i],
                    'donation_id' => $id_donation
                );      
                $this->db->insert($this->tableStrategieReduction, $data);
            }
        }
        
        for($i=0 ; $i<count($periodeReduction);$i++){  
            if($periodeReduction[$i] != '' && $pourcentageReduction[$i] != ''){ 
                $data = array(
                    'periode_reduction_id' => $periodeReduction[$i],
                    'pourcentage_reduction' => $pourcentageReduction[$i],
                    'donation_id' => $id_donation
                );      
                $this->db->insert($this->tableStrategieReduction, $data);
            }
        }

        $this->db->where($this->tablePeriodeRecuperation.'.donation_id = '.$id_donation);
        $this->db->delete($this->tablePeriodeRecuperation);

        for($i=0 ; $i<count($periodeRecuperation);$i++){  
            if($periodeRecuperation[$i] != '' && $horaireDebut[$i] != '' && $horaireFin[$i] != ''){ 
                $data = array(
                    'jour' => $periodeRecuperation[$i],
                    'horaire_debut' => $horaireDebut[$i],
                    'horaire_fin' => $horaireFin[$i],
                    'donation_id' => $id_donation
                );      
                $this->db->insert($this->tablePeriodeRecuperation, $data);
            }
        }



    }



    public function saveDonation($idEA,$organisation_id, $idProduit, $quantite, $datePeremption){
        $data = array(
            "entreprise_id" => $idEA,
            "produit_id_produit" => $idProduit,
            "moment" => date('Y-m-d H:i:s'),
            "quantite" => $quantite,
            "date_peremption_produit" => $datePeremption,
            "organisation_id"=>$organisation_id,
            "etat_donation_id"=>2
        );
        $this->db->insert($this->tableDonation, $data);
    }

    public function saveProduit($nom, $categorie, $type, $descr, $infoNutris, $poids, $volume, $prix, $refrigere, $congele, $consoDirect, $groupement){
        $data = array(
            'nom_produit' => $nom,
            'type_produit_id' => $type,
            'description' => $descr,
            'poids' => $poids,
            'type_aliment_id' => $categorie,
            'volume' => $volume,
            'prix' => $prix,
            'groupement'=>$groupement
        );
        $this->db->insert($this->tableProduit, $data);
        $idProduit = $this->db->insert_id();
        
        $data = array(
            'produit_id' => $idProduit, 
            'equipement_id' => 1, // frigo
            'etat' => $congele
        );
        $this->db->insert($this->tableProduitEquipement, $data);
        $data = array(
            'produit_id' => $idProduit, 
            'equipement_id' => 2, // Réfrigérateur
            'etat' => $refrigere
        );
        $this->db->insert($this->tableProduitEquipement, $data);
        $data = array(
            'produit_id' => $idProduit, 
            'equipement_id' => 4, // Pas d'équipement (Consommation directe)
            'etat' => $consoDirect
        );
        $this->db->insert($this->tableProduitEquipement, $data);

        for($i=0; $i<count($infoNutris); $i++){
            if( $infoNutris[$i]>0 ){
                $data = array(
                'produit_id' => $idProduit,
                'information_nutritionnelle_id' => $infoNutris[$i]
                );
                $this->db->insert($this->tableProduitInformationNutritionnelle, $data);
            }
            
        }

        return $idProduit;
    }

    public function updateProduit($id_produit, $nom, $categorie, $type, $descr, $infoNutris, $poids, $volume, $prix, $refrigere, $congele, $consoDirect, $groupement){
        $data = array(
            'nom_produit' => $nom,
            'type_produit_id' => $type,
            'description' => $descr,
            'poids' => $poids,
            'type_aliment_id' => $categorie,
            'volume' => $volume,
            'prix' => $prix,
            'groupement'=>$groupement
        );
        $this->db->where($this->tableProduit.'.id_produit = '.$id_produit);
        $this->db->update($this->tableProduit, $data);
        
        $data = array( 
            'equipement_id' => 1, // frigo
            'etat' => $congele
        );
        $this->db->where($this->tableProduitEquipement.'.produit_id = '.$id_produit.' and equipement_id = 1');
        $this->db->update($this->tableProduitEquipement, $data);
        
        $data = array(
            'equipement_id' => 2, // Réfrigérateur
            'etat' => $refrigere
        );
        $this->db->where($this->tableProduitEquipement.'.produit_id = '.$id_produit.' and equipement_id = 2');
        $this->db->update($this->tableProduitEquipement, $data);
        
        $data = array(
            'equipement_id' => 4, // Pas d'équipement (Consommation directe)
            'etat' => $consoDirect
        );
        $this->db->where($this->tableProduitEquipement.'.produit_id = '.$id_produit.' and equipement_id = 4');
        $this->db->update($this->tableProduitEquipement, $data);

        $this->db->where($this->tableProduitInformationNutritionnelle.'.produit_id = '.$id_produit);
        $this->db->delete($this->tableProduitInformationNutritionnelle);
        for($i=0; $i<count($infoNutris); $i++){
            if( $infoNutris[$i]>0 ){
                $data = array(
                    'produit_id' => $id_produit,
                    'information_nutritionnelle_id' => $infoNutris[$i]
                );
                
                $this->db->insert($this->tableProduitInformationNutritionnelle, $data);
            }
            
        }

    }



    public function saveVenteImport($idEA,$organisation_id, $idProduit, $quantite, $datePeremption, $moment, $raisonSurplus){
        $data = array(
            "entreprise_id" => $idEA,
            "produit_id_produit" => $idProduit,
            "moment" => $moment,
            "quantite" => $quantite,
            "date_peremption_produit" => $datePeremption,
            "organisation_id"=>$organisation_id,
            "etat_donation_id"=>2
        );
        $this->db->insert($this->tableDonation, $data);
        $id_donation = $this->db->insert_id();

        for($i=0; $i<count($raisonSurplus); $i++){
            $in = trim($raisonSurplus[$i]);
            if(!empty($in)){
                $inInDb = $this->db->query('SELECT * FROM '.$this->tableRaisonSurplus.' WHERE LOWER(label)="'.$in.'" AND (proprietaire=0 OR proprietaire='.$idEA.')')->result();
                if(count($inInDb) > 0){
                    $in = $inInDb[0]->id_raison_surplus;
                }
                else{
                    $this->db->insert($this->tableRaisonSurplus, array('label'=>$in, 'proprietaire'=>$idEA));
                    $in = $this->db->insert_id();
                }
                $data = array(
                    'donation_id' => $id_donation,
                    'raison_surplus_id' => $in
                );
                $this->db->insert($this->tableRaisonDonation, $data);
            }
        }
    }

    public function saveProduitImport($nom, $categorie, $type, $descr, $infoNutris, $poids, $volume, $prix, $refrigere, $congele, $idEA, $groupement){
        $typeInDb = $this->db->query('SELECT * FROM '.$this->tableTypeProduit.' WHERE LOWER(label)="'.strtolower($type).'" AND (proprietaire=0 OR proprietaire='.$idEA.')')->result();
        $typeID = 0;
        if(count($typeInDb) > 0){
            $typeID = $typeInDb[0]->id_type_produit;
        }
        else{
            $this->db->insert($this->tableTypeProduit, array('label'=>$type, 'proprietaire'=>$idEA));
            $typeID = $this->db->insert_id();
        }
        $typeCategDb = $this->db->query('SELECT * FROM '.$this->tableTypeAliment.' WHERE LOWER(label)="'.strtolower($categorie).'" AND (proprietaire=0 OR proprietaire='.$idEA.')')->result();
        $categID = 0;
        if(count($typeCategDb) > 0){
            $categID = $typeCategDb[0]->id_type_aliment;
        }
        else{
            $this->db->insert($this->tableTypeAliment, array('label'=>$type, 'proprietaire'=>$idEA));
            $categID = $this->db->insert_id();
        }
        $data = array(
            'nom_produit' => $nom,
            'type_produit_id' => $typeID,
            'description' => $descr,
            'poids' => $poids,
            'volume' => $volume,
            'type_aliment_id' => $categID,
            'prix' => $prix,
            'groupement'=>$groupement
        );
        $this->db->insert($this->tableProduit, $data);
        $idProduit = $this->db->insert_id();

        $congele = strcasecmp($congele, 'oui') == 0 ? 1 : 0;
        $data = array(
            'produit_id' => $idProduit, 
            'equipement_id' => 1, // frigo
            'etat' => $congele
        );
        $this->db->insert($this->tableProduitEquipement, $data);
        
        $refrigere = strcasecmp($refrigere, 'oui') == 0 ? 1 : 0;
        $data = array(
            'produit_id' => $idProduit, 
            'equipement_id' => 2, // Réfrigérateur
            'etat' => $refrigere
        );
        $this->db->insert($this->tableProduitEquipement, $data);
        
        for($i=0; $i<count($infoNutris); $i++){
            $in = trim($infoNutris[$i]);
            if(!empty($in)){
                $inInDb = $this->db->query('SELECT * FROM '.$this->tableInformationNutritionnelle.' WHERE LOWER(label)="'.strtolower($in).'" AND (proprietaire=0 OR proprietaire='.$idEA.')')->result();
                if(count($inInDb) > 0){
                    $in = $inInDb[0]->id_information_nutritionnelle;
                }
                else{
                    $this->db->insert($this->tableInformationNutritionnelle, array('label'=>$in));
                    $in = $this->db->insert_id();
                }
                $data = array(
                    'produit_id' => $idProduit,
                    'information_nutritionnelle_id' => $in
                );
                $this->db->insert($this->tableProduitInformationNutritionnelle, $data);
            }
        }

        $result = array(
            'id_produit' => $idProduit,
            'nom' => $nom,
            'type' => $type
        );
        return $result;
    }

    public function savePhotoProduit($idProduit, $url, $idPhoto, $idEA){
        if($url != null){
            $data = array(
                'produit_id' => $idProduit,
                'url' => $url,
                'proprietaire' => $idEA
            );
        }
        else{
            $this->db->where('id_photo_produit', $idPhoto);
            $urlInDB = $this->db->get($this->tablePhotoProduit)->result()[0]->url;
            $data = array(
                'produit_id' => $idProduit,
                'url' => $urlInDB,
                'proprietaire' => $idEA
            );
        }
        
        $this->db->insert($this->tablePhotoProduit, $data);
    }

    public function updatePhotoProduit($idProduit, $url, $idPhoto, $idEA){
        if($url != null){
            
            $data = array(
                'produit_id' => $idProduit,
                'url' => $url,
                'proprietaire' => $idEA
            );
        }
        else{
            $this->db->where('id_photo_produit', $idPhoto);
            $urlInDB = $this->db->get($this->tablePhotoProduit)->result()[0]->url;

            $data = array(
                'produit_id' => $idProduit,
                'url' => $urlInDB,
                'proprietaire' => $idEA
            );
        }
        
        $this->db->insert($this->tablePhotoProduit, $data);
    }

    public function DeletePhotoProduit($idProduit, $notToDelete=[]){

        $ignore = array();
        for($i=0; $i<count($notToDelete); $i++){
            $ignore[] = $notToDelete[$i];
        }
        if(count($ignore)>0){
            $this->db->where_not_in($this->tablePhotoProduit.'.id_photo_produit', $ignore);
        }

        $this->db->where($this->tablePhotoProduit.'.produit_id = '.$idProduit);
        $this->db->delete($this->tablePhotoProduit);

    }
    

    public function getPeriodeReduction(){
        $result = $this->db->query('SELECT * FROM '.$this->tablePeriodeReduction)->result();
        return $result;
    }

    public function getPeriodeReductionNotPerWeek(){
        
        $ignore = array(1, 2, 3, 4);
        $this->db->select('*');
        $this->db->from($this->tablePeriodeReduction);
        $this->db->where_not_in( $this->tablePeriodeReduction.'.id_periode_reduction' , $ignore);
        $result = $this->db->get()->result();
        return $result;

    }


    public function getAdresseByAdresseRamassageId($id_adresse_ramassage){
        $fields = " label, latitude, longitude, ville";
        $query = "SELECT ".$fields. " FROM ".$this->tableAdresse. " WHERE id_adresse = " .$id_adresse_ramassage;
        $adresse = $this->db->query($query)->result();
        if(count($adresse) == 1){
            return $adresse[0];
        }else{
            return null;
        }
    }

    public function addNewPeriodeStrategie( $label, $nbJourTotal){
        $data = array(
            'label' => $label,
            'nombre_jour' => $nbJourTotal
        );
        $this->db->insert($this->tablePeriodeReduction, $data);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    public function getStrategieMarketingNotPerWeek($id_donation){
        
        $ignore = array(1, 2, 3, 4);
        $this->db->select('*');
        $this->db->from($this->tableStrategieReduction);
        $this->db->join($this->tablePeriodeReduction, $this->tableStrategieReduction.'.periode_reduction_id = '.$this->tablePeriodeReduction.'.id_periode_reduction');
        $this->db->where($this->tableStrategieReduction.'.donation_id = '.$id_donation);
        $this->db->where_not_in( $this->tableStrategieReduction.'.periode_reduction_id' , $ignore);
        $result = $this->db->get()->result();
        return $result;
    }

    public function getStrategieMarketingPerWeek($id_donation){
        
        $consider = array(1, 2, 3, 4);
        $this->db->select('*');
        $this->db->from($this->tableStrategieReduction);
        $this->db->join($this->tablePeriodeReduction, $this->tableStrategieReduction.'.periode_reduction_id = '.$this->tablePeriodeReduction.'.id_periode_reduction');
        $this->db->where($this->tableStrategieReduction.'.donation_id = '.$id_donation);
        $this->db->where_in( $this->tableStrategieReduction.'.periode_reduction_id' , $consider);
        $result = $this->db->get()->result();
        return $result;

    }

    //getPeriodeRecuperation
    public function getPeriodeRecuperation($id_donation){
        $this->db->select('*');
        $this->db->from($this->tablePeriodeRecuperation);
        $this->db->where($this->tablePeriodeRecuperation.'.donation_id = '.$id_donation);
        $result = $this->db->get()->result();
        return $result;
    }

    
    public function getTypeProduitNameById($id_type_produit){
        $this->db->select('label');
        $this->db->from($this->tableTypeProduit);
        $this->db->where($this->tableTypeProduit.'.id_type_produit = '.$id_type_produit);
        $result = $this->db->get()->result();
        if( empty($result) ){
            return null;
        }
        return $result[0];
    }

}