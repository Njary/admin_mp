<?php 
    
    $jours = array('Tous les jours', 'Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche');

    function infoNutrisSelected($infoNutris, $allInfoNutris){
        for($k=0; $k<count($allInfoNutris); $k++){
            if($allInfoNutris[$k]->id_information_nutritionnelle == $infoNutris->id_information_nutritionnelle){
                return true;
            }
        }
        return false;
    }
    function raisonSurplusSelected($raisonSurplus, $allRaisonSurplus){
        for($k=0; $k<count($allRaisonSurplus); $k++){
            if($allRaisonSurplus[$k]->id_raison_surplus == $raisonSurplus->id_raison_surplus){
                return true;
            }
        }
        return false;
    }


?>
<input type="hidden" id="statut_vente_modif" value="<?php echo ($venteModif==null)?0:1; ?>" />
<input type="hidden" id="id_etat_donation" value="<?php echo ($venteModif==null)?-1:$venteModif->etat_donation_id; ?>" />
<input type="hidden" id="id_produit" value="<?php echo ($venteModif==null)?-1:$venteModif->id_produit; ?>" />
<input type="hidden" id="id_donation" value="<?php echo ($venteModif==null)?-1:$venteModif->id_donation; ?>" />

<div class="card card-default" ng-app="VenteApp" ng-controller="VenteCtrl">
    <div class="card-header d-flex p-0">
        <h3 class="card-title p-3">Enregistrement</h3>
        <!--<ul class="nav nav-pills ml-auto p-2">

            <?php //echo ($venteModif==null)?'Enregistrement':'Modification (brouillon)'; ?>

            <li class="nav-item"><a class="nav-link active show" href="#tab_1" data-toggle="tab">Formulaire</a></li> 
            //// Lien qui peut encore servir mais en attente
            <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab">Importer</a></li>
            ////  
                    value="<?php //echo ($vente==null)?'':$vente->nom; ?>"
        </ul>-->
    </div>
    <!-- /.card-header -->
    <div class="card-body">
    <?php if($profilOk){ 
            /*if($contrat['etat'] == 1){*/
        ?>
        <div class="tab-content">
            <div id="tab_1" class="tab-pane active show">
                <div class="row" id="div-input-form-list">
                    <div class="card card-success col-md-12">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="div-input-nom1">
                                        <label>Nom</label>
                                        <input id="input-nom1" class="form-control" type="text" placeholder="Nom du produit"  value="<?php echo ($venteModif==null)?'':$venteModif->nom_produit; ?>"/>
                                    </div>
                                    <div class="form-group" id="div-input-groupement1">
                                        <label>Groupement</label><br>
                                        <label style="margin-right: 10px;">
                                            <input class="type-groupement radio-groupement-unite" id="input-groupement-unite1" type="radio" name="unite" <?php if( $venteModif == null || ($venteModif != null && $venteModif->groupement == 1)) { echo "checked = true"; } ?> />
                                        Par unité
                                        </label>
                                        <label style="margin-right: 10px;">
                                            <input class="type-groupement radio-groupement-portion" id="input-groupement-portion1" type="radio" name="unite" <?php if( $venteModif != null && $venteModif->groupement == 3) { echo "checked = true"; } ?> />
                                        Par portion
                                        </label>
                                        <label>
                                            <input class="type-groupement radio-groupement-totalite" id="input-groupement-total1" type="radio" name="unite" <?php if( $venteModif != null && $venteModif->groupement == 2) { echo "checked = true"; } ?> />
                                        Totalité des produits <span  id="info_choix_totalite" title="Surtout pour les agriculteurs" class="float-right badge info_choix_totalite tippy" style="background-color: #00AFAA; color: #fff; cursor: pointer !important; margin-left: 10px;"><i class="fa fa-info"></i>nfo</span>
                                        </label>
                                    </div>
                                    <div class="form-group" id="div-input-poids1">
                                        <label>Poids</label>
                                        <div class="input-group mb-3">
                                            <input id="input-poids1" class="form-control" type="text" ng-keyup="formatNumberToMileSep('#input-poids1')" placeholder="Poids de l'unité" value= <?php echo ($venteModif==null)?'':($venteModif->poids<0?'':$venteModif->poids); ?>>
                                            <div class="input-group-append">
                                                <span class="input-group-text">Kg</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" id="div-input-volume1">
                                        <label>Volume <span class="info_vente_facultatif1">(facultatif)</span></label>
                                        <div class="input-group mb-3">
                                            <input id="input-volume1" class="form-control" type="text" ng-keyup="formatNumberToMileSep('#input-volume1')" placeholder="Volume de l'unité" value=<?php echo ($venteModif==null)?'':($venteModif->volume<0?'':$venteModif->volume); ?>>
                                            <div class="input-group-append">
                                                <span class="input-group-text"><var>m<sup>3</sup></var></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" id="div-input-prix1">
                                        <label>Prix original <span class="info_vente_totalite1 info_to_hide" > par Kg </span> </label>
                                        <div class="input-group mb-3">
                                            <input id="input-prix1" class="form-control" type="text" placeholder="Prix de l'unité" ng-keyup="makePrixInAppli('#input-prix1')" value=<?php echo ($venteModif==null)?'':($venteModif->prix<0?0:$venteModif->prix); ?>>
                                            <div class="input-group-append">
                                                <span class="input-group-text">MGA</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" id="div-input-quantite1">
                                        <label>Quantité(s)</label>
                                        <div class="input-group mb-3">
                                            <input id="input-quantite1" class="form-control" type="text" ng-keyup="formatNumberToMileSep('#input-quantite1')" placeholder="Quantité" value=<?php echo ($venteModif==null)?'':($venteModif->quantite<0?1:$venteModif->quantite); ?>>
                                        </div>
                                    </div>
                                    <div class="form-group" id="div-input-quantite-minimum1">
                                        <label>Quantité minimum <span class="info_vente_totalite1 info_to_hide"> par Kg (facultatif)</span> <span class="info_vente_facultatif1">(facultatif)</span>  <span  id="info_qte_min" title="C’est la quantité minimum que le client doit acheter du produit" class="float-right badge info_qte_min tippy" style="background-color: #00AFAA; color: #fff; cursor: pointer !important; margin-left: 10px;"><i class="fa fa-info"></i>nfo</span> </label>
                                        <div class="input-group mb-3">
                                            <input id="input-quantite-minimum1" class="form-control" type="text" ng-keyup="formatNumberToMileSep('#input-quantite-minimum1')" placeholder="Quantité minimum " value=<?php echo ($venteModif==null)?'':($venteModif->minimum_qte_vente<0?1:$venteModif->minimum_qte_vente); ?> >
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <!--label style=" margin-bottom: 0rem !important;">Pourcentage de réduction </label><p style="font-size: 0.8em; margin-bottom: 0.5rem;"> (Remise de prix par défaut pour la vente du produit) </p-->
                                        <label> Pourcentage de réduction 
                                            <span  id="info_pourcentage_reduc" title="Remise de prix par défaut pour la vente du produit, Ex: Votre produit coûte 10000 Ar et vous avez choisi une réduction de 25%. Sur notre application le prix affiché sera 7500Ar." class="float-right badge info_pourcentage_reduc tippy" style="background-color: #00AFAA; color: #fff; cursor: pointer !important; margin-left: 10px;">

                                                <i class="fa fa-info"></i>nfo

                                            </span>
                                        </label>
                                        <div class="input-group mb-3">
                                            <input id="input-pourcentage-reduction1" class="form-control" placeholder="Pourcentage de réduction" ng-keyup="makePrixInAppli('#input-prix1')" value=<?php echo ($venteModif==null)?'':($venteModif->pourcentage_reduction<0?0:$venteModif->pourcentage_reduction); ?> >
                                            <div class="input-group-append">
                                                <span class="input-group-text">%</span>
                                            </div>
                                        </div>
                                        <div id="div-input-pourcentage-reduction1"></div>

                                        <label> Prix sur l'application :</label>
                                        <div class="input-group mb-3" id="div-input-prix-sur-appli1">

                                            <input id="input-prix-sur-appli1" class="form-control" type="text" placeholder="Prix affiché sur l'application mobile" value="<?php echo ($venteModif==null)?'':((($venteModif->prix - (($venteModif->prix * $venteModif->pourcentage_reduction)/100))<0)?0:($venteModif->prix - (($venteModif->prix * $venteModif->pourcentage_reduction)/100))); ?>" disabled >
                                            <div class="input-group-append">
                                                <span class="input-group-text">MGA</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" id="div-input-date-peremption-production1">
                                    
                                        <label style="margin-right: 10px;">
                                            <input class="radio-choix-type-date" id="input-choix-date-peremption1" type="radio"  name="type-date" value=1 <?php if( $venteModif == null || $venteModif->type_date == 1) { echo "checked = true"; } ?> />
                                        Date de péremption
                                        </label>
                                        <label>
                                            <input id="input-choix-date-production1" type="radio" name="type-date" value=2 <?php if( $venteModif != null && $venteModif->type_date == 2) { echo "checked = true"; } ?> />
                                        Date de production
                                        </label>
                                        <div class="input-group mb-3">
                                            <?php  $date = ($venteModif != null && $venteModif->date_peremption_produit != '')? date('d/m/Y', strtotime($venteModif->date_peremption_produit)) : ''; ?>
                                            <input id="input-date-peremption-production1" class="form-control datepicker" type="text" placeholder="Date de péremption" value=<?php echo $date; ?>>
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label id="scroll-strategie-prix">Stratégie marketing du prix (facultatif) 
                                            <span  id="info_pourcentage_reduc" title="La stratégie marketing vous aide à établir automatiquement  un système de réduction du prix selon l’approximation de la date de péremption du produit." class="float-right badge info_strategie_reduc tippy" style="background-color: #00AFAA; color: #fff; cursor: pointer !important; margin-left: 10px;">

                                                <i class="fa fa-info"></i>nfo

                                            </span>
                                        </label>

                                        <!-- DEBUT STRATEGIE PAR SEMAINE -->
                                        <div class="form-group row">
                                            <div class="col-6">
                                                <div class="input-group mb-3">
                                                    <input id="input-strategie-4-sem" class="form-control" type="text" value="4 Semaines" disabled >
                                                </div>
                                            </div>
                                            <div class="col-6 div-percent-par-sem-to-test" >
                                                <div class="input-group mb-3" >
                                                    <input id="input-pourcentage_reduction_strategie-4-sem1" name="pourcentage_reduction_strategie-4-sem1" type="text" class="form-control percent-par-sem-to-test" placeholder="Réduction" required="" list="propositionReduction4sem" 
                                                        value  = "<?php if($venteModif != null && (count($venteModif->strategieMarketingPerWeek)>0)){
                                                                    for($i=0; $i<count($venteModif->strategieMarketingPerWeek); $i++){
                                                                        if($venteModif->strategieMarketingPerWeek[$i]->periode_reduction_id == 1){
                                                                            echo $venteModif->strategieMarketingPerWeek[$i]->pourcentage_reduction;
                                                                        }
                                                                    } 
                                                                } ?> " >
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">%</span>
                                                    </div>
                                                    <datalist id="propositionReduction4sem">
                                                        <option>20</option>
                                                        <option>25</option>
                                                        <option>30</option>
                                                        <option>50</option>
                                                        <option>60</option>
                                                        <option>70</option>
                                                        <option>80</option>
                                                        <option>90</option>
                                                    </datalist>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-6">
                                                <div class="input-group mb-3">
                                                    <input id="input-strategie-3-sem" class="form-control" type="text" value="3 Semaines" disabled >
                                                </div>
                                            </div>
                                            <div class="col-6 div-percent-par-sem-to-test">
                                                <div class="input-group mb-3"  >
                                                    <input id="input-pourcentage_reduction_strategie-3-sem1" name="pourcentage_reduction_strategie-3-sem1" type="text" class="form-control percent-par-sem-to-test" placeholder="Réduction" required="" list="propositionReduction3sem" 
                                                        value  = "<?php if($venteModif != null && (count($venteModif->strategieMarketingPerWeek)>0)){
                                                                    for($i=0; $i<count($venteModif->strategieMarketingPerWeek); $i++){
                                                                        if($venteModif->strategieMarketingPerWeek[$i]->periode_reduction_id == 2){
                                                                            echo $venteModif->strategieMarketingPerWeek[$i]->pourcentage_reduction;
                                                                        }
                                                                    } 
                                                                } ?> "  >
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">%</span>
                                                    </div>
                                                    <datalist id="propositionReduction3sem">
                                                        <option>20</option>
                                                        <option>25</option>
                                                        <option>30</option>
                                                        <option>50</option>
                                                        <option>60</option>
                                                        <option>70</option>
                                                        <option>80</option>
                                                        <option>90</option>
                                                    </datalist>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-6" >
                                                <div class="input-group mb-3">
                                                    <input id="input-strategie-2-sem" class="form-control" type="text" value="2 Semaines" disabled >
                                                </div>
                                            </div>
                                            <div class="col-6 div-percent-par-sem-to-test" >
                                                <div class="input-group mb-3">
                                                    <input id="input-pourcentage_reduction_strategie-2-sem1" name="pourcentage_reduction_strategie-2-sem1" type="text" class="form-control percent-par-sem-to-test" placeholder="Réduction" required="" list="propositionReduction2sem" 
                                                        value  =  "<?php if($venteModif != null && (count($venteModif->strategieMarketingPerWeek)>0)){
                                                                    for($i=0; $i<count($venteModif->strategieMarketingPerWeek); $i++){
                                                                        if($venteModif->strategieMarketingPerWeek[$i]->periode_reduction_id == 3){
                                                                            echo $venteModif->strategieMarketingPerWeek[$i]->pourcentage_reduction;
                                                                        }
                                                                    } 
                                                                } ?> "  >
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">%</span>
                                                    </div>
                                                    <datalist id="propositionReduction2sem">
                                                        <option>20</option>
                                                        <option>25</option>
                                                        <option>30</option>
                                                        <option>50</option>
                                                        <option>60</option>
                                                        <option>70</option>
                                                        <option>80</option>
                                                        <option>90</option>
                                                    </datalist>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-6 div-percent-par-sem-to-test">
                                                <div class="input-group mb-3">
                                                    <input id="input-strategie-1-sem" class="form-control" type="text" value="1 Semaine" disabled >
                                                </div>
                                            </div>
                                            <div class="col-6" id="div-input-pourcentage_reduction_strategie-1-sem1">
                                                <div class="input-group mb-3">
                                                    <input id="input-pourcentage_reduction_strategie-1-sem1" name="pourcentage_reduction_strategie-1-sem1" type="text" class="form-control percent-par-sem-to-test" placeholder="Réduction" required="" list="propositionReduction1sem" 
                                                        value  =  "<?php if($venteModif != null && (count($venteModif->strategieMarketingPerWeek)>0)){
                                                                    for($i=0; $i<count($venteModif->strategieMarketingPerWeek); $i++){
                                                                        if($venteModif->strategieMarketingPerWeek[$i]->periode_reduction_id == 4){
                                                                            echo $venteModif->strategieMarketingPerWeek[$i]->pourcentage_reduction;
                                                                        }
                                                                    } 
                                                                } ?> " >
                                                    <div class="input-group-append">
                                                        <span class="input-group-text">%</span>
                                                    </div>
                                                    <datalist id="propositionReduction1sem">
                                                        <option>20</option>
                                                        <option>25</option>
                                                        <option>30</option>
                                                        <option>50</option>
                                                        <option>60</option>
                                                        <option>70</option>
                                                        <option>80</option>
                                                        <option>90</option>
                                                    </datalist>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- FIN STRATEGIE PAR SEMAINE -->

                                        <div class="form-group row strategie1">
                                          <div class="col-5">

                                                <select id="select-strategie-prix1" data-placeholder="Période du stratégie" class="form-control select2 select-strategie-prix1 periode-to-test " style="width: 100%;">

                                                    <option></option>
                                                    <?php for($i=0; $i<count($periodeReduction); $i++){ ?>
                                                        <option value="<?php echo $periodeReduction[$i]->id_periode_reduction; ?>" <?php if($venteModif != null && (count($venteModif->strategieMarketing)>0)){
                                                                    if($venteModif->strategieMarketing[0]->periode_reduction_id == $periodeReduction[$i]->id_periode_reduction){ echo "selected"; }
                                                            } ?> > <?php echo $periodeReduction[$i]->label; ?>
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                                <!--div class="add-periode-strategie">
                                                     <a href="#"  ng-click="formClick('1')" data-toggle="modal" data-target="#modal-new-periode-strategie" class=""><span class="float-right badge bg-success"><i class="fa fa-plus"></i> Ajouter</span></a>
                                                </div-input-->
                                                <div class="add-periode-strategie">
                                                     <a href="#" ng-click="createPeriodeStrategie(1)" data-toggle="modal" data-target="#modal-new-periode-strategie" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
                                                </div>
                                               
                                                <input id="nb_periode_reduction" type="hidden" value="<?php echo count($periodeReduction); ?>"/>

                                                    
                                          </div>
                                          <div class="col-5 div-percent-to-test" >
                                            <div class="input-group mb-3">
                                                <input id="input-pourcentage_reduction_strategie1" name="pourcentage_reduction_strategie1" type="text" class="form-control percent-to-test" placeholder="Réduction" required="" list="propositionReduction" 
                                                    value  = <?php if($venteModif != null && (count($venteModif->strategieMarketing)>0)){
                                                                echo $venteModif->strategieMarketing[0]->pourcentage_reduction; 
                                                            } ?> >
                                                <div class="input-group-append">
                                                    <span class="input-group-text">%</span>
                                                </div>
                                                <datalist id="propositionReduction">
                                                    <option>20</option>
                                                    <option>25</option>
                                                    <option>30</option>
                                                    <option>50</option>
                                                    <option>60</option>
                                                    <option>70</option>
                                                    <option>80</option>
                                                    <option>90</option>
                                                </datalist>
                                            </div>
                                          </div>
                                          <div class="form-group col-2">
                                            <a id="" class="btn btn-primary btn-block addStrategie" ng-click="addStrategieMarketing()" title="Ajouter une autre stratégie marketing"><span>Ajouter</span></a>
                                          </div>
                                        </div>
                                        <!-- SELECT OPTION STRATEGIE MARKETING -->
                                        <div id="containerSelectStrategieMarketing" style=" display: none;">
                                            <select id="selectStrategieMarketing" class="periode-to-test-other">
                                                <option></option>
                                                <?php for($i=0; $i<count($periodeReduction); $i++){ ?>
                                                        <option value="<?php echo $periodeReduction[$i]->id_periode_reduction; ?>"> <?php echo $periodeReduction[$i]->label; ?>
                                                        </option>
                                                <?php } ?>
                                            </select>
                                        </div>
                                        <!-- ::::::::::::::::::::::::::::::::::::::::: -->

                                    </div>
                        
                                    <div id="otherStrategie" class="form-group">
                                        <input type="hidden" id="nb_strategie_marketing" value="<?php  if($venteModif != null){echo count($venteModif->strategieMarketing);} ?>"/>
                                     
                                        <?php if($venteModif != null && (count($venteModif->strategieMarketing)>1)){
                                        ?>
                                        
                                        <?php 
                                                for($i=1;$i<count($venteModif->strategieMarketing);$i++){
                                            ?>

                                                <div class="form-group row strategie<?php echo $i+1;  ?>">
                                                    <div class="col-5 ">
                                                        <select id="select-strategie-prix<?php echo $i+1;  ?>" name="periodeStrategie<?php echo $i+1;  ?>" data-placeholder="Période du stratégie" class="form-control select2 periode-to-test" style="width: 100%;">
                                                            <option></option>
                                                            <?php for($j=0; $j<count($periodeReduction); $j++){ ?>
                                                                <option value="<?php echo $periodeReduction[$j]->id_periode_reduction; ?>"<?php if($venteModif->strategieMarketing[$i]->periode_reduction_id == $periodeReduction[$j]->id_periode_reduction){ echo "selected"; }
                                                                ?>

                                                                ><?php echo $periodeReduction[$j]->label; ?>
                                                                </option>
                                                            <?php } ?>
                                                        </select>
                                                        <a href="#" ng-click="createPeriodeStrategie(<?php echo $i+1;  ?>)" data-toggle="modal" data-target="#modal-new-periode-strategie" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
                                                    </div>
                                                    <div class="col-5">
                                                        <div class="input-group mb-3">
                                                            <input  id="pourcentageStrategie" name="pourcentage_reduction_strategie<?php echo $i+1;  ?>" type="text" class="form-control percent-to-test" placeholder="Réduction" required="" list="propositionReduction" 
                                                                 value  = <?php echo $venteModif->strategieMarketing[$i]->pourcentage_reduction; ?> 
                                                            >
                                                            <div class="input-group-append">
                                                                <span class="input-group-text">%</span>
                                                            </div>
                                                            <datalist  id="propositionReduction">
                                                                <option>20</option>
                                                                <option>25</option>
                                                                <option>30</option>
                                                                <option>50</option>
                                                                <option>60</option>
                                                                <option>70</option>
                                                                <option>80</option>
                                                                <option>90</option>
                                                            </datalist>
                                                        </div>
                                                    </div>
                                                    <div class="form-group col-2">
                                                        <a class="btn btn-default btn-block" ng-click="removeStrategie('.strategie<?php echo $i+1;  ?>')" title="Supprimer cette stratégie marketing"><i class="fa fa-minus"></i></a>
                                                    </div>
                                                </div>  
                                        
                                        <?php   } 
                                            }   
                                        ?>
                                        <!--  removeStrategie( \'.strategie1\')-->
                                    </div>
                                    

                                    <div class="form-group">
                                        <label class="check" style="font-weight: bolder !important;">
                                            <input id="input-refrigere1" type="checkbox" class="flat-red" <?php if( $venteModif != null && $venteModif->refrigere == 1) { echo "checked = true"; } ?> /> Le produit nécessite d'être réfrigéré
                                        </label>
                                        <label class="check" style="font-weight: bolder !important;">
                                            <input id="input-congele1" type="checkbox" class="flat-red" <?php if( $venteModif != null && $venteModif->congele == 1) { echo "checked = true"; } ?> /> Le produit nécessite d'être congelé
                                        </label>
                                        <label class="check" style="font-weight: bolder !important;">
                                            <input id="input-consoDirect1" type="checkbox" class="flat-red" <?php if( $venteModif != null && $venteModif->consoDirect == 1) { echo "checked = true"; } ?> /> Le produit nécessite ni d'être réfrigéré ni d'être congelé
                                        </label>
                                    </div>
                                    <!--
                                    <div class="callout callout-success info_detaille">
                                        <div class="row">
                                            <div class="col-sm-1">
                                                (1) :
                                            </div>
                                            <div class="col-sm-11">
                                                Remise de prix par défaut pour la vente du produit, Ex: Votre produit coûte 10000 Ar et vous avez choisi une réduction de 25%. Sur notre application le prix affiché sera 7500Ar. 
                                            </div>
                                        </div>
                                        <br>
                                         <div class="row">
                                            <div class="col-sm-1">
                                                (2) :
                                            </div>
                                            <div class="col-sm-11">
                                                La stratégie marketing vous aide à établir automatiquement  un système de réduction du prix selon l’approximation de la date de péremption du produit.
                                            </div>
                                        </div>
                                    </div> -->
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Catégorie</label>
                                        <div class="row" id="div-select-categorie1">
                                            <div class="col-12">
                                                <select onchange="angular.element(this).scope().TYPE.onValueChanged(1)" id="select-categorie1" data-placeholder="Catégorie du produit" class="select-categorie form-control select2" style="width: 100%;">
                                                <option></option>
                                                    <?php for($i=0; $i<count($categories); $i++){ ?>
                                                        <option value="<?php echo $categories[$i]->id_type_aliment; ?>" <?php if($venteModif != null && $categories[$i]->id_type_aliment == $venteModif->type_aliment_id ) { echo "selected"; }?> > <?php echo $categories[$i]->label; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <div >
                                                     <a href="#" id="bouton-form-ajouter-categorie"  ng-click="formClick('1')" data-toggle="modal" data-target="#modal-new-categorie" title="Ajouter une nouvelle catégorie" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
                                                </div>
                                            </div>
                                            <!--<div class="col-1" style="margin-left: -7.5px;">
                                                <button ng-click="formClick('1')" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal-new-categorie" title="Ajouter une nouvelle catégorie"><i class="fa fa-plus"></i></button>
                                            </div-input-->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Type</label>
                                        <div class="row" id="div-select-type1">
                                            <div class="col-12">
                                                <select id="select-type1" data-placeholder="Type du produit" class="select-type form-control select2" style="width: 100%;">
                                                <option></option>
                                                    <?php for($i=0; $i<count($types); $i++){ ?>
                                                        <option value="<?php echo $types[$i]->id_type_produit; ?>" <?php if($venteModif != null && $types[$i]->id_type_produit == $venteModif->type_produit_id ) { echo "selected"; }?> > <?php echo $types[$i]->label; ?>
                                                            
                                                        </option>
                                                    <?php } ?>
                                                </select>
                                                 <div style="display: none">
                                                     <a href="#" id="bouton-form-ajouter-type"  ng-click="formClick('1')" ><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
                                                </div>
                                            </div>
                                            <!--div class="col-1" style="margin-left: -7.5px;">
                                                <button ng-click="formClick('1')" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal-new-type" title="Ajouter un nouveau type"><i class="fa fa-plus"></i></button>
                                            </div-input-->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Informations nutritionnelles (facultatif) </label>
                                        <div class="row">
                                            <div class="col-12" id="div-select-info-nutris1">
                                                <select id="select-info-nutris1" class="select-info-nutris form-control select2 select2-hidden-accessible" multiple="" data-placeholder="Informations nutritionnelles" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                <?php for($i=0; $i<count($infoNutris); $i++){ ?>
                                                    <option value="<?php echo $infoNutris[$i]->id_information_nutritionnelle; ?>" <?php if($venteModif != null && infoNutrisSelected($infoNutris[$i], $venteModif->info_nutris)) { echo "selected"; } ?> ><?php echo $infoNutris[$i]->label; ?></option>
                                                <?php } ?>
                                                </select>
                                                <div style="display: none">
                                                     <a href="#" id="bouton-form-ajouter-info-nutri" ng-click="formClick('1')" ><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
                                                </div>
                                            </div>
                                            <!--div class="col-1" style="margin-left: -7.5px;">
                                                <button ng-click="formClick('1')" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal-new-info-nutri" title="Ajouter une information nutritionnelle"><i class="fa fa-plus"></i></button>
                                            </div-input-->
                                        </div>
                                    </div>
                                    
                                    <!--<div class="form-group">
                                        <label>Surplus alimentaire ? </label><br/>
                                        <label style="margin-right: 10px;">
                                            <input id="input-surplus-alim-oui1" type="radio" name="surplus" <?php //if( $venteModif != null && count($venteModif->raisonSurplus) > 0) { echo "checked = true"; } ?> />
                                        Oui
                                        </label>
                                        <label>
                                            <input class="radio-surplus-alimentaire" id="input-surplus-alim-non1" type="radio" name="surplus" <?php //if( $venteModif == null || count($venteModif->raisonSurplus) == 0) { echo "checked = true"; } ?>/>
                                        Non
                                        </label>
                                    </div>-->
                                   
                                    <div class="form-group">
                                        <label>Raisons des surplus (facultatif)</label>
                                        <div class="row">
                                            <div class="col-12" id="div-select-raison-surplus1">
                                                <select id="select-raison-surplus1" class="select-raison-surplus form-control select2 select2-hidden-accessible" multiple="" data-placeholder="Raison des surplus" style="width: 100%;" tabindex="-1" aria-hidden="true" <?php// if( $venteModif == null || ($venteModif !=null && count($venteModif->raisonSurplus) == 0 ) ) { echo "disabled "; } ?> >
                                                <?php for($i=0; $i<count($raisonSurplus); $i++){ ?>
                                                    <option value="<?php echo $raisonSurplus[$i]->id_raison_surplus; ?>" <?php if($venteModif != null && raisonSurplusSelected($raisonSurplus[$i], $venteModif->raisonSurplus)) { echo "selected"; } ?> ><?php echo $raisonSurplus[$i]->label; ?></option>
                                                <?php } ?>
                                                </select>
                                                <div style="display: none">
                                                     <a href="#" id="bouton-form-ajouter-raison-surplus"  ng-click="formClick('1')" ><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
                                                </div>
                                            </div>
                                            <!--div class="col-1" style="margin-left: -7.5px;">
                                                <button  id="ajout_raison_surplus1" ng-click="formClick('1')" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal-new-raison-surplus" <?php //if( $venteModif == null || ($venteModif !=null && count($venteModif->raisonSurplus) == 0 ) ) { echo "disabled "; } ?> title="Ajouter un raison de surplus"><i class="fa fa-plus"></i></button>
                                            </div-input-->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Description (facultatif)</label>
                                        <textarea id="textarea-description1" class="form-control" rows="3" placeholder="Description du produit" ><?php echo ($venteModif==null)?'':$venteModif->description; ?></textarea>
                                    </div>

                                    <div class="form-group">
                                        <label>Lieu de collecte du produit </label><br/>
                                        <label style="margin-right: 10px;">
                                            <input  class="radio-lieu-ramassage" id="input-lieu-ramassage-adresse_entreprise1" type="radio" name="lieuRamassage" <?php if($venteModif == null || ($venteModif != null && $venteModif->adresseRamassage == null)) { echo "checked = true"; } ?> />
                                        Adresse courant
                                        </label>
                                        <label>
                                            <input id="input-lieu-ramassage-autre1" type="radio" name="lieuRamassage"  <?php if($venteModif != null && $venteModif->adresseRamassage != null) { echo "checked = true"; } ?> />
                                        Autre
                                        </label>

                                        <div class="form-group" id="formAdresse">
                                            <div id="div-erreur-adresse"></div>

                                            <div class="form-group " id="adresseCourant">
                                                
                                               <?php //if( $venteModif == null ) { ?>
                                                    <div class="callout callout-warning" >
                                                       
                                                            <span style=" display: inline-flex; align-items: center; " id="adresseEntreprise1"><?php echo $adresse[0]->label.', '.$adresse[0]->code_postal.' '.$adresse[0]->ville.', '.$adresse[0]->pays; ?></span>
                                                    </div> 
                                                <?php //} ?>

                                                <input type="hidden" id="id_adresseCourantHidden1" value="<?php echo $adresse[0]->id_adresse; ?>"/>
                                                <input type="hidden" id="adresseCourantHidden1" value="<?php echo $adresse[0]->label; ?>"/>
                                                <input type="hidden" id="codePostalCourantHidden1" value="<?php echo $adresse[0]->code_postal; ?>"/>
                                                <input type="hidden" id="villeCourantHidden1" value="<?php echo $adresse[0]->ville; ?>"/>
                                                <input type="hidden" id="paysCourantHidden1" value="<?php echo $adresse[0]->pays_id; ?>"/>
                                                <input type="hidden" id="latitudeCourantHidden1" value="<?php echo $adresse[0]->latitude; ?>">
                                                <input type="hidden" id="longitudeCourantHidden1" value="<?php echo $adresse[0]->longitude; ?>">  
                                            </div>

                                            <div class="form-group " >
                                                <div class="form-group" >
                                                    <div class="input-group mb-3">
                                                        <input type="hidden" id="id_lieu_rmsg1" value="<?php echo ($venteModif==null)?'' : (($venteModif->adresseRamassage != null)?$venteModif->adresseRamassage->id_adresse : ''); ?>" />
                                                        <input name="autre_adresse1" type="text" id="autreAdresse1" class="form-control adresse-to-test" placeholder="Autre adresse*" value="<?php echo ($venteModif==null)?'':(($venteModif->adresseRamassage != null)?$venteModif->adresseRamassage->label : ''); ?>">
                                                    </div>
                                                </div>
                                                <div class="form-group div-adresse-to-test">
                                                    <div class="input-group mb-3 ">
                                                        <input name="code_postal_autre_adresse1" id="codePostalAutreAdresse1" class="form-control adresse-to-test" type="text"  placeholder="Code postal*" require="" value="<?php echo ($venteModif==null)?'': (($venteModif->adresseRamassage != null)?$venteModif->adresseRamassage->code_postal : ''); ?>"/>
                                                    </div>
                                                </div>
                                                <div class="form-group div-adresse-to-test">
                                                    <div class="input-group mb-3">
                                                        <input name="ville_autre_adresse1" type="text" id="villeAutreAdresse1" class="form-control adresse-to-test" placeholder="Ville*" require="" value="<?php echo ($venteModif==null)?'':(($venteModif->adresseRamassage != null)?$venteModif->adresseRamassage->ville : ''); ?>"/>
                                                    </div>
                                                </div>
                                                <div class="form-group div-adresse-to-test">
                                                
                                                        <select id="pays_autre_adresse1" name="pays_autre_adresse1" data-placeholder="Pays*" class="form-control select2 adresse-to-test" style="width: 100%;" require="">
                                                            <option></option>
                                                            <?php //$pays = array('Madagascar','La Réunion','Maurice','Comore');
                                                            for($i=0; $i<count($pays); $i++){ ?>
                                                                <option value="<?php echo $pays[$i]->id_pays; ?>" 
                                                                    <?php if($venteModif!=null  && $venteModif->adresseRamassage != null){ 
                                                                        if($venteModif->adresseRamassage->pays_id == $pays[$i]->id_pays){
                                                                             echo 'selected';
                                                                         }
                                                                     }; ?>><?php echo $pays[$i]->label ?></option>
                                                            <?php } ?>
                                                        </select>
                                                    
                                                </div>

                                                <h6 id="scroll-coordonnee-entreprise">Coordonnée géographique (Latitude, Longitude)</h6>
                
                                                <div class="form-group row div-lat-long-to-test">
                                                    <div class="form-group col-md-5">
                                                        <input id="infoLatitude1" name="latitude_entreprise1" value="<?php echo ($venteModif==null)?'':(($venteModif->adresseRamassage != null)?$venteModif->adresseRamassage->latitude : ''); ?>" type="text" class="form-control form-to-test adresse-to-test lat-to-test" placeholder="Latitude*" require="" title="Latitude"/>
                                                    </div>
                                                    <div class="form-group col-md-5">
                                                        <input id="infoLongitude1" name="longitude_entreprise1" value="<?php echo ($venteModif==null)?'':(($venteModif->adresseRamassage != null)?$venteModif->adresseRamassage->longitude : ''); ?>" type="text" class="form-control form-to-test adresse-to-test long-to-test " placeholder="Longitude*" require="" title="Longitude"/>
                                                    </div>
                                                    <div class="col-md-2">
                                                        <a href="#"  class="btn btn-primary btn-block" ng-click="initShowMap(1)" data-toggle="modal" data-target="#modal-show-map" title="Sélectionner les coordonnées sur une carte" ><i class="fa fa-map"></i> </a>
                                                    </div>
                                                
                                                </div>

                                            </div>
                                        </div>

                                    </div>

                                    <div >
                                        <div class="form-group ">
                                            <label>Période de collecte du produit </label><br/>
                                                <div class="row">
                                                    <div class="col-4">
                                                        <label>Jour</label>
                                                    </div>
                                                    <div class="col-3">
                                                        <label>Horaire début</label>
                                                    </div>
                                                    <div class="col-3">
                                                        <label>Horaire fin</label>
                                                    </div>
                                                    <div class="col-2">

                                                    </div>
                                                </div>
                                                <div class="row recuperation1">
                                                    <div class="col-4 div-periode-recup-to-test">

                                                        <select id="select-periode-recuperation1" data-placeholder="Période de récupération" class="form-control select2 select-periode-recuperation1 periode-recup-to-test" style="width: 100%;">  
                                                            <option></option>
                                                            <?php for($i=0; $i<count($jours); $i++){ ?>
                                                                <option value="<?php echo $jours[$i]; ?>" <?php if($venteModif != null && (count($venteModif->periodeRecuperation)>0)){

                                                                     echo (strcmp($venteModif->periodeRecuperation[0]->jour, $jours[$i])==0) ?'selected': ''; }?>> <?php echo $jours[$i] ?>
                                                                         
                                                                </option>

                                                            <?php } ?>   
                                                        </select>
                                                        
                                                    </div>
                                                      <div class="col-3 div-horaire-debut-to-test">
                                                        <div class="input-group mb-3" id="dateTimepicker">
                                                            <input id="input-horaire-debut1" name="horaire-debut1" type="text" class="form-control horaire-debut-to-test" placeholder="Début" required="" 
                                                                value  = <?php if($venteModif != null && (count($venteModif->periodeRecuperation)>0)){
                                                                            echo $venteModif->periodeRecuperation[0]->horaire_debut; 
                                                                        } ?> >
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                                            </div>
                                                        </div>
                                                      </div>
                                                      <div class="col-3 div-horaire-fin-to-test">
                                                        <div class="input-group mb-3">
                                                            <input id="input-horaire-fin1" name="horaire-fin1" type="text" class="form-control horaire-fin-to-test" placeholder="Fin" required="" 
                                                                value  = <?php if($venteModif != null && (count($venteModif->periodeRecuperation)>0)){
                                                                            echo $venteModif->periodeRecuperation[0]->horaire_fin; 
                                                                        } ?> >
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                                            </div>
                                                        </div>
                                                      </div>
                                                      <div class="form-group col-2">
                                                        <a id="" class="btn btn-primary btn-block addPeriodeRecuperation" ng-click="addPeriodeRecuperation()" title="Ajouter une autre période de récupération"><span>Ajouter</span></a>
                                                      </div>
                                                </div>
                                                <div class="div-erreur-horaire"></div>   

                                                <input id="nb_periode_recuperation" type="hidden" value="<?php if($venteModif != null){ echo count($venteModif->periodeRecuperation); }else{ echo "1"; } ?>"/>

                                                <!-- SELECT OPTION DES JOURS DE RECUPERATION -->
                                                <div id="containerSelectPeriodeRecuperation" style=" display: none;">
                                                    <select id="selectPeriodeRecuperation" class="periode-recup-to-test-other">
                                                        <option></option>
                                                        <?php for($i=0; $i<count($jours); $i++){ ?>
                                                                <option value="<?php echo $jours[$i]; ?>"> <?php echo $jours[$i] ?>
                                                                </option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                                <!-- ::::::::::::::::::::::::::::::::::::::::::::: -->
                                        </div>
                                    </div>

                                    <div id="otherPeriodeRecuperation" class="form-group">


                                        <?php if($venteModif != null && (count($venteModif->periodeRecuperation)>1)){ ?>
                                        
                                        <?php   for($j=1;$j<count($venteModif->periodeRecuperation);$j++){  ?>

                                                <div class="form-group row recuperation<?php echo $j+1; ?>">
                                                    <div class="col-4">

                                                        <select name="selectPeriodeRecuperation<?php echo $j+1; ?>" data-placeholder="Période de récupération" class="form-control select2 periode-recup-to-test" style="width: 100%;" style="width: 100%;">  
                                                            <option></option>
                                                            <?php for($i=0; $i<count($jours); $i++){ ?>
                                                                <option value="<?php echo $jours[$i]; ?>" <?php echo  (strcmp($venteModif->periodeRecuperation[$j]->jour, $jours[$i])==0)?'selected': ''; ?>><?php echo $jours[$i] ?></option>
                                                            <?php } ?>   
                                                        </select>
                                                            
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="input-group mb-3">
                                                            <input name="horaire-debut<?php echo $j+1; ?>" type="text" class="form-control horaire-debut-to-test" placeholder="Début" required="" 
                                                                value  = <?php echo $venteModif->periodeRecuperation[$j]->horaire_debut;  ?> >
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-3">
                                                        <div class="input-group mb-3">
                                                            <input  name="horaire-fin<?php echo $j+1; ?>" type="text" class="form-control horaire-fin-to-test" placeholder="Fin" required="" 
                                                                value  = <?php echo $venteModif->periodeRecuperation[$j]->horaire_fin;  ?> >
                                                            <div class="input-group-append">
                                                                <span class="input-group-text"><i class="fa fa-clock-o"></i></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                      <div class="form-group col-2">
                                                       <a class="btn btn-default btn-block" ng-click="removePeriodeRecuperation('.recuperation<?php echo $j+1; ?>')" title="Supprimer période de récupération"><i class="fa fa-minus"></i></a>
                                                    </div>
                                                </div>
                                         
                                         <?php  } 
                                            }   
                                        ?>
                                    </div>

                                    <div class="form-group">
                                        <label>Photos</label><br>
                                        <div class="row">
                                            <div class="col-6">
                                                <label ng-click="formClick('1')" for="input-file-image1" class="form-control btn btn-secondary">Ajouter depuis mon ordinateur<i class="fa fa-picture-o" style="margin-left:8px;"></i></label>
                                                <input id="input-file-image1" onchange="angular.element(this).scope().onFileImageChange(this)" type="file" accept="image/*" style="display: none;" multiple />
                                            </div>
                                            <div class="col-6">
                                                <button ng-click="showPhotoPicker('1', '<?php echo base_url(''); ?>','<?php echo base_url(''); ?>g-p-p')" class="form-control btn btn-secondary"><strong style="color: white;">Ajouter depuis le serveur</strong><i class="fa fa-picture-o" style="margin-left:8px;"></i></button>
                                            </div>
                                        </div>
                                        <div class="row" id="div-input-file-product1">
                                            <?php if($venteModif != null){ 

                                                    for($i=0; $i<count($venteModif->photo);$i++){ ?>
                                                    <div class="col-6 div-input-img-preview" id="div-input-img-product<?php echo $venteModif->photo[$i]['id_photo_produit']; ?>">
                                                        <input type="hidden" class="photoInBase" value="<?php echo $venteModif->photo[$i]['id_photo_produit']; ?>"/>
                                                        <img src="<?php echo base_url().$venteModif->photo[$i]['url']; ?>"/> 
                                                        <button ng-click="removeImageProduct(<?php echo $venteModif->photo[$i]['id_photo_produit']; ?>)" class="btn btn-secondary"><i class="fa fa-trash" ></i></button>
                                                    </div>
                                                <?php 
                                                    }
                                                } ?>
                                        </div>
                                        <div class="row" id="div-file-preview1">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- validateFormulaire('<?php echo base_url(''); ?>p-f-v') -->
                <div class="card-footer right">
                    <button id="btn-save-draft" ng-click="saveDraft('<?php echo base_url(''); ?>s-a-d')" class="btn btn-secondary float-left">Enregistrer comme brouillon</button>
                    <button id="btn-reset-all" ng-click="resetAllField()" class="btn btn-secondary float-left" title="Réinitialiser tous les champs" style="margin-left:8px;"><i class="fa fa-repeat"></i></button>
                    <button id="btn-validate" ng-click="checkFormulaire('<?php echo base_url(''); ?>p-f-v')" class="btn btn-primary">Valider</button>
                    <div id="lds-roller" class="lds-roller" style="float: right;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                </div>
            </div>

            <div id="tab_2" class="tab-pane">
                <div class="row">
                    <div class="col-md-12">
                        <div class="rv-div-input-import row">
                            <div class="col-md-8">
                                <label id="label-file-excel" for="input-file-excel" class="form-control btn btn-primary col-12" style="white-space: inherit;padding: 16px 0;">Sélectionner le fichier Excel à importer</label>
                                <input id="input-file-excel" onchange="$('#label-file-excel').html(this.value)" type="file" accept=".xls,.xlsx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel" style="display: none;" />
                            </div>
                            <div class="col-md-2">
                                <button id="btn-import" ng-click="import('<?php echo base_url(''); ?>i-d')" class="btn btn-secondary col-12" style="padding: 16px 0;">Valider</button>
                            </div>
                            <div class="col-md-2">
                                <a id="btn-download-model" href="<?php echo base_url(''); ?>uploads/default/excel.vente.xlsx" class="btn btn-secondary col-12" style="padding: 16px 0;">Télécharger le modèle</a>
                            </div>
                            <div id="div-input-photo-excel" class="card" style="display: none;">
                                <div class="card-header border-transparent">
                                    <h3 class="card-title">Photos pour (Non obligatoire):</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table class="table m-0">
                                            <thead>
                                                <tr>
                                                    <th>Nom</th>
                                                    <th>Type</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td id="td-nom-excel">nom</td>
                                                    <td id="td-type-excel">type</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row" id="div-input-image-preview-excel">

                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer clearfix">
                                    <label for="input-image-excel" class="btn btn-sm btn-secondary float-left">Parcourir mon ordinateur</label>
                                    <input id="input-image-excel" onchange="angular.element(this).scope().onFileImageExcelChange(this)" type="file" accept="image/*" style="display: none;" multiple />
                                    <button ng-click="showPhotoPicker('0', '<?php echo base_url(''); ?>','<?php echo base_url(''); ?>g-p-p', '2')" class="btn btn-sm btn-secondary float-left" style="margin-left: 5px;"><strong style="color: white;">Photo depuis le serveur</strong></button>
                                    <button ng-click="importExcelPhoto('<?php echo base_url(''); ?>i-ep-d')" class="btn btn-sm btn-primary float-right">Importer les photos</button>
                                </div>
                                <!-- /.card-footer -->
                                </div>
                            <p>
                                <div id="lds-roller-import" class="lds-roller" style="margin: 0 auto;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php /*}*/} ?>
    <!-- /.row -->
    </div>
    
    
    <!-- ***************************************SECTION NY MODAL*************************************** -->
    <!-- MODAL POUR AJOUT NOUVEAU PERIODE STRATEGIE -->
    <div class="modal fade" id="modal-new-periode-strategie" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Ajout d'une période de stratégie</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form>
                <div class="row">
                    <div class="col-4" title="Nombre de jour de la stratégie">
                        <label>Jour</label>
                        <div class="input-group mb-3">
                            <input id="input-nombre-jour" class="form-control" type="number" min="0" placeholder="Jour">
                        </div>
                    </div>
                    <div class="col-4" title="Nombre de semaine de la stratégie">
                        <label>Semaine</label>
                        <div class="input-group mb-3">
                            <input id="input-nombre-semaine" class="form-control" type="number" min="0" placeholder="Semaine">
                        </div>
                    </div>
                    <div class="col-4" title="Nombre de mois de la stratégie">
                        <label>Mois</label>
                        <div class="input-group mb-3">
                            <input id="input-nombre-mois" class="form-control" type="number" min="0" placeholder="Mois">
                        </div>
                    </div>
                </div>
                <div class="col-12" style="font-size: 12px;color: grey;padding: 0;margin-bottom: 10px;"> Veuillez renseigner au moins une des trois champs </div>
                 <!--div ng-if="infoErrorPeriode" class="col-12" style="font-size: 12px;color: red !important;padding: 0;margin-bottom: 10px;"> "La période de stratégie saisie est invalide, veuillez vérifier ce que vous avez saisie !" </div-input-->
                <input type="hidden" id="identifiantMere" value="">

            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            <button ng-click="addPeriodeStrategie('<?php echo base_url(''); ?>n-per-stra')" type="button" class="btn btn-primary" data-dismiss="modal">Ajouter</button>
        </div>
        </div>
    </div>
    </div>
    <!-- MODAL POUR AJOUT NOUVEAU TYPE PRODUIT -->
    <div class="modal fade" id="modal-new-type" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Ajout d'un nouveau type de produit</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form>
            <div class="form-group">
                <label for="type-name-modal" class="col-form-label">Type:</label>
                <input name="type" type="text" class="form-control" id="type-name-modal">
            </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            <button id="bouton-ajouter-type" ng-click="addNewType('<?php echo base_url(''); ?>n-type')" type="button" class="btn btn-primary" data-dismiss="modal">Ajouter</button>
        </div>
        </div>
    </div>
    </div>
    <!-- MODAL POUR AJOUT NOUVEAU CATEGORIE PRODUIT -->
    <div class="modal fade" id="modal-new-categorie" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Ajout d'une nouvelle catégorie de produit</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form>
            <div class="form-group">
                <label for="type-name-modal" class="col-form-label">Catégorie:</label>
                <input name="type" type="text" class="form-control" id="categorie-name-modal">
            </div>
            <div>
                <label for="type-info-nutri-modal" class="col-form-label">Informations nutritionnelles correspondantes (facultatif)</label>
                <select id="select-type-info-nutri-modal" class="form-control select2" multiple="true" style="width: 100%;" >
                <?php for($i=0; $i<count($infoNutris); $i++){ ?>
                    <option value="<?php echo $infoNutris[$i]->id_information_nutritionnelle; ?>"><?php echo $infoNutris[$i]->label; ?></option>
                <?php } ?>
                </select>
            </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            <button ng-click="TYPE.addNewCategorie('<?php echo base_url(''); ?>n-cat-p')" type="button" class="btn btn-primary" data-dismiss="modal">Ajouter</button>
        </div>
        </div>
    </div>
    </div>
    <!-- MODAL POUR AJOUT NOUVEAU INFORMATION NUTRITIONNELLE -->
    <div class="modal fade" id="modal-new-info-nutri" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ajout d'une nouvelle informations nutritionnelle</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                <div class="form-group">
                    <label for="info-nutris-name-modal" class="col-form-label">Information nutritionnelle:</label>
                    <input type="text" class="form-control" id="info-nutris-name-modal">
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                <button id="bouton-ajouter-info-nutri" ng-click="addNewInfoNutris('<?php echo base_url(''); ?>n-info-nutris')" type="button" class="btn btn-primary" data-dismiss="modal">Ajouter</button>
            </div>
            </div>
        </div>
    </div>
    <!-- MODAL POUR AJOUT NOUVEAU RAISON DE SURPLUS -->
    <div class="modal fade" id="modal-new-raison-surplus" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ajout d'une nouvelle raison de surplus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                <div class="form-group">
                    <label for="raison-surplus-name-modal" class="col-form-label">Raison de surplus:</label>
                    <input type="text" class="form-control" id="raison-surplus-name-modal">
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                <button id="bouton-ajouter-raison-surplus" ng-click="addNewRaisonSurplus('<?php echo base_url(''); ?>n-raison-surplus')" type="button" class="btn btn-primary" data-dismiss="modal">Ajouter</button>
            </div>
            </div>
        </div>
    </div>
    <!-- MODAL POUR AFFICHER MESSAGE ALERT -->
    <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 id="modal-alert-title" class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="text-align: center;">
                <p id="modal-alert-message"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
            </div>
            </div>
        </div>
    </div>

    <!-- MODAL POUR AFFICHER MESSAGE ALERT VENTE Finish-->
    <div class="modal fade" id="modal-alert-vente" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 id="modal-alert-title-vente" class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="text-align: center;">
                <p id="modal-alert-message-vente"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" ng-click="venteFinish(1)">OK</button>
            </div>
            </div>
        </div>
    </div>

    <!-- MODAL POUR AFFICHER LOADING -->
    <div class="w3-modal" id="modal-loading">
        <div class="w3-modal-content w3-animate-top w3-card-4 modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 id="modal-loading-title" class="modal-title"></h5>
            </div>
            <div class="modal-body" style="text-align: center;">
                <div class="lds-roller" style="margin: 0 auto; display: block;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            </div>
            <div class="modal-footer">
            </div>
            </div>
        </div>
    </div>
    <!-- MODAL QUI BLOCK VENTE SI INFO PROFIL INCOMPLETE -->
    <div class="modal fade" id="modal-block-vente" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Informations incomplètes</h5>
                <a id="link-vente-making" href="<?php echo base_url(''); ?>c-m-v" style="display: none;"></a>
            </div>
            <div class="modal-body" style="text-align: center;">
                <p>Vous ne pouvez effectuer de vente que si les informations sur votre entreprise sont complètes.</p>
                <?php if(!$profilOk){ ?>
                <p>Veuillez les compléter dans votre page profil, à savoir l'adresse, son type et les types d'aliments que votre entreprise produit.</p>
                <?php } 
                    /*else{
                        if(!$protocoleOk){ 
                            echo "<p>Veuillez compléter le protocole d'entente dans votre page profil.</p>";
                        }
                        else{
                            echo "<p>Veuillez accepter le protocole d'entente dans votre page profil.</p>";
                        }
                    }*/
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="location.href='<?php echo base_url(''); if(!$profilOk){ echo 'entreprise/profil/3'; } else { /*if(!$protocoleOk){ echo 'entreprise/profil/4'; } else{*/ echo 'entreprise/profil'; /*}*/ } ?>'">Voir mon profil</button>
            </div>
            </div>
        </div>
    </div>
    <!-- MODAL OU ON CHOISIT DES PHOTOS DEPUIS SERVEUR -->
    <div class="modal fade" id="modal-photo-picker" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Choisissez une photo qui vous convient</h5>
            </div>
            <div class="modal-body">
                <input ng-keyup="searchPhoto('<?php echo base_url(''); ?>','<?php echo base_url(''); ?>g-p-p-v')" id="input-photo-modal" class="form-control" style="margin-bottom: 20px;" placeholder="Rechercher selon le nom du produit" type="text" />
                <div id="lds-roller-photo-modal" class="lds-roller" style="margin: 0 auto;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                <div class="row" id="div-input-file-preview-modal">
                    
                </div> </div>
            <div class="modal-footer">
            </div>
            </div>
        </div>
    </div>

    <!-- MODAL POUR AFFICHER LA CARTE -->
    <div class="modal fade" id="modal-show-map" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Choisir les coordonnées sur la carte </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="contenu_carte">
                        <div class="row">           
                            <div class="col-6">
                                <div class="row">
                                    <div class="col-4">
                                        <label>Latitude : </label><br/><label>Longitude : </label>
                                    </div>
                                    <div class="col-8">
                                        <div class="lat_value_in_map" style="margin-bottom: .5rem !important;" > (Non défini) </div>
                                        <div class="long_value_in_map" style="margin-bottom: .5rem !important;" > (Non défini) </div>
                                    </div>
                                </div>
                                <span id="info"></span><br>
                                <input id="identifiantMere" type="hidden" value="">
                            </div>
                            <div class="col-2"></div>
                            <div class="col-4">
                                <div id="autocomplete_list" class="ui-widget">
                                    <input id="search_in_carte" class="form-control" placeholder="Rechercher" autocomplete="on">
                                    <!--<datalist id="json-datalist"></datalist>-->
                                    <!--<div class="input-group-append">
                                      <button class="btn btn-primary btn-navbar " type="submit">
                                        <i class="fa fa-search"></i>
                                      </button>
                                    </div> -->
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div id='map' style="width: 100%!important; height: 250px;"></div>
                        </div>
                    </div>
                    <div class="loader load_map">
                        <div id="lds-roller-responsable" class="lds-roller" style=""><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                        <br/>
                    </div>
                    <div class="load_map" style="text-align: center !important; margin-top: 10px !important;">Chargement de la carte . . . </div>
                </div>
                <div class="modal-footer">
                    <div class="contenu_carte" >
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                        <button ng-click="putCoordonneChoisi()" type="button" class="btn btn-primary" data-dismiss="modal">Choisir</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    var profilOk = <?php echo $profilOk==true?1:0; ?>;
    var protocoleOk = <?php echo $protocoleOk==true?1:0; ?>;
    /*var contratAccepter = <?php 
        /*if($profilOk){
            echo $contrat['etat'];
        }
        else{
            echo -1;
        }*/
    ?>;*/
    var base_url = '<?php echo base_url(''); ?>';

        function ajouterCategorie(idForm){

            $("#bouton-form-ajouter-categorie").click();
            var new_cat = $("#select-categorie"+idForm).data("select2").dropdown.$search.val();
            $("#categorie-name-modal").val( new_cat);

        }

        function ajouterType(idForm){

            $("#bouton-form-ajouter-type").click();
            var new_type = $("#select-type"+idForm).data("select2").dropdown.$search.val();
            $("#type-name-modal").val( new_type);
            $("#bouton-ajouter-type").click();

        }

        function ajouterInfoNutris(idForm){

            $("#bouton-form-ajouter-info-nutri").click();            
            var nouveau =  getValueSearchField( "div-select-info-nutris"+idForm );

            $("#info-nutris-name-modal").val(nouveau);
            $("#bouton-ajouter-info-nutri").click();
        }

        function ajouterRaisonSurplus(idForm){

            $("#bouton-form-ajouter-raison-surplus").click();            
            var nouveau = getValueSearchField( "div-select-raison-surplus"+idForm ) ;

            $("#raison-surplus-name-modal").val( nouveau);
            $("#bouton-ajouter-raison-surplus").click();
        }

        function getValueSearchField(idDivParent){
            for(var i = 0 ; i < $('.select2-search__field').length ; i++){
                if(idDivParent == $( $('.select2-search__field')[i] ).parent().parent().parent().parent().parent().parent().attr('id')){
                    return $('.select2-search__field')[i].value;
                }

                
            }
            return "";    
        }

</script>


<script src="https://www.gstatic.com/firebasejs/5.10.1/firebase-app.js"></script>
<script src="https://www.gstatic.com/firebasejs/5.10.1/firebase-firestore.js"></script>
<script>
  // Initialize Firebase
  /*var config = {
    apiKey: "AIzaSyAL25o7WT-fEy4oJ9P10hFvvG9ssxZr6BI",
    authDomain: "manzerpartazer-55257.firebaseapp.com",
    databaseURL: "https://manzerpartazer-55257.firebaseio.com",
    projectId: "manzerpartazer-55257",
    storageBucket: "manzerpartazer-55257.appspot.com",
    messagingSenderId: "249167550160"
  };
  firebase.initializeApp(config);*/
</script>