<?php 
    function infoNutrisSelected($infoNutris, $allInfoNutris){
        for($k=0; $k<count($allInfoNutris); $k++){
            if($allInfoNutris[$k]->id_information_nutritionnelle == $infoNutris->id_information_nutritionnelle){
                return true;
            }
        }
        return false;
    }
    function raisonSurplusSelected($raisonSurplus, $allRaisonSurplus){
        for($k=0; $k<count($allRaisonSurplus); $k++){
            if($allRaisonSurplus[$k]->id_raison_surplus == $raisonSurplus->id_raison_surplus){
                return true;
            }
        }
        return false;
    }


?>

<input type="hidden" id="statut_don_modif" value="<?php echo ($donModif==null)?0:1; ?>" />
<input type="hidden" id="id_etat_donation" value="<?php echo ($donModif==null)?-1:$donModif->etat_donation_id; ?>" />
<input type="hidden" id="id_produit" value="<?php echo ($donModif==null)?-1:$donModif->id_produit; ?>" />
<input type="hidden" id="id_donation" value="<?php echo ($donModif==null)?-1:$donModif->id_donation; ?>" />
<input type="hidden" id="base_url" value="<?php echo base_url(''); ?>" />


<div class="card card-default" ng-app="DonApp" ng-controller="DonCtrl">
    <div class="card-header d-flex p-0">
        <h3 class="card-title p-3">Enregistrement</h3>
        <ul class="nav donPills nav-pills ml-auto p-2">
            <li class="nav-item"><a class="nav-link active show" href="#tab_1" data-toggle="tab">Formulaire</a></li>
            <li class="nav-item"><a class="nav-link" href="#tab_2" data-toggle="tab">Importer</a></li>
        </ul>
    </div>
    <!-- /.card-header -->
    <div class="card-body">
    <?php if($protocoleOk && $profilOk){ 
        if($contrat['etat'] == 1){
        ?>
        <div class="tab-content">
            <div id="tab_1" class="tab-pane active show">
                <div class="row" id="div-form-list">
                    <div class="card col-md-12">
                        <div class="card-header" style="background-color: #00AFAA; color: #fff;">
                            <h3 class="card-title">Formulaire 1</h3>
                            <div class="card-tools">
                                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group" id="div-input-nom1">
                                        <label>Nom</label>
                                        <input id="input-nom1" class="form-control" type="text" placeholder="Nom du produit" value="<?php echo ($donModif==null)?'':$donModif->nom_produit; ?>"/>
                                    </div>
                                    <div class="form-group" id="div-input-groupement1">
                                        <label>Groupement</label><br>
                                        <label style="margin-right: 10px;">
                                            <input class="type-groupement radio-groupement-unite" id="input-groupement-unite1" type="radio" checked="true" name="unite"  <?php if( $donModif == null || ($donModif != null && $donModif->groupement == 1)) { echo "checked = true"; } ?>/>
                                        Par unité
                                        </label>
                                        <!-- <label style="margin-right: 10px;">
                                            <input class="type-groupement radio-groupement-portion" id="input-groupement-portion1" type="radio" name="unite" <?php //if( $donModif != null && $donModif->groupement == 3) { //echo "checked = true"; } ?> />
                                        Par portion <sup> (1) </sup>
                                        </label>
                                    -->
                                        <label>
                                            <input class="type-groupement radio-groupement-totalite" id="input-groupement-total1" type="radio" name="unite" <?php if( $donModif != null && $donModif->groupement == 2) { echo "checked = true"; } ?>/>
                                        Totalité des produits
                                        </label>
                                        
                                    </div>
                                    <div class="form-group" id="div-input-poids1">
                                        <label>Poids</label>
                                        <div class="input-group mb-3">
                                            <input id="input-poids1" class="form-control" type="text" ng-keyup="formatNumberToMileSep('#input-poids1')" placeholder="Poids de l'unité" value= <?php echo ($donModif==null)?'':$donModif->poids; ?> >
                                            <div class="input-group-append">
                                                <span class="input-group-text">Kg</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" id="div-input-volume1">
                                        <label>Volume <span class="info_don_facultatif1">(facultatif)</span></label>
                                        <div class="input-group mb-3">
                                            <input id="input-volume1" class="form-control" type="text" ng-keyup="formatNumberToMileSep('#input-volume1')" placeholder="Volume de l'unité" value=<?php echo ($donModif==null)?'':$donModif->volume; ?>>
                                            <div class="input-group-append">
                                                <span class="input-group-text"><var>m<sup>3</sup></var></span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" id="div-input-prix1">
                                        <label>Prix 
                                            <span  id="info_prix" title="Ecrire les chiffres avec un espace et sans virgule, Ex : 10 000 000" class="float-right badge info_prix tippy" style="background-color: #00AFAA; color: #fff; cursor: pointer !important; margin-left: 10px;">

                                                <i class="fa fa-info"></i>nfo

                                            </span>
                                        </label>
                                        <div class="input-group mb-3" >
                                            <input id="input-prix1" class="form-control" type="text" ng-keyup="formatNumberToMileSep('#input-prix1')" placeholder="Prix de l'unité" value=<?php echo ($donModif==null)?'':$donModif->prix; ?> >
                                            <div class="input-group-append">
                                                <span class="input-group-text">MGA</span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group" id="div-input-quantite1">
                                        <label>Quantité(s)</label>
                                        <div class="input-group mb-3">
                                            <input id="input-quantite1" class="form-control" type="text" ng-keyup="formatNumberToMileSep('#input-quantite1')" placeholder="Quantité"  value=<?php echo ($donModif==null)?'':$donModif->quantite; ?>>
                                        </div>
                                    </div>
                                    <div class="form-group" id="div-input-date-peremption-production1">
                                        <!--<label>Date de péremption <sup> (2) </sup></label>-->
                                        <label style="margin-right: 10px;">
                                            <input class="radio-choix-type-date" id="input-choix-date-peremption1" type="radio"  name="type-date1" value=1 <?php if( $donModif == null || $donModif->type_date == 1) { echo "checked = true"; } ?> />
                                        Date de péremption 

                                            <span  id="info_date_peremption" title="Date de péremtion ou Date limite de consommation
                                                Pour assurer la logistique de la donation l’ajout du produit doit se faire entre 1 à  5 jours avant la donation dépendant de la journée de demande" class="float-right badge info_date_peremption tippy" style="background-color: #00AFAA; color: #fff; cursor: pointer !important; margin-left: 10px;">

                                                <i class="fa fa-info"></i>nfo

                                            </span> 

                                        </label>
                                        <label>
                                            <input id="input-choix-date-production1" type="radio" name="type-date1" value=2 <?php if( $donModif != null && $donModif->type_date == 2) { echo "checked = true"; } ?> />
                                        Date de production
                                        </label>

                                        <div class="input-group mb-3" > <?php  $date = ($donModif != null && $donModif->date_peremption_produit != '')? date('d/m/Y', strtotime($donModif->date_peremption_produit)) : ''; ?> 
                                            <input id="input-date-peremption1" class="form-control datepicker" type="text" placeholder="Date de péremption" value=<?php echo $date; ?>>
                                            <div class="input-group-append">
                                                <span class="input-group-text">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="check" style="font-weight: bolder !important;">
                                            <input id="input-refrigere1" type="checkbox" class="flat-red" <?php if( $donModif != null && $donModif->refrigere == 1) { echo "checked = true"; } ?>> Le produit nécessite d'être réfrigéré
                                        </label>
                                        <label class="check" style="font-weight: bolder !important;">
                                            <input id="input-congele1" type="checkbox" class="flat-red" <?php if( $donModif != null && $donModif->congele == 1) { echo "checked = true"; } ?> > Le produit nécessite d'être congelé
                                        </label>
                                        <label class="check" style="font-weight: bolder !important;">
                                            <input id="input-consoDirect1" type="checkbox" class="flat-red" <?php if( $donModif != null && $donModif->consoDirect == 1) { echo "checked = true"; } ?> /> Le produit nécessite ni d'être réfrigéré ni d'être congelé  

                                            <span  id="info_necessite_produit" title="Pour savoir si votre produit doit être réfrigéré ou congelé, consultez notre guide « Règlementations de donation »" class="float-right badge info_necessite_produit tippy" style="background-color: #00AFAA; color: #fff; cursor: pointer !important; margin-left: 10px;">

                                                <i class="fa fa-info"></i>nfo

                                            </span> 

                                        </label>
                                    </div>

                                    <!--
                                    <div class="callout callout-success info_detaille">
                                        <div class="row">
                                            <div class="col-sm-1">
                                                (1) :
                                            </div>
                                            <div class="col-sm-11">
                                                Ce type de mesure est uniquement pour les restaurants
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-1">
                                                (2) :
                                            </div>
                                            <div class="col-sm-11">
                                                (ou Date limite de consommation) 
                                                Pour assurer la logistique de la donation l’ajout du produit doit se faire entre 1 à  5 jours avant la donation dépendant de la journée de demande 
                                            </div>
                                        </div>
                                        <br>
                                        <div class="row">
                                            <div class="col-sm-1">
                                                (3) :
                                            </div>
                                            <div class="col-sm-11">
                                               Pour savoir si votre produit doit être réfrigéré ou congelé, consultez notre guide « Règlementations de donation » en cliquant <a href="#" style="color:black !important; ">ici</a>.
                                            </div>
                                        </div>
                                    </div>
                                    -->

                                </div>
                                <div class="col-md-6">
                                    <div class="form-group" >
                                        <label>Catégorie</label>
                                        <div class="row" id="div-select-categorie1">
                                            <div class="col-12">
                                                <select onchange="angular.element(this).scope().TYPE.onValueChanged(1)" id="select-categorie1" data-placeholder="Catégorie du produit" class=" form-control select2 select-categorie" style="width: 100%;">
                                                <option></option>
                                                    <?php for($i=0; $i<count($categories); $i++){ ?>
                                                        <option value="<?php echo $categories[$i]->id_type_aliment; ?>" <?php if($donModif != null && $categories[$i]->id_type_aliment == $donModif->type_aliment_id ) { echo "selected"; }?>><?php echo $categories[$i]->label; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <div >
                                                     <a href="#" id="bouton-form-ajouter-categorie1"  ng-click="formClick('1')"  data-toggle="modal" data-target="#modal-new-categorie" title="Ajouter une nouvelle catégorie" class=""><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
                                                </div>
                                            </div>
                                            <!--div class="col-1" style="margin-left: -7.5px;">
                                                <button ng-click="formClick('1')" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal-new-categorie" title="Ajouter une nouvelle catégorie"><i class="fa fa-plus"></i></button>
                                            </div-->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Type</label>
                                        <div class="row" id="div-select-type1">
                                            <div class="col-12">
                                                <select id="select-type1" data-placeholder="Type du produit" class="select-type form-control select2" style="width: 100%;">
                                                <option></option>
                                                    <?php for($i=0; $i<count($types); $i++){ ?>
                                                        <option value="<?php echo $types[$i]->id_type_produit; ?>" <?php if($donModif != null && $types[$i]->id_type_produit == $donModif->type_produit_id ) { echo "selected"; }?> ><?php echo $types[$i]->label; ?></option>
                                                    <?php } ?>
                                                </select>
                                                <div style="display: none">
                                                     <a href="#" id="bouton-form-ajouter-type1" ng-click="formClick('1')"   ><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
                                                </div>
                                            </div>
                                            <!--div class="col-1" style="margin-left: -7.5px;">
                                                <button ng-click="formClick('1')" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal-new-type" title="Ajouter un nouveau type de produit"><i class="fa fa-plus"></i></button>
                                            </div-->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Informations nutritionnelles (facultatif)</label>
                                        <div class="row">
                                            <div class="col-12" id="div-select-info-nutris1" >
                                                <select id="select-info-nutris1" class="select-info-nutris form-control select2 select2-hidden-accessible" multiple="" data-placeholder="Informations nutritionnelles " style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                <?php for($i=0; $i<count($infoNutris); $i++){ ?>
                                                    <option value="<?php echo $infoNutris[$i]->id_information_nutritionnelle; ?>" <?php if($donModif != null && infoNutrisSelected($infoNutris[$i], $donModif->info_nutris)) { echo "selected"; } ?> ><?php echo $infoNutris[$i]->label; ?></option>
                                                <?php } ?>
                                                </select>
                                                <div style="display: none">
                                                     <a href="#" id="bouton-form-ajouter-info-nutri1" ng-click="formClick('1')"  ><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
                                                </div>
                                            </div>
                                            <!--div class="col-1" style="margin-left: -7.5px;">
                                                <button ng-click="formClick('1')" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal-new-info-nutri" title="Ajouter une information nutritionnelle"><i class="fa fa-plus"></i></button>
                                            </div-->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Raisons des surplus (facultatif)</label>
                                        <div class="row">
                                            <div class="col-12" id="div-select-raison-surplus1">
                                                <select id="select-raison-surplus1" class="select-raison-surplus form-control select2 select2-hidden-accessible" multiple="" data-placeholder="Raison des surplus" style="width: 100%;" tabindex="-1" aria-hidden="true">
                                                <?php for($i=0; $i<count($raisonSurplus); $i++){ ?>
                                                    <option value="<?php echo $raisonSurplus[$i]->id_raison_surplus; ?>" <?php if($donModif != null && raisonSurplusSelected($raisonSurplus[$i], $donModif->raisonSurplus)) { echo "selected"; } ?> ><?php echo $raisonSurplus[$i]->label; ?></option>
                                                <?php } ?>
                                                </select>
                                                <div style="display: none">
                                                     <a href="#"  ng-click="formClick('1')"  id="bouton-form-ajouter-raison-surplus1"><span class="float-right badge" style="background-color: #00AFAA; color: #fff;"><i class="fa fa-plus"></i> Ajouter</span></a>
                                                </div>
                                            </div>
                                            <!--div class="col-1" style="margin-left: -7.5px;">
                                                <button ng-click="formClick('1')" type="button" class="btn btn-secondary" data-toggle="modal" data-target="#modal-new-raison-surplus" title="Ajouter un raison de surplus"><i class="fa fa-plus"></i></button>
                                            </div-->
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Description (facultatif)</label>
                                        <textarea id="textarea-description1" class="form-control" rows="3" placeholder="Description du produit" ><?php echo ($donModif==null)?'':$donModif->description; ?></textarea>
                                    </div>
                                    <div class="form-group">
                                        <label>Photos</label><br>
                                        <div class="row">
                                            <div class="col-6">
                                                <label ng-click="formClick('1')" for="input-file-image1" class="form-control btn btn-secondary">Ajouter depuis mon ordinateur<i class="fa fa-picture-o" style="margin-left:8px;"></i></label>
                                                <input id="input-file-image1" onchange="angular.element(this).scope().onFileImageChange(this)" type="file" accept="image/*" style="display: none;" multiple />
                                            </div>
                                            <div class="col-6">
                                                <button ng-click="showPhotoPicker('1', '<?php echo base_url(''); ?>','<?php echo base_url(''); ?>g-p-p')" class="form-control btn btn-secondary"><strong style="color: white;">Ajouter depuis le serveur</strong><i class="fa fa-picture-o" style="margin-left:8px;"></i></button>
                                            </div>
                                        </div>
                                        <div class="row" id="div-file-product1">
                                            <?php if($donModif != null){ 

                                                    for($i=0; $i<count($donModif->photo);$i++){ ?>
                                                    <div class="col-6 div-img-preview" id="div-img-product<?php echo $donModif->photo[$i]['id_photo_produit']; ?>">
                                                        <input type="hidden" class="photoInBase" value="<?php echo $donModif->photo[$i]['id_photo_produit']; ?>"/>
                                                        <img src="<?php echo base_url().$donModif->photo[$i]['url']; ?>"/> 
                                                        <button ng-click="removeImageProduct(<?php echo $donModif->photo[$i]['id_photo_produit']; ?>)" class="btn btn-secondary"><i class="fa fa-trash" ></i></button>
                                                    </div>
                                                <?php 
                                                    }
                                                } ?>
                                        </div>
                                        <div class="row" id="div-file-preview1">
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-footer right">
                    
                    <button id="btn-save-draft" ng-click="saveDraft('<?php echo base_url(''); ?>s-d-a-d')" class="btn btn-secondary float-left" >Enregistrer comme brouillon</button>
                    <button id="btn-reset-all" ng-click="resetAllField()" class="btn btn-secondary float-left" title="Réinitialiser tous les champs" style="margin-left:8px;"><i class="fa fa-repeat"></i></button>
                    <button id="btn-add-from" ng-click="addForm('<?php echo base_url(''); ?>','<?php echo base_url(''); ?>g-p-p')" class="btn btn-primary">Ajouter plus de formulaire</button>
                    <button id="btn-validate" ng-click="validateForms('<?php echo base_url(''); ?>p-f-d')" class="btn btn-primary" style="margin-left:8px;">Valider</button>
                    <div id="lds-roller" class="lds-roller" style="float: right;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                </div>
            </div>
            <div id="tab_2" class="tab-pane">
                <div class="row">
                    <div class="col-md-12">
                        <div class="rv-div-import row">
                            <div class="col-md-8">
                                <label id="label-file-excel" for="input-file-excel" class="form-control btn btn-primary col-12" style="white-space: inherit;padding: 16px 0;">Sélectionner le fichier Excel à importer</label>
                                <input id="input-file-excel" onchange="$('#label-file-excel').html(this.value)" type="file" accept=".xls,.xlsx, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet,application/vnd.ms-excel" style="display: none;" />
                            </div>
                            <div class="col-md-2">
                                <button id="btn-import" ng-click="import('<?php echo base_url(''); ?>i-d')" class="btn btn-secondary col-12" style="padding: 16px 0;">Valider</button>
                            </div>
                            <div class="col-md-2">
                                <a id="btn-download-model" href="<?php echo base_url(''); ?>uploads/default/excel.donation.xlsx" class="btn btn-secondary col-12" style="padding: 16px 0;">Télécharger le modèle</a>
                            </div>
                            <div id="div-photo-excel" class="card" style="display: none;">
                                <div class="card-header border-transparent">
                                    <h3 class="card-title">Photos pour (Non obligatoire):</h3>
                                </div>
                                <!-- /.card-header -->
                                <div class="card-body p-0">
                                    <div class="table-responsive">
                                        <table class="table m-0">
                                            <thead>
                                                <tr>
                                                    <th>Nom</th>
                                                    <th>Type</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td id="td-nom-excel">nom</td>
                                                    <td id="td-type-excel">type</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="row" id="div-image-preview-excel">

                                    </div>
                                    <!-- /.table-responsive -->
                                </div>
                                <!-- /.card-body -->
                                <div class="card-footer clearfix">
                                    <label for="input-image-excel" class="btn btn-sm btn-secondary float-left">Parcourir mon ordinateur</label>
                                    <input id="input-image-excel" onchange="angular.element(this).scope().onFileImageExcelChange(this)" type="file" accept="image/*" style="display: none;" multiple />
                                    <button ng-click="showPhotoPicker('0', '<?php echo base_url(''); ?>','<?php echo base_url(''); ?>g-p-p', '2')" class="btn btn-sm btn-secondary float-left" style="margin-left: 5px;"><strong style="color: white;">Photo depuis le serveur</strong></button>
                                    <button ng-click="importExcelPhoto('<?php echo base_url(''); ?>i-ep-d')" class="btn btn-sm btn-primary float-right">Importer les photos</button>
                                </div>
                                <!-- /.card-footer -->
                                </div>
                            <p>
                                <div id="lds-roller-import" class="lds-roller" style="margin: 0 auto;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php }} ?>
    <!-- /.row -->
    </div>
    
    <!-- ***************************************SECTION NY MODAL*************************************** -->
    <!-- MODAL POUR AJOUT NOUVEAU TYPE PRODUIT -->
    <div class="modal fade" id="modal-new-type" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Ajout d'un nouveau type de produit</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form>
            <div class="form-group">
                <label for="type-name-modal" class="col-form-label">Type:</label>
                <input name="type" type="text" class="form-control" id="type-name-modal">
            </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            <button id="bouton-ajouter-type" ng-click="addNewType('<?php echo base_url(''); ?>n-type')" type="button" class="btn btn-primary" data-dismiss="modal">Ajouter</button>
        </div>
        </div>
    </div>
    </div>
    <!-- MODAL POUR AJOUT NOUVEAU CATEGORIE PRODUIT -->
    <div class="modal fade" id="modal-new-categorie" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title">Ajout d'une nouvelle catégorie de produit</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <form>
            <div class="form-group">
                <label for="type-name-modal" class="col-form-label">Catégorie:</label>
                <input name="type" type="text" class="form-control" id="categorie-name-modal">
            </div>
            <div>
                <label for="type-info-nutri-modal" class="col-form-label">Informations nutritionnelles correspondantes (facultatif) </label>
                <select id="select-type-info-nutri-modal" class="form-control select2" multiple="true" style="width: 100%;" >
                <?php for($i=0; $i<count($infoNutris); $i++){ ?>
                    <option value="<?php echo $infoNutris[$i]->id_information_nutritionnelle; ?>"><?php echo $infoNutris[$i]->label; ?></option>
                <?php } ?>
                </select>
            </div>
            </form>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
            <button ng-click="TYPE.addNewCategorie('<?php echo base_url(''); ?>n-cat-p')" type="button" class="btn btn-primary" data-dismiss="modal">Ajouter</button>
        </div>
        </div>
    </div>
    </div>
    <!-- MODAL POUR AJOUT NOUVEAU INFORMATION NUTRITIONNELLE -->
    <div class="modal fade" id="modal-new-info-nutri" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ajout d'une nouvelle informations nutritionnelle</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                <div class="form-group">
                    <label for="info-nutris-name-modal" class="col-form-label">Information nutritionnelle:</label>
                    <input type="text" class="form-control" id="info-nutris-name-modal">
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                <button id="bouton-ajouter-info-nutri"  ng-click="addNewInfoNutris('<?php echo base_url(''); ?>n-info-nutris')" type="button" class="btn btn-primary" data-dismiss="modal">Ajouter</button>
            </div>
            </div>
        </div>
    </div>
    <!-- MODAL POUR AJOUT NOUVEAU RAISON DE SURPLUS -->
    <div class="modal fade" id="modal-new-raison-surplus" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Ajout d'une nouvelle raison de surplus</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                <div class="form-group">
                    <label for="raison-surplus-name-modal" class="col-form-label">Raison de surplus:</label>
                    <input type="text" class="form-control" id="raison-surplus-name-modal">
                </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                <button id="bouton-ajouter-raison-surplus" ng-click="addNewRaisonSurplus('<?php echo base_url(''); ?>n-raison-surplus')" type="button" class="btn btn-primary" data-dismiss="modal">Ajouter</button>
            </div>
            </div>
        </div>
    </div>
    <!-- MODAL POUR AFFICHER MESSAGE ALERT -->
    <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 id="modal-alert-title" class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="text-align: center;">
                <p id="modal-alert-message"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
            </div>
            </div>
        </div>
    </div>
    <!-- MODAL POUR AFFICHER LOADING -->
    <div class="w3-modal" id="modal-loading">
        <div class="w3-modal-content w3-animate-top w3-card-4 modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 id="modal-loading-title" class="modal-title"></h5>
            </div>
            <div class="modal-body" style="text-align: center;">
                <div class="lds-roller" style="margin: 0 auto; display: block;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            </div>
            <div class="modal-footer">
            </div>
            </div>
        </div>
    </div>
    <!-- MODAL QUI BLOCK DONATION SI INFO PROFIL INCOMPLETE -->
    <div class="modal fade" id="modal-block-donation"  tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Informations incomplètes</h5>
                <a id="link-donation-making" href="<?php echo base_url(''); ?>c-m-d" style="display: none;"></a>
            </div>
            <div class="modal-body" style="text-align: center;">
                <p>Vous ne pouvez effectuer un don que si les informations sur votre entreprise sont complètes.</p>
                <?php if(!$profilOk){ ?>
                <p>Veuillez les compléter dans votre page profil, à savoir l'adresse, son type et les types d'aliments que votre entreprise produit.</p>
                <?php } 
                    else{
                        if(!$protocoleOk){ 
                            echo "<p>Veuillez compléter le <strong> protocole d'entente </strong>dans votre page profil.</p>";
                        }
                        else{
                            echo "<p>Veuillez accepter le <strong> protocole d'entente</strong> dans votre page profil.</p>";
                        }
                    }
                ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" onclick="location.href='<?php echo base_url(''); if(!$profilOk){ echo 'entreprise/profil/3'; } else { if(!$protocoleOk){ echo 'entreprise/profil/4'; } else{ echo 'entreprise/profil'; } } ?>'">Voir mon profil</button>
            </div>
            </div>
        </div>
    </div>
    <!-- MODAL OU ON CHOISIT DES PHOTOS DEPUIS SERVEUR -->
    <div class="modal fade" id="modal-photo-picker" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Choisissez une photo qui vous convient</h5>
            </div>
            <div class="modal-body">
                <input ng-keyup="searchPhoto('<?php echo base_url(''); ?>','<?php echo base_url(''); ?>g-p-p')" id="input-photo-modal" class="form-control" style="margin-bottom: 20px;" placeholder="Rechercher selon le nom du produit" type="text" />
                <div id="lds-roller-photo-modal" class="lds-roller" style="margin: 0 auto;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                <div class="row" id="div-file-preview-modal">
                    
                </div> </div>
            <div class="modal-footer">
            </div>
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    var profilOk = <?php echo $profilOk==true?1:0; ?>;
    var protocoleOk = <?php echo $protocoleOk==true?1:0; ?>;
    var contratAccepter = <?php 
        if($protocoleOk && $profilOk){
            echo $contrat['etat'];
        }
        else{
            echo -1;
        }
    ?>;
    var base_url = '<?php echo base_url(''); ?>';

 

        function ajouterCategorie(idForm){

            

            $("#bouton-form-ajouter-categorie"+idForm).click();
            var new_cat = $("#select-categorie"+idForm).data("select2").dropdown.$search.val();
            $("#categorie-name-modal").val( new_cat);

        }

        function ajouterType(idForm){

            $("#bouton-form-ajouter-type"+idForm).click();
            var new_type = $("#select-type"+idForm).data("select2").dropdown.$search.val();
            $("#type-name-modal").val( new_type);
            $("#bouton-ajouter-type").click();
        }

        function ajouterInfoNutris(idForm){

            $("#bouton-form-ajouter-info-nutri"+idForm).click();            
            var nouveau =  getValueSearchField( "div-select-info-nutris"+idForm );

            $("#info-nutris-name-modal").val(nouveau);
            $("#bouton-ajouter-info-nutri").click();
        }

        function ajouterRaisonSurplus(idForm){

            $("#bouton-form-ajouter-raison-surplus"+idForm).click();            
            var nouveau = getValueSearchField( "div-select-raison-surplus"+idForm ) ;

            $("#raison-surplus-name-modal").val( nouveau);
            $("#bouton-ajouter-raison-surplus").click();
        }

        function getValueSearchField(idDivParent){
            for(var i = 0 ; i < $('.select2-search__field').length ; i++){
                if(idDivParent == $( $('.select2-search__field')[i] ).parent().parent().parent().parent().parent().parent().attr('id')){
                    return $('.select2-search__field')[i].value;
                }

                
            }
            return "";    
        }

</script>