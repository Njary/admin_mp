<div class="call-out "><?php //var_dump($listProduitDonation); ?></div>

<input type="hidden" name="choix_active" id="choix_active" value="<?php echo $choix_active; ?>">
<div class="card card-primary card-outline" ng-app="ProduitApp" ng-controller="ProduitCtrl">
    <div class="card-header d-flex p-0">
        <h3 class="card-title p-3"></h3>
        <ul class="nav productList nav-pills mr-auto p-2">
            <li class="nav-item"><a id="tabDon" class="listProd nav-link active show" href="#tab_1" data-toggle="tab">Donation</a></li>
            <li class="nav-item"><a id="tabVente" class="listProd nav-link" href="#tab_2" data-toggle="tab">Vente</a></li>
        </ul>
    </div>
    <div class="card-body listeProduit">
    	<div class="tab-content">
            <div id="tab_1" class="tab-pane active show">
				<div class="table responsive">
				  <table id="table_donation" class="table table-striped table_produit">
				    <thead>
				    <tr>
				      <th>Nom </th>
				      <th>Quantité
				      <th>Date de péremption</th>
				      <th>Catégorie</th>
				      <th>Etat</th>
				    </tr>
				    </thead>
				    <tbody>
					    <?php for($i=0; $i<count($listProduitDonation); $i++){?>
						    <tr>
						      <td><?php echo $listProduitDonation[$i]->nom_produit; ?></td>
						      <td>
						      	<?php
						      		switch($listProduitDonation[$i]->groupement){
						      			case 1 :
						      			 	if( $listProduitDonation[$i]->volume > 0 ){
						      			 		echo  $listProduitDonation[$i]->poids * $listProduitDonation[$i]->quantite.' kg | '.$listProduitDonation[$i]->volume * $listProduitDonation[$i]->quantite.' <var>m<sup>3</sup></var>'; 
						      			 	}else{
						      			 		echo  $listProduitDonation[$i]->poids * $listProduitDonation[$i]->quantite.' kg';
						      			 	}
						      				break;
						      			case 2 :
						      				echo $listProduitDonation[$i]->poids.' kg | '.$listProduitDonation[$i]->volume.' <var>m<sup>3</sup></var>';
						      				break;
						      			default:
						      				if( $listProduitDonation[$i]->volume > 0 ){
						      			 		echo  $listProduitDonation[$i]->poids * $listProduitDonation[$i]->quantite.' kg | '.$listProduitDonation[$i]->volume * $listProduitDonation[$i]->quantite.' <var>m<sup>3</sup></var>'; 
						      			 	}else{
						      			 		echo  $listProduitDonation[$i]->poids * $listProduitDonation[$i]->quantite.' kg';
						      			 	}
						      				break;
						      		}
						      	?>

						      </td>
						      <td><?php echo date("d M Y",strtotime($listProduitDonation[$i]->date_peremption_produit)); ?></td>
						      <td><?php echo $listProduitDonation[$i]->categorie; ?></td> 		      			    
						      <td><?php echo $listProduitDonation[$i]->etat_donation; ?></td>
						    </tr>
					    <?php } ?>
				      
				    </tbody>
				    <tfoot>
				    	
				    </tfoot>
				  </table>
				</div>
        	</div>
            <div id="tab_2" class="tab-pane">
            	<div class="table responsive">
					 <table id="table_vente" class="table table-striped table_produit">
					    <thead>
					    	<tr>
						      	  <th>Nom </th>
						      	  <th>Prix original</th>
							      <th>Pourcentage de réduction</th>
							      <th>Date de péremption (ou production)</th>
							      <th>Catégorie</th>
							      <th>Surplus</th>
							      <th>Action</th>
						    </tr>
						</thead>
						 <tbody>
						    <?php for($i=0; $i<count($listProduitVente); $i++){?>
							    <tr>
							      <td><?php echo $listProduitVente[$i]->nom_produit; ?></td>
							      <td><?php echo $listProduitVente[$i]->prix.' MGA'; ?></td>
							      <td>
							      		<?php
							      			if(count($listProduitVente[$i]->strategie) > 0){
							      				$strategieSize = count($listProduitVente[$i]->strategie);
							      				echo $listProduitVente[$i]->pourcentage_reduction.'% <i class="fa fa-arrow-right"></i> ';
							      				for($k=0; $k<$strategieSize -1 ;$k++){
							      					echo $listProduitVente[$i]->strategie[$k]->pourcentage_reduction.' % ('.$listProduitVente[$i]->strategie[$k]->label.') <i class="fa fa-arrow-right"></i> ';
							      				}
							      				echo $listProduitVente[$i]->strategie[$strategieSize -1]->pourcentage_reduction.' % ('.$listProduitVente[$i]->strategie[$k]->label.') ';

							      			}else{
							      				echo $listProduitVente[$i]->pourcentage_reduction.' % ';
							      			}
							      		?>	

							      </td>
							      <td>
							      		<?php
							      			switch($listProduitVente[$i]->type_date){
							      				case 1 :
							      					echo date("d M Y",strtotime($listProduitVente[$i]->date_peremption_produit));
							      					break;
							      				case 2 :
							      					echo date("d M Y",strtotime($listProduitVente[$i]->date_peremption_produit)).' (prod.)';
							      					break;
							      			}
							      		?>

							      </td>
							      <td><?php echo $listProduitVente[$i]->categorie; ?></td>
							      <td>
							      		<?php
							      			if(count($listProduitVente[$i]->raison_surplus) > 0 ){
							      				echo 'Oui';
							      			}else{
							      				echo 'Non';
							      			}
							      		?>
							      </td>    		      			    
							      <td>
							      	<a class="product_action" ng-click="giveIdToChange('<?php echo $listProduitVente[$i]->id_donation; ?>')" data-toggle="modal" data-target="#modal-change-to-donation" style="cursor: pointer; margin-left:5px; margin-right: 5px;" title="Changer en donation"><i class="fa fa-heart" style="color: #F05423" ></i> </a> 
							      	<a class="product_action" href="<?php echo base_url(''); ?>vente/<?php echo $listProduitVente[$i]->id_donation; ?>" title="Modifier ce produit"><i class="fa fa-edit" style="cursor: pointer; margin-left:5px; margin-right: 5px;"></i></a>
							      </td>
							    </tr>
						    <?php } ?>
					      
					    </tbody>
					   	<tfoot>
				    
				    	</tfoot>
					</table>
				</div>
        	</div>
		</div>
	</div>


<!-- MODAL CONFIRMATIOM CHANGEMENT EN DONATION -->
	<div class="modal fade" id="modal-change-to-donation" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Demande de confirmation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Voulez-vous changer ce produit en une donation?
                <input type="hidden" name="produitToChangeDonation" value=""/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                <button ng-click="changeProduitToDonation('<?php echo base_url(''); ?>c-p-t-d')" type="button" class="btn btn-primary" data-dismiss="modal">Valider</button>
            </div>
            </div>
        </div>
    </div>
    <!-- MODAL POUR AFFICHER MESSAGE ALERT -->
    <div class="modal fade" id="modal-alert" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 id="modal-alert-title" class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" style="text-align: center;">
                <p id="modal-alert-message"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">OK</button>
            </div>
            </div>
        </div>
    </div>
    <!-- MODAL POUR AFFICHER LOADING -->
    <div class="w3-modal" id="modal-loading">
        <div class="w3-modal-content w3-animate-top w3-card-4 modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 id="modal-loading-title" class="modal-title"></h5>
            </div>
            <div class="modal-body" style="text-align: center;">
                <div class="lds-roller" style="margin: 0 auto; display: block;"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
            </div>
            <div class="modal-footer">
            </div>
            </div>
        </div>
    </div>
</div>