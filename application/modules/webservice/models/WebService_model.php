<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class WebService_model extends CI_Model{

	protected $table = 'token';

    public function save($token){

        // DATE D'EXPIRATION DU TOKEN : 7 JOURS 
        
        $date_expiration = time()+3600*24*7;

        $data = array(
            "chaine"        => $token,
            "actif"         => 1,
            "expiration"    => $date_expiration
        );
        $this->db->insert($this->table,$data); 
        return $this->db->insert_id();
    }


    public function update($id_consommateur,$consommateur){
        $this->db->where("id_consommateur",$id_consommateur);
        $this->db->update($this->table,$consommateur);
    }

    public function unvalidate_token($token){
        $data = array(
            "actif" => 0
        );
        $this->db->where("chaine",$token);
        $this->db->update($this->table,$data);    

    }

    public function check_token_validation($token){


        $now = (int)time();
        $myquery = $this->db->query('SELECT id_token FROM '.$this->table.' WHERE chaine = "'.$token.'" AND actif = 1 AND expiration > "'.$now.'"');
        $result = $myquery->result();
        if(count($result)>0){
            return 1;
        }        
        return 0;
    }

}