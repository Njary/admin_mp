<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class WsConsommateur extends MY_Controller {

	protected $RAYON_ZONE = 1.5;

	/*
	RAPPEL DES STATUS:
		1.  success
		0.  token non valide
		-1. identifiant non valide
		-2. identifiant déjà pris 
		-3. champ(s) obligatoire(s) non rempli(s)
		-4. erreur du serveur
		-5. quantité produit commandé > quantité produit dispo
	*/


	/*
	$json = file_get_contents('php://input');
	$obj = json_decode($json);

	    $nom = $obj->nom;
	    $prenom = $obj->prenom;
	    $mail = $obj->mail;

	headers: {
	Accept: 'application/json',
	'Content-Type': 'application/json',
	},

	*/
	public function __construct(){
		parent::__construct();
		$this->load->model("utilisateur/Utilisateur_model","user");
		$this->load->model("entreprise/Entreprise_model","entreprise");
		$this->load->model("consommateur/Consommateur_model","consommateur");
		$this->load->model("WebService_model","wsmodel");
		$this->load->model("Produit/Produit_model","produit");
		$this->load->model("Produit/Don_model","don");
		$this->load->model("Produit/Vente_model","vente");

		/* set default timezone to Madagascar */
		date_default_timezone_set('Etc/GMT-3');
		/* set default timezone to Madagascar */
	}

	private function genererToken($longeur){
		$token = "";
        $chaine = "a0b1c2d3e4f5g6h7i8j9klmnpqrstuvwxy1a2c3z4e5e6f7g8h9y";
        srand((double)microtime()*1000000);
        for($i=0; $i<$longeur; $i++){
            $token .= $chaine[rand()%strlen($chaine)];
        }
        return $token;
	} 


	public function inscription(){
		// get json data from request
		$json = file_get_contents('php://input');
		$obj = json_decode($json);

		// get properties from json object	
		$nom = (isset($obj->nom) && $obj->nom != null)? $obj->nom : null;
		$prenom = (isset($obj->prenom) && $obj->prenom != null)? $obj->prenom : null;
		$mail = (isset($obj->mail) && $obj->mail != null)? $obj->mail : null;
		$password = (isset($obj->password) && $obj->password != null)? $obj->password : null;
		$mobile_banking = (isset($obj->mobile_banking) && $obj->mobile_banking != null)? $obj->mobile_banking : null;
		$adresse_de_livraison = (isset($obj->adresse_de_livraison) && $obj->adresse_de_livraison != null)? $obj->adresse_de_livraison : null;
		$numero_telephone = (isset($obj->numero_telephone) && $obj->numero_telephone != null)? $obj->numero_telephone : null;
	
		$response = new stdClass();	

		// VERIFICATION DES CHAMPS OBLIGATOIRE
		if($nom != null && $mail != null && $password != null){

			// VERFICATION SI LE MAIL EXITE DEJA
			if($this->user->userPseudoExist($mail)['exist'] == false){
				// INSCRIPTION UTILISATEUR
				$utilisateur_data = array(
					"pseudo"	=> $mail,
					"password"	=> $this->encryption->encrypt($password)	
				);
				$utilisateur_id = $this->user->save($utilisateur_data);

				// INSCRIPTION CONSOMMATEUR
				
				$consommateur_data = array(
					"utilisateur_id"		=> $utilisateur_id,
					"nom" 					=> $nom,
					"prenom"				=> $prenom,
					"mail"					=> $mail,
					"mobile_banking"		=> $mobile_banking,
					"adresse_de_livraison"	=> $adresse_de_livraison,
					"numero_telephone"		=> $numero_telephone
				);

				$consommateur_id = $this->consommateur->save($consommateur_data);

				if($consommateur_id != null){
					$response->status = 1;			
					$consommateur_data['id_consommateur'] = $consommateur_id;
					$response->data = $consommateur_data;
					
				}
				else{
					// echo "echec", identifiant consommateur non trouve;
					$response->status = -1;
					$response->data = new stdClass;
									
				}				
			}
			else{
				//echo "ce mail est deja pris";
				$response->status = -2;
				$response->data = new stdClass;
			}		
		}				
		else{
			//echo "données vides";
			$response->status = -3;
			$response->data = new stdClass;
		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function connexion(){

		$response = new stdClass();

		// création du token et enregistrement du token dans le serveur
		$token = $this->genererToken(50);
		$token_id = $this->wsmodel->save($token);

		// get json data from request
		$json = file_get_contents('php://input');
		$obj = json_decode($json);

		// get properties from json object	
		$mail = (isset($obj->mail) && $obj->mail != null)? $obj->mail : null;
		$password = (isset($obj->password) && $obj->password != null)? $obj->password : null;

		if($token_id != null){
			// VERIFICATION DES CHAMPS OBLIGATOIRE
			if($mail != null && $password != null){
			
				$idUser = $this->user->isAuthorized($mail,$password,$this->encryption);
				
				// SI USER AUTORISE
				if($idUser>0){

					$cur_consommateur["user"] = $this->consommateur->getConsommateurByIdUtilisateur($idUser);
					
					if(count($cur_consommateur["user"])>0){
						$cur_consommateur["user"] = $cur_consommateur["user"][0];
						$response->status = 1;
						$cur_consommateur['token'] = $token;
						$response->data = $cur_consommateur;
					}
				}
				else{
					// die("identifiant non valide"); MOT DE PASSE OU PSEUDO NON VALIDE
					$response->status = -1;
					$response->data = new stdClass;

				}
			}
			else{
				//die("identifiant vide");
				$response->status = -3;
				$response->data = new stdClass;
			}
		}
		else{
			//die("erreur de création du token");
			$response->status = -4;
			$response->data = new stdClass;
		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}


	public function modification(){

		$response = new stdClass();

		// get json data from request
		$json = file_get_contents('php://input');
		$obj = json_decode($json);



		// get properties from json object	
		$token = (isset($obj->token) && $obj->token != null)? $obj->token : null;
		
		$utilisateur_id = (isset($obj->utilisateur_id) && $obj->utilisateur_id != null)? $obj->utilisateur_id : null;
		$nom = (isset($obj->nom) && $obj->nom != null)? $obj->nom : null;
		$prenom = (isset($obj->prenom) && $obj->prenom != null)? $obj->prenom : null;
		$mail = (isset($obj->mail) && $obj->mail != null)? $obj->mail : null;
		$password = (isset($obj->password) && $obj->password != null)? $obj->password : null;
		$mobile_banking = (isset($obj->mobile_banking) && $obj->mobile_banking != null)? $obj->mobile_banking : null;
		$adresse_de_livraison = (isset($obj->adresse_de_livraison) && $obj->adresse_de_livraison != null)? $obj->adresse_de_livraison : null;
		$numero_telephone = (isset($obj->numero_telephone) && $obj->numero_telephone != null)? $obj->numero_telephone : null;

		$consommateur_data = array(
				"utilisateur_id"		=> $utilisateur_id,
				"nom" 					=> $nom,
				"prenom"				=> $prenom,
				"mail"					=> $mail,
				"mobile_banking"		=> $mobile_banking,
				"adresse_de_livraison"	=> $adresse_de_livraison,
				"numero_telephone"		=> $numero_telephone
		);

		// VERIFICATION DE LA VALIDITE DU TOKEN
		if($this->wsmodel->check_token_validation($token) == 1){
		//if($this->wsmodel->check_token_validation("f5i2r7cu5n40hzaf5gi63eic91tlrapcvs4ef1c4hw409nxcjz") == 1){
			$info_consommateur = $this->consommateur->getConsommateurByIdUtilisateur($utilisateur_id);
			if(count($info_consommateur)>0){
				if($this->modificationInformation($info_consommateur[0]->id_consommateur,$utilisateur_id,$consommateur_data)){
				$response->status = 1;
				$consommateur_data['id_consommateur'] = $info_consommateur[0]->id_consommateur;			
				$consommateur_data['utilisateur_id'] = $utilisateur_id;			
				$response->data = $consommateur_data;
				}
				else{
					echo "echec de la modification";
					$response->status = -2;
					$response->data = new stdClass;
				}
			}else{
				// consommateur non trouvé
				$response->status = -1;
				$response->data = new stdClass;
			}
		}
		else{
			
			// TOKEN NON VALIDE
			//echo "token non valide";
			$response->status = 0;
			$response->data = new stdClass;

		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function modificationInformation($id_consommateur,$id_utilisateur,$data){
		
		
		// TO DO MISE A JOUR EMAIL OU PSEUDO DE L'UTILISATEUR
		$his_mail = $this->consommateur->getMailById($id_consommateur);
		
		$consommateur_data = array();

		// verification des info modifiées
		if(isset($data['nom']) && $data['nom'] != null){
			$consommateur_data['nom'] = $data['nom'];
		}
		if(isset($data['prenom']) && $data['prenom'] != null){
			$consommateur_data['prenom'] = $data['prenom'];
		}
		if(isset($data['mobile_banking']) && $data['mobile_banking'] != null){
			$consommateur_data['mobile_banking'] = $data['mobile_banking'];
		}
		if(isset($data['adresse_de_livraison']) && $data['adresse_de_livraison'] != null){
			$consommateur_data['adresse_de_livraison'] = $data['adresse_de_livraison'];
		}
		if(isset($data['numero_telephone']) && $data['numero_telephone'] != null){
			$consommateur_data['numero_telephone'] = $data['numero_telephone'];
		}
		if(isset($data["mail"]) && $data["mail"] != null)
		{
			if($data["mail"] != $his_mail){
				// LE MAIL DONNNE N'EXISTE PAS ENCORE 
				if($this->user->userPseudoExist($data["mail"])['exist'] == false){
					$consommateur_data["mail"] = $data["mail"];
					$consommateur_data_temp["mail"] = $data["mail"];
					$utilisateur_data = array(
						"pseudo"	=> $data["mail"],
					);

					$this->user->update($id_utilisateur,$utilisateur_data);	
					$this->consommateur->updateByIdUtilisateur($id_utilisateur,$consommateur_data_temp);
				}
				// LE MAIL DONNE EST DEJA UTILISE PAR LES UTILISATEURS, ON ANNULE TOUT
				else{
					//echo "ce mail existe deja \n"; 
					return false;
				}
			}
		}

		$this->consommateur->update($id_consommateur,$consommateur_data);
		return true;		
	}

	public function listerProduit(){

		$response = new stdClass();

		// get json data from request
		$json = file_get_contents('php://input');
		$obj = json_decode($json);

		// get properties from json object	
		// filtre par type de produit
		// filtre par type d'aliment
		// filtre par prix

		$indexStart = (isset($obj->indexStart) && $obj->indexStart != null)? $obj->indexStart : null;
		$resultLength = (isset($obj->resultLength) && $obj->resultLength != null)? $obj->resultLength : null;
		
		$token = (isset($obj->token) && $obj->token != null)? $obj->token : null;
		$id_utilisateur = (isset($obj->id_utilisateur) && $obj->id_utilisateur != null)? $obj->id_utilisateur : null;
		$type_produit_id = (isset($obj->type_produit_id) && $obj->type_produit_id != null)? $obj->type_produit_id : null;
		$type_aliment_id = (isset($obj->type_aliment_id) && $obj->type_aliment_id != null)? $obj->type_aliment_id : null;
		$prix_min = (isset($obj->prix_min) && $obj->prix_min != null)? $obj->prix_min : -1;
		$prix_max = (isset($obj->prix_max) && $obj->prix_max != null)? $obj->prix_max : -1;
		$motCle = (isset($obj->motCle) && $obj->motCle != null)? $obj->motCle : "";
		$typeMotsCle = (isset($obj->typeMotsCle) && $obj->typeMotsCle != null)? $obj->typeMotsCle : 0;		
		$date_limite = (isset($obj->date_limite) && $obj->date_limite != null)? $obj->date_limite : null;		
		$info_nutr = (isset($obj->info_nutr) && $obj->info_nutr != null)? $obj->info_nutr : null;		
		$latitude = (isset($obj->latitude) && $obj->latitude != null)? $obj->latitude : null;		
		$longitude = (isset($obj->longitude) && $obj->longitude != null)? $obj->longitude : null;
		$type_produit = (isset($obj->typeProduit) && $obj->typeProduit != null)? $obj->typeProduit : null;
		$listeEntreprise = 	(isset($obj->listeEntreprise) && $obj->listeEntreprise != null)? $obj->listeEntreprise : null;
		$sorter = (isset($obj->tri) && $obj->tri != null)? $obj->tri : null;

		if((int)$sorter == -1){
			$sorter = 0;
		}	
	
		// VERIFICATION DE LA VALIDITE DU TOKEN

		if($this->wsmodel->check_token_validation($token) == 1){

			$info_consommateur = $this->consommateur->getConsommateurByIdUtilisateur($id_utilisateur);
			
			if(count($info_consommateur)==1){

				$id_consommateur = $info_consommateur[0]->id_consommateur;

				$produits = $this->don->findByCondition($indexStart,$resultLength,$typeMotsCle, $motCle,$type_produit_id,$type_aliment_id,$prix_min,$prix_max,$date_limite,$info_nutr,$latitude,$longitude, $type_produit, $listeEntreprise );	

				$response->status = 1;

				if($produits != null && count($produits)>0){

					foreach ($produits as $produit) {

						/*get entreprise */
						$entreprise = $this->entreprise->getByid($produit->entreprise_id);

						/*if(count($entreprise) == 1){
							$produit->entrepriseName = $entreprise[0]->nom_entreprise;
							$produit->logoEntreprise = base_url().$entreprise[0]->logo;
						}*/

						if(count($entreprise) == 1){
							$produit->entrepriseName = $entreprise[0]->nom_entreprise;

							if (strpos($entreprise[0]->logo, 'user_placeholder') !== false) {
							   	$produit->logoEntreprise = base_url().'assets/img/'.$entreprise[0]->logo;
							}else{
								$produit->logoEntreprise = base_url().$entreprise[0]->logo;
							}
							
						}
						/*get entreprise */
						
						/* adresse ramassage */
						if($produit->adresse_rm_id != null){
							$adresse_entreprise = $this->vente->getAdresseByAdresseRamassageId($produit->adresse_rm_id);
							
							if($adresse_entreprise != null){		
								$son_adresse = new stdClass();
								$son_adresse->label = $adresse_entreprise->label;
								$son_adresse->latitude = $adresse_entreprise->latitude;
								$son_adresse->longitude = $adresse_entreprise->longitude;
								$son_adresse->ville = $adresse_entreprise->ville;
								$produit->adresse_rammassage = $son_adresse;
							}
							else{
								$produit->adresse_rammassage = new stdClass;
							}
						}
						else{
								$produit->adresse_rammassage = new stdClass;
						}
						unset($produit->adresse_rm_id);
						/* adresse ramassage */


						/*strategie reduction */
						$date = new DateTime();
						$per = new DateTime($produit->dateLimite);
						// transformation en jour
						$now = $date->getTimestamp()/(3600*24);	
						$per = $per->getTimestamp()/(3600*24);
						// intervalle
						$difference = ceil($per - $now);
						$pourcentage_reduction = $this->don->findIdPeriodeReductionByNombreJour($produit->id_donation,$difference);
						
						$produit->prix = (int) $produit->prix;
						$produit->prixNormal = $produit->prix;

						if($pourcentage_reduction != null){
							$produit->prix = $produit->prix - $produit->prix*($pourcentage_reduction->pourcentage_reduction/100);
							$produit->pourcentage_reduction = $pourcentage_reduction->pourcentage_reduction;
						}else{
							$produit->prix = $produit->prix - $produit->prix*($produit->pourcentage_reduction/100);
						}
						/*strategie reduction */


						$produit->dateLimite = date_create($produit->dateLimite);
						$produit->dateLimite = date_format($produit->dateLimite,"d/m/Y");

						$tp_produit = $this->produit->getTypeProduitByIdTypeProduit($produit->type_produit_id);
					
						$produit->type_produit = (count($tp_produit)==1)?$tp_produit[0]->label_type_produit:"Aucun";

						$ta_produit = $this->produit->getTypeAlimentByIdTypeAliment($produit->type_aliment_id);

						$produit->type_aliment = (count($ta_produit))?$ta_produit[0]->label_type_aliment:"Aucune";

						$nb_vue_du_produit = $this->consommateur->getNombreDeVueByIdDonation($produit->id_donation);

						$produit->vue = (count($nb_vue_du_produit)==1)?$nb_vue_du_produit[0]->vue:0;
								
						/* favoris */
						$produit->isFavoris = $this->consommateur->produitIsFavoris($id_consommateur,$produit->id_donation);				
						/* favoris */	

						/* photo produit */
						$photo = $this->produit->getUrlPhotoProduitByIdProduit($produit->id_produit);
						$produit->photo = ($photo != null) ? base_url().$photo->url : "-1";			
						/* photo produit */		

						/* date recup commande */
						$date_recup = $this->don->getPeriodeRecuperationCommandeByIdDonation($produit->id_donation);
						$produit->date_recup = ($date_recup != null) ? $date_recup : [];
						/* date recup commande */
					}

					////////////////////
					$produitFiltrer = [];


					// si max=-1 et min a une valeur prendre les produits ayant prix>= min
			        if($prix_max == -1 && $prix_min != -1){

			            foreach ( $produits as  $produit) {
			            	if( $produit->prix >= $prix_min  ){
			            		array_push( $produitFiltrer, $produit );
			            	}
			            }  

			        }
			        // si min=-1 et max a une valeur prendre les produits ayant prix<= max
			        else if($prix_min == -1 && $prix_max != -1){

			            foreach ( $produits as  $produit) {
			            	if( $produit->prix <= $prix_max  ){
			            		array_push( $produitFiltrer, $produit );
			            	}
			            }  

			        }
			        // si min et max ont des valeurs prendre les produits entre ces valeurs
			        else if($prix_min !=-1 && $prix_max != -1){

			            foreach ( $produits as  $produit) {
			            	if( $produit->prix >= $prix_min && $produit->prix <= $prix_max ){
			            		array_push( $produitFiltrer, $produit );
			            	}
			            }  

			        }
			        // si min=-1 et max=-1 ne pas mettre de filtre sur le prix
			        else{

			            foreach ( $produits as  $produit) {
			            	array_push( $produitFiltrer, $produit );
			            }


			        }

			        if($sorter == 0){ //Ordonnée par date de péremption

			        	$date_peremption = array();
						foreach ($produitFiltrer as $key => $row)
						{
						    $date_peremption[$key] = $row->dateLimite;
						}
						array_multisort($date_peremption, SORT_ASC, $produitFiltrer);
			        	
			        }else if($sorter == 1){ //Ordonnée par prix croissant

			        	$price = array();
						foreach ($produitFiltrer as $key => $row)
						{
						    $price[$key] = $row->prix;
						}
						array_multisort($price, SORT_ASC, $produitFiltrer);

			        }else if($sorter == 2){ //Ordonnée par reduction actuel decroissant

			        	$redux = array();
						foreach ($produitFiltrer as $key => $row)
						{
						    $redux[$key] = $row->pourcentage_reduction;
						}
						array_multisort($redux, SORT_DESC, $produitFiltrer);
			        	
			        }else if($sorter ==3){ //Ordonnée par date d'ajout produit

			        	$date_ajout = array();
						foreach ($produitFiltrer as $key => $row)
						{
						    $date_ajout[$key] = $row->moment;
						}
						array_multisort($date_ajout, SORT_DESC, $produitFiltrer);
			        	
			        }
					///////////////////
					// $response->data = $produits; // Ancien filtre (par prix original mais pas prix reduit)
					$response->data = $produitFiltrer;
				}
				else{
					$response->data = [];
				}

				$tools = new stdClass();
				//All information nutritionnelle
				$infoNutris = $this->don->wsGetAllInfoNutris();
				$tools->infoNutri = $infoNutris;

				//All Entreprise
				$entreprise = $this->entreprise->wsGetAllEntrepriseWithVente();
				$tools->entreprise = $entreprise;

				//All Type produit
				$type_produit = $this->produit->wsGetTypeProduit();
				$tools->typeProduit = $type_produit;

				$response->utils = $tools;


			}
			else{
				// consommateur non trouvé
				$response->status = -1;
				$response->data = new stdClass;
				$response->utils = new stdClass;
			}	
		} 
		else{
			
			$response->status = 0;
			$response->data = new stdClass;
			$response->utils = new stdClass;
			
		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function profil(){
		$response = new stdClass();

		$json = file_get_contents('php://input');
		$obj = json_decode($json);

		$token = (isset($obj->token) && $obj->token != null)? $obj->token : null;
		$id_utilisateur = (isset($obj->id_utilisateur) && $obj->id_utilisateur != null)? $obj->id_utilisateur : null;

		if($this->wsmodel->check_token_validation($token) == 1){

			if($id_utilisateur != null){
				// get the first consummer
				$info_consommateur = $this->consommateur->getConsommateurByIdUtilisateur($id_utilisateur);
				// les info_consommateur sont nulles
				if(count($info_consommateur)>0){
					$info_consommateur = $info_consommateur[0];
					$info = [];
					$data_consommateur = [];
					$info["mobile_banking"] = $info_consommateur->mobile_banking; 
					$info["adresse_de_livraison"] = $info_consommateur->adresse_de_livraison; 
					$info["numero_telephone"] = $info_consommateur->numero_telephone; 
					$pref_consommateur = $this->consommateur->getPreferenceAlimentaire($info_consommateur->id_consommateur);

					// si ses preferences existent, càd le consommateur a déjà commandé
					if($pref_consommateur != null){
						$data_consommateur = array_merge($info,$pref_consommateur);
					}
					// sinon elles sont null
					else{
						$info["preference_alimentaire"] = null;
						$info["quantite_CO2_sauve"] = null;
						$info["quantite_aliment_sauve"] = null;
						$data_consommateur = $info;
					}

					$response->status = 1;
					$response->data = $data_consommateur;
				}
				else{
					// die("identifiant du consommateur non valide");
					$response->status = -1;
					$response->data = new stdClass;
				}
			}
			else{
				// die("identifiant oublié");
				$response->status = -3;
				$response->data = new stdClass;
			}
		}
		else{
			$response->status = 0;
			$response->data = new stdClass;
		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function detailProduit(){
		
		$response = new stdClass();
		// get json data from request
		$json = file_get_contents('php://input');
		$obj = json_decode($json);

		$token = (isset($obj->token) && $obj->token != null)? $obj->token : null;
		$id_donation = (isset($obj->id_donation) && $obj->id_donation != null)? $obj->id_donation : null;
		$id_utilisateur = (isset($obj->id_utilisateur) && $obj->id_utilisateur != null)? $obj->id_utilisateur : null;
		// VERIFICATION DE LA VALIDITE DU TOKEN

		if($this->wsmodel->check_token_validation($token) == 1){

			$info_consommateur = $this->consommateur->getConsommateurByIdUtilisateur($id_utilisateur);
			
			if(count($info_consommateur)==1){

				$id_consommateur = $info_consommateur[0]->id_consommateur;

				if($id_donation != null){

					$produits = $this->don->findProduitByIdDonation($id_donation);	

					$response->status = 1;

					if(count($produits) == 1){

						$produit = $produits[0];

						/*get entreprise */
						$entreprise = $this->entreprise->getByid($produit->entreprise_id);

						if(count($entreprise) == 1){
							$produit->entrepriseName = $entreprise[0]->nom_entreprise;

							if (strpos($entreprise[0]->logo, 'user_placeholder') !== false) {
							   	$produit->logoEntreprise = base_url().'assets/img/'.$entreprise[0]->logo;
							}else{
								$produit->logoEntreprise = base_url().$entreprise[0]->logo;
							}

							/*get responsable etreprise*/
							$responsables = $this->entreprise->getResponsablesEntreprise($entreprise[0]->id_entreprise);

							if($responsables != null){
								if(count($responsables)>0){

									$info_responsable = new stdClass;
									$info_responsable->nom = $responsables[0]->nom." ".$responsables[0]->prenom;
									$info_responsable->poste = $responsables[0]->poste;
									$info_responsable->email = $responsables[0]->email;
									$info_responsable->telephone = $responsables[0]->telephone;

									$produit->responsable_entreprise = $info_responsable;
								}else{
									$produit->responsable_entreprise = new stdClass;
								}
								
							}else{
								$produit->responsable_entreprise = new stdClass;
							}
							/*get responsable etreprise*/
							
						}
						/*get entreprise */



						/* adresse ramassage */
						if($produit->adresse_rm_id != null){
							$adresse_entreprise = $this->vente->getAdresseByAdresseRamassageId($produit->adresse_rm_id);
							
							if($adresse_entreprise != null){		
								$son_adresse = new stdClass();
								$son_adresse->label = $adresse_entreprise->label;
								$son_adresse->latitude = $adresse_entreprise->latitude;
								$son_adresse->longitude = $adresse_entreprise->longitude;
								$son_adresse->ville = $adresse_entreprise->ville;
								$produit->adresse_rammassage = $son_adresse;
							}
							else{
								$produit->adresse_rammassage = new stdClass;
							}
						}
						else{
								$produit->adresse_rammassage = new stdClass;
						}
						unset($produit->adresse_rm_id);
						/* adresse ramassage */

						/*strategie reduction */

						$date = new DateTime();
						$per = new DateTime($produit->dateLimite);
						// transformation en jour
						$now = $date->getTimestamp()/(3600*24);	
						$per = $per->getTimestamp()/(3600*24);
						// intervalle
						$difference = ceil($per - $now);
						$pourcentage_reduction = $this->don->findIdPeriodeReductionByNombreJour($produit->id_donation,$difference);
						
						$produit->prixNormal = (int) $produit->prixNormal;
						$produit->prixReduit = $produit->prixNormal - $produit->prixNormal*($produit->pourcentage_reduction/100);
						if($pourcentage_reduction != null){
							$produit->prixReduit = $produit->prixNormal - $produit->prixNormal*($pourcentage_reduction->pourcentage_reduction/100);
							$produit->pourcentage_reduction = $pourcentage_reduction->pourcentage_reduction;
						}
						/*strategie reduction */
						
						$produit->dateLimite = date_create($produit->dateLimite);
						$produit->dateLimite = date_format($produit->dateLimite,"d/m/Y");

						$produit->moment = date_create($produit->moment);
						$produit->moment = date_format($produit->moment,"d/m/Y");

						$tp_produit = $this->produit->getTypeProduitByIdTypeProduit($produit->type_produit_id);

						$produit->type_produit = (count($tp_produit)==1)?$tp_produit[0]->label_type_produit:"Aucun";

						$ta_produit = $this->produit->getTypeAlimentByIdTypeAliment($produit->type_aliment_id);

						$produit->type_aliment = (count($ta_produit))?$ta_produit[0]->label_type_aliment:"Aucune";
						
						$nb_vue_du_produit = $this->consommateur->getNombreDeVueByIdDonation($produit->id_donation);

						$produit->vue = (count($nb_vue_du_produit)==1)?$nb_vue_du_produit[0]->vue:0;
						
						/* favoris */
						$produit->isFavoris = $this->consommateur->produitIsFavoris($id_consommateur,$produit->id_donation);				
						/* favoris */
						
						/* photo produit */
						$photo = $this->produit->getUrlPhotoProduitByIdProduit($produit->id_produit);
						$produit->photo = ($photo != null) ? base_url().$photo->url : "-1";			
						/* photo produit */	

						/* date recup commande */
						$date_recup = $this->don->getPeriodeRecuperationCommandeByIdDonation($produit->id_donation);
						$produit->date_recup = ($date_recup != null) ? $date_recup : [];
						/* date recup commande */

						$response->data = $produit;	
					
					}else{
						// produit non trouvé
						$response->status = -1;
						$response->data = new stdClass;
					}
					
				}
				else{
					// die("identifiant du donation oublié");
					$response->status = -3;
					$response->data = new stdClass;
				}
			}
			else{
				// consommateur non trouvé
				$response->status = -1;
				$response->data = new stdClass;
			}	
		}
		else{
			$response->status = 0;
			$response->data = new stdClass;
		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function ajouterFavoris(){
		//vue_produit:1 - favoris :1
		$response = new stdClass();
		$json = file_get_contents('php://input');
		$obj = json_decode($json);

		$token = (isset($obj->token) && $obj->token != null)? $obj->token : null;
		$id_utilisateur = (isset($obj->id_utilisateur) && $obj->id_utilisateur != null)? $obj->id_utilisateur : null;
		$id_donation = (isset($obj->id_donation) && $obj->id_donation != null)? $obj->id_donation : null;

		if($this->wsmodel->check_token_validation($token) == 1){

			if($id_utilisateur != null && $id_donation != null){
				// get the first consummer
				$info_consommateur = $this->consommateur->getConsommateurByIdUtilisateur($id_utilisateur);
			
				if(count($info_consommateur)==1){
					$info_donation = $this->don->findDonationById($id_donation);
					if(count($info_donation) == 1){
						$id_consommateur = (int) $info_consommateur[0]->id_consommateur;
						$id_donation = (int) $info_donation[0]->id_donation;
						/*
							si pas son favori alors ajouter
							sinon update a son favori
						*/
						if(!$this->consommateur->getActionConsByIdConsommateurByIdDonation($id_consommateur,$id_donation))
						{
							$this->consommateur->addFavoris($id_consommateur,$id_donation);
						}
						else{
							if($this->consommateur->produitIsFavoris($id_consommateur,$id_donation)){
								$this->consommateur->updateToFavoris($id_consommateur,$id_donation,0);
							}else{
								$this->consommateur->updateToFavoris($id_consommateur,$id_donation,1);
							}
						}		

						$response->status = 1;
						$response->data = new stdClass;

					}else{
						// die("identifiant de la donation non valide");
						$response->status = -1;
						$response->data = new stdClass;
					}
				}
				else{
					// die("identifiant du consommateur non valide");
					$response->status = -1;
					$response->data = new stdClass;
				}
			}
			else{
				// die("identifiant oublié");
				$response->status = -3;
				$response->data = new stdClass;
			}
		}
		else{
			$response->status = 0;
			$response->data = new stdClass;
		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function ajouterVueProduit(){
		//vue_produit:1 - favoris :1
		$response = new stdClass();
		$json = file_get_contents('php://input');
		$obj = json_decode($json);

		$token = (isset($obj->token) && $obj->token != null)? $obj->token : null;
		$id_utilisateur = (isset($obj->id_utilisateur) && $obj->id_utilisateur != null)? $obj->id_utilisateur : null;
		$id_donation = (isset($obj->id_donation) && $obj->id_donation != null)? $obj->id_donation : null;

		if($this->wsmodel->check_token_validation($token) == 1){

			if($id_utilisateur != null && $id_donation != null){
				// get the first consummer
				$info_consommateur = $this->consommateur->getConsommateurByIdUtilisateur($id_utilisateur);
			
				if(count($info_consommateur)==1){
					$info_donation = $this->don->findDonationById($id_donation);
					if(count($info_donation) == 1){
						$id_consommateur = (int) $info_consommateur[0]->id_consommateur;
						$id_donation = (int) $info_donation[0]->id_donation;
						if(!$this->consommateur->getActionConsByIdConsommateurByIdDonation($id_consommateur,$id_donation))
						{
							$this->consommateur->addVueProduit($id_consommateur,$id_donation);
						}	
						$response->status = 1;
						$response->data = new stdClass;

					}else{
						// die("identifiant de la donation non valide");
						$response->status = -1;
						$response->data = new stdClass;
					}
				}
				else{
					// die("identifiant du consommateur non valide");
					$response->status = -1;
					$response->data = new stdClass;
				}
			}
			else{
				// die("identifiant oublié");
				$response->status = -3;
				$response->data = new stdClass;
			}
		}
		else{
			$response->status = 0;
			$response->data = new stdClass;
		}


		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function favoris(){

		$response = new stdClass();

		$json = file_get_contents('php://input');
		$obj = json_decode($json);

		$token = (isset($obj->token) && $obj->token != null)? $obj->token : null;
		$id_utilisateur = (isset($obj->id_utilisateur) && $obj->id_utilisateur != null)? $obj->id_utilisateur : null;

		if($this->wsmodel->check_token_validation($token) == 1){

			if($id_utilisateur != null){
	
				$info_consommateur = $this->consommateur->getConsommateurByIdUtilisateur($id_utilisateur);
				
				if(count($info_consommateur)>0){
					
					$info_consommateur = $info_consommateur[0];
					$response->status = 1;

					$id_donations = $this->consommateur->getFavorisByIdConsommateur($info_consommateur->id_consommateur);
					
					if($id_donations != null){

						$data_consommateur = [];

						foreach ($id_donations as $id_donation) {
						
							$produits = $this->don->findProduitByIdDonation($id_donation->donation_id);	

							if($produits != null){

								foreach ($produits as $produit) {
									
									/*get entreprise */
									$entreprise = $this->entreprise->getByid($produit->entreprise_id);

									if(count($entreprise) == 1){
										$produit->entrepriseName = $entreprise[0]->nom_entreprise;
										$produit->logoEntreprise = base_url().$entreprise[0]->logo;
									}
									/*get entreprise */
									
									/* adresse ramassage */
									if($produit->adresse_rm_id != null){
										$adresse_entreprise = $this->vente->getAdresseByAdresseRamassageId($produit->adresse_rm_id);
										
										if($adresse_entreprise != null){		
											$son_adresse = new stdClass();
											$son_adresse->label = $adresse_entreprise->label;
											$son_adresse->latitude = $adresse_entreprise->latitude;
											$son_adresse->longitude = $adresse_entreprise->longitude;
											$son_adresse->ville = $adresse_entreprise->ville;
											$produit->adresse_rammassage = $son_adresse;
										}
										else{
											$produit->adresse_rammassage = new stdClass;
										}
									}
									else{
											$produit->adresse_rammassage = new stdClass;
									}
									unset($produit->adresse_rm_id);
									/* adresse ramassage */

									/*strategie reduction */
									$date = new DateTime();
									$per = new DateTime($produit->dateLimite);
									// transformation en jour
									$now = $date->getTimestamp()/(3600*24);	
									$per = $per->getTimestamp()/(3600*24);
									// intervalle
									$difference = ceil($per - $now);
									$pourcentage_reduction = $this->don->findIdPeriodeReductionByNombreJour($produit->id_donation,$difference);
									
									$produit->prix = (int) $produit->prixNormal;

									if($pourcentage_reduction != null){
										$produit->prix = $produit->prix - $produit->prix*($pourcentage_reduction->pourcentage_reduction/100);
										$produit->pourcentage_reduction = $pourcentage_reduction->pourcentage_reduction;
									
									}else{
										$produit->prix = $produit->prix - $produit->prix*($produit->pourcentage_reduction/100);
									}
									/*strategie reduction */

									$produit->dateLimite = date_create($produit->dateLimite);
									$produit->dateLimite = date_format($produit->dateLimite,"d/m/Y");

									$produit->moment = date_create($produit->moment);
									$produit->moment = date_format($produit->moment,"d/m/Y");

									$tp_produit = $this->produit->getTypeProduitByIdTypeProduit($produit->type_produit_id);

									$produit->type_produit = (count($tp_produit)==1)?$tp_produit[0]->label_type_produit:"Aucun";

									$ta_produit = $this->produit->getTypeAlimentByIdTypeAliment($produit->type_aliment_id);

									$produit->type_aliment = (count($ta_produit))?$ta_produit[0]->label_type_aliment:"Aucune";
								
									$nb_vue_du_produit = $this->consommateur->getNombreDeVueByIdDonation($produit->id_donation);

									$produit->vue = (count($nb_vue_du_produit)==1)?$nb_vue_du_produit[0]->vue:0;

									/* photo produit */
									$photo = $this->produit->getUrlPhotoProduitByIdProduit($produit->id_produit);
									$produit->photo = ($photo != null) ? base_url().$photo->url : "-1";			
									/* photo produit */	
									
									/* date recup commande */
									$date_recup = $this->don->getPeriodeRecuperationCommandeByIdDonation($produit->id_donation);
									$produit->date_recup = ($date_recup != null) ? $date_recup : [];
									/* date recup commande */
								}
							}
							if(count($produit) == 1){
								array_push($data_consommateur,$produits[0]);								
							}

						}

						$response->data = $data_consommateur;	
					}
					else{
						//die("pas de produits trouvés");
						$response->data = new stdClass;
					}
				}
				else{
					// die("identifiant du consommateur non valide");
					$response->status = -1;
					$response->data = new stdClass;
				}
			}
			else{
				// die("identifiant oublié");
				$response->status = -3;
				$response->data = new stdClass;
			}
		}
		else{
			$response->status = 0;
			$response->data = new stdClass;
		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function ajouterCommande(){

		$response = new stdClass();
		$json = file_get_contents('php://input');
		$obj = json_decode($json);

		$token = (isset($obj->token) && $obj->token != null)? $obj->token : null;
		$id_utilisateur = (isset($obj->id_utilisateur) && $obj->id_utilisateur != null)? $obj->id_utilisateur : null;
		
		$produits = (isset($obj->produits) && $obj->produits != null)? $obj->produits : null;

		if($this->wsmodel->check_token_validation($token) == 1){

			if($id_utilisateur != null){
				// get the first consummer
				$info_consommateur = $this->consommateur->getConsommateurByIdUtilisateur($id_utilisateur);
			
				if(count($info_consommateur)==1){

					$info_consommateur = $info_consommateur[0];

					$id_consommateur = (int) $info_consommateur->id_consommateur;


					$etat_commande = 1; 	// payé 
					$now = new DateTime();
					$date = $now->format('Y-m-d H:i:s');	// date et heure actuelle
					$data_commande = array(
						"consommateur_id" 	=> $id_consommateur,
						"etat"				=> $etat_commande,
						"date"				=> $date, 
					);

					$commande = $this->consommateur->commandeTransaction($data_commande,$produits);
					$response->status = $commande->transation_status;
					$response->data = $commande->data;					
				}
				else{
					// die("identifiant du consommateur non valide");
					$response->status = -1;
					$response->data = new stdClass;
				}
			}
			else{
				// die("identifiant oublié");
				$response->status = -3;
				$response->data = new stdClass;
			}
		}
		else{
			$response->status = 0;
			$response->data = new stdClass;
		}

		header('Content-Type: application/json');
		echo json_encode($response);

	}

	public function listerCommande(){
		$response = new stdClass();
		$json = file_get_contents('php://input');
		$obj = json_decode($json);
		$produits_commandes = [];
		$token = (isset($obj->token) && $obj->token != null)? $obj->token : null;
		$id_utilisateur = (isset($obj->id_utilisateur) && $obj->id_utilisateur != null)? $obj->id_utilisateur : null;

		// VERIFICATION DE LA VALIDITE DU TOKEN

		if($this->wsmodel->check_token_validation($token) == 1){

			$info_consommateur = $this->consommateur->getConsommateurByIdUtilisateur($id_utilisateur);
			
			if(count($info_consommateur)==1){

				$id_consommateur = $info_consommateur[0]->id_consommateur;

				$ses_commandes = $this->consommateur->getCommandeByIdConsommateur($id_consommateur);

				if(count($ses_commandes) > 0){
				
					foreach ($ses_commandes as $commande) {
						$commande->montant_total = 0;					
						$commande->nombre_de_produits = 0;

						/* etat commande */
						$commande->etat = $this->consommateur->getLabelCommandeByIdEtatCommande($commande->etat)->label;
						/* etat commande */

						/* formatage date commande */
						$heure = explode(" ",$commande->date);
						$commande->heure = substr($heure[1],0,5);

						$sa_date = date_create($commande->date);
						$commande->labelMois = date_format($sa_date,"M");
						
						$date_co = date_create($commande->date);
						$commande->date = date_format($date_co, "d/m/Y");
						/* formatage date commande */

						$ses_produits = $this->consommateur->getProduitCommandeByIdCommande($commande->commande_id);

						if(count($ses_produits) > 0){

							$response->status = 1;

							foreach ($ses_produits as $produit) {
								$commande->montant_total += $produit->montant;
								$commande->nombre_de_produits += $produit->qte;
							}

							$response->data = $ses_commandes;

						}else{
							// produit commandé non trouvé
							$response->status = -1;
							$response->data = new stdClass;							
						}
					}

				}else{
					// aucune commande trouvée
					$response->status = 1;
					$response->data = [];					
				}
			}
			else{
				// consommateur non trouvé
				$response->status = -1;
				$response->data = new stdClass;
			}	
		}
		else{
			$response->status = 0;
			$response->data = new stdClass;
		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function detailCommande(){
		
		$response = new stdClass();
		// get json data from request
		$json = file_get_contents('php://input');
		$obj = json_decode($json);

		$token = (isset($obj->token) && $obj->token != null)? $obj->token : null;
		$id_commande = (isset($obj->id_commande) && $obj->id_commande != null)? $obj->id_commande : null;
		// VERIFICATION DE LA VALIDITE DU TOKEN

		if($this->wsmodel->check_token_validation($token) == 1){
			
			if($id_commande != null){

				$commande = $this->consommateur->getCommandeByIdCommande($id_commande);

				if($commande != null){

					$commande->montant_total = 0;					
					$commande->nombre_de_produits = 0;					
					$commande->produits = [];

					/* etat commande */
					$commande->etat = $this->consommateur->getLabelCommandeByIdEtatCommande($commande->etat)->label;
					/* etat commande */

					/* cout de livraison commande */
					$commande->cout_livraison = 0;				
					/* cout de livraison commande */				

					/* formatage date commande */
					$heure = explode(" ",$commande->date);
					$commande->heure = substr($heure[1],0,5);
					$date_co = date_create($commande->date);
					$commande->date = date_format($date_co, "d/m/Y");
					/* formatage date commande */

					$ses_produits = $this->consommateur->getProduitCommandeByIdCommande($id_commande);


					if(count($ses_produits) > 0){

						$response->status = 1;

						foreach ($ses_produits as $son_produit) {

							$commande->montant_total += $son_produit->montant;
							$commande->nombre_de_produits += $son_produit->qte;
							

							/* un produit de la commande */
							$un_produit = $this->don->findProduitByIdDonation($son_produit->donation_id);	

							if(count($un_produit) == 1){

								$produit = $un_produit[0];

								/*get entreprise */
								$entreprise = $this->entreprise->getByid($produit->entreprise_id);

								if(count($entreprise) == 1){
									$produit->entrepriseName = $entreprise[0]->nom_entreprise;
									$produit->logoEntreprise = base_url().$entreprise[0]->logo;
								}
								/*get entreprise */
								
								/* adresse ramassage */
								$adresse_entreprise = $this->vente->getAdresseByAdresseRamassageId($produit->adresse_rm_id);
								if($adresse_entreprise != null){		
									$son_adresse = new stdClass();
									$son_adresse->label = $adresse_entreprise->label;
									$son_adresse->latitude = $adresse_entreprise->latitude;
									$son_adresse->longitude = $adresse_entreprise->longitude;
									$son_adresse->ville = $adresse_entreprise->ville;
									$produit->adresse_rammassage = $son_adresse;
								}
								else{
									$produit->adresse_rammassage = new stdClass;
								}
								unset($produit->adresse_rm_id);
								/* adresse ramassage */

								/* prix et qté commandée*/
								$produit->prix = $son_produit->prix_achat_unite;
								$produit->qte_commande = $son_produit->qte;
								/* prix et qté commandée*/
								
								$produit->dateLimite = date_create($produit->dateLimite);
								$produit->dateLimite = date_format($produit->dateLimite,"d/m/Y");

								$produit->moment = date_create($produit->moment);
								$produit->moment = date_format($produit->moment,"d/m/Y");

								$tp_produit = $this->produit->getTypeProduitByIdTypeProduit($produit->type_produit_id);

								$produit->type_produit = (count($tp_produit)==1)?$tp_produit[0]->label_type_produit:"Aucun";

								$ta_produit = $this->produit->getTypeAlimentByIdTypeAliment($produit->type_aliment_id);

								$produit->type_aliment = (count($ta_produit))?$ta_produit[0]->label_type_aliment:"Aucune";
							
								$nb_vue_du_produit = $this->consommateur->getNombreDeVueByIdDonation($produit->id_donation);

								$produit->vue = (count($nb_vue_du_produit)==1)?$nb_vue_du_produit[0]->vue:0;

								/* photo produit */
								$photo = $this->produit->getUrlPhotoProduitByIdProduit($produit->id_produit);
								$produit->photo = ($photo != null) ? base_url().$photo->url : "-1";			
								/* photo produit */	
								
								/* date recup commande */
								$date_recup = $this->don->getPeriodeRecuperationCommandeByIdDonation($produit->id_donation);
								$produit->date_recup = ($date_recup != null) ? $date_recup : [];
								/* date recup commande */
								
								array_push($commande->produits, $produit);	
								
							}
							else{
								// produit non trouvé
								$response->status = -1;
								$response->data = new stdClass;
							}
								
						}

						$response->data = $commande;
					}
					else{
						// produit commandé non trouvé
						$response->status = -1;
						$response->data = new stdClass;							
					}

				}
				else{
					// commande non trouvée
					$response->status = -1;
					$response->data = new stdClass;
				}
			}
			else{
				// die("identifiant de la commande oublié");
				$response->status = -3;
				$response->data = new stdClass;
			}
			
		}
		else{
			$response->status = 0;
			$response->data = new stdClass;
		}

		header('Content-Type: application/json');
		echo json_encode($response);
	}

	public function feedback(){
		$response = new stdClass();
		$json = file_get_contents('php://input');
		$obj = json_decode($json);

		$token = (isset($obj->token) && $obj->token != null)? $obj->token : null;
		$id_utilisateur = (isset($obj->id_utilisateur) && $obj->id_utilisateur != null)? $obj->id_utilisateur : null;
		
		$feedbacks = (isset($obj->data) && $obj->data != null)? $obj->data : null;

		if($this->wsmodel->check_token_validation($token) == 1){

			if($id_utilisateur != null){
				// get the first consummer
				$info_consommateur = $this->consommateur->getConsommateurByIdUtilisateur($id_utilisateur);
			
				if(count($info_consommateur)==1){

					$info_consommateur = $info_consommateur[0];

					$id_consommateur = (int) $info_consommateur->id_consommateur;

					if(count($feedbacks) > 0 ){

						$now = new DateTime();
						$date = $now->format('Y-m-d H:i:s');	// date et heure actuelle

						foreach ($feedbacks as $feedback) {
							$data_feedback = array(
								"label_feedback_id"		=> $feedback->id_label_feedback,
								"note"					=> $feedback->note,
								"commentaire"			=> $feedback->commentaire,
								"consommateur_id"		=> $id_consommateur,
								"moment"				=> $date
							);
							$id_feedback = $this->consommateur->saveFeedback($data_feedback);
							if($id_feedback != null){
								// success	
								$response->status = 1;
								$response->data = new stdClass;
							}
							else{
								// feedback non enregistrée
								$response->status = -4;
								$response->data = new stdClass;
							}		
						}	
					}
					else{
						// si il n'y a pas de données
						$response->status = -3;
						$response->data = new stdClass;
					}	
				}
				else{
					// die("identifiant du consommateur non valide");
					$response->status = -1;
					$response->data = new stdClass;
				}
			}
			else{
				// die("identifiant oublié");
				$response->status = -3;
				$response->data = new stdClass;
			}
		}
		else{
			$response->status = 0;
			$response->data = new stdClass;
		}

		header('Content-Type: application/json');
		echo json_encode($response);

	}

	public function getAllEntrepriseAProximity(){

		$response = new stdClass();
		$json = file_get_contents('php://input');
		$obj = json_decode($json);

		$token = (isset($obj->token) && $obj->token != null)? $obj->token : null;
		$latitude = (isset($obj->latitude) && $obj->latitude != null)? $obj->latitude : null;		
		$longitude = (isset($obj->longitude) && $obj->longitude != null)? $obj->longitude : null;

		// VERIFICATION DE LA VALIDITE DU TOKEN
		if($this->wsmodel->check_token_validation($token) == 1){

			if($latitude != null && $longitude != null){

				if($latitude != 0 && $longitude != 0){

					$response->status = 1;

					$entreprise = $this->entreprise->wsGetAllEntrepriseWithVenteAProximity();
					$liste_distance = [];

					for($i=0; $i<count($entreprise); $i++){
						$distance = $this->distanceInKmBetweenEarthCoordinates( $latitude, $longitude, $entreprise[$i]->latitude, $entreprise[$i]->longitude );
						array_push( $liste_distance, $distance );
					}

					$entreprise_proximity = [];
					for($j=0; $j<count($liste_distance); $j++){
						if( $liste_distance[$j] < $this->RAYON_ZONE ){
							array_push( $entreprise_proximity, $entreprise[$j] );
						}
					}

					if(count($entreprise_proximity) > 0){
						$response->message = "Entreprise à proximité trouvé";
						$response->data = $entreprise_proximity;
					}else{
						$response->message = "Aucune entreprise n'est à proximité";
						$response->data = $entreprise_proximity;
					}
					
				}else{

					$response->status = -1;
					$response->message = "Erreur de coordonnee";
					$response->data = new stdClass;

				}

			}else{

				$response->status = -2;
				$response->message = "Coordonnee indéfini";
				$response->data = new stdClass;
				

			}

		}else{
			
			$response->status = 0;
			$response->message = "token invalide";
			$response->data = new stdClass;
			
		}

		header('Content-Type: application/json');
		echo json_encode($response);

	}

	public function degreesToRadians( $degrees ) {
        return $degrees * pi() / 180;
    }

    public function distanceInKmBetweenEarthCoordinates( $lat1, $lon1, $lat2, $lon2){
        
        $earthRadiusKm = 6371;

        $dLat = $this->degreesToRadians($lat2-$lat1);
        $dLon = $this->degreesToRadians($lon2-$lon1);

        $lat1 = $this->degreesToRadians($lat1);
        $lat2 = $this->degreesToRadians($lat2);

        $a = sin($dLat/2) * sin($dLat/2) + sin($dLon/2) * sin($dLon/2) * cos($lat1) * cos($lat2);
        $c = 2 * atan2( sqrt($a), sqrt(1-$a));

        return $earthRadiusKm * $c;
    }
}