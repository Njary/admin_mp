<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class NotificationDhl extends MY_Controller {

	private $from;
	private $to;
	private $subject;
	
	function __construct(){
		parent::__construct();
		
		$this->load->model("entreprise/Entreprise_model","entreprise");
		$config = Array(
          'protocol' => 'smtp',
          'smtp_host' => 'ssl://smtp.googlemail.com',
          'smtp_port' => 465,
          'smtp_user' => 'tsiox.dev@gmail.com', 
          'smtp_pass' => 'tsiox.dev.2017.account', 
          'mailtype' => 'html',
          'charset' => 'utf-8',
          'wordwrap' => TRUE
        );


        
        $this->load->library('email', $config);
		$this->subject = "RAMASSAGE DE PRODUITS";
		$this->from = "tsiox.dev@gmail.com";
		$this->name = "Foodwise";
		$this->to = "tsiox.dev@gmail.com";
		
	}
	public function index()
	{
		
	}

	public function send($entreprise_id){
	
		$adresses = $this->entreprise->getAdressesEntreprise($entreprise_id);
		$responsables = $this->entreprise->getResponsablesEntreprise($entreprise_id);
		$responsable = $responsables[0];
		
		$this->email->set_newline("\r\n");
        $this->email->from($this->from, $this->name);
        $this->email->to($this->to);
        $this->email->subject($this->subject);
        

        
        $this->email->message("

<p>Bonjour, </p>
<br>
<p> <b>VOICI LES INFORMATIONS CONCERNANT LE RAMASSAGE :  </b></p>

<p>LIEU : ".$adresses[0]->label."</p>

<p>PERSONNE DE CONTACT DE L'ENTREPRISE : </p>
<p>Nom : <b>".$responsable->nom."</b></p>
<p>Poste : <b>".$responsable->poste."</b></p>
<p>Email : <b>".$responsable->email."</b></p>
<p>Téléphone : <b>".$responsable->telephone."</b></p>


<p>LIEU DE LIVRAISON : </p>

<p>DETAIL DU CONTENU DE L'ENVOI: </p>
<p>Type de contenu : <b>Aliments</b></p>
<p>Poids total : <b>45 Kg</b></p>
<p>Volume : <b>1 m2</b></p>
<br>
<p>Cordialement,</p>
<p>L'équipe Foodwise</p>");    

    
      
        return $this->email->send() ? true : false;
	}
}
