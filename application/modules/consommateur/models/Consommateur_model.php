<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Consommateur_model extends CI_Model{

	protected $table = 'consommateur';
    protected $tableProduitCommande = "produit_commande";
    protected $tableCommande = "commande";
    protected $tableDonation = "donation";
    protected $tableProduit = "produit";
    protected $tableTypeProduit = "type_produit";
    protected $tableActionConsommateur = "action_consommateur";
    protected $tableEtatCommande = "etat_commande";
    protected $tableFeedback = "feedback";

    public function __construct(){
        parent::__construct();
        $this->load->model("Produit/Produit_model","produit");
        $this->load->model("Produit/Don_model","don");
        $this->load->model("Produit/Vente_model","vente");

        /* set default timezone to Madagascar */
        date_default_timezone_set('Etc/GMT-3');
        /* set default timezone to Madagascar */
    }

    public function getById($id_consommateur){
        $query = $this->db->get_where($this->table,array("id_consommateur"=>(int)$id_consommateur));
        return $query->result();
    }

    public function getConsommateurByIdUtilisateur($utilisateur_id){
        $query = $this->db->get_where($this->table,array("utilisateur_id"=>(int)$utilisateur_id));
        return $query->result();
    }

    public function save($data){
        $this->db->insert($this->table,$data); 
        return $this->db->insert_id();
    }


    public function update($id_consommateur,$consommateur){
        $this->db->where("id_consommateur",$id_consommateur);
        $this->db->update($this->table,$consommateur);
    }

    public function updateByIdUtilisateur($utilisateur_id,$consommateur){
        $this->db->where("utilisateur_id",$utilisateur_id);
        $this->db->update($this->table,$consommateur);
    }

    public function mailExist($mail){
        $mail = trim($mail);

        $result = $this->db->query('SELECT mail FROM '.$this->table.' WHERE LOWER(mail)="'.strtolower($mail).'"')->result();
        if(count($result)>0){
            return true;
        }       
        return false;
    }

    public function getMailById($id_consommateur){
        $result = $this->db->query('SELECT mail FROM '.$this->table.' WHERE id_consommateur="'.$id_consommateur.'"')->result();
        if(count($result)>0){
            return $result[0]->mail;
        }
        
        return '';
    }

    // LES FORMULES DE JULIA
    private function quantite_CO2_sauve($poid_total){
        return $poid_total*1.9;
    }

    private function quantite_aliment_sauve($poid_total){
        return $poid_total/0.25;
    }
    // LES FORMULES DE JULIA

    public function getPreferenceAlimentaire($id_consommateur){
        // select donation_id in produit commande

        $query1 = "SELECT donation_id FROM ". $this->tableProduitCommande. " INNER JOIN " .$this->tableCommande. " ON ".$this->tableProduitCommande.".id_produit_commande = ".$this->tableCommande.".id_commande WHERE ".$this->tableCommande. ".consommateur_id = ".$id_consommateur;

        $result = $this->db->query($query1)->result();

        if($result != null){
            $result_to_string = "(";

            for($i=0;$i<count($result);$i++){

                if($result[$i]->donation_id == null)
                    return null;
                if(count($result) == 1){
                       $result_to_string .= $result[$i]->donation_id. ")";
                }
                else
                {
                    if($i==count($result)-1){
                    $result_to_string .= $result[$i]->donation_id. ")";                    
                }
                    else{
                        $result_to_string .= $result[$i]->donation_id. ",";                      
                    }
                }
            }

            $query2 = "SELECT produit_id_produit FROM ".$this->tableDonation. " wHERE id_donation in ". $result_to_string;

            $id_donations = $this->db->query($query2)->result();

            $id_donations_to_string = "(";

            for($i=0;$i<count($id_donations);$i++){
                if(count($id_donations) == 1){
                       $id_donations_to_string .= $id_donations[$i]->produit_id_produit. ")";
                }
                else
                {
                    if($i==count($id_donations)-1){
                    $id_donations_to_string .= $id_donations[$i]->produit_id_produit. ")";                    
                }
                    else{
                        $id_donations_to_string .= $id_donations[$i]->produit_id_produit. ",";                      
                    }
                }
            }

            $query3 = "SELECT * FROM ".$this->tableProduit. " wHERE id_produit in ". $id_donations_to_string;

            $les_produits = $this->db->query($query3)->result();


            $poid_total = 0;

            for($i=0;$i<count($les_produits);$i++){
                $poid_total+= $les_produits[$i]->poids;
            }
            
            // preond les 3 premier label de type de produit pour ses type produit préférés 
            $query4 = "SELECT label FROM ". $this->tableTypeProduit. " GROUP BY label ORDER BY count(*) LIMIT 3";

            $result4 =  $this->db->query($query4)->result();

            $to_return = array();
            
            // les formules de JULIA pour les calculs des quantités aliments sauvés et quantités CO2 sauvés
            $to_return["preference_alimentaire"] = $result4;
            $to_return["quantite_CO2_sauve"] = $this->quantite_CO2_sauve($poid_total);
            $to_return["quantite_aliment_sauve"] = $this->quantite_aliment_sauve($poid_total);
            // les formules de JULIA pour les calculs des quantités aliments sauvés et quantités CO2 sauvés

            return $to_return;
        }
        else{
            return null;
        }

        
    }

    public function addFavoris($consommateur_id,$donation_id){
        
        $data = array(
            "vue_produit"       => 1,
            "favoris"           => 1,
            "consommateur_id"   => $consommateur_id,
            "donation_id"       => $donation_id
        );

        $this->db->insert($this->tableActionConsommateur,$data); 
        return $this->db->insert_id();
    }

    public function addVueProduit($consommateur_id,$donation_id){
        
        $data = array(
            "vue_produit"       => 1,
            "favoris"           => 0,
            "consommateur_id"   => $consommateur_id,
            "donation_id"       => $donation_id
        );

        $this->db->insert($this->tableActionConsommateur,$data); 
        return $this->db->insert_id();
    }

    public function getActionConsByIdConsommateurByIdDonation($id_consommateur,$id_donation){
        $query = $this->db->get_where($this->tableActionConsommateur,array("consommateur_id"=>(int)$id_consommateur,"donation_id" => (int) $id_donation));
        $result = $query->result();
        if(count($result) == 1)
            return 1;
        else
            return 0;
    }

    public function getActionConsFavoriByIdConsommateurByIdDonation($id_consommateur,$id_donation,$favoris){
        $query = $this->db->get_where($this->tableActionConsommateur,
            array(
                "consommateur_id"   => (int)$id_consommateur,
                "donation_id"       => (int) $id_donation,
                "favoris"           => $favoris
            )
        );

        $result = $query->result();
        if(count($result) == 1)
            return 1;
        else
            return 0;
    }

    public function updateToFavoris($id_consommateur,$id_donation,$favoris){
        $data = array(
            "consommateur_id"   => (int)$id_consommateur,
            "donation_id"       => (int) $id_donation,
            "favoris"           => $favoris
        );

        $this->db->where(array(
                "consommateur_id"   => (int)$id_consommateur,
                "donation_id"       => (int) $id_donation
            ));

        $this->db->update($this->tableActionConsommateur,$data);
    }

    public function getNombreDeVueByIdDonation($donation_id){
        $query = "SELECT count(vue_produit) as vue FROM ".$this->tableActionConsommateur. " WHERE donation_id = ".$donation_id. " GROUP BY donation_id";
        return $this->db->query($query)->result();
    }

    public function getFavorisByIdConsommateur($consommateur_id){
        $consommateur_id = (int) $consommateur_id;
        $query = "SELECT donation_id FROM ".$this->tableActionConsommateur. " WHERE consommateur_id = ".$consommateur_id. " AND favoris = 1 " ;
        return $this->db->query($query)->result();
    }

    public function produitIsFavoris($consommateur_id,$donation_id){
        $consommateur_id = (int) $consommateur_id;
        $donation_id = (int) $donation_id;
        $mes_conditions = " WHERE consommateur_id = ".$consommateur_id. " AND donation_id = ". $donation_id. " AND favoris = 1 ";
        $query = "SELECT id_action_consommateur FROM ".$this->tableActionConsommateur. $mes_conditions;
        $result =  $this->db->query($query)->result();
        if(count($result) == 1)
            return 1;
        else
            return 0;    
    }

    public function addCommande($data){
        $this->db->insert($this->tableCommande,$data); 
        return $this->db->insert_id();
    }

    public function addProduitCommande($data){
        $this->db->insert($this->tableProduitCommande,$data); 
        return $this->db->insert_id();
    }

    public function getCommandeByIdCommande($id_commande){
        $id_commande = (int) $id_commande;
        $query = "SELECT id_commande,date, etat FROM ".$this->tableCommande. " WHERE id_commande = ".$id_commande ;
        $result = $this->db->query($query)->result();
        if(count($result) == 1)
            return $result[0];
        else
            return null;
    }

    public function getCommandeByIdConsommateur($consommateur_id){
        $consommateur_id = (int) $consommateur_id;
        $query = "SELECT id_commande as commande_id, date, etat FROM ".$this->tableCommande. " WHERE consommateur_id = ".$consommateur_id. " ORDER by date DESC" ;
        return $this->db->query($query)->result();
    }
    
    public function getProduitCommandeByIdCommande($commande_id){
        $commande_id = (int) $commande_id;
        $query = "SELECT donation_id, prix_achat_unite,prix_achat_unite*qte as montant, qte FROM ".$this->tableProduitCommande. " WHERE commande_id = ".$commande_id ;
        $result = $this->db->query($query)->result();    
        return $result;   
    }

    public function getLabelCommandeByIdEtatCommande($id_etat){
        $id_etat = (int) $id_etat;
        $query = "SELECT label FROM ".$this->tableEtatCommande. " WHERE id_etat_commande = ".$id_etat ;
        $result = $this->db->query($query)->result();
        if(count($result) == 1)
            return $result[0];
        else
            return null;   
    }

    public function saveFeedback($data){
        $this->db->insert($this->tableFeedback,$data); 
        return $this->db->insert_id();
    }

    public function commandeTransaction($commande, $produits){

        $response = new stdClass();
        $response->transation_status = 1;
        $response->data = new stdClass;

        $this->db->trans_begin();

        // insertion commande
        $id_commande = $this->saveCommande($commande); 
        // insertion produits
        foreach ($produits as $produit) {
            
            $info_donation = $this->don->findDonationById($produit->id);

            if(count($info_donation) == 1){

                $info_donation = $info_donation[0];
                if( $produit->qte <= $info_donation->quantite ){
                    
                    $data_produit_commande = array(
                        "commande_id"       => $id_commande,
                        "qte"               => $produit->qte,
                        "prix_achat_unite"  => $produit->prix_achat,
                        "donation_id"       => $produit->id
                    );

                    $update_result =  $this->don->updateQuantiteProduit($info_donation->id_donation,$produit->qte);
                    $id_produit_commande = $this->consommateur->addProduitCommande($data_produit_commande);
                    
                    if($id_produit_commande == null || $update_result == false){
                        $response->data = new stdClass;
                        $response->transation_status = -4;
                        break;
                    }
                }
                else{
                    
                    $produit = $this->don->findProduitIdDonationNomproduitByIdDonation($produit->id);
                    if(count($produit) > 0)
                        $produit = $produit[0];
                    $response->data = $produit;
                    $response->transation_status = -5; 
                    break;
                }       
            }
            else{
                $response->data = new stdClass;
                $$response->transation_status = -1;
                break;
            }
        }

        if ($response->transation_status == 1) {
            # Something went wrong.
            $this->db->trans_commit();
            return $response;
        } 
        else {
            # Everything is Perfect. 
            # Committing data to the database.
            $this->db->trans_rollback();
            return $response;
        }
    
    }

    public function saveCommande($data){
        $this->db->insert($this->tableCommande,$data); 
        return $this->db->insert_id();
    }
}