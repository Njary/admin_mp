<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends MX_Controller {

	function __construct()
	{
		parent::__construct();
		$this->layout->set_charset('UTF-8');
		$this->layout->addMenu(array(
			"menu"=>array(
				"name"=>"Tableau de bord",
				"link"=>"admin/dashboard",
				"action"=>"Tableau de bord",
				"icon"=>"fa-dashboard"
			)
		));
		$this->layout->addMenu(array(
			"title"=>"Produits"
			)
		);

		$this->layout->addMenu(array(
			"menu"=>array(
				"name"=>"Liste des ventes et dons",
				"link"=>"produit",
				"action"=>"Liste",
				"icon"=>"fa-list"
			)
		));	

		$this->layout->addMenu(array(
			"menu"=>array(
				"name"=>"Faire une vente",
				"link"=>"vente",
				"action"=>"Vente",
				"icon"=>"fa-usd"
			)
		));		

		$this->layout->addMenu(array(
			"menu"=>array(
				"name"=>"Faire un don",
				"link"=>"don",
				"action"=>"Donation",
				"icon"=>"fa-heart"
			)
		));	

		$this->layout->addMenu(array(
			"menu"=>array(
				"name"=>"Aide",
				"link"=>"aide",
				"action"=>"Aide",
				"icon"=>"fa-info"
			)
		));	

		

		//Liste des CSS
		$this->layout->ajouter_css('../plugins/datepicker/datepicker3');
		$this->layout->ajouter_css('../plugins/datetimepicker/bootstrap-datetimepicker.min');
		$this->layout->ajouter_css('../plugins/timepicker/bootstrap-timepicker');
		$this->layout->ajouter_css('../plugins/select2/select2.min');
		$this->layout->ajouter_css('../plugins/iCheck/all');
		$this->layout->ajouter_css('../plugins/tippy-v0.2.6/tippy');
		$this->layout->ajouter_css('../css/don');
		$this->layout->ajouter_css('../css/croppie');
		$this->layout->ajouter_css('toastr.min');
		$this->layout->ajouter_css('style');
		$this->layout->ajouter_css('w3');
		//Liste des bibliothèques JS 
		$this->layout->ajouter_js('../js/jquery.fileDownload');
		$this->layout->ajouter_js('../plugins/datepicker/bootstrap-datepicker');
		$this->layout->ajouter_js('../plugins/datetimepicker/bootstrap-datetimepicker.min');
		$this->layout->ajouter_js('../plugins/timepicker/bootstrap-timepicker');
		$this->layout->ajouter_js('../plugins/datepicker/locales/bootstrap-datepicker.fr');
		$this->layout->ajouter_js('../plugins/select2/select2.full.min');
		$this->layout->ajouter_js('../plugins/iCheck/icheck.min');
		$this->layout->ajouter_js('../plugins/tippy-v0.2.6/tippy');
		$this->layout->ajouter_js('../js/jquery.Jcrop.min');
		$this->layout->ajouter_js('toastr.min');
		$this->layout->ajouter_js('validator');
		$this->layout->ajouter_js('../js/croppie');
	}

	protected function checkPrivilege(){
		$idUser=$this->session->userdata("idUser");

		if(empty($idUser) || $idUser<0)
			redirect(site_url("login"));
	}

	protected function action($action){
		$this->layout->setAction($action);
	}

	
}
