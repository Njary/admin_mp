<!DOCTYPE html>
<html>
<head>
  <link rel="icon" type="image/png" href="<?php echo base_url('assets/dist/img/icon_foodwise.png'); ?>" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>FW | Inscription</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/plugins/select2/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/plugins/iCheck/all.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <?php foreach($css as $url): ?>
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $url; ?>" />
  <?php endforeach; ?>

  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/css/style.css">
</head>
<body class="hold-transition register-page">
  <input type="hidden" id="site_url" value="<?php echo site_url("");?>"/>
<div class="register-box">
  <div class="register-logo">
    <a href="#">Food<b>Wise</b></a>
  </div>

  <?php if(isset($succes_ben) && $succes_ben==1): ?>
    <div class="card" ng-app="registrationApp" ng-controller="formRegistrationCtrl">
      <div class="card-body register-card-body" style="text-align: center;">
        Votre enregistrement a été prise en compte. L'équipe vous remercie pour votre confiance.
      </div>
    </div>
  <?php else: ?>
    <div class="card" ng-app="registrationApp" ng-controller="formRegistrationCtrl">
      <div class="card-body register-card-body">
        <p class="login-box-msg">Inscription <?php echo $type_compte['nom']; ?></p>
        <div class="col-12" style="font-size: 12px;color: grey;padding: 0;margin-bottom: 10px;">Les champs avec des étoiles (*) sont obligatoires</div>

        <?php if($type_compte['code']==1): ?>
          <form action="<?php echo site_url('register'); ?>" method="POST" id="formRegister">
            <div class="form-group row">
              <div class="col-10">
                <select id="select_types_entreprise" name="type_entreprise[]" class="form-control select2" multiple="multiple" data-placeholder="Choisir le type de votre entreprise*"
                        style="width: 100%;" required="">
                  <?php foreach($types_entreprise as $type): ?>
                    <option value="<?php echo $type->id_type_entreprise; ?>"><?php echo $type->label; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="col-2">
                <a class="btn btn-primary btn-block" ng-click="addTypeEntreprise()" title="Ajouter un autre type"><i class="fa fa-plus-circle"></i></a>
              </div>
            </div>


            <div class="form-group has-feedback">
              <input name="nom_entreprise" type="text" class="form-control" placeholder="Nom de votre entreprise*" value="<?php echo set_value('nom_entreprise', ''); ?>" required>
              <span class="fa fa-building form-control-feedback"></span>
            </div>
            
            <div class="form-group row">
              <div class="col-10">
                <input name="adresse_entreprise" type="text" class="form-control" placeholder="Adresse de votre entreprise*" value="<?php echo set_value('adresse_entreprise', ''); ?>" required="">
                <!-- <span class="fa fa-map-marker form-control-feedback"></span> -->
              </div>
              <div class="col-2">
                <a class="btn btn-primary btn-block" ng-click="addAdresse()" title="Ajouter un autre adresse"><i class="fa fa-plus-circle"></i></a>
              </div>
            </div>

            <div id="otherAdress">
        
            </div>


            <div class="form-group has-feedback">
              <label>Responsable numéro 1</label>
              <input name="responsable1_nom" type="text" class="form-control" placeholder="Nom*" required="">
            </div>
            <div class="form-group has-feedback">
              <input name="responsable1_poste" type="text" class="form-control" placeholder="Poste*" required="">
            </div>
            <div class="form-group has-feedback">
              <input name="responsable1_email" type="email" class="form-control" placeholder="Email*" required="">
            </div>
            <div class="form-group has-feedback">
              <input name="responsable1_phone" type="text" class="form-control" placeholder="Téléphone*" required="">
            </div>

            <div class="row" ng-if="!twoResponsable">
              <div class="col-10 offset-1" style="margin-bottom: 10px;">
                <a class="btn btn-default btn-block btn-sm" ng-click="addResponsable()">Ajouter un deuxième responsable</a>
              </div>
            </div>

            <div ng-if="twoResponsable">
              <div class="form-group has-feedback">
                <label>Responsable numéro 2</label>
                <input name="responsable2_nom" type="text" class="form-control" placeholder="Nom*">
              </div>
              <div class="form-group has-feedback">
                <input name="responsable2_poste" type="text" class="form-control" placeholder="Poste*">
              </div>
              <div class="form-group has-feedback">
                <input name="responsable2_email" type="email" class="form-control" placeholder="Email*">
              </div>
              <div class="form-group has-feedback">
                <input name="responsable2_phone" type="text" class="form-control" placeholder="Téléphone*">
              </div>
            </div>

            <div class="has-feedback">
              <label>Chiffre d'affaires</label>
            </div>

            <div class="form-group has-feedback input-group">
              <input name="ca_entreprise" type="number" class="form-control" placeholder="Saisir le montant">
              <div class="input-group-append">
                <span class="input-group-text">MGA</span>
              </div>
            </div>

            <div class="form-group" id="typesAliment">
              <label>Type d'aliments</label><br>
              <?php foreach($types_aliment as $type): ?>
                <label class="check">
                  <input name="type_aliment_<?php echo $type->id_type_aliment; ?>" type="checkbox" class="flat-red">
                  <?php echo $type->label; ?>
                </label>
              <?php endforeach; ?>
              </label>
            </div>

            <div class="row">
              <div class="col-4" style="margin-top: -14px;margin-bottom: 15px;">
                <a class="btn btn-default btn-block btn-sm" ng-click="addTypeAliment()">Autre</a>
              </div>
            </div>

            <div class="form-group has-feedback">
              <label>Quantité de produits alimentaires(déchets alimentaire)</label>
            </div>

            <div class="form-group has-feedback input-group">
              <input name="qte_kg" type="number" class="form-control" placeholder="Quantité en poids">
              <div class="input-group-append">
                <span class="input-group-text">Kg</span>
              </div>
            </div>

            <div class="form-group has-feedback input-group">
              <input name="qte_mga" type="number" class="form-control" placeholder="Montant">
              <div class="input-group-append">
                <span class="input-group-text">MGA</span>
              </div>
            </div>

            <div class="form-group has-feedback">
              <label>Information d'authentification</label>
            </div>

            <div class="form-group has-feedback">
              <input ng-model="pseudo" name="pseudo" type="text" class="form-control" placeholder="Pseudo*" value="<?php echo set_value('pseudo', ''); ?>" ng-blur="checkPseudo(pseudo)" required>
              <span class="fa fa-user form-control-feedback"></span>
              <div class="loader col-2" style="position: absolute;right: -9px;top: 0;"><img src="<?php echo img_url('spin.svg'); ?>" ng-if="checkingPseudo"></div>
              <span class="form-error" ng-if="pseudoValid">Ce pseudo est déjà utilisé par un autre compte</span>
            </div>

            
            <div class="form-group has-feedback">
              <input ng-model="password" name="password" type="password" class="form-control" placeholder="Mot de passe*" value="<?php echo set_value('password', ''); ?>" ng-change="requiredRetype()" required>
              <span class="fa fa-lock form-control-feedback"></span>
            </div>

            <div class="form-group has-feedback">
              <input ng-model="cpassword" name="cpassword" type="password" class="form-control" placeholder="Resaisir mot de passe*" value="<?php echo set_value('cpassword', ''); ?>" ng-change="validForm()" required>
              <span class="fa fa-lock form-control-feedback"></span>
            </div>

            <div class="row" ng-if="!formIsValid">
              <span class="form-error">Les mots de passe ne correspondent pas</span>
            </div>


            <div class="row">
              <!-- /.col -->
              <div class="col-4 offset-2">
                <a class="btn btn-secondary btn-block btn-flat" href="<?php echo site_url(); ?>">Annuler</a>
              </div>
              <div class="col-6">
                <button name="creer_compte" type="submit" class="btn btn-primary btn-block btn-flat">Créer mon compte</button>
              </div>
              <!-- /.col -->
            </div>
          </form>
        <?php elseif($type_compte['code']==2): ?>
          <form action="<?php echo site_url('registerReceiver'); ?>" method="POST" id="formRegisterBen">
            <div class="form-group row">
              <div class="col-12">
                <select id="select_type_organisation" name="type_organisation" class="form-control select2" data-placeholder="Choisir le type de votre organisation*"
                        style="width: 100%;" required="">
                  <?php foreach($types_organisation as $type): ?>
                    <option value="<?php echo $type->id_type_organisation; ?>"><?php echo $type->label; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
            </div>


            <div class="form-group has-feedback">
              <input name="nom_organisation" type="text" class="form-control" placeholder="Nom de votre organisation*" required>
              <span class="fa fa-building form-control-feedback"></span>
            </div>
            
            <div class="form-group row">
              <div class="col-12">
                <input name="adresse_organisation" type="text" class="form-control" placeholder="Adresse de votre organisation*" value="<?php echo set_value('adresse_entreprise', ''); ?>" required="">
                <!-- <span class="fa fa-map-marker form-control-feedback"></span> -->
              </div>
            </div>

            <div class="form-group has-feedback">
              <input name="numero" type="text" class="form-control" placeholder="Numéro du bénéficiaire*">
            </div>


            <div class="form-group has-feedback">
              <label>Responsable numéro 1</label>
              <input name="responsable1_nom" type="text" class="form-control" placeholder="Nom*" required="">
            </div>
            <div class="form-group has-feedback">
              <input name="responsable1_poste" type="text" class="form-control" placeholder="Poste*" required="">
            </div>
            <div class="form-group has-feedback">
              <input name="responsable1_email" type="email" class="form-control" placeholder="Email*" required="">
            </div>
            <div class="form-group has-feedback">
              <input name="responsable1_phone" type="text" class="form-control" placeholder="Téléphone*" required="">
            </div>

            <div class="row" ng-if="!twoResponsable">
              <div class="col-10 offset-1" style="margin-bottom: 10px;">
                <a class="btn btn-default btn-block btn-sm" ng-click="addResponsable()">Ajouter un deuxième responsable</a>
              </div>
            </div>

            <div ng-if="twoResponsable">
              <div class="form-group has-feedback">
                <label>Responsable numéro 2</label>
                <input name="responsable2_nom" type="text" class="form-control" placeholder="Nom*">
              </div>
              <div class="form-group has-feedback">
                <input name="responsable2_poste" type="text" class="form-control" placeholder="Poste*">
              </div>
              <div class="form-group has-feedback">
                <input name="responsable2_email" type="email" class="form-control" placeholder="Email*">
              </div>
              <div class="form-group has-feedback">
                <input name="responsable2_phone" type="text" class="form-control" placeholder="Téléphone*">
              </div>
            </div>

            <div class="">
              <label>Ages des membres</label>
            </div>
              
            <?php foreach($tranches_age as $tranche): ?>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" style="width: 120px;"><div style="margin:0 auto;"><?php echo $tranche->label; ?> ans</div></span>
                  </div>
                  <input name="age_<?php echo $tranche->id_tranche_age; ?>" type="number" class="form-control" placeholder="Saisir l'effectif*" required>
                </div>
              </div>
            <?php endforeach; ?>
            

            <div class="form-group" id="infoNutri">
              <label>Informations nutritionnelles</label><br>
              <?php foreach($info_nutri as $type): ?>
                <label class="check">
                  <input name="info_nutri_<?php echo $type->id_information_nutritionnelle; ?>" type="checkbox" class="flat-red">
                  <?php echo $type->label; ?>
                </label>
              <?php endforeach; ?>
              </label>
            </div>

            <div class="row">
              <div class="col-4" style="margin-top: -14px;margin-bottom: 15px;">
                <a class="btn btn-default btn-block btn-sm" ng-click="addInfoNutri()">Autre</a>
              </div>
            </div>

            <div class="form-group">
              <label>Avez-vous un frigo ?</label><br>
              <label style="margin-right: 10px;">
                <input type="radio" name="frigo" class="minimal" value="1">
                Oui
              </label>
              <label>
                <input type="radio" name="frigo" class="minimal" value="0" checked="">
                Non
              </label>
            </div>

            <div class="form-group">
              <label>Avez-vous un réfrigérateur ?</label><br>
              <label style="margin-right: 10px;">
                <input type="radio" name="refrigerateur" class="minimal" value="1">
                Oui
              </label>
              <label>
                <input type="radio" name="refrigerateur" class="minimal" value="0" checked="">
                Non
              </label>
            </div>

            <div class="form-group">
              <label>Avez-vous un moyen de transport ?</label><br>
              <label style="margin-right: 10px;">
                <input type="radio" name="transport" class="minimal" value="1">
                Oui
              </label>
              <label>
                <input type="radio" name="transport" class="minimal" value="0" checked="">
                Non
              </label>
            </div>



            <div class="row">
              <!-- /.col -->
              <div class="col-4 offset-2">
                <a class="btn btn-secondary btn-block btn-flat" href="<?php echo site_url(); ?>">Annuler</a>
              </div>
              <div class="col-6">
                <button name="creer_compte" type="submit" class="btn btn-primary btn-block btn-flat">Créer mon compte</button>
              </div>
              <!-- /.col -->
            </div>
          </form>
        <?php endif; ?>

        <div id="modal_type_entreprise" class="modal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Type d'entreprise</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Ajouter nouveau type</label>
                  <input type="text" class="form-control" ng-model="new_type" placeholder="Saisir un libellé pour le nouveau type">
                </div>
                <div class="form-error" ng-if="notValidTypeEntreprise">
                  Veuilez remplir correctement le champs ci-dessus
                </div>
              </div>
              <div class="modal-footer">
                <div class="loader col-1" ng-if="loadType"><img src="<?php echo img_url('spin.svg'); ?>"></div>
                <button type="button" class="btn btn-primary" ng-click="createTypeEntreprise()">Enregistrer</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
              </div>
            </div>
          </div>
        </div>

        <div id="modal_type_aliment" class="modal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Type d'aliments</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Ajouter nouveau type</label>
                  <input type="text" class="form-control" ng-model="new_type_aliment" placeholder="Saisir un libellé pour le nouveau type">
                </div>
                <div class="form-error" ng-if="notValidTypeEntreprise">
                  Veuilez remplir correctement le champs ci-dessus
                </div>
              </div>
              <div class="modal-footer">
                <div class="loader col-1" ng-if="loadType"><img src="<?php echo img_url('spin.svg'); ?>"></div>
                <button type="button" class="btn btn-primary" ng-click="createTypeAliment()">Enregistrer</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
              </div>
            </div>
          </div>
        </div>

        <div id="modal_info_nutri" class="modal" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Informations nutritionnelles</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label>Ajouter une nouvelle information nutritionnelle</label>
                  <input type="text" class="form-control" ng-model="new_info_nutri" placeholder="Saisir un libellé pour la nouvelle information nutritionnelle">
                </div>
                <div class="form-error" ng-if="notValidInfoNutri">
                  Veuilez remplir correctement le champs ci-dessus
                </div>
              </div>
              <div class="modal-footer">
                <div class="loader col-1" ng-if="loadType"><img src="<?php echo img_url('spin.svg'); ?>"></div>
                <button type="button" class="btn btn-primary" ng-click="createInfoNutri()">Enregistrer</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
              </div>
            </div>
          </div>
        </div>

      </div>
      <!-- /.form-box -->
    </div><!-- /.card -->
  <?php endif; ?>

</div>
<!-- /.register-box -->



<!-- jQuery -->
<script src="<?php echo base_url(''); ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(''); ?>assets/js/angular_1.7.2.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(''); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(''); ?>assets/plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url(''); ?>assets/plugins/select2/select2.full.min.js"></script>

<script>
  $(function () {
    $('.select2').select2({
    width: '100%',
    placeholder: 'Choiser ou ajouter',
    language: {
      noResults: function() {
        return '<button id="no-results-btn" onclick="noResultsButtonClicked()">No Result Found</a>';
      },
    },
    escapeMarkup: function(markup) {
      return markup;
    },
  });;

    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    })
  });

function noResultsButtonClicked() {
  alert('You clicked the "No Result Found" button.');
}

</script>

<?php foreach($js as $key=>$url): ?>
    <script type="text/javascript" src="<?php echo $url; ?>"></script> 
<?php endforeach; ?>

<script src="<?php echo base_url(''); ?>assets/js/registration.js"></script>
</body>
</html>
