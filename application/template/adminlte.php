<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="icon" type="image/png" href="<?php echo base_url('assets/dist/img/icon_foodwise.png'); ?>" />
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>FW | <?php echo $action; ?></title>
  
  <?php foreach($css as $url): ?>
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $url; ?>" />
  <?php endforeach; ?>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/plugins/font-awesome/css/font-awesome.min.css">

  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <!-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> -->

  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/css/style.css">
  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/css/contrat.css">

  <script type="text/javascript">
      var BASE_URL = "<?php echo base_url()?>";
  </script>
</head>
<body class="hold-transition sidebar-mini">
  <input type="hidden" id="site_url" value="<?php echo site_url("");?>"/>
<div class="wrapper">
  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <!-- <li class="nav-item d-none d-sm-inline-block">
        <a href="index3.html" class="nav-link">Home</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="#" class="nav-link">Contact</a>
      </li> -->
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Rechercher" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fa fa-search"></i>
          </button>
        </div>
      </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link" title="Déconnexion" data-toggle="dropdown" href="#">
          <i class="fa fa-power-off"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-mr dropdown-menu-right">
          <a href="<?php echo site_url('logout'); ?>" class="dropdown-item">
            <!-- Message Start -->
            <div class="media">
              <div class="media-body">
                <h3 class="dropdown-item-title">
                  Se déconnecter
                </h3>
              </div>
            </div>
            <!-- Message End -->
          </a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4" style="z-index: 1001;">
    <!-- Brand Logo -->
    <a href="<?php echo site_url(); ?>" class="brand-link">
      <img src="<?php echo base_url(''); ?>assets/dist/img/icon_foodwise.png" alt="AdminLTE Logo" class="brand-image elevation-3"
           style="opacity: .8">
      <span class="brand-text font-weight-light">FoodWise</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar" id="main-sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="<?php 
          if(!isset($logoEntreprise) || empty($logoEntreprise) || strcmp($logoEntreprise, "user_placeholder.png")==0){
            echo base_url('assets/img/user_placeholder.png');
          }
          else{
            echo base_url($logoEntreprise);
          };
          //echo img_url($logoEntreprise); 
          ?>" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="<?php echo site_url('entreprise/profil'); ?>" class="d-block"><?php echo $nomEntreprise; ?></a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <?php foreach($list_menu as $menu): ?>
            
            <?php if(array_key_exists("title",$menu)): ?>
              <li class="nav-header"><?php echo $menu["title"]; ?></li>
            <?php endif; ?>

            <?php if(array_key_exists("menu",$menu)): ?>
              <li class="nav-item <?php echo (array_key_exists('sous_menu',$menu['menu']))?'has-treeview':''; ?>">
                <a href="<?php echo site_url($menu['menu']['link']); ?>" class="nav-link <?php echo ($action==$menu['menu']['action'])?'active':''; ?>">
                  <i class="nav-icon fa <?php echo $menu['menu']['icon'] ?>"></i>
                  <p>
                    <?php echo $menu["menu"]["name"]; ?>
                  </p>
                </a>
                <?php if(array_key_exists('sous_menu',$menu['menu'])): ?>
                  <ul class="nav nav-treeview">
                    <?php foreach($menu['menu']['sous_menu'] as $sous_menu): ?>
                      <li class="nav-item">
                        <a href="<?php echo site_url($sous_menu['link']); ?>" class="nav-link <?php echo ($action==$menu['menu']['action'])?'active':''; ?>">
                          <i class="nav-icon fa <?php echo $sous_menu['icon'] ?>"></i>
                          <p>
                            <?php echo $sous_menu["name"]; ?>
                          </p>
                        </a>
                      </li>
                    <?php endforeach; ?>
                  </ul>
                <?php endif; ?>
              </li>
            <?php endif; ?>

          <?php endforeach; ?>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark"><?php echo $action; ?></h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <!-- <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <?php echo $output; ?>
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- Default to the left -->
    <strong>Powered by <a href="#">SOFTIMAD</a>.</strong>
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="<?php echo base_url(''); ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(''); ?>assets/plugins/jquery-ui/jquery-ui.js"></script>
<!-- Bootstrap -->
<script src="<?php echo base_url(''); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- AdminLTE App -->
<script src="<?php echo base_url(''); ?>assets/dist/js/adminlte.js"></script>

<!-- ANGULAJS -->
<script src="<?php echo base_url(''); ?>assets/js/angular_1.7.2.min.js"></script>

<!-- PAGE PLUGINS -->
<!-- SparkLine -->
<script src="<?php echo base_url(''); ?>assets/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- jVectorMap -->
<script src="<?php echo base_url(''); ?>assets/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url(''); ?>assets/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- SlimScroll 1.3.0 -->
<script src="<?php echo base_url(''); ?>assets/plugins/slimScroll/jquery.slimscroll.min.js"></script>

<?php foreach($js as $key=>$url): ?>
    <script type="text/javascript" src="<?php echo $url; ?>"></script> 
<?php endforeach; ?>

<?php if( isset( $need_map ) && $need_map ): ?>
  <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.js'></script>
  <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet' />
<?php endif; ?>

<script type="text/javascript">
  $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    })
</script>
</body>
</html>
