<!DOCTYPE html>
<html>
<head>
  <link rel="icon" type="image/png" href="<?php echo base_url('assets/dist/img/icon_foodwise.png'); ?>" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>FW | Inscription</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/plugins/select2/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/plugins/iCheck/all.css">

  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <?php foreach($css as $url): ?>
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $url; ?>" />
  <?php endforeach; ?>

  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/css/style.css">
</head>
<body class="hold-transition register-page">
  <input type="hidden" id="site_url" value="<?php echo site_url("");?>"/>
<div class="register-box" style="width: 390px;">
  <div class="register-logo">
    <a href="#">Food<b>Wise</b></a>
  </div>

  <?php if(isset($succes_ben) && $succes_ben==1): ?>
    <div class="card" ng-app="registrationApp" ng-controller="formRegistrationCtrl">
      <div class="card-body register-card-body" style="text-align: center;">
        Votre enregistrement a été prise en compte. L'équipe vous remercie pour votre confiance.
      </div>
    </div>
  <?php else: ?>
    <div class="card" ng-app="registrationApp" ng-controller="formRegistrationCtrl" ng-init="listPays = <?php  echo htmlspecialchars(json_encode($pays)); ?>">
      <div class="card-body register-card-body">
        <p class="login-box-msg">Inscription <?php echo $type_compte['nom']; ?></p>
        <div class="col-12" style="font-size: 12px;color: grey;padding: 0;margin-bottom: 10px;">Les champs avec des étoiles (*) sont obligatoires</div>

        <?php if($type_compte['code']==1): ?>
          <form action="<?php echo site_url('register'); ?>" method="POST" id="formRegister">
            <label>Nom de l'entreprise*</label>
            <div class="form-group has-feedback">
              <input ng-model="pseudo" ng-blur="checkPseudo(pseudo)" name="nom_entreprise" type="text" class="form-control" placeholder="Nom de votre entreprise*" id="nom_entreprise" required>
              <span class="fa fa-building form-control-feedback"></span>
              <div class="loader-default col-2" style="position: absolute;right: -9px;top: 0;"><img src="<?php echo img_url('spin.svg'); ?>" ng-if="checkingPseudo"></div>
              <span class="form-error" ng-if="pseudoValid">Un compte est déjà associé à ce nom d'entreprise</span>
            </div>

            <div class="form-group">
              <label>Pays*</label>
               <select id="select_pays" name="responsable1_pays" class="select-pays form-control select2" data-placeholder="Pays où se situe l'entreprise*"style="width: 100%;" required=""
              ng-model="selectPays[0]" ng-change="changePays(0)">
                  <option></option>
                  <?php foreach($pays as $p): ?>
                    <option value="<?php echo $p->id_pays; ?>"><?php echo $p->label; ?></option>
                  <?php endforeach; ?>
                </select>
            </div>

            <label>Poste*</label>
            <div class="form-group row">
              <div class="col-12">
                <select id="select_poste" name="responsable1_poste" class="select-poste form-control select2" data-placeholder="Poste à l'enceinte de l'entreprise*"
                        style="width: 100%;" required="">
                        <option></option>
                  <?php foreach($postes as $poste): ?>
                    <option value="<?php echo $poste->id_poste; ?>"><?php echo $poste->label; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <!--<div class="col-3">
                <a class="btn btn-primary btn-block" ng-click="addPoste('#select_poste',1)" title="Ajouter un autre type"><span>Ajouter</span></a>
              </div>-->
            </div>

            <div class="form-group has-feedback">
              <div class="input-group input-group">
                  <div class="input-group-prepend">
                    <select class="form-control select2" name="sexe">
                      <option value="M">Mr</option>
                      <option value="F">Mme</option>
                    </select>
                  </div>
                  <input name="responsable1_nom" type="text" class="form-control" placeholder="Nom*" required="">
              </div>
            </div>

            <div class="form-group has-feedback">
              <input name="responsable1_prenom" type="text" class="form-control" placeholder="Prénom(s)*" required="">
            </div>

            <div class="form-group has-feedback">
              <input ng-keyup="verifMail()" id="responsable1_email" name="responsable1_email" type="email" class="form-control" placeholder="Email*" required="">
              <div class="loader-default col-2" id="loader-email" style="display: none; position: absolute;right: -9px;top: 0;"><img src="<?php echo img_url('spin.svg'); ?>"></div>
              <span class="form-error" id="msg-email" style="display: none;">cette adresse e-mail est déjà utilisée par un membre du plateforme</span>
            </div>
            <div class="form-group has-feedback">
              <div class="row">
                <div class="col-md-3">
                  <span class="input-group-text labelCompte" id="labelTelephone0"> - </span>
                </div>
                <div class="col-md-9">
                  <input id="numero_telephone" ng-blur="verifyFormatTelephone(0)" name="responsable1_phone" type="text" class="form-control" placeholder="Téléphone*" required="">
                    <span class="loader-default col-1" id="loader-responsable1_phone-resp0" style="display: none; position: absolute;right: 0px;top: 5px;"><img src="<?php echo img_url('spin.svg'); ?>"></span>
                  <input type="hidden" id="loader_checking" val="<?php echo img_url('spin.svg'); ?>"/>
                </div>
              </div>
            </div>
            
            <label>Information d'authentification</label>
            <div class="form-group has-feedback">
              <input ng-model="password" name="password" type="password" class="form-control" placeholder="Mot de passe*" value="<?php echo set_value('password', ''); ?>" ng-change="requiredRetype()" required >
              <span class="fa fa-lock form-control-feedback"></span>
            </div>
            <div class="row" ng-if="!mdpValid">
              <span class="form-error">La longueur du mot de passe doit être supérieur ou égal à 6</span>
            </div>
            <div class="form-group has-feedback">
              <input ng-model="cpassword" name="cpassword" type="password" class="form-control" placeholder="Resaisir mot de passe*" value="<?php echo set_value('cpassword', ''); ?>" ng-change="validForm()" required>
              <span class="fa fa-lock form-control-feedback"></span>
            </div>

            <div class="row" ng-if="!formIsValid">
              <span class="form-error">Les mots de passe ne correspondent pas</span>
            </div>


            <div class="row">
              <!-- /.col -->
              <!-- <div class="col-4 offset-2">
                <a class="btn btn-secondary btn-block btn-flat" href="<?php echo site_url(); ?>">Annuler</a>
              </div> -->
              <div class="col-7 offset-5">
                <button ng-disabled="mailInvalid" name="creer_compte" type="submit" class="btn btn-primary btn-block btn-flat">Créer mon compte</button>
              </div>
              <!-- /.col -->
            </div>
          </form>

        <?php elseif($type_compte['code']==2): ?>

          <form action="<?php echo site_url('registerReceiver'); ?>" method="POST" id="formRegisterBen">
            <div class="form-group row">
              <div class="col-10">
                <select id="select_type_organisation" name="type_organisation" class="form-control select2" data-placeholder="Type d'organisation*"
                        style="width: 100%;" required="">
                    <option disabled selected value>Type d'organisation*</option>
                  <?php foreach($types_organisation as $type): ?>
                    <option value="<?php echo $type->id_type_organisation; ?>"><?php echo $type->label; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="col-2">
                <a class="btn btn-primary btn-block" ng-click="addTypeOrganisation()" title="Ajouter un autre type d'organisation"><span>Ajouter</span></i></a>
              </div>
            </div>


            <div class="form-group has-feedback">
              <input name="nom_organisation" type="text" class="form-control" placeholder="Nom de votre organisation*" required>
              <span class="fa fa-building form-control-feedback"></span>
            </div>
            
            <div class="form-group row">
              <div class="col-12">
                <input name="adresse_organisation" type="text" class="form-control" placeholder="Adresse de votre organisation*" value="<?php echo set_value('adresse_entreprise', ''); ?>" required="">
              </div>
            </div>
            <div class="form-group col-12">
              <input name="code_postal_organisation" type="text" class="form-control" placeholder="Code postal*" value="<?php echo set_value('adresse_entreprise', ''); ?>" required="">
            </div>
            <div class="form-group col-12">
              <input name="ville_organisation" type="text" class="form-control" placeholder="Ville*" value="<?php echo set_value('adresse_entreprise', ''); ?>" required="">
            </div>
            <div class="form-group col-12">
              <select id="select_ben_pays" name="pays_organisation" class="form-control select2" style="width: 100%;" required="" ng-model="selectBenPays[0]" ng-change="changeBenPays(0)" ng-init="initValBenPays(0)">  
               >
                 <option ></option>
                  <?php foreach($pays as $p): ?>
                    <option value="<?php echo $p->id_pays; ?>"><?php echo $p->label; ?></option>
                  <?php endforeach; ?>
              </select>
            </div>

            <div class="form-group row">
              <div class="col-10">
                <select id="select_beneficiaire" name="beneficiaire[]" class="form-control select2 select2-hidden-accessible" multiple="multiple" data-placeholder="Type de bénéficiaires*"
                        style="width: 100%;" required="">
                  <?php foreach($beneficiaires as $type): ?>
                    <option value="<?php echo $type->id_beneficiaire_organisation; ?>"><?php echo $type->label; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="col-2">
                <a class="btn btn-primary btn-block" ng-click="addBeneficiaire()" title="Ajouter un autre type de bénéficiaires"><span>Ajouter</span></a>
              </div>
            </div>

            <div class="form-group has-feedback">
              <input name="numero" type="text" class="form-control" placeholder="Nombre de bénéficiaires*">
            </div>


            <div class="form-group has-feedback">
              <label>Responsable numéro 1</label>
              <div class="input-group input-group-md">
                <div class="input-group-prepend" id="sexe-select">
                  <select class="form-control select2" name="responsable1_sexe">
                    <option value="M">Mr</option>
                    <option value="F">Mme</option>
                  </select>
                </div>
                <input name="responsable1_nom" type="text" class="form-control" placeholder="Nom*" required="">
              </div>
            </div>

            <div class="form-group has-feedback">
              <input name="responsable1_prenom" type="text" class="form-control" placeholder="Prénom(s)*" required="">
            </div>

            <div class="form-group row">
              <div class="col-9">
                <select id="select_poste1" name="responsable1_poste" class="select-poste form-control select2" data-placeholder="Poste à l'enceinte de l'organisme*"
                        style="width: 100%;" required="">
                        <option disabled selected value>Poste à l'enceinte de l'entreprise*</option>
                  <?php foreach($postes as $poste): ?>
                    <option value="<?php echo $poste->id_poste; ?>"><?php echo $poste->label; ?></option>
                  <?php endforeach; ?>
                </select>
              </div>
              <div class="col-3">
                <a class="btn btn-primary btn-block" ng-click="addPoste('#select_poste1',2)" title="Ajouter un autre type"><span>Ajouter</span></a>
              </div>
            </div>
            
            <div class="form-group has-feedback">
              <input ng-keyup="verifMail2('1')" id="responsable1_email" name="responsable1_email" type="email" class="form-control" placeholder="Email*" required="">
              <div class="loader-default col-2" id="loader-email-resp1" style="display: none; position: absolute;right: -9px;top: 0;"><img src="<?php echo img_url('spin.svg'); ?>"></div>
              <span class="form-error" id="msg-email-resp1" style="display: none;">cette adresse e-mail est déjà utilisée par un membre du plateforme</span>
            </div>
            <div class="form-group has-feedback">

              <div class="row">
                  <div class="col-md-3">
                    <span class="input-group-text labelBenTelephone0" > - </span>
                  </div>
                  <div class="col-md-9">
                    <input name="responsable1_phone" id="responsable1_BenPhone" type="text" class="form-control form-to-test" placeholder="Téléphone*" required="">
                  </div>
              </div>
            </div>

            <div class="row" ng-if="!twoResponsable">
              <div class="col-10 offset-1" style="margin-bottom: 10px;">
                <a class="btn btn-default btn-block btn-sm" ng-click="addResponsable()">Ajouter un deuxième responsable</a>
              </div>
            </div>

            <div ng-if="twoResponsable">
              <div class="form-group has-feedback" id="res2">
                <label>Responsable numéro 2</label>
                <div class="input-group input-group-md">
                  <div class="input-group-prepend">
                    <select class="form-control select2" name="responsable2_sexe">
                      <option value="M">Mr</option>
                      <option value="F">Mme</option>
                    </select>
                  </div>
                  <input name="responsable2_nom" type="text" class="form-control" placeholder="Nom*">
                </div>
              </div>

              <div class="form-group has-feedback">
                <input name="responsable2_prenom" type="text" class="form-control" placeholder="Prénom(s)*">
              </div>

              <div class="form-group row">
                <div class="col-9">
                  <select id="select_poste2A" name="responsable2_poste" class="select-poste form-control select2" data-placeholder="Poste à l'enceinte de l'entreprise*"
                          style="width: 100%;">
                      <option disabled selected value>Poste à l'enceinte de l'entreprise*</option>
                    <?php foreach($postes as $poste): ?>
                      <option value="<?php echo $poste->id_poste; ?>"><?php echo $poste->label; ?></option>
                    <?php endforeach; ?>
                  </select>
                </div>
                <div class="col-3">
                  <a class="btn btn-primary btn-block" ng-click="addPoste('#select_poste2A',2)" title="Ajouter un autre type"><span>Ajouter</span></a>
                </div>
              </div>

              <div class="form-group has-feedback">
                <input ng-keyup="verifMail2('2')" id="responsable2_email" name="responsable2_email" type="email" class="form-control" placeholder="Email*">
                <div class="loader-default col-2" id="loader-email-resp2" style="display: none; position: absolute;right: -9px;top: 0;"><img src="<?php echo img_url('spin.svg'); ?>"></div>
                <span class="form-error" id="msg-email-resp2" style="display: none;">cette adresse e-mail est déjà utilisée par un membre du plateforme</span>
              </div>
              <div class="form-group has-feedback">
                <div class="row">
                    <div class="col-md-3">
                      <span class="input-group-text labelBenTelephone0" > - </span>
                    </div>
                    <div class="col-md-9">
                      <input name="responsable2_phone" id="responsable2_BenPhone" type="text" class="form-control form-to-test" title="Numéro téléphone du second responsable" placeholder="Téléphone*">
                    </div>
                </div>
                
              </div>
            </div>

            <div class="">
              <label>Ages des enfants</label>
            </div>
              
            <?php foreach($tranches_age as $tranche): ?>
              <div class="form-group">
                <div class="input-group">
                  <div class="input-group-prepend">
                    <span class="input-group-text" style="width: 120px;"><div style="margin:0 auto;"><?php echo $tranche->label; ?> ans</div></span>
                  </div>
                  <input name="age_<?php echo $tranche->id_tranche_age; ?>" type="number" min="0" class="form-control" placeholder="Saisir l'effectif*" value="0">
                </div>
              </div>
            <?php endforeach; ?>
            

            <div class="form-group" id="infoNutri">
              <label>Informations nutritionnelles</label><br>
              <?php foreach($info_nutri as $type): ?>
                <label class="check">
                  <input name="info_nutri_<?php echo $type->id_information_nutritionnelle; ?>" type="checkbox" class="flat-red">
                  <?php echo $type->label; ?>
                </label>
              <?php endforeach; ?>
              </label>
            </div>

            <div class="row">
              <div class="col-4" style="margin-top: -14px;margin-bottom: 15px;">
                <a class="btn btn-default btn-block btn-sm" ng-click="addInfoNutri()">Autre</a>
              </div>
            </div>

            <div class="form-group">
              <label>Avez-vous un frigo ?*</label><br>
              <label style="margin-right: 10px;">
                <input type="radio" name="frigo" class="minimal" value="1" required="">
                Oui
              </label>
              <label>
                <input type="radio" name="frigo" class="minimal" value="0">
                Non
              </label>
            </div>

            <div class="form-group">
              <label>Avez-vous un réfrigérateur ?*</label><br>
              <label style="margin-right: 10px;">
                <input type="radio" name="refrigerateur" class="minimal" value="1" required="">
                Oui
              </label>
              <label>
                <input type="radio" name="refrigerateur" class="minimal" value="0">
                Non
              </label>
            </div>

            <div class="form-group">
              <label>Avez-vous un moyen de transport ?*</label><br>
              <label style="margin-right: 10px;">
                <input type="radio" name="transport" class="minimal" value="1" required="">
                Oui
              </label>
              <label>
                <input type="radio" name="transport" class="minimal" value="0">
                Non
              </label>
            </div>

            <div class="row">
              <div class="col-7 offset-5">
                <button ng-disabled="mailStateIsGood()" name="creer_compte_ben" type="submit" class="btn btn-primary btn-block btn-flat">Créer mon compte</button>
              </div>
              <!-- /.col -->
            </div>
          </form>
        <?php endif; ?>

        <div id="modal_new_beneficiaire" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Ajout d'un autre type de receveur d'aliments</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Receveur d'aliment</label>
                  <input type="text" class="form-control" ng-model="new_beneficiaire" placeholder="Saisir un libellé pour le type de receveur d'aliment">
                </div>
                <div class="form-error" ng-if="notValidBeneficiaire">
                  Veuilez remplir correctement le champs ci-dessus
                </div>
              </div>
              <div class="modal-footer">
                <div class="loader-default col-1" ng-if="loadType"><img src="<?php echo img_url('spin.svg'); ?>"></div>
                <button type="button" class="btn btn-primary" ng-click="createBeneficiaire()">Enregistrer</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
              </div>
            </div>
          </div>
        </div>

        <div id="modal_new_type_organisation" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Type d'organisation</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Type</label>
                  <input type="text" class="form-control" ng-model="new_type_organisation" placeholder="Saisir un libellé pour le type d'organisation">
                </div>
                <div class="form-error" ng-if="notValidTypeOrganisation">
                  Veuilez remplir correctement le champs ci-dessus
                </div>
              </div>
              <div class="modal-footer">
                <div class="loader-default col-1" ng-if="loadType"><img src="<?php echo img_url('spin.svg'); ?>"></div>
                <button type="button" class="btn btn-primary" ng-click="createTypeOrganisation()">Enregistrer</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
              </div>
            </div>
          </div>
        </div>


        <div id="modal_new_poste" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Titre ou poste au sein de l'organisme</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Poste</label>
                  <input type="text" id="nouveau_poste" class="form-control" ng-model="new_type" placeholder="Saisir un libellé pour votre poste">
                </div>
               <!-- <div class="form-error" ng-if="notValidTypeEntreprise">
                  Veuillez remplir correctement le champs ci-dessus
                </div>-->
              </div>
              <div class="modal-footer">
                <div class="loader-default col-1" ng-if="loadType"><img src="<?php echo img_url('spin.svg'); ?>"></div>
                <button id="ajouterPosteBouton" type="button" class="btn btn-primary" ng-click="createPoste('#select_poste',1)">Enregistrer</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
              </div>
            </div>
          </div>
        </div>

        <div id="modal_type_aliment" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Type d'aliments</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label for="exampleInputEmail1">Ajouter nouveau type</label>
                  <input type="text" class="form-control" ng-model="new_type_aliment" placeholder="Saisir un libellé pour le nouveau type">
                </div>
                <div class="form-error" ng-if="notValidTypeEntreprise">
                  Veuilez remplir correctement le champs ci-dessus
                </div>
              </div>
              <div class="modal-footer">
                <div class="loader-default col-1" ng-if="loadType"><img src="<?php echo img_url('spin.svg'); ?>"></div>
                <button type="button" class="btn btn-primary" ng-click="createTypeAliment()">Enregistrer</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
              </div>
            </div>
          </div>
        </div>

        <div id="modal_info_nutri" class="modal fade" tabindex="-1" role="dialog">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title">Informations nutritionnelles</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <div class="form-group">
                  <label>Ajouter une nouvelle information nutritionnelle</label>
                  <input type="text" class="form-control" ng-model="new_info_nutri" placeholder="Saisir un libellé pour la nouvelle information nutritionnelle">
                </div>
                <div class="form-error" ng-if="notValidInfoNutri">
                  Veuilez remplir correctement le champs ci-dessus
                </div>
              </div>
              <div class="modal-footer">
                <div class="loader-default col-1" ng-if="loadType"><img src="<?php echo img_url('spin.svg'); ?>"></div>
                <button type="button" class="btn btn-primary" ng-click="createInfoNutri()">Enregistrer</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
              </div>
            </div>
          </div>
        </div>

        <!-- MODAL POUR AJOUT DE POSTE -->
        <div class="modal fade" id="modal-new-poste" tabindex="-1" role="dialog" aria-hidden="true">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <h5 class="modal-title" id="">Ajout d'un nouveau poste</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
              <div class="modal-body">
                <form>
                  <div class="form-group">
                    <label class="col-form-label">Nom du poste:</label>
                    <input type="text" class="form-control" id="input-nom-post-new">
                  </div>
                </form>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Annuler</button>
                <button type="button" class="btn btn-primary" ng-click="addNewPoste('<?php echo base_url(''); ?>a-n-p')">Ajouter</button>
              </div>
            </div>
          </div>
        </div>

      </div>
      <!-- /.form-box -->
    </div><!-- /.card -->
  <?php endif; ?>

</div>
<!-- /.register-box -->



<!-- jQuery -->
<script src="<?php echo base_url(''); ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(''); ?>assets/js/angular_1.7.2.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(''); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(''); ?>assets/plugins/iCheck/icheck.min.js"></script>
<script src="<?php echo base_url(''); ?>assets/plugins/select2/select2.full.min.js"></script>

<script>
  $(function () {
    $('#select_poste').select2({
    width: '100%',
    placeholder: 'Choiser ou ajouter',
    language: {
      noResults: function() {
        return '<button id="no-results-btn" class="btn btn-primary btn-block" onclick="ajouterPoste()">Ajouter poste</a>';
      },
    },
    escapeMarkup: function(markup) {
      return markup;
    },
    });

    
   

    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    })
  });

  
  function ajouterPoste() {


      var nouveauPoste = $("#select_poste").data("select2").dropdown.$search.val();
      $("#nouveau_poste").val(nouveauPoste);
      
      console.log($("#nouveau_poste").val());
      $("#ajouterPosteBouton").click();
           


    console.log("eyo");
}
</script>

<?php foreach($js as $key=>$url): ?>
    <script type="text/javascript" src="<?php echo $url; ?>"></script> 
<?php endforeach; ?>

<script src="<?php echo base_url(''); ?>assets/js/registration.js"></script>
</body>
</html>
