<!DOCTYPE html>
<html>
	<head>
		<link rel="icon" type="image/png" href="<?php echo img_url('assets/img/dm.png'); ?>" />
		<title>FW | <?php echo $titre; ?></title>		
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
		<?php foreach($css as $url): ?>
			<link rel="stylesheet" type="text/css" media="screen" href="<?php echo $url; ?>" />
		<?php endforeach; ?>
		<?php if(count($js)>0): ?>
			<script type="text/javascript" src="<?php echo $js[0]; ?>"></script>
		<?php endif; ?>
		</style>
	</head>

	<body class="" data-spy="scroll" data-target=".navbar">
		<div class="app-bg cont">
		<input type="hidden" id="site_url" value="<?php echo site_url("");?>"/>
		<?php echo $output; ?>
		</div>
		<?php foreach($js as $key=>$url): ?>
				<script type="text/javascript" src="<?php if($key!=0) echo $url; ?>"></script> 
		<?php endforeach; ?>
	</body>
</html>