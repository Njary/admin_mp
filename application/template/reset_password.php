<!DOCTYPE html>
<html>
<head>
  <link rel="icon" type="image/png" href="<?php echo base_url('assets/dist/img/icon_foodwise.png'); ?>" />
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>FW | Authentification</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/plugins/iCheck/square/blue.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <?php foreach($css as $url): ?>
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo $url; ?>" />
  <?php endforeach; ?>

  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/css/style.css">
</head>
<body class="hold-transition login-page">
  <input type="hidden" id="site_url" value="<?php echo site_url("");?>"/>
<div class="login-box">
  <div class="login-logo">
    <a href="#">Food<b>Wise</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card"  ng-app="resetApp" ng-controller="formReset">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Réinitialisation de mot de passe</p>
      <?php if(isset($reset) && $reset==1): ?>
        <div class="form-group has-feedback">
          <input ng-model="password" name="password" type="password" class="form-control" placeholder="Mot de passe*">
          <span class="fa fa-lock form-control-feedback"></span>
        </div>

        <div class="form-group has-feedback">
          <input ng-model="cpassword" name="cpassword" type="password" class="form-control" placeholder="Resaisir mot de passe*"ng-change="validForm()" required>
          <span class="fa fa-lock form-control-feedback"></span>
        </div>

        <div class="row" ng-if="formIsValid==-1">
          <span class="form-error">Les mots de passe ne correspondent pas</span>
        </div>
        <!-- /.col -->
        <div class="col-6 offset-3">
          <button class="btn btn-primary btn-block btn-flat" name="valider_login" ng-click="reinitPassword(<?php echo $utilisateur_id; ?>,password)">Réinitialiser</button>
          <div class="loader-default col-4" style="margin: 5px auto;" ng-if="loading"><img src="<?php echo img_url('spin.svg'); ?>"></div>
        </div>
        <!-- /.col -->
        </div>
      <?php else: ?>
        <div class="form-group has-feedback">
          <input ng-focus="mailStatus=-2" ng-model="mail" name="email" type="email" class="form-control" placeholder="Adresse mail du responsable numéro 1">
          <span class="fa fa-envelope form-control-feedback"></span>
          <div class="loader-default col-2" style="position: absolute;right: -9px;top: 0;" ng-if="loading"><img src="<?php echo img_url('spin.svg'); ?>"></div>
          <span class="form-error" ng-if="mailStatus==0">Aucun responsable numéro 1 correspond à l'adresse mail {{ mail }}</span>
          <span class="form-succes" ng-if="mailStatus==1">Un lien a été envoyer par email vers {{ mail }}</span>
        </div>
          <!-- /.col -->
          <div class="col-6 offset-3">
            <button class="btn btn-primary btn-block btn-flat" name="valider_login" ng-click="checkMail(mail)">Vérifier</button>
          </div>
          <!-- /.col -->
        </div>
      <?php endif; ?>

      <p class="mb-1" style="text-align: center;margin-top: -10px;">
        <a href="<?php echo site_url('login'); ?>">Annuler</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?php echo base_url(''); ?>assets/plugins/jquery/jquery.min.js"></script>
<script src="<?php echo base_url(''); ?>assets/js/angular_1.7.2.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(''); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(''); ?>assets/plugins/iCheck/icheck.min.js"></script>

<?php foreach($js as $key=>$url): ?>
    <script type="text/javascript" src="<?php echo $url; ?>"></script> 
<?php endforeach; ?>

<script src="<?php echo base_url(''); ?>assets/js/reset_password.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    })
  })
</script>
</body>
</html>
