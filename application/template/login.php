<!DOCTYPE html>
<html>
<head>
  <link rel="icon" type="image/png" href="<?php echo base_url('assets/dist/img/icon_foodwise.png'); ?>" />

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title> FW | Authentification</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/dist/css/adminlte.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/plugins/iCheck/square/blue.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">

  <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/css/style.css">

    <link rel="stylesheet" href="<?php echo base_url(''); ?>assets/css/toastr.min.css">

</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="#">Food<b>Wise</b></a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Veuillez vous identifier</p>

      <?php if($error==0): ?>
        <div class="row">
          <span class="form-error">Nom de votre entreprise ou mot de passe incorrect</span>
        </div>
      <?php endif; ?>

      <form method="POST" action="<?php echo site_url('utilisateur/acces/authenticate'); ?>">
        <div class="form-group has-feedback">
          <input name="username" type="text" class="form-control" placeholder="Nom de votre entreprise">
          <span class="fa fa-envelope form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
          <input name="password" type="password" class="form-control" placeholder="Mot de passe">
          <span class="fa fa-lock form-control-feedback"></span>
        </div>
        <div class="row" style="margin-bottom: 10px;">
          <!-- /.col -->
          <div class="col-6 offset-3">
            <button type="submit" class="btn btn-primary btn-block btn-flat" name="valider_login">S'identifier</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
      <!-- /.social-auth-links -->

      <p class="mb-1" style="text-align: center;">
        <a href="<?php echo site_url('forget_password'); ?>">Mot de passe oublié</a>
      </p>
      <p class="mb-0" style="text-align: center;">
        <a href="<?php echo site_url('registration'); ?>" class="text-center">Créer un compte</a>
      </p>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="<?php echo base_url(''); ?>assets/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?php echo base_url(''); ?>assets/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- iCheck -->
<script src="<?php echo base_url(''); ?>assets/plugins/iCheck/icheck.min.js"></script>

<script src="<?php echo base_url(''); ?>assets/js/toastr.min.js"></script>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass   : 'iradio_square-blue',
      increaseArea : '20%' // optional
    });
    
    <?php if(  $this->session->flashdata('inscription_reussie')  ) { ?>

          toastr.success('Votre enregistrement a été pris en compte.','Notification', {timeout:5000});
    
    <?php } ?>



  })
</script>
</body>
</html>
