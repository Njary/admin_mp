<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');  
 
require_once APPPATH."/libraries/PHPWord/autoload.php";
class Word extends \PhpOffice\PhpWord\TemplateProcessor {
    public function __construct($documentTemplate) {
        parent::__construct($documentTemplate);
    }
}
?>