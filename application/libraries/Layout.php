<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Layout
{
	private $CI;
	private $var = array();
	
/*
|===============================================================================
| Constructeur
|===============================================================================
*/
	
public function __construct()
{
	$this->CI =& get_instance();
	
	$this->var['output'] = '';

	$this->var['list_menu'] = array();
	
	//	Le titre est compos� du nom de la m�thode et du nom du contr�leur
	//	La fonction ucfirst permet d'ajouter une majuscule
	$this->var['titre'] = ucfirst($this->CI->router->fetch_method()) . ' - ' . ucfirst($this->CI->router->fetch_class());
	
	//	Nous initialisons la variable $charset avec la m�me valeur que
	//	la cl� de configuration initialis�e dans le fichier config.php
	$this->var['charset'] = $this->CI->config->item('charset');
	
	$this->var['css'] = array();
	$this->var['js'] = array();

	$this->var['error']=0;

	$this->var['type_compte']=array("code"=>-1,"nom"=>"...");

	$this->var['action']="Accueil";
}
	
/*
|===============================================================================
| M�thodes pour charger les vues
|	. addView : ajouter element dans output
|	. view(templateName) : afficher la page en utilisant le template templateName(qui est par d�faut 'default')
|===============================================================================
*/

	public function view($templateName='adminlte',$params=null)
	{	
		if($templateName=='adminlte'){
			$this->var["username"]=$this->CI->session->userdata("username");
			$this->var["nomEntreprise"]=$this->CI->session->userdata("nomEntreprise");
			$this->var["logoEntreprise"]=$this->CI->session->userdata("logoEntreprise");
		}
		if($params==null){
			$data = $this->var;
		}else{
			$data = array_merge($params,$this->var);
		}
		$this->CI->load->view('../template/'.$templateName, $data);
	}

	public function addView($name, $data = array())
	{
		$this->var['output'] .= $this->CI->load->view($name, $data, true);
		return $this;
	}

	public function addView_list($list=array()){
		foreach ($list as $key => $value) {
			$this->addView($value);
		}
	}

	public function set_titre($titre)
	{
		if(is_string($titre) AND !empty($titre))
		{
			$this->var['titre'] = $titre;
			return true;
		}
		return false;
	}

	public function set_charset($charset)
	{
		if(is_string($charset) AND !empty($charset))
		{
			$this->var['charset'] = $charset;
			return true;
		}
		return false;
	}
	public function ajouter_css($nom)
	{
		if(is_string($nom) AND !empty($nom) AND file_exists('./assets/css/' . $nom . '.css'))
		{
			$this->var['css'][] = css_url($nom);
			return true;
		}
		return false;
	}

	public function ajouter_css_list($list=array()){
		foreach ($list as $key => $value) {
			$this->ajouter_css($value);
		}
	}

	public function ajouter_js($nom)
	{
		if(is_string($nom) AND !empty($nom) AND file_exists('./assets/js/' . $nom . '.js'))
		{
			$this->var['js'][] = js_url($nom);
			return true;
		}
		return false;
	}

	public function ajouter_js_list($list=array()){
		foreach ($list as $key => $value) {
			$this->ajouter_js($value);
		}
	}

	public function addMenu($menu){
		$this->var['list_menu'][]=$menu;
	}

	public function setError($error){
		$this->var['error']=$error;
	}

	public function setTypeCompte($code,$nom){
		$this->var['type_compte']["code"]=$code;
		$this->var['type_compte']["nom"]=$nom;
	}

	public function setAction($action){
		$this->var['action']=$action;
	}

	public function listMenu(){
		var_dump($this->var['list_menu']);
	}
}