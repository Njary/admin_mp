<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'utilisateur/acces';
$route['login/(:num)'] = 'utilisateur/acces/login/$1';
$route['login'] = 'utilisateur/acces/login';
$route['logout'] = 'utilisateur/acces/logout';
$route['registration'] = 'utilisateur/acces/inscription';
$route['register'] = 'utilisateur/acces/register';
$route['registerReceiver'] = 'utilisateur/acces/registerReceiver';
$route['a-n-p'] = 'utilisateur/acces/addNewPoste';
$route['d-r-m-e'] = 'utilisateur/acces/deleteResponsable';
$route['u-r-m-e'] = 'utilisateur/acces/updateResponsable';
$route['accueil'] = 'admin/dashboard';
$route['don'] = 'produit/don';


$route['consommateur/inscription'] = 'webservice/wsConsommateur/inscription';
$route['consommateur/modification'] = 'webservice/wsConsommateur/modification';
$route['consommateur/connexion'] = 'webservice/wsConsommateur/connexion';
$route['consommateur/produit'] = 'webservice/wsConsommateur/listerProduit';
$route['consommateur/profil'] = 'webservice/wsConsommateur/profil';
$route['consommateur/produit/detail'] = 'webservice/wsConsommateur/detailProduit';
$route['consommateur/ajouterFavoris'] = 'webservice/wsConsommateur/ajouterFavoris';
$route['consommateur/ajouterVueProduit'] = 'webservice/wsConsommateur/ajouterVueProduit';
$route['consommateur/favoris'] = 'webservice/wsConsommateur/favoris';
$route['consommateur/ajouterCommande'] = 'webservice/wsConsommateur/ajouterCommande';
$route['consommateur/commande'] = 'webservice/wsConsommateur/listerCommande';
$route['consommateur/commande/detail'] = 'webservice/wsConsommateur/detailCommande';
$route['consommateur/feedback'] = 'webservice/wsConsommateur/feedback';
$route['consommateur/entrepriseAproximity'] = 'webservice/wsConsommateur/getAllEntrepriseAProximity';


$route['vente'] = 'produit/vente';
$route['produit'] = 'produit/produit';
$route['vente/(:num)'] = 'produit/vente/faireUneVente/$1';

$route['notification/(:num)'] = 'email/notificationDhl/send/$1';
$route['produit/(:num)'] = 'produit/successSave/$1';
$route['n-type'] = 'produit/don/addNewType';
$route['n-cat-p'] = 'produit/don/addNewCategorie';
$route['n-info-nutris'] = 'produit/don/addNewInfoNutris';
$route['n-raison-surplus'] = 'produit/don/addNewRaisonSurplus';
$route['c-p-t-d'] = 'produit/produit/changeProduitToDonation';
$route['p-f-d'] = 'produit/don/pushFormDonation';
$route['p-f-v'] = 'produit/vente/pushFormVente';
$route['s-a-d'] = 'produit/vente/saveAsDraft';
$route['n-per-stra'] = 'produit/vente/addNewPeriodeStrategie';
$route['s-d-a-d'] = 'produit/don/saveDonAsDraft';
$route['i-d'] = 'produit/don/import';
$route['i-ep-d'] = 'produit/don/importExcelPhoto';
$route['c-m-d'] = 'produit/don/canMakeDonation';
$route['c-m-v'] = 'produit/vente/canMakeVente';
$route['g-p-p'] = 'produit/don/getPhotoPoduits';
$route['g-p-p-v'] = 'produit/vente/getPhotoPoduits';
$route['g-r-d'] = 'admin/getRecentDonation';
$route['g-q-p-y'] = 'admin/getQuantityPerYear';
$route['g-m-p-y'] = 'admin/getMontantPerYear';
$route['g-n-p-g'] = 'admin/getNombrePerGenre';
$route['g-q-p-g'] = 'admin/getQtePerGenre';
$route['g-r-v'] = 'admin/getRecentVente';
$route['g-q-s-p-y'] = 'admin/getQuantitySalesPerYear';
$route['g-m-s-p-y'] = 'admin/getMontantSalesPerYear';
$route['g-n-s-p-g'] = 'admin/getNombreSalesPerGenre';
$route['g-q-s-p-g'] = 'admin/getQteSalesPerGenre';
$route['g-n-d-v-p-y'] = 'admin/getNombreDonVentePerYear';
$route['forget_password'] = 'utilisateur/acces/forget_password';
$route['enregistrement_beneficiaire'] = 'utilisateur/acces/enregistrementReceveur';
$route['404_override'] = '';
$route['translate_uri_dashes'] = TRUE;

$route['n-p-a-b'] = 'admin/nePlusAfficherBienvenu';
