-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 26, 2018 at 09:25 AM
-- Server version: 5.6.11
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `manzer_partazer`
--

-- --------------------------------------------------------

--
-- Table structure for table `token`
--

CREATE TABLE `token` (
  `id_token` int(11) NOT NULL,
  `chaine` varchar(250) NOT NULL,
  `actif` tinyint(1) NOT NULL,
  `expiration` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `token`
--

INSERT INTO `token` (`id_token`, `chaine`, `actif`, `expiration`) VALUES
(8, 'a87we6596u9y4uj9gqf58yf7pl3ng25wahjwel11pureiffeic', 1, 0),
(9, '4198qbsilv5x4rn8l5ye0ma2y17eqmacc7w8f9zt695ghra2uf', 1, 1538486926),
(10, 'f5i2r7cu5n40hzaf5gi63eic91tlrapcvs4ef1c4hw409nxcjz', 1, 1538487860),
(11, 'ng291k25zxh5wk873c6jnx6d9k8fs641z2e5ekchzx21364vy4', 1, 1538546188),
(12, 'y641nkl19g63wc47ztlfea65wg61gx67s4hv3ky7a4l334cdy2', 1, 1538550097),
(13, 'cve6e1yiyh5e6h7a475cudzpebng41sihfscur5a65w84hne01', 1, 1538550161);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `token`
--
ALTER TABLE `token`
  ADD PRIMARY KEY (`id_token`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `token`
--
ALTER TABLE `token`
  MODIFY `id_token` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
